# Debugging

## Configuration

Please ensure your local PHP configuration is up-to-date with the `php.ini` file located at repository root.
This file is automatically loaded by Symfony CLI web server if you use it.

Make sure your dependencies are up-to-date with latest changes in `composer.lock`:
```shell
$ composer install
```

Update your local environment variable in `.env.local` with the following values:
```shell
APP_DEBUG=1
```

## Usage

At any time you can dump a variable inside PHP files with `dump()` and `dd()` (aka *dump die*).

A debug bar is also available at the bottom of the page.
To find out more please check [http://phpdebugbar.com/](http://phpdebugbar.com/).
