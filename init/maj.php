<?php

include '../php/constantes.php';
include '../php/database.php';

$oConn = DbConnection();

// Mise � jour � venir ...

// $sQuery = "SELECT * FROM T_DROIT WHERE NM_CODE_DRT = 104";
$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 104';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    // $sQuery = "INSERT INTO T_DROIT (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Collaborateurs - Donn�es confidentielles', 104, 0)";
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Collaborateurs - Donn�es confidentielles', 104, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM T_DROIT WHERE NM_CODE_DRT = 105';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    // $sQuery = "INSERT INTO T_DROIT (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Axes - Donn�es confidentielles', 105, 0)";
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Axes - Donn�es confidentielles', 105, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Personnes morales - Consultation', 18, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Personnes morales - Edition', 19, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Personnes morales - Suppression', 20, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Personnes morales - Donn�es confidentielles', 106, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 107';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Editions de r�f�rence - Consultation', 21, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Editions de r�f�rence - Edition', 22, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Editions de r�f�rence - Suppression', 23, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Editions de r�f�rence - Donn�es confidentielles', 107, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 108';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Oeuvres - Consultation', 24, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Oeuvres - Edition', 25, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Oeuvres - Suppression', 26, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Oeuvres - Donn�es confidentielles', 108, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 109';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Hyperliens - Consultation', 27, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Hyperliens - Edition', 28, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Hyperliens - Suppression', 29, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Hyperliens - Donn�es confidentielles', 109, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 110';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Outils de recherche - Consultation', 30, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Outils de recherche - Edition', 31, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Outils de recherche - Suppression', 32, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Outils de recherche - Donn�es confidentielles', 110, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 111';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Colloques - Consultation', 33, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Colloques - Edition', 34, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Colloques - Suppression', 35, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Colloques - Donn�es confidentielles', 111, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 112';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Documents - Consultation', 36, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Documents - Edition', 37, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Documents - Suppression', 38, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Documents - Donn�es confidentielles', 112, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 113';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Volumes - Consultation', 39, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Volumes - Edition', 40, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Volumes - Suppression', 41, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Volumes - Donn�es confidentielles', 113, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 114';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - T�ches - Consultation', 42, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - T�ches - Edition', 43, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - T�ches - Suppression', 44, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - T�ches - Donn�es confidentielles', 114, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 115';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Errata - Consultation', 45, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Errata - Edition', 46, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Errata - Suppression', 47, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Errata - Donn�es confidentielles', 115, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 116';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Comptes rendus - Consultation', 48, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Comptes rendus - Edition', 49, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Comptes rendus - Suppression', 50, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Comptes rendus - Donn�es confidentielles', 116, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 117';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - R�impressions de volume - Consultation', 51, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - R�impressions de volume - Edition', 52, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - R�impressions de volume - Suppression', 53, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - R�impressions de volume - Donn�es confidentielles', 117, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

$sQuery = 'SELECT * FROM t_droit WHERE NM_CODE_DRT = 118';
$oRs = DbExecRequete($sQuery, $oConn);

if (0 == DbNbreEnreg($oRs)) {
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Echanges - Consultation', 54, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Echanges - Edition', 55, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Gestion - Echanges - Suppression', 56, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
    $sQuery = "INSERT INTO t_droit (LB_DROIT_DRT, NM_CODE_DRT, BL_NON_SUPPRIMABLE_DRT) VALUES ('Web SC - Public - Echanges - Donn�es confidentielles', 118, 0)";
    $oRs = DbExecRequete($sQuery, $oConn);
}

DbClose($oConn);
