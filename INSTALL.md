# Install project

## Docker

Go to repository root, then start containers:
```shell
$ docker-compose up
```

After a while, once database has been imported, open the website in your browser at [http://127.0.0.1:34000/](http://127.0.0.1:34000/)

## Local installation

### Prerequisites

- PHP >= 8.1
- MySQL (InnoDB)

check [composer.json](./composer.json) for more information.

### Installation steps

1. Import the database and run migrations.

2. Create a `.env.local` at repository root and override `.env` variables with your local configuration.

3. Install composer dependencies
    ```shell
    $ composer install
    ```

4. Configure a webserver

### Use PHP built-in webserver

Go to repository root then start PHP built-in webserver (local development only):

```shell
$ php -S localhost:8000
```

You can now open the website in your browser at [http://127.0.0.1:8000/](http://127.0.0.1:8000/)
