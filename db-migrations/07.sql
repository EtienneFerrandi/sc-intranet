 --   Table tâche

 -- Creation
CREATE TABLE `sc_t_tache` (   `PK_TACHE_TCH` int(11) NOT NULL AUTO_INCREMENT,   `LB_TACHE_TCH` varchar(50) NOT NULL DEFAULT '',   `NM_ORDRE_TCH` smallint(6) NOT NULL DEFAULT 0,   PRIMARY KEY (`PK_TACHE_TCH`) );
INSERT INTO `sc_t_tache` SELECT * FROM `t_tache`;

 -- Ajout des colonnes manquantes
ALTER TABLE `sc_t_tache` ADD `TX_DESCRIPTION_TCH` VARCHAR(255) NULL  DEFAULT NULL  AFTER `NM_ORDRE_TCH`;
ALTER TABLE `sc_t_tache` ADD `TX_VOL_OVR_CONCERNE` VARCHAR(255) NULL  DEFAULT NULL  AFTER `TX_DESCRIPTION_TCH`;
ALTER TABLE sc_t_tache ADD `TX_TYPE_TCH` VARCHAR(255)  NULL  DEFAULT NULL  COMMENT 'Etape du workflow dont relève la tâche : préparation du manuscrit/révision du manuscrit/préparation du volume'  AFTER `TX_DESCRIPTION_TCH`;

