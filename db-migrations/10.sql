-- References ON DELETE CASCADE to allow deletions -- 

-- table sc_t_assoc_ovr_usr_tch --
ALTER TABLE `sc_t_assoc_ovr_usr_tch` ADD FOREIGN KEY (`FK_OUT_REF_OVR`) REFERENCES `sc_t_oeuvre` (`PK_OEUVRE_OVR`) ON DELETE CASCADE;
ALTER TABLE `sc_t_assoc_ovr_usr_tch` ADD FOREIGN KEY (`FK_OUT_REF_USR`) REFERENCES `sc_t_utilisateur` (`PK_UTILISATEUR_USR`) ON DELETE CASCADE;
ALTER TABLE `sc_t_assoc_ovr_usr_tch` ADD FOREIGN KEY (`FK_OUT_REF_TCH`) REFERENCES `sc_t_tache` (`PK_TACHE_TCH`) ON DELETE CASCADE;
ALTER TABLE `sc_t_assoc_ovr_usr_tch` ADD FOREIGN KEY (`FK_OUT_REF_VOL`) REFERENCES `sc_t_volume` (`PK_VOLUMEINFOS_VIF`) ON DELETE CASCADE;


-- table sc_t_assoc_au_ovr --
ALTER TABLE `sc_t_assoc_au_ovr` ADD  FOREIGN KEY (`FK_AUTEUR_ANCIEN_AAN`) REFERENCES `sc_t_auteur` (`PK_AUTEUR_ANCIEN_AAN`) ON DELETE CASCADE;
ALTER TABLE `sc_t_assoc_au_ovr` ADD  FOREIGN KEY (`FK_OEUVRE_OVR`) REFERENCES `sc_t_oeuvre` (`PK_OEUVRE_OVR`) ON DELETE CASCADE;


-- table sc_t_assoc_vol_ovr --

ALTER TABLE sc_t_assoc_vol_ovr ADD FOREIGN KEY (FK_AVO_REF_VOL) REFERENCES sc_t_volume(PK_VOLUMEINFOS_VIF) ON DELETE CASCADE;
ALTER TABLE sc_t_assoc_vol_ovr ADD FOREIGN KEY (FK_AVO_REF_VOL) REFERENCES sc_t_volume(PK_VOLUMEINFOS_VIF) ON DELETE CASCADE;

