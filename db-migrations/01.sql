-- Augmentation de la longueur de la fonction GROUP_CONCAT

SET SESSION group_concat_max_len = 10000000000;

-- Table Auteur

-- Création de la table T_AUTEUR

CREATE TABLE `sc_t_auteur` (   `PK_AUTEUR_ANCIEN_AAN` int(11) NOT NULL AUTO_INCREMENT,   `FK1_AAN_REF_ZNG` int(11) DEFAULT NULL,   `FK2_AAN_REF_ZNG` int(11) DEFAULT NULL,   `FK3_AAN_REF_ZNG` int(11) DEFAULT NULL,   `FK4_AAN_REF_ZNG` int(11) DEFAULT NULL,   `TX_NOM_FRANCAIS_AAN` varchar(100) NOT NULL DEFAULT '',   `TX_NOM_CLAVIS_AAN` varchar(100) DEFAULT NULL,   `BL_SAINT_AAN` tinyint(1) DEFAULT NULL,   `DT_NAISSANCE_INF_AAN` smallint(5) unsigned DEFAULT NULL,   `DT_NAISSANCE_SUP_AAN` smallint(5) unsigned DEFAULT NULL,   `BL_NAISSANCE_APPROX_AAN` tinyint(1) DEFAULT NULL,   `BL_NAISSANCE_AVANT_JC_AAN` tinyint(1) DEFAULT NULL,   `TX_DOUTE_LIEU_NAISSANCE_AAN` varchar(255) DEFAULT NULL,   `DT_MORT_INF_AAN` smallint(5) unsigned DEFAULT NULL,   `DT_MORT_SUP_AAN` smallint(5) unsigned DEFAULT NULL,   `BL_MORT_APPROX_AAN` tinyint(1) DEFAULT NULL,   `TX_DOUTE_LIEU_MORT_AAN` varchar(255) DEFAULT NULL,   `TX_BIOGRAPHIE_AAN` text DEFAULT NULL,   `TX_REMARQUES_AAN` text DEFAULT NULL,   `TX_PERIODE_ACTIVITE_AAN` text DEFAULT NULL,   `TX_FICHECERF_NUMERO_AAN` varchar(10) DEFAULT NULL,   `TX_ABREVIATION_AAN` varchar(25) DEFAULT NULL,   `ID_AUTANC_BIPALYON` int(10) unsigned DEFAULT NULL,   PRIMARY KEY (`PK_AUTEUR_ANCIEN_AAN`),   KEY `AAN_REF_ZNG_1_FK` (`FK1_AAN_REF_ZNG`),   KEY `AAN_REF_ZNG_2_FK` (`FK2_AAN_REF_ZNG`),   KEY `AAN_REF_ZNG_3_FK` (`FK3_AAN_REF_ZNG`),   KEY `AAN_REF_ZNG_4_FK` (`FK4_AAN_REF_ZNG`) ) ;
INSERT INTO `sc_t_auteur` SELECT * FROM `t_auteur_ancien`;


-- Suppression des champs inutiles

ALTER TABLE `sc_t_auteur` DROP `FK1_AAN_REF_ZNG`;
ALTER TABLE `sc_t_auteur` DROP FK2_AAN_REF_ZNG;
ALTER TABLE `sc_t_auteur` DROP FK3_AAN_REF_ZNG;
ALTER TABLE `sc_t_auteur` DROP `FK4_AAN_REF_ZNG`;
ALTER TABLE `sc_t_auteur` DROP `BL_SAINT_AAN`;
ALTER TABLE `sc_t_auteur` DROP `DT_NAISSANCE_INF_AAN`;
ALTER TABLE `sc_t_auteur` DROP `DT_NAISSANCE_SUP_AAN`;
ALTER TABLE `sc_t_auteur` DROP `BL_NAISSANCE_APPROX_AAN`;
ALTER TABLE `sc_t_auteur` DROP `BL_NAISSANCE_AVANT_JC_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_DOUTE_LIEU_NAISSANCE_AAN`;
ALTER TABLE `sc_t_auteur` DROP `DT_MORT_INF_AAN`;
ALTER TABLE `sc_t_auteur` DROP `DT_MORT_SUP_AAN`;
ALTER TABLE `sc_t_auteur` DROP `BL_MORT_APPROX_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_DOUTE_LIEU_MORT_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_BIOGRAPHIE_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_PERIODE_ACTIVITE_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_FICHECERF_NUMERO_AAN`;
ALTER TABLE `sc_t_auteur` DROP `TX_ABREVIATION_AAN`;
ALTER TABLE `sc_t_auteur` DROP `ID_AUTANC_BIPALYON`;

-- Creation des nouveaux champs

ALTER TABLE `sc_t_auteur` ADD `TX_URL_BIBLINDEX_AAN` VARCHAR(255)  NULL  DEFAULT NULL COMMENT 'hyperlien vers l’auteur dans Biblindex';

-- Ajout du lien biblindex dans la colonne LIEN_BIBLINDEX
-- les noms des BDD babel et biblindex sont éventuellement à modifier 

UPDATE sc_t_auteur a
LEFT JOIN biblindex.person p 
    ON p.discr = 'author' AND p.babel_id = a.`PK_AUTEUR_ANCIEN_AAN` 
SET a.TX_URL_BIBLINDEX_AAN = CONCAT('https://biblindex.org/fr/author/', p.id);


-- Table oeuvre 
-- cf. 2.sql


-- Table assoc auteur_oeuvre
-- cf. 3.sql


-- Table volume
-- cf. 4.sql


-- Table t_volume_reimp
-- Cf. 5.sql 


-- Table utilisateur
-- CF. 6.sql


 -- Table tâche
 -- Cf. 7.sql


-- Table assoc ovr usr tch
-- Cf. 8.sql



