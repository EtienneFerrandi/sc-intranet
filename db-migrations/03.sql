-- Table assoc auteur_oeuvre
-- Creation

CREATE TABLE `sc_t_assoc_au_ovr` (
    FK_AUTEUR_ANCIEN_AAN int(11) DEFAULT(NULL),
    FK_OEUVRE_OVR int(11) DEFAULT(NULL)
    );
    
-- Remplissage de la table

INSERT INTO sc_t_assoc_au_ovr(FK_OEUVRE_OVR)
SELECT PK_OEUVRE_OVR FROM `sc_t_oeuvre`;

UPDATE sc_t_assoc_au_ovr AS ao
INNER JOIN t_oeuvre AS O
    ON ao.FK_OEUVRE_OVR = O.PK_OEUVRE_OVR
SET ao.FK_AUTEUR_ANCIEN_AAN = O.FK_OVR_REF_AAN;


