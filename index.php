<?php

require_once 'bootstrap.php';

include 'php/debug.php';
if ($_ENV['APP_DEBUG'] ?? false) {
    error_reporting(\E_ALL);
    $debugbar = \debug::getInstance()->getDebugbar();
    $debugbarRenderer = $debugbar->getJavascriptRenderer();
}

include 'php/scErrorHandler.php';

// pour la reprise de donnees, on ne fixe pas de limite pour le temps d'execution du code PHP
// Cf. http://fr3.php.net/set_time_limit
// set_time_limit(0);
// ini_set('max_execution_time', 0);
// ini_set('session.gc_maxlifetime', 120*60);  // 120 min

date_default_timezone_set('Europe/Paris');

if (isset($_POST['xls']) && 'export excel' == $_POST['xls']) {
    header('Location:php/traitementexcel.php');
    header('Content-type: application/vnd.ms-excel');
    header('Content-disposition: attachment');
    // ouvre excel pour afficher ton tableau ("inline" ouvre le document excel dans ton navigateur)
}

include 'php/session.php';
include 'php/constantes.php';
include 'php/common.php';
include 'php/database.php';

// API
if (preg_match('/^\/api\/(.*)/', $_SERVER['REQUEST_URI'] ?? '', $matches)) {
    header('Accept: application/json');
    header('Content-Type: application/json; charset=utf-8');
    [$path] = explode('?', $matches[1]);
    [$resource] = explode('/', $path);
    try {
        if (!file_exists("php/api/{$resource}.php")) {
            throw new \RuntimeException('Resource not found');
        }
        include "php/api/{$resource}.php";
    } catch (\Throwable $throwable) {
        http_response_code(404);
        echo json_encode([
            'error' => $throwable->getMessage(),
        ]);
        exit(1);
    }
    exit(0);
}

$pageid = $_GET['pageid'] ?? '';
$ddlsectionid = $_GET['ddlsectionid'] ?? '';
if (!file_exists('php/ecrans/'.$pageid.'.php')) {
    header('Location: /?pageid=connexion');
    exit(1);
}
include 'php/sql.php';

// PASSWORDS ENCRYPTION
// include 'php/encrypt_password.php';

// ob_start("ob_gzhandler");	// on compresse le flux HTML avec gzip
if (SITE_GZIP_ACTIF == '1') {
    include 'php/obgz_header.php';
}	// require_once

// include('php/session.php');

if (file_exists(SITE_PHYSICALPATH.'/php/ecrans/requests/'.$pageid.'-request.php')) {
    include 'php/ecrans/requests/'.$pageid.'-request.php';
}

$charset = 'text/html; charset=utf-8';	// iso-8859-1
if (isset($_GET['charset'])) {
    if ('' != $_GET['charset']) {
        $charset = 'text/html; charset='.$_GET['charset'];
    }
}

$noheader = array_key_exists('noheader', $_GET);

// Désactive la fonctionnalité PHP qui ajoute un anti-slash à tout apostrophe
// get_magic_quotes_gpc(0);
// get_magic_quotes_runtime(0);
// set_magic_quotes_runtime(0);
// fix_magic_quotes(); // Attention: deprecated in 5.3.0 and removed in 5.4.0 !!! cf. http://php.net/manual/en/security.magicquotes.php

// ================== On commence à générer le flux HTML de sortie ...
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="<?php echo $charset; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="-1">
<link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/select2/css/select2.min.css" rel="stylesheet">
<link href="/assets/select2/css/select2-bootstrap-5-theme.min.css" rel="stylesheet">
<script src="/assets/jquery/jquery-3.6.3.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/select2/js/select2.full.min.js"></script>
    <?php
if (isset($_SESSION['PK_UTILISATEUR_USR'])) {
    if (!(isset($_REQUEST['refreshdelai']))) {
        echo '<meta http-equiv="Refresh" content="1800;url=/?pageid=connexion&endofsession=ok&auto=ok">'.chr(13).chr(10);
    } else {
        $refreshetape = $_REQUEST['refreshetape'] + 1;
        $refreshdelai = $_REQUEST['refreshdelai'];
        $refreshurl = $_REQUEST['refreshurl'].'&refreshdelai='.$refreshdelai.'&refreshurl='.urlencode((string) $_REQUEST['refreshurl']);
        $refreshurl .= '&refreshetape='.$refreshetape;
        echo '<meta http-equiv="Refresh" content="'.$refreshdelai.';url='.$refreshurl.'">'.chr(13).chr(10);
    }
}
?>
<meta name="robots" content="index,follow">
<meta name="description" content="édition de textes patristiques grecs, latins et orientaux des Pères de l Eglise (Antiquité et prolongements médiévaux) dans leur langue originale, avec traduction française. Equipe de recherche CNRS.">

<?php
/*
 * rto
 * 22/06/2005
 * Inclusion des fichiers contenant les mots clefs pour chaque page. Si pas de fichier,
 * met les mots-clefs par défaut : generic-keywords.php
 * et affiche le nom du fichier à créer
 */

$sSiteDossier = '';
if (SITE_DOSSIER != '') {
    $sSiteDossier = '/'.SITE_DOSSIER;
} else {
    $sSiteDossier = '';
}

?>
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#790a41">
<meta name="msapplication-TileColor" content="#790a41">
<meta name="theme-color" content="#ffffff">
<title>Institut des Sources Chrétiennes - CNRS (HiSoMA, UMR 5189)</title>
<style type="text/css">
<!--

/* ================================== MISE EN FORME PAR DEFAUT ================================ */

a:link.std {
	color: #95004A;
	text-decoration: none;
	font-weight: bold;
}
a:visited.std {
	color: #95004A;
	text-decoration: none;
	font-weight: bold;
}
a:hover.std {
	color: #3399FF;
	text-decoration: none;
}
a:active.std {
	color: #3399FF;
	text-decoration: none;
}
a:link.std-back {
	color: #0ddb12;
	text-decoration: none;
	font-weight: bold;
}
a:visited.std-back {
	color: #0ddb12;
	text-decoration: none;
	font-weight: bold;
}
a:hover.std-back {
	color: #3399FF;
	text-decoration: none;
}
a:active.std-back {
	color: #3399FF;
	text-decoration: none;
}
body {
	background-color: #FFFAD9;
	color: #000000;
	font-family: Times;
<?php
if (isset($_SESSION['PK_UTILISATEUR_USR'])) {
    echo 'border: 4px solid #FF0000;';
}
?>
}

/* ==================================== MENU & SOUS-MENU ====================================== */

span.aide {
	font-family: Times, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
	text-decoration:none
}
span.aidemot {
	color: #000000;
	background-color: #FFFF00;
}
div.posaide {
	position: absolute;
	visibility: hidden;
}
table.menu {
	background-color: #ffffff;
}
td.menu_on {
	background-color: #019549;
}
td.menu_off {
	background-color: #95004A;
	cursor:pointer;
}
nav.navbar{
    background-color: #FDCE9F;
    }


/* ========================================= DATAGRID  ======================================== */

a:link.dtg-entete {
	color: #ffffff;
	text-decoration: none;
}
a:visited.dtg-entete {
	color: #ffffff;
	text-decoration: none;
}
a:hover.dtg-entete {
	color: #3399FF;
	text-decoration: none;
}
a:active.dtg-entete {
	color: #3399FF;
	text-decoration: none;
}
a:link.dtg-lignes {
	color: #95004A;
	text-decoration: underline;
}
a:visited.dtg-lignes {
	color: #95004A;
	text-decoration: underline;
}
a:hover.dtg-lignes {
	color: #3399FF;
	text-decoration: none;
}
a:active.dtg-lignes {
	color: #3399FF;
	text-decoration: none;
}

table.datagrid-entete {
	background-color: #C90063;
}
th.datagrid-entete {
    background-color: #C90063;
}
.font-datagrid-entete {
	color: #FFFFFF;
}

td.datagrid-ligne1 {
	background-color: #95004A;
}
th.text-center {
    background-color: #95004A;
    font-weight: bold;
    color: #ffffff;
}
th.title_gestion{
    text-align: center;
    background-color: #C90063;
    color: #ffffff;
}
th.enregistrements{
    background-color: #C90063;
    text-align: center;
    color: #ffffff;
}
th.plus{
    background-color: #C90063;
}
tr.datagrid-lignesA {
	background-color: #FDCE9F;
}
tr.datagrid-lignesB {
	background-color: #FCDEC1;
}
td.datagrid-lignesA {
	background-color: #FDCE9F;
}
td.datagrid-lignesB {
	background-color: #FCDEC1;
}
.clavis{
    color: #95004A;
}
.work_title:not(:last-child):after{
    content: ',';
}

/* =============================== Select2 =========================== */
.select2-container {
    width: 100%!important;
}
.select2-results__option,
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    text-align: left;
}
.select2-container .select2-selection--multiple .select2-selection__rendered {
    white-space: break-spaces;
}

table.table-header-fixed thead {
    background-color: #fff9d9;
    position: sticky;
    top: 0;
    z-index: 2;
}

#tbl_usr_task tr td {
    text-align: center;
    vertical-align: top;
}
#tbl_usr_task tr td:nth-child(1) input,
#tbl_usr_task tr td:nth-child(2) input,
#tbl_usr_task tr td:nth-child(3) input {
    min-width: 240px;
}
#tbl_usr_task tr td:nth-child(4) input,
#tbl_usr_task tr td:nth-child(5) input {
    min-width: 145px;
}
#tbl_usr_task tr td:nth-child(6) textarea {
    min-height: 76px;
    min-width: 240px;
}

/* =============================== !!!!! A METTRE A JOUR !!!!! =========================== */

input { font-family: Times; }
textarea { font-family: Times; }
select { font-family: Times; }

select.x-small { font-family: Times; font-size: 8pt }
p {
	text-align: justify;
	font-family: Times;
}
p.centre {
	text-align: center;
	font-family: Times;
}
li {
	margin-top: 5px;
	margin-bottom: 5px;
    white-space: normal;
}
.btn
{
	border:1px solid #A4A6B5;
	background-color: #95004A;
	padding-left:4px;
	padding-right:4px;
	padding-top:1px;
	padding-bottom:1px;
	color:#FFFFFF;
	cursor:pointer;
	font-size:11pt;
	font-weight: bold;
}
.btn-small
{
	border:1px solid #A4A6B5;
	background-color: #95004A;
	padding-left:3px;
	padding-right:3px;
	padding-top:1px;
	padding-bottom:1px;
	color:#FFFFFF;
	cursor:pointer;
	font-size:10pt;
	font-weight: bold;
}
.btn-xsmall
{
	border:1px solid #A4A6B5;
	background-color: #95004A;
	padding-left:2px;
	padding-right:2px;
	padding-top:1px;
	padding-bottom:1px;
	color:#FFFFFF;
	cursor:pointer;
	font-size:8pt;
	font-weight: bold;
}
.inputfile
{
	font-size:11pt
}

.font-titre1 {
	color:#95004A;
	font-size:18pt;
	font-family: Times;
}
.font-big {
	font-size:14pt;
	font-family: Times;
}
.font-normal {
	font-size:11pt;
	font-family: Times;
}
.font-small {
	font-size:10pt;
	font-family: Times;
}
.font-xsmall {
	font-size:9pt;
	font-family: Times;
}
.font-xxsmall {
	font-size:8pt;
	font-family: Times;
}
.font-couleur-claire {
	color:#FFFAD9;
}
.font-couleur-sc {
	color:#95004A;
}
.font-couleur-erreur {
	color:#ff0000;
}
.font-index {
	font-weight: bold;
	font-variant: small-caps;
}
.font-greek {
	font-family: SymbolGreekII
}
div {
 text-align: center;
}
.phpdebugbar,
.phpdebugbar div {
    text-align: unset;
}
.div-titre-de-page {
	color: #003300;
	font-size: 24px;
	font-weight: bold;
	font-style: normal;
	font-variant: small-caps;
	background-image: url("/img/common/Collection_3bis.jpg");
	margin-top: 5px;
	margin-bottom: 30px;
}

td.couleur2 {
	/* background-color: #FFB164 */
}
.h1-statique {
	 color: #003300;
	 font-size: 20px;
	 font-weight: bold;
	 margin-top: 10px;
	margin-bottom: 10px;
}
.h2-statique {
	color: #003300;
	font-size: 18px;
	font-weight: bold;
	margin-top: 10px;
	margin-bottom: 10px;
}
span.auteur{
	font-variant: small-caps;
}

.small-caps {
    font-variant:small-caps;
}

a:link {
	color: #95004A;
	text-decoration: none;
}
a:visited {
	color: #2E5A5A;
	text-decoration: none;
}
a:hover {
	/*text-decoration: none;
	color: #FF359A;*/
	color : red;
	text-decoration : underline;
}
a:active {
	text-decoration: none;
	color: #FF00CC;
}

.tabover{background : #fee3f0; color : #000000; font-weight : bold;}
.tabout{background : #FFFBDD; color : #000000; font-weight : bold;}
.tabtitre{ background:#95004A; color : #FFFAD9; font-weight : bold; border-color : #95004A}

.drapeau_over{border: 2px solid navy;}
.drapeau_out{border-right : 1px solid black;border-bottom: 1px solid black;}

.alpha{
border-bottom: 2px solid black;
border-right: 2px solid black;
border-top: 1px solid gray;
border-left: 1px solid gray;
}

.alphaover{
color : red;
border-bottom: 2px solid red;
border-right: 2px solid red;
border-top: 1px solid #FF6F6F;
border-left: 1px solid #FF6F6F;
}

.retour{
font-style : italic;
font-weight : bold;
}

.stitre{
font-style : italic;
font-weight : bold;
}

.gras
{font-weight : bold};

a:link.rien {color : #95004A}
a:visited.rien {color : #95004A}
a:hover.rien {color : #95004A; text-decoration : none;}
a:active.rien {color : #95004A;}

.infobulle{
	position: absolute;
	visibility : hidden;
	display : none;
	border: 1px solid #000000;
	border-right : solid 2px black;
	border-bottom : solid 2px black;
	padding: 10px;
	font-size:10pt;
	font-family: Times;
	background-color: #AC9CFD;
	color : #FFFAD9;
	font-weight : bold;
}



/* couleur des fonds tableaux */
.contour	{background-color: #C90063;} // Couleur de fond de la barre
.fond_formu	{background-color: #F5D07A;}
/*D58328*/

/* couleur du texte */
.Verdana_gris		{font-family: Verdana; color: #FFFAD9; font-size: 10px;}
.Verdana_blanc		{font-family: Verdana; color: #FFFFFF; font-size: 10px;}

-->

table#tbl_usr_task input:not([type="button"]):not([type="checkbox"]):not([type="radio"]):not([type="date"]),
table#tbl_usr_task select,
table#tbl_usr_task textarea {
    width: 100%;
}

.list_works_au {column-count: 4;}
.list_works_au h5 {color: #790a41; border: 1px solid;}
.list_works_au li{ white-space: normal;word-wrap: break-word;}
.list_works_au li a { white-space: normal;word-wrap: break-word;}

.btn-secondary{
    background-color: #95004A;
}
.btn-light{
    background-color: #95004A;
}

</style>

<script language="javascript" src="js/common.js"></script>
<?php echo isset($debugbarRenderer) ? $debugbarRenderer->renderHead() : null; ?>
</head>
<body>
<?php if(!$noheader): ?>
<nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <span class="navbar-brand" style="color: #2f2f2f">INTRANET</span>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/?pageid=connexion">Connexion</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Gestion
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=autanciens">Auteurs anciens</a></li>
                        <li><a class="dropdown-item" href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=oeuvres">&#338;uvres</a></li>
                        <li><a class="dropdown-item" href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=volumes">Volumes</a></li>
                        <li><a class="dropdown-item" href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=utilisateurs">Utilisateurs</a></li>
                    </ul>
                </li>
                <li>
                    <a class="nav-link active" aria-current="page" href="/?pageid=volumes_preparation">Volumes à venir</a>
                </li>
                <li>
                    <a class="nav-link active" aria-current="page" href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=reimpressions">Réimpressions</a>
                </li>
            </ul>

            <form class="d-flex">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                <a title="Me déconnecter" href="/?rubriqueid=intranet&pageid=connexion&endofsession=ok&auto=no" class="fa fa-sign-out"
                   style="font-size:24px; <?php if(!array_key_exists('PK_UTILISATEUR_USR', $_SESSION)){?> display: none; <?php }?>">
                </a>
            </form>
        </div>
    </div>
</nav>
<?php endif; ?>
<div align="center">
<center>
    <table class="menu" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  	<td width="100%" nowrap align="center" bgcolor="#FFFAD9"><?php include 'php/top.php'; ?></td>
  </tr>
    <?php
    if (empty($ddlsectionid)) {
        echo '<tr><td width="100%" nowrap align="center" bgcolor="#FFFAD9">';
        include 'php/ecrans/'.$pageid.'.php';
        echo '</td></tr>';
    } else { ?>
      <tr>
        <td width="100%" nowrap align="center" bgcolor="#FFFAD9"><?php include 'php/ecrans/gestion/gestion-'.$ddlsectionid.'.php'; ?></td>
      </tr>
<?php } ?>
</table>
</center>
</div>
<script language="javascript">document.title = '<?php echo $title; ?>';</script>
<?php echo isset($debugbarRenderer) ? $debugbarRenderer->render() : null; ?>
</body>

</html>

<?php
if (SITE_GZIP_ACTIF == '1') {
    include 'php/obgz_footer.php';
}	// require_once
?>
