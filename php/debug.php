<?php

class debug
{
    private static self $instance;

    public static function getInstance(): self
    {
        self::$instance = self::$instance ?? new self();

        return self::$instance;
    }

    public function __construct(
        private $debugbar = new \DebugBar\StandardDebugBar()
    ) {
    }

    public function getDebugbar(): Debugbar\DebugBar
    {
        return $this->debugbar;
    }
}
