<?php

/**
 * Rajout de code pour forcer les noms de table en minuscule afin
 * d'éviter de reprendre tout le code php du site
 * But : passer sur serveur mysql hébergé sur machine Linux et
 * regler problème de casse
 * David GOUDARD - 19/08/2015.
 **/
function _sqlInsert($_table, $_valeurs, $oLink)
{
    $_table = strtolower((string) $_table);

    $champs = explode('¤', (string) _recupChamps($_table));
    $champs_nb = count($champs);

    $valeurs = explode('¤', (string) $_valeurs);
    $valeurs_nb = count($valeurs);

    if (($champs_nb - 1) != $valeurs_nb) {	// Le -1 permet d'ignorer la clef primaire !
        trigger_error('Erreur lors d\'un INSERT dans la table '.$_table.' : le nombre de valeurs != nbre de champs !', \E_USER_WARNING);
        rollback($oLink);
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        exit(1);	// On stoppe l'exécution du code
    }

    $contraintes = explode('¤', (string) _recupContraintes($_table));
    $types = explode('¤', (string) _recupTypes($_table));

    $sRequete = 'INSERT INTO '.$_table.' (';
    $mettreUneVirgule = false;

    for ($i = 1; $i < $champs_nb; ++$i) {	// On ignore le cas $i=0 (clef primaire) !
        if ('not null' == $contraintes[$i]) {
            if ('' != $valeurs[$i - 1]) {
                if ($mettreUneVirgule) {
                    $sRequete .= ', ';
                } else {
                    $mettreUneVirgule = true;
                }
                $sRequete .= $champs[$i];
            } else {
                trigger_error('Erreur lors d\'un INSERT dans la table '.$_table.' : le champ '.$champs[$i].' ne peut être NULL !', \E_USER_WARNING);
                rollback($oLink);
                header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
                exit(1);	// On stoppe l'exécution du code
            }
        } elseif ('ignore' != $contraintes[$i]) {
            if ($mettreUneVirgule) {
                $sRequete .= ', ';
            } else {
                $mettreUneVirgule = true;
            }
            $sRequete .= $champs[$i];
        }
    }

    $sRequete .= ') VALUES (';

    $mettreUneVirgule = false;

    for ($i = 1; $i < $champs_nb; ++$i) {	// On ignore le cas $i=0 (clef primaire) !
        if ('not null' == $contraintes[$i] || '' != $valeurs[$i - 1]) {
            if ($mettreUneVirgule) {
                $sRequete .= ', ';
            } else {
                $mettreUneVirgule = true;
            }

            $sRequete .= _getValeur($types[$i], $valeurs[$i - 1]);
        } elseif ('ignore' != $contraintes[$i]) {
            if ($mettreUneVirgule) {
                $sRequete .= ', ';
            } else {
                $mettreUneVirgule = true;
            }

            $sRequete .= 'null';
        }
    }

    $sRequete .= ')';	// echo $sRequete;

    $oRecordset = DbExecRequete($sRequete, $oLink);

    $identity = DbRecupIdentity($oLink);

    return $identity;
}

/**
 * Fonction permettant de mettre à jour une table en parcourant l'ensemble de ses champs.
 *
 * @param string $_table
 * @param string $_valeurs
 * @param mysqli $oLink
 */
function _sqlUpdate($_table, $_valeurs, $oLink)
{
    $_table = strtolower($_table);

    $champs = explode('¤', (string) _recupChamps($_table));
    $champs_nb = count($champs);

    $valeurs = explode('¤', $_valeurs);
    $valeurs_nb = count($valeurs);

    if ($champs_nb != $valeurs_nb) {
        trigger_error('Erreur lors d\'un UPDATE dans la table '.$_table.' : le nombre de valeurs != nbre de champs !', \E_USER_WARNING);
        rollback($oLink);
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        exit(1);	// On stoppe l'exécution du code
    }

    $contraintes = explode('¤', (string) _recupContraintes($_table));
    $types = explode('¤', (string) _recupTypes($_table));

    $sRequete = 'UPDATE '.$_table.' SET ';
    $mettreUneVirgule = false;

    for ($i = 1; $i < $champs_nb; ++$i) {
        if ($mettreUneVirgule) {
            $sRequete .= ', ';
        } else {
            $mettreUneVirgule = true;
        }

        if ('' != $valeurs[$i]) {
            $sRequete .= $champs[$i].' = ';

            $sRequete .= _getValeur($types[$i], $valeurs[$i]);
        } elseif ('not null' == $contraintes[$i]) {
            trigger_error('Erreur lors d\'un UPDATE dans la table '.$_table.' : le champ '.$champs[$i].' ne peut être NULL !', \E_USER_WARNING);
            rollback($oLink);
            header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
            exit(1);	// On stoppe l'exécution du code
        } else {
            $sRequete .= $champs[$i].' = null';
        }
    }

    $sRequete .= ' WHERE '.$champs[0].' = '.$valeurs[0];

    $oRecordset = DbExecRequete($sRequete, $oLink);
}

function _sqlDelete($_table, $_valeurs, $oLink)
{
    $_table = strtolower((string) $_table);

    $champs = explode('¤', (string) _recupChamps($_table));
    $champs_nb = count($champs);

    $valeurs = explode('¤', (string) $_valeurs);
    $valeurs_nb = count($valeurs);

    if ($champs_nb != $valeurs_nb) {
        trigger_error('Erreur lors d\'un DELETE dans la table '.$_table.' : le nombre de valeurs != nbre de champs !', \E_USER_WARNING);
        rollback($oLink);
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        exit(1);	// On stoppe l'exécution du code
    }

    $types = explode('¤', (string) _recupTypes($_table));

    $sRequete = 'DELETE FROM '.$_table.' WHERE ';
    $mettreUnET = false;
    $nbCondition = 0;

    for ($i = 0; $i < $champs_nb; ++$i) {
        if ('' != $valeurs[$i]) {
            if ($mettreUnET) {
                $sRequete .= ' AND ';
            } else {
                $mettreUnET = true;
            }

            ++$nbCondition;
            $sRequete .= $champs[$i].' = ';

            $sRequete .= _getValeur($types[$i], $valeurs[$i]);
        }
    }

    if ($nbCondition > 0) {
        $oRecordset = DbExecRequete($sRequete, $oLink);
    } else {
        trigger_error('Erreur lors d\'un DELETE dans la table '.$_table.' : il manque une condition !', \E_USER_WARNING);
        rollback($oLink);
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        exit(1);	// On stoppe l'exécution du code
    }
}

/**
 * Fonction permettant de mettre à jour une table de manière sélective.
 *
 * @param string $_table                : table à mettre à jour
 * @param string $_champs_num_where     : indices des champs par rapport auxquels on fait l'UPDATE
 * @param string $_champs_num_update    : indices des champs à mettre à jour
 * @param string $_champs_valeur_where  : valeurs des champs par rapport auxquels on fait l'UPDATE
 * @param string $_champs_valeur_update : valeurs des champs à mettre à jour
 * @param mysqli $oLink                 : connexion à la base que l'on va utiliser pour faire l'UPDATE
 */
function _sqlUpdateSelectif($_table, $_champs_num_where, $_champs_num_update,
                            $_champs_valeur_where, $_champs_valeur_update, $oLink)
{
    $_table = strtolower($_table);

    $num_update = explode('¤', $_champs_num_update);	// indices des champs à mettre à jour
    $num_update_nb = count($num_update);	// nb d'indices à mettre à jour
    $valeurs_update = explode('¤', $_champs_valeur_update);	// valeurs des champs à mettre à jour
    $valeurs_update_nb = count($valeurs_update);	// nb de valeurs à mettre à jour

    $num_where = explode('¤', $_champs_num_where);	// indices des champs par rapport auxquels on fait l'UPDATE
    $num_where_nb = count($num_where);	// nb d'indices par rapport auxquels on fait l'UPDATE
    $valeurs_where = explode('¤', $_champs_valeur_where);	// valeurs des champs par rapport auxquelles on fait l'UPDATE
    $valeurs_where_nb = count($valeurs_where);	// nb de valeurs par rapport auxquelles on fait l'UPDATE

    if ($num_update_nb != $valeurs_update_nb || $num_update_nb < 1) {
        trigger_error('Erreur lors d\'un UPDATE sélectif dans la table '.$_table.' : le nombre de valeurs != nbre de champs OU BIEN il n\'y a aucun champ à mettre à jour !', \E_USER_WARNING);
        rollback($oLink);
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        exit(1);	// On stoppe l'exécution du code
    }

    // On récupère l'ensemble des éléments de la table concernée
    $champs = explode('¤', (string) _recupChamps($_table));
    $types = explode('¤', (string) _recupTypes($_table));
    $contraintes = explode('¤', (string) _recupContraintes($_table));

    $sRequete = 'UPDATE '.$_table.' SET ';
    $mettreUneVirgule = false;

    for ($i = 0; $i < $num_update_nb; ++$i) {
        if ($mettreUneVirgule) {
            $sRequete .= ', ';
        } else {
            $mettreUneVirgule = true;
        }

        if ('' != $valeurs_update[$i]) {
            $sRequete .= $champs[$num_update[$i]].' = ';

            $sRequete .= _getValeur($types[$num_update[$i]], $valeurs_update[$i]);
        } elseif ('not null' == $contraintes[$num_update[$i]]) {
            trigger_error('Erreur lors d\'un UPDATE multiple dans la table '.$_table.' : le champ '.$champs[$num_update[$i]].' ne peut être NULL !', \E_USER_WARNING);
            rollback($oLink);
            header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
            exit(1);	// On stoppe l'exécution du code
        } else {
            $sRequete .= $champs[$num_update[$i]].' = null';
        }
    }

    // Clause WHERE

    $sRequete .= ' WHERE ';
    $mettreUnAND = false;

    for ($i = 0; $i < $num_where_nb; ++$i) {
        if ($mettreUnAND) {
            $sRequete .= ' AND ';
        } else {
            $mettreUnAND = true;
        }

        if ('' != $valeurs_where[$i]) {
            $sRequete .= $champs[$num_where[$i]].' = ';

            $sRequete .= _getValeur($types[$num_where[$i]], $valeurs_where[$i]);
        } elseif ('not null' == $contraintes[$num_where[$i]]) {
            trigger_error('Erreur lors d\'un UPDATE sélectif dans la table '.$_table.' : le champ '.$champs[$num_where[$i]].' ne peut être NULL !', \E_USER_WARNING);
            rollback($oLink);
            header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
            exit(1);	// On stoppe l'exécution du code
        } else {
            $sRequete .= $champs[$num_where[$i]].' = null';
        }
    }

    // $sRequete .= " WHERE ".$champs[0]." = ".$valeurs[0];	// echo $sRequete;
    // $sRequete .= " WHERE ".$champs[$_champs_num[0]]." = ".$valeurs[0];	// echo $sRequete;

    // echo $sRequete;

    $oRecordset = DbExecRequete($sRequete, $oLink);
}

function _getValeur($_type, $_valeur)
{
    $valeur = '';

    if (null != $_valeur) {
        if ('string' == $_type || 'date' == $_type
        || 'datetime' == $_type) {
            $valeur .= "'";
        }

        if ('string' == $_type) {	// On double les apostrophes pour le SGBD
            $valeur .= str_replace("'", "''", (string) $_valeur);
        } elseif ('date' == $_type) {	// (format FR ==> format US)
            $valeur .= _dateFRtoDateUS($_valeur);
        } elseif ('float' == $_type) {	// Nombres décimaux (format FR ==> format US)
            $valeur .= str_replace(',', '.', (string) $_valeur);
        } else {
            $valeur .= $_valeur;
        }

        if ('string' == $_type || 'date' == $_type
        || 'datetime' == $_type) {
            $valeur .= "'";
        }
    } else {
        $valeur = 'null';
    }

    return _trim($valeur);
}

function _recupChamps($_table)
{
    // on fait l'inverse, nom de table en majuscule (pour éviter de tout changer...)
    // d.goudard 19/08/2015
    $_table = strtoupper((string) $_table);

    $champs = '';
    switch ($_table) {
        // case "t_assoc_ovr_omc":
        case 'T_ASSOC_OVR_OMC':
            $champs = 'PK_AOM¤FK_AOM_REF_OVR¤FK_AOM_REF_OMC';
            break;
            /*
            case "T_ASSOC_AAN_BEL":
            $champs = "PK_ASSOC_AANBEL¤FK_AAE_REF_AAN¤FK_AAE_REF_BEL";
            break;
            */
            /*
        case "T_ASSOC_AAN_BIB":
            $champs = "PK_ASSOC_AANBIB¤FK_ANB_REF_AAN¤FK_ANB_REF_BIB";
            break;
            */
        case 'T_ASSOC_AAN_DOC':
            $champs = 'PK_ASSOC_AANDOC¤FK_AND_REF_AAN¤FK_AND_REF_DOC¤BL_DEFAUT_AND';
            break;
        case 'T_ASSOC_AAN_HPL':
            $champs = 'PK_ASSOC_AANHPL¤FK_AAH_REF_AAN¤FK_AAH_REF_HPL¤BL_DEFAUT_AAH';
            break;
        case 'T_ASSOC_AAN_IMG':
            $champs = 'PK_ASSOC_AANIMG¤FK_AAI_REF_AAN¤FK_AAI_REF_IMG¤BL_DEFAUT_AAI';
            break;
        case 'T_ASSOC_AAN_ORH':
            $champs = 'PK_ASSOC_AANORH¤FK_AAO_REF_AAN¤FK_AAO_REF_ORH';
            break;
        case 'T_ASSOC_AAN_USR':
            $champs = 'PK_ASSOC_AANUSR¤FK_AANUSR_REF_AAN¤FK_AANUSR_REF_USR¤TX_REMARQUE_AANUSR';
            break;
            /*
        case "T_ASSOC_AGL_DOC":
            $champs = "PK_ASSOC_AGLDOC¤FK_AAD_REF_AGL¤FK_AAD_REF_DOC¤BL_DEFAUT_AAD";
            break;
            */
            /*
        case "T_ASSOC_AGL_PML":
            $champs = "PK_ASSOC_AGLPML¤FK_AAP_REF_AGL¤FK_AAP_REF_PML¤FK_AAP_REF_AAP";
            $champs .= "¤BL_NATURE_PARTICIPATION_AAP";
            break;
            */
            /*
        case "T_ASSOC_AGL_UAS_NPT":
            $champs = "PK_ASSOC_AGLUASNPT¤FK_AST_REF_AGL¤FK_AST_REF_UAS¤FK_AST_REF_NPT";
            break;
            */
            /*
        case "T_ASSOC_AUD_AAN":
            $champs = "PK_ASSOC_AUDAAN¤FK_ADN_REF_AUD¤FK_ADN_REF_AAN¤BL_DEFAUT_ADN";
            break;
            */
            /*
        case "T_ASSOC_AUD_USR":
            $champs = "PK_ASSOC_AUDUSR¤FK_AAU_REF_AUD¤FK_AAU_REF_USR¤BL_DEFAUT_AAU";
            break;
            */
        case 'T_ASSOC_CLQ_AAN':
            $champs = 'PK_ASSOC_CLQAAN¤FK_ACA_REF_CLQ¤FK_ACA_REF_AAN¤BL_DEFAUT_ACA';
            break;
        case 'T_ASSOC_CLQ_DOC':
            $champs = 'PK_ASSOC_CLQDOC¤FK_ACD_REF_CLQ¤FK_ACD_REF_DOC¤BL_DEFAUT_ACD';
            break;
        case 'T_ASSOC_CLQ_HPL':
            $champs = 'PK_ASSOC_CLQHPL¤FK_ACH_REF_CLQ¤FK_ACH_REF_HPL¤BL_DEFAUT_ACH';
            break;
        case 'T_ASSOC_CLQ_PML':
            $champs = 'PK_ASSOC_CLQPML¤FK_ACP_REF_CLQ¤FK_ACP_REF_PML¤BL_DEFAUT_ACP';
            break;
        case 'T_ASSOC_CLQ_USR_NPT':	// 5 champs
            $champs = 'PK_ASSOC_CLQUSRNPT¤FK_CUN_REF_CLQ¤FK_CUN_REF_USR';
            $champs .= '¤FK_CUN_REF_NPT¤TX_REMARQUES_CUN';
            break;
        case 'T_ASSOC_CRD_USR':
            $champs = 'PK_ASSOC_CRDUSR¤FK_ADR_REF_CRD¤FK_ADR_REF_USR';
            break;
            /*
        case "T_ASSOC_DOC_USR_NPT":
            $champs = "PK_ASSOC_DOCUSRNPT¤FK_DUN_REF_DOC¤FK_DUN_REF_USR¤FK_DUN_REF_NPT";
            break;
            */
            /*
        case "T_ASSOC_ECH_AAN":
            $champs = "PK_ASSOC_ECHAAN¤FK_AEA_REF_ECH¤FK_AEA_REF_AAN¤TX_REMARQUES_AEA";
            break;
            */
            /*
        case "T_ASSOC_ECH_DOC":
            $champs = "PK_ASSOC_ECHDOC¤FK_AED_REF_ECH¤FK_AED_REF_DOC¤BL_DEFAUT_AED";
            break;
            */
            /*
        case "T_ASSOC_ECH_OVR":
            $champs = "PK_ASSOC_ECHOVR¤FK_AER_REF_ECH¤FK_AER_REF_OVR";
            break;
            */
            /*
        case "T_ASSOC_ECH_POL":
            $champs = "PK_ASSOC_ECHPOL¤FK_AEP_REF_ECH¤FK_AEP_REF_POL¤BL_DEFAUT_AEP";
            break;
            */
            /*
        case "T_ASSOC_ECH_USR_NPT":
            $champs = "PK_ASSOC_ECHUSRNPT¤FK_EUN_REF_ECH¤FK_EUN_REF_USR";
            $champs .= "¤FK_EUN_REF_NPT¤BL_EMETTEUR_EUN¤BL_RECEPTEUR_EUN";
            break;
             */
            /*
        case "T_ASSOC_FMT_DOC":
            $champs = "PK_ASSOC_FMTDOC¤FK_AFD_REF_FMT¤FK_AFD_REF_DOC¤BL_DEFAUT_AFD";
            break;
            */
            /*
        case "T_ASSOC_FMT_PML":
            $champs = "PK_ASSOC_FMTPML¤FK_AFP_REF_FMT¤FK_AFP_REF_PML¤BL_DEFAUT_AFP";
            $champs .= "¤TX_PERIODE_VALIDITE_COLLABORATION_AFP";
            break;
            */
            /*
        case "T_ASSOC_FMT_USR_NPT":
            $champs = "PK_ASSOC_FMTUSRNPT¤FK_FUN_REF_FMT¤FK_FUN_REF_USR";
            $champs .= "¤FK_FUN_REF_NPT¤TX_DATES_PARTICIPATION_FUN";
            break;
             */
        case 'T_ASSOC_HPL_HTC':
            $champs = 'PK_ASSOC_HPLHTC¤FK_AHT_REF_HPL¤FK_AHT_REF_HTC';
            break;
        case 'T_ASSOC_HPL_HTM':
            $champs = 'PK_ASSOC_HPLHTM¤FK_AHM_REF_HPL¤FK_AHM_REF_HTM';
            break;
        case 'T_ASSOC_OVR_OGR':
            $champs = 'PK_ASSOC_OVROGR¤FK_AOG_REF_OVR¤FK_AOG_REF_OGR¤BL_DEFAUT_AOG¤TX_REMARQUES_AOG';
            break;
        case 'T_ASSOC_OVR_OTM':
            $champs = 'PK_ASSOC_OVROTM¤FK_AOO_REF_OVR¤FK_AOO_REF_OTM¤BL_DEFAUT_AOO';
            break;
        case 'T_ASSOC_OVR_USR_TCH':	// 27 champs
            $champs = 'PK_ASSOC_OVRUSRTCH¤FK_OUT_REF_OVR¤FK_OUT_REF_USR¤FK_OUT_REF_TCH';
            $champs .= '¤FK_OUT_REF_AVT¤FK_OUT_REF_VOL¤BL_MANUSCRIT_REMIS¤TX_REMARQUES_OUT¤BL_ENGAGEMENT_COLLAB_OUT';
            $champs .= '¤DT_ENGAGEMENT_COLLAB_AN_OUT¤TX_ENGAGEMENT_COLLAB_MOIS_OUT';
            $champs .= '¤DT_REMISE_MANUSC_PREVUE_AN_OUT¤TX_REMISE_MANUSC_PREVUE_MOIS_OUT';
            $champs .= '¤DT_REMISE_EFFECTIVE_AN_OUT¤TX_REMISE_EFFECTIVE_MOIS_OUT';
            $champs .= '¤TX_REMISE_EFFECTIVE_JOUR_OUT¤DT_ECHEANCE_PARUTION_PREVUE_AN_OUT';
            $champs .= '¤TX_ECHEANCE_PARUTION_PREVUE_MOIS_OUT';
            $champs .= '¤BL_CONVENTION_ENVOYEE_OUT¤DT_CONVENTION_ENVOI_AN_OUT¤TX_CONVENTION_ENVOI_MOIS_OUT';
            $champs .= '¤TX_CONVENTION_ENVOI_JOUR_OUT¤BL_CONVENTION_SIGNEE_OUT¤DT_CONVENTION_RETOUR_AN_OUT';
            $champs .= '¤TX_CONVENTION_RETOUR_MOIS_OUT¤TX_CONVENTION_RETOUR_JOUR_OUT¤TX_REMARQUES_CONFIDENTIELLES_OUT';
            break;
        case 'T_ASSOC_PFL_DRT':
            $champs = 'PK_ASSOC_PFLDRT¤FK_APT_REF_PFL¤FK_APT_REF_DRT';
            break;
            /*
        case "T_ASSOC_PML_HPL":
            $champs = "PK_ASSOC_PMLHPL¤FK_APH_REF_PML¤FK_APH_REF_HPL¤BL_DEFAUT_APH";
            break;
            */
        case 'T_ASSOC_PML_PMP':
            $champs = 'PK_ASSOC_PMLPMP¤FK_APP_REF_PML¤FK_APP_REF_PMP¤BL_DEFAUT_APP';
            $champs .= '¤BL_ARCHIVE_APP¤TX_PERIODE_VALIDITE_PROFIL_APP';
            break;
        case 'T_ASSOC_POL_AAN':
            $champs = 'PK_ASSOC_POLAAN¤FK_APA_REF_POL¤FK_APA_REF_AAN¤BL_DEFAUT_ALA';
            break;
        case 'T_ASSOC_POL_USR':
            $champs = 'PK_ASSOC_POLUSR¤FK_PUN_REF_POL¤FK_PUN_REF_USR¤BL_COORDINATEUR_PUR';
            $champs .= '¤BL_RESPONSABLE_SCIENTIFIQUE_PUR¤TX_REMARQUES_PUR';
            break;
            /*
        case "T_ASSOC_USR_BEL":
            $champs = "PK_ASSOC_USRBEL¤FK_ASB_REF_USR¤FK_ASB_REF_BEL";
            break;
            */
            /*
        case "T_ASSOC_USR_BIB":
            $champs = "PK_ASSOC_USRBIB¤FK_AUB_REF_USR¤FK_AUB_REF_BIB";
            $champs .= "¤BL_REDACTEUR_AUB¤BL_AUTEUR_AUB¤BL_MENTIONNE_AUB";
            break;
            */
        case 'T_ASSOC_USR_PFL':
            $champs = 'PK_ASSOC_USRPFL¤FK_AUP_REF_USR¤FK_AUP_REF_PFL¤BL_DEFAUT_AUP';
            $champs .= '¤BL_ACTIF_AUP¤TX_PERIODE_ACTIVITE_AUP';
            break;
        case 'T_ASSOC_USR_PML':
            $champs = 'PK_ASSOC_USRPML¤FK_ARL_REF_USR¤FK_ARL_REF_PML¤BL_DEFAUT_ARL';
            $champs .= '¤BL_ACTIF_ARL¤TX_PERIODE_ACTIVITE_ARL¤TX_REMARQUES_ARL';
            break;
            /*
        case "T_ASSOC_USR_USR":
            $champs = "PK_ASSOC_USRUSR¤FK1_AUU_REF_USR¤FK2_AUU_REF_USR¤BL_DEFAUT_AUU";
            break;
            */
        case 'T_ASSOC_VOL_CRD':
            $champs = 'PK_ASSOC_VOLCRD¤FK_AVC_REF_VOL¤FK_AVC_REF_CRD';
            break;
        case 'T_ASSOC_VOL_OVR':
            $champs = 'PK_ASSOC_VOLOVR¤FK_AVO_REF_VOL¤FK_AVO_REF_OVR¤BL_A_REPRENDRE_AVO¤TX_PAGES_AVO';
            break;
        // case "T_ASSOC_VRP_USR":
        //	$champs = "PK_ASSOC_VRPUSR¤FK_AVU_REF_VRP¤FK_AVU_REF_USR¤BL_DEFAUT_AVU";
        //	break;
        case 'T_AUTEUR_ANCIEN':	// 23 champs
            $champs = 'PK_AUTEUR_ANCIEN_AAN¤FK1_AAN_REF_ZNG¤FK2_AAN_REF_ZNG¤FK3_AAN_REF_ZNG';
            $champs .= '¤FK4_AAN_REF_ZNG¤TX_NOM_FRANCAIS_AAN¤TX_NOM_CLAVIS_AAN¤BL_SAINT_AAN';
            $champs .= '¤DT_NAISSANCE_INF_AAN¤DT_NAISSANCE_SUP_AAN¤BL_NAISSANCE_APPROX_AAN';
            $champs .= '¤BL_NAISSANCE_AVANT_JC_AAN¤TX_DOUTE_LIEU_NAISSANCE_AAN¤DT_MORT_INF_AAN';
            $champs .= '¤DT_MORT_SUP_AAN¤BL_MORT_APPROX_AAN¤TX_DOUTE_LIEU_MORT_AAN';
            $champs .= '¤TX_BIOGRAPHIE_AAN¤TX_REMARQUES_AAN¤TX_PERIODE_ACTIVITE_AAN';
            $champs .= '¤TX_FICHECERF_NUMERO_AAN¤TX_ABREVIATION_AAN¤ID_AUTANC_BIPALYON';
            break;
        case 'T_AXE':	// 3 champs
            $champs = 'PK_AXE_AXE¤LB_AXE_AXE¤TX_PRESENTATION_AXE';
            break;
            /*
        case "T_BIBLIO_ELEMENT":  // 7 champs
                $champs = "PK_BIBLIOELT_BEL¤FK_BEL_REF_BER¤FK_BEL_REF_BAR";
                $champs .= "¤FK_BEL_REF_BSL¤DT_MAJ_BEL¤BL_A_TRAITER_BEL";
                $champs .= "¤NM_BIPALYON_BPLIT_BEL";
                break;
                */
            /*
        case "T_BIBLIOELT_ARTREVUE":  // 13 champs
                $champs = "PK_BIBLIOELTARTREVUE_BAR¤FK_BAR_REF_PML¤LB_BIBLIOELTARTREVUE_BAR";
                $champs .= "¤TX_SOUSTITRE_BAR¤BL_ETALII_BAR¤TX_PAGES_BAR¤TX_NUMERO_REVUE_BAR";
                $champs .= "¤DT_PARUTION_AN_BAR¤TX_PARUTION_MOIS_BAR¤TX_ABREVIATION_USUELLE_BAR";
                $champs .= "¤NM_PRIX_BAR¤TX_STATUT_BAR¤TX_REMARQUES_BAR";
                break;
             */
            /*
        case "T_BIBLIOELT_LIVRE": // 14 champs
                $champs = "PK_BIBLIOELTLIVRE_BER¤FK_BER_REF_BET¤FK_BER_REF_PML";
                $champs .= "¤LB_BIBLIOELTLIVRE_BER¤TX_SOUSTITRE_BER¤BL_ETALII_BER";
                $champs .= "¤TX_PERSONNES_EDITANT_BER¤DT_PARUTION_AN_BER¤NM_NOMBRE_PAGES_BER";
                $champs .= "¤TX_ISBN_BER¤NM_PRIX_BER¤TX_STATUT_BER¤TX_ABREVIATION_USUELLE_BER";
                $champs .= "¤TX_REMARQUES_BER";
             */
            /*
        case "T_BIBLIOELT_SSLIVRE": // 9 champs
                $champs = "PK_BIBLIOELTSSLIVRE_BSL¤FK_BSL_REF_BET¤FK_BSL_REF_BER¤LB_BIBLIOELTSSLIVRE_BSL";
                $champs .= "¤TX_SOUSTITRE_BSL¤BL_ETALII_BSL¤TX_PAGES_BSL¤TX_ABREVIATION_USUELLE_BSL¤TX_REMARQUES_BSL";
                break;
                */
            /*
        case "T_BIBLIOELT_TYPE":
                $champs = "PK_BIBLIOELTTYPE_BET¤LB_BIBLIOELTTYPE_BET¤TX_TYPE_BET";
                break;
                */
        case 'ASSOC_BIBDATA_VERSET':
            $champs = 'PK_BIDATAVERSET_ABV¤FK_ABV_REF_BDT¤FK_ABV_REF_BVT';
            break;
            /*
        case "T_BIB_CORRESPONDANCE":  // 7 champs
            $champs = "PK_BIBCORRESPONDANCE_BCP¤FK1_BCP_REF_BLT¤FK2_BCP_REF_BLT";
            $champs .= "¤FK1_BCP_REF_BCH¤FK2_BCP_REF_BCH¤FK1_BCP_REF_BVT¤FK2_BCP_REF_BVT";
            break;
            */
        case 'T_BIB_CHAPITRE':
            $champs = 'PK_BIBCHAPITRE_BCH¤FK_BCH_REF_BLT¤LB_BIBCHAPITRE_BCH¤NM_MAXVERSET_BCH';
            break;
        case 'T_BIB_PRECISION':
            $champs = 'PK_BIBPRECISION_BBP¤TX_NOMPRECISION_BBP¤TX_COMPLEMENT_BBP¤NM_ORDRETRI_BBP';
            break;
        case 'T_BIB_TYPE':
            $champs = 'PK_BIBTYPE_BTP¤LB_BIBTYPE_BTP';
            break;
        case 'T_BIB_VERSET':
            $champs = 'PK_BIBVERSET_BVT¤FK_BVT_REF_BCH¤LB_BIBVERSET_BVT¤TX_VERSET_BVT';
            break;
        case 'T_BIBDATA': // 25 champs
            $champs = 'PK_BIBDATA_BDT¤FK_BDT_REF_BLT¤FK_BDT_REF_BBP¤FK_BDT_REF_USR¤FK_BDT_REF_AAN¤FK_BDT_REF_OVR¤FK_BDT_REF_SOV';
            $champs = '¤ TX_SOURCE_BDT¤TX_CADP_BDT¤TX_LIB_BDT¤TX_CHA_BDT¤TX_PAR_BDT¤TX_PAG_BDT';
            $champs = '¤TX_LIG_BDT¤TX_VEL_BDT¤TX_SIG_BDT¤TX_IND_BDT¤TX_LETTREAPPEL_BDT';
            $champs = '¤TX_NOTE_BDT¤TX_NUMNOTE_BDT¤TX_TEXTEBIBLIQUE_BDT¤TX_TEXTE_TRADUCTIONBIB_BDT';
            $champs = '¤DT_VALIDATION_BDT¤TX_VALIDATEUR_BDT¤ID_SOURCE';
            break;
        case 'T_BIBDATA2': // 17 champs
            $champs = 'PK_BIBDATA_BDT¤FK_BDT_REF_BLT¤FK_BDT_REF_USR¤FK_BDT_REF_AAN¤FK_BDT_REF_OVR¤FK_BDT_REF_SOV';
            $champs = '¤TX_LIB_BDT¤TX_CHA_BDT¤TX_PAR_BDT¤TX_PAG_BDT';
            $champs = '¤TX_LIG_BDT¤TX_SIG_BDT¤TX_SOURCE_BDT¤TX_IND_BDT';
            $champs = '¤DT_VALIDATION_BDT¤TX_VALIDATEUR_BDT¤ID_SOURCE';
            break;
        // case "t_bib_liste": // 9 champs
        case 'T_BIB_LISTE': // 9 champs
            $champs = 'PK_BILISTE_BLT¤FK_BLT_REF_BTP¤TX_ABREVIATION_BLT¤TX_NOMLIVRE_BLT¤TX_NOMLIVRE_EN_BLT';
            $champs .= '¤TX_NOMLIVRE_DE_BLT¤TX_NOMLIVRE_IT_BLT¤NM_ORDRETRI_BLT¤NM_MAXCHAP_BLT¤ID_SOURCE';
            break;
        case 'T_COLLOQUE':	// 18 champs
            $champs = 'PK_COLLOQUE_CLQ¤FK1_CLQ_REF_ZNG¤FK2_CLQ_REF_ZNG¤LB_COLLOQUE_CLQ';
            $champs .= '¤DT_DATE_AN_CLQ¤TX_DATE_MOIS_CLQ¤TX_DATE_JOUR_CLQ¤TX_DATE_REMARQUES_CLQ';
            $champs .= '¤TX_NATURE_CLQ¤TX_DESCRIPTIF_CLQ¤TX_ZONE_INFOS_CLQ';
            $champs .= '¤TX_RENSEIGNEMENTS_PRATIQUES_CLQ¤BL_ACTIF_CLQ¤BL_PRIVE_CLQ¤BL_SC_CLQ';
            $champs .= '¤DT_DATEFIN_AN_CLQ¤TX_DATEFIN_MOIS_CLQ¤TX_DATEFIN_JOUR_CLQ';
            break;
            /*
        case "T_COMPTEUR":
            $champs = "PK_COMPTEUR_CPT¤DT_COMPTEUR_CPT¤TX_PAGE_CPT¤TX_CLIENT_IP_CPT";
            $champs .= "¤TX_CLIENT_LANGUE_CPT¤TX_CLIENT_NAVIGATEUR_CPT¤TX_CLIENT_REQUETE_CPT";
            $champs .= "¤TX_CLIENT_PAGE_PRECEDENTE_CPT";
            break;
            */
        case 'T_COMPTE_RENDU':	// 21 champs
            $champs = 'PK_COMPTERENDU_CRD¤FK_CRD_REF_DOC¤FK_CRD_REF_PML¤BL_VOLUME_ENVOYE_CRD';
            $champs .= '¤DT_ENVOI_VOLUME_AN_CRD¤TX_ENVOI_VOLUME_MOIS_CRD¤TX_ENVOI_VOLUME_JOURS_CRD';
            $champs .= '¤BL_CR_PARU_CRD¤DT_CR_PARUTION_AN_CRD¤TX_CR_PARUTION_MOIS_CRD';
            $champs .= '¤BL_CR_RECU_CRD¤DT_CR_RECEPTION_AN_CRD¤TX_CR_RECEPTION_MOIS_CRD';
            $champs .= '¤TX_CR_RECEPTION_JOURS_CRD¤TX_NUMERO_REVUE_CRD¤TX_PAGES_REVUE_CRD';
            $champs .= '¤BL_PRIVE_CRD¤BL_ACTIF_CRD¤TX_CR_CORPS_CRD¤TX_REMARQUES_CRD¤FK_CRD_REF_USR';
            break;
            /*
        case "T_COTISATION_AASC":
            $champs = "PK_COTISATIONAASC_CAC¤FK_CAC_REF_UAS¤FK_CAC_REF_PMA¤DT_REGLEMENT_CAC";
            $champs .= "¤NM_MONTANT_CAC¤TX_MODE_REGLEMENT_CAC¤BL_VIA_SITE_CAC";
            break;
             */
        case 'T_DOCUMENT':	// 16 champs
            $champs = 'PK_DOCUMENT_DOC¤FK_DOC_REF_DTP¤FK_DOC_REF_USR1¤FK_DOC_REF_USR2¤FK_DOC_REF_USR3¤FK_DOC_REF_IMG¤LB_DOCUMENT_DOC';
            $champs .= '¤TX_FICHIER_DOC¤BL_ACTIF_DOC¤BL_ROMPU_DOC¤BL_PRIVE_DOC¤TX_COMMENTAIRE_DOC';
            $champs .= '¤DT_UPLOAD_DOC¤TX_MOIS_CREATION_DOC¤TX_ANNEE_CREATION_DOC¤TX_REMARQUES_DOC';
            break;
        case 'T_DOCUMENT_TYPE':
            $champs = 'PK_DOCTYPE_DTP¤LB_DOCTYPE_DTP¤TX_REMARQUES_DTP';
            break;
        case 'T_EDITION_REFERENCE':
            $champs = 'PK_EDITION_REFERENCE_ERF¤FK_ERF_REF_PML¤LB_EDITION_REFERENCE_ERF';
            $champs .= '¤TX_ABREVIATION_ERF¤TX_ANNEES_EDITION_ERF';
            $champs .= '¤TX_NOMBRE_TOMES_ERF¤TX_EDITEURSSCI_ERF'; // ¤TX_LIEU_PUBLICATION_ERF
            $champs .= '¤TX_ANNEE_1ERE_PUBLICATION_ERF¤BL_SERIE_ERF¤TX_SERIE_ANNEE_DEBUT_ERF¤TX_SERIE_ANNEE_FIN_ERF';
            $champs .= '¤TX_LIEUX_PUBLICATION_ERF¤TX_LIEU_EDITION_INITIALE_ERF¤TX_LIEUX_EDITIONS_POSTERIEURES_ERF';
            $champs .= '¤TX_PRECISIONS_ERF';
            break;
        case 'T_ERRATUM':
            $champs = 'PK_ERRATUM_ERT¤FK_ERT_REF_VOL¤NM_PAGE_ERT¤TX_LOCALISATION_DANS_PAGE_ERT';
            $champs .= '¤TX_TEXTE_ORIGINAL_ERT¤TX_TEXTE_CORRIGE_ERT¤TX_REMARQUES_ERT';
            break;
        case 'T_HYPERLIEN':
            $champs = 'PK_HYPERLIEN_HPL¤FK_HPL_REF_HTS¤FK_HPL_REF_IMG¤LB_HYPERLIEN_HPL¤TX_URL_HPL';
            $champs .= '¤BL_ACTIF_HPL¤BL_ROMPU_HPL¤BL_PRIVE_HPL¤TX_COMMENTAIRE_HPL';
            break;
        case 'T_HYPERLIEN_THEME':
            $champs = 'PK_HYPTHEME_HTM¤LB_HYPTHEME_HTM¤NM_ORDRE_HTM';
            break;
        case 'T_HYPERLIEN_TYPE_CONTENU':
            $champs = 'PK_HYPTYPECONTENU_HTC¤LB_HYPTYPECONTENU_HTC';
            break;
        case 'T_HYPERLIEN_TYPE_SITE':
            $champs = 'PK_HYPTYPESITE_HTS¤LB_HYPTYPESITE_HTS';
            break;
        case 'T_IMAGE':
            $champs = 'PK_IMAGE_IMG¤LB_IMAGE_IMG¤TX_SOURCE_IMG¤TX_FICHIER_IMG¤BL_ACTIF_IMG¤BL_PRIVE_IMG';
            break;
        case 'T_IP_AUTORISEE':
            $champs = 'PK_IPAUTORISEE_IPA¤LB_IPAUTORISEE_IPA¤TX_NOM_MACHINE_IPA';
            break;
        case 'T_OEUVRE':
            $champs = 'PK_OEUVRE_OVR¤FK_OVR_REF_AAN¤FK1_OVR_REF_ERF¤FK2_OVR_REF_ERF';
            $champs .= '¤FK1_OVR_REF_USR¤FK2_OVR_REF_USR¤FK3_OVR_REF_USR';
            $champs .= '¤BL_FRAGMENT_OVR¤TX_TYPE_OVR¤FK_PARENT_OVR';
            $champs .= '¤TX_TITRE_LATIN_OVR¤TX_OPUS_BPLIT_OVR¤TX_TITRE_FRANCAIS_OVR';
            $champs .= '¤TX_CLAVIS_OVR¤TX_CLAVIS_TYPE_OVR¤TX_CLAVIS_CPLT_OVR¤TX_CLAV_BPLIT_OVR';
            // $champs .= "¤TX_OEUVRE_MAITRE_LT_OVR¤TX_OEUVRE_MAITRE_FR_OVR¤TX_ABREVIATION_OVR";
            $champs .= '¤TX_ABREVIATION_OVR';
            $champs .= '¤TX_PATROLOGIE_OVR¤TX_PATROLOGIE_TOME_OVR¤TX_PATROLOGIE_PAGES_OVR';
            $champs .= '¤TX_EDITIONREF_TOME_OVR¤TX_EDITIONREF_PAGES_OVR¤TX_EDITIONREF_PREC_OVR¤TX_ANNEE_EDITIONREF_OVR'; // 21
            $champs .= '¤TX_EDITIONREF2_TOME_OVR¤TX_EDITIONREF2_PAGES_OVR¤TX_EDITIONREF2_PREC_OVR¤TX_ANNEE_EDITIONREF2_OVR';
            $champs .= '¤TX_RESUME_OVR¤TX_REMARQUES_OVR¤BL_DATE_INF_APPROX_OVR';
            $champs .= '¤DT_DATE_INF_AN_OVR¤TX_DATE_INF_MOIS_OVR¤BL_DATE_SUP_APPROX_OVR';
            $champs .= '¤DT_DATE_SUP_AN_OVR¤TX_DATE_SUP_MOIS_OVR¤TX_SIEC1_BPLIT_OVR¤TX_SIEC2_BPLIT_OVR¤BL_INITIATION_OVR';
            $champs .= '¤BL_OEUVRE_INTERNE_OVR¤TX_TYPE_MANUSCRIT_DISPO_OVR¤TX_LOCALISATION_OVR¤TX_RQS_LOCALISATION_OVR';
            $champs .= '¤TX_CADP_OVR¤TX_LANG_OVR¤TX_LANG_ORIGIN_OVR¤TX_LANG_TRANSM_OVR';
            $champs .= '¤TX_INDEX_OVR¤TX_ETAT_OVR';
            $champs .= '¤TX_NOTICE_OVR¤TX_SUITE_OVR';
            $champs .= '¤NM_CIT_OVR¤TX_REM_OVR';
            $champs .= '¤TX_BIBLIO_OVR¤TX_DATE_OVR¤ID_SOURCE¤NM_PARENT_OVR¤PK_SSOEUVRE_SOV';
            $champs .= '¤TX_BIBLINDEX_HAS_USED_OVR¤TX_BIBLINDEX_SHOULD_USE_OVR';
            break;
        case 'T_OUTILS_RECHERCHE':
            $champs = 'PK_OUTILSRCH_ORH¤FK_ORH_REF_ORHT¤FK_ORH_REF_ORHR¤FK_ORH_REF_USR1¤FK_ORH_REF_USR2¤FK_ORH_REF_USR3';
            $champs .= '¤LB_OUTILSRCH_ORH¤TX_DESCRIPTION_ORH¤TX_HYPERLIEN_ORH¤TX_MOIS_CREATION_ORH¤TX_ANNEE_CREATION_ORH';
            $champs .= '¤TX_JOUR_MAJ_ORH¤TX_MOIS_MAJ_ORH¤TX_ANNEE_MAJ_ORH';
            break;
        case 'T_OUTILS_RECHERCHE_TYPE':
            $champs = 'PK_OUTILSRCH_TYPE_ORHT¤LB_LIBELLE_ORHT';
            break;
        case 'T_OUTILS_RECHERCHE_RUBRIQUE':
            $champs = 'PK_OUTILSRCH_RUBRIQUE_ORHR¤LB_LIBELLE_ORHR¤TX_REMARQUE_ORHR';
            break;
        case 'T_OEUVRE_GENRE':
            $champs = 'PK_OVRGENRE_OGR¤LB_OVRGENRE_OGR¤LB_OVRGENRE_UK_OGR';
            break;
        case 'T_OEUVRE_MOTCLEF':
            $champs = 'PK_OVRMOTCLEF_OMC¤LB_OVRMOTCLEF_OMC';
            break;
        case 'T_OEUVRE_THEME':
            $champs = 'PK_OVRTHM_OTM¤LB_OVRTHM_OTM¤LB_OVRTHM_UK_OTM¤NM_NIVEAU_OTM¤NM_PARENT_OTM¤NM_ORDRE_OTM';
            break;
        case 'T_PERSONNE_MORALE':	// 18 champs
            $champs = 'PK_PERSMORALE_PML¤FK_PML_REF_PMT¤FK1_PML_REF_ZNG¤FK2_PML_REF_ZNG¤FK_PML_REF_PMA¤LB_PERSMORALE_PML';
            $champs .= '¤TX_ABREVIATION_PML¤TX_CONTACT_PRENOM_PML¤TX_CONTACT_NOM_PML¤TX_CONTACT_CIVILITE_PML';
            $champs .= '¤TX_ADRESSE_VOIE_PML¤TX_ADRESSE_CP_PML¤TX_ADRESSE_BP_PML¤TX_ADRESSE_CEDEX_PML';
            $champs .= '¤TX_TEL_FIXE_PML¤TX_TEL_FAX_PML¤TX_EMAIL_PML¤TX_REMARQUES_PML';
            break;
        case 'T_PERSMORALE_AASC':
            $champs = 'PK_PERSMORALEAASC_PMA¤TX_TYPE_PMA¤BL_ADHERENT_PMA¤TX_MODE_BULLETIN_PMA';
            $champs .= '¤DT_COTISATION_EXPIRATION_PMA¤BL_RECU_SOUHAITE_PMA¤TX_REMARQUES_PMA';
            break;
        case 'T_PERSMORALE_TYPE':
            $champs = 'PK_PERSMORALETYPE_PMT¤LB_PERSMORALETYPE_PMT';
            break;
        case 'T_POLE':
            $champs = 'PK_POLE_POL¤FK_POL_REF_AXE¤LB_POLE_POL¤LB_POLE_EN_POL¤TX_PRESENTATION_POL';
            $champs .= '¤FK_POL_REF_USR1¤FK_POL_REF_USR2¤FK_POL_REF_USR3';
            break;
        case 'T_TRACE':
            $champs = 'PK_TRACE_TRC¤TX_DOMAINE_TRC¤NM_ITEMID_TRC¤LB_TRACE_TRC¤TX_TYPE_TRC¤DT_TRACE_TRC¤NM_UTILISATEUR_TRC';
            break;
        case 'T_UTILISATEUR':   // 52 champs
            $champs = 'PK_UTILISATEUR_USR¤FK1_USR_REF_ZNG¤FK2_USR_REF_ZNG';
            $champs .= '¤FK3_USR_REF_ZNG¤FK4_USR_REF_ZNG¤FK_USR_REF_UIP¤FK_USR_REF_UTD';
            $champs .= '¤FK_USR_REF_USC¤FK_USR_REF_UCLB¤FK_USR_REF_IMG¤FK_USR_REF_UAS¤TX_LOGIN_USR';    // 12
            $champs .= '¤TX_PASSWORD_USR¤DT_DERNIEREMAJPWD_USR';
            $champs .= '¤TX_NOM_USR¤TX_PRENOM_USR¤TX_SEXE_USR¤TX_FONCTION_USR¤TX_CIVILITE_USR';
            $champs .= '¤BL_DECEDE_USR¤DT_NAISSANCE_AN_USR¤TX_NAISSANCE_MOIS_USR';
            $champs .= '¤TX_NAISSANCE_JOUR_USR¤DT_DECES_AN_USR¤TX_DECES_MOIS_USR¤TX_DECES_JOUR_USR';
            $champs .= '¤TX_PERSO_ADRESSE_VOIE_USR¤TX_PERSO_ADRESSE_CP_USR¤TX_PERSO_ADRESSE_BP_USR';
            $champs .= '¤TX_PERSO_ADRESSE_CEDEX_USR¤TX_PERSO_TEL_FIXE_USR¤TX_PERSO_TEL_MOBILE_USR';
            $champs .= '¤TX_PERSO_TEL_FAX_USR¤TX_PERSO_EMAIL_USR¤TX_PERSO_URL_USR¤TX_PRO_ADRESSE_VOIE_USR';
            $champs .= '¤TX_PRO_ADRESSE_CP_USR¤TX_PRO_ADRESSE_BP_USR¤TX_PRO_ADRESSE_CEDEX_USR';
            $champs .= '¤TX_PRO_TEL_FIXE_USR¤TX_PRO_TEL_MOBILE_USR¤TX_PRO_TEL_FAX_USR';
            $champs .= '¤TX_PRO_EMAIL_USR¤TX_PRO_URL_USR¤TX_REMARQUES_USR¤BL_CS_MEMBRE_ACTUEL_USR¤BL_CS_MEMBRE_COMMISSION_USR¤TX_CS_PASSE_USR';
            $champs .= '¤BL_RETRAITE_USR¤TX_NATIONALITE_USR¤TX_FORMATION_ETUDES_USR¤DT_DERNIERE_CNX_USR';
            break;
        case 'T_UTILISATEUR_AASC':
            $champs = 'PK_UTILAASC_UAS¤FK_UAS_REF_ASR¤BL_ADHERENT_UAS¤TX_TYPE_UAS';
            $champs .= '¤TX_MODE_BULLETIN_UAS¤DT_COTISATION_EXPIRATION_UAS¤BL_RECU_SOUHAITE_UAS';
            $champs .= '¤TX_REMARQUES_UAS';
            break;
        case 'T_UTILISATEUR_COLLAB':
            $champs = 'PK_UTILISATEUR_UCLB¤TX_DOMAINE_RECHERCHE_UCLB';
            $champs .= '¤TX_TRAVAUX_EN_COURS_UCLB¤TX_PUBLICATIONS_RECENTES_UCLB¤TX_PROJETS_RECHERCHE_UCLB';
            break;
        case 'T_UTILISATEUR_ETUDIANT':
            $champs = 'PK_UTILETUD_UTD¤TX_NIVEAU_ETUDES_UTD¤TX_DOMAINE_ETUDES_UTD¤TX_UNIVERSITE_UTD¤BL_REUNIONS_DOCTORANTS';
            break;
        case 'T_UTILISATEUR_IMP':
            $champs = 'PK_UTILIMP_UIP¤FK_UIP_REF_PML¤TX_DOMAINE_RESPONSABILITE_UIP¤TX_REMARQUES_UIP';
            break;
        case 'T_UTILISATEUR_SC':
            $champs = 'PK_UTILSC_USC¤FK_USC_REF_SUS¤DT_ENTREE_USC¤TX_TEL_NUMERO_INTERNE_USC';
            $champs .= '¤TX_STATUT_COMPLEMENT_USC¤TX_REMARQUES_USC¤TX_DOMAINE_RECHERCHE_USC';
            $champs .= '¤TX_TRAVAUX_EN_COURS_USC¤TX_PUBLICATIONS_RECENTES_USC¤TX_PROJETS_RECHERCHE_USC';
            break;
        case 'T_VOLUME':
            $champs = 'PK_VOLUME_VOL¤FK_VOL_REF_VIF¤FK_VOL_REF_PML';
            break;
        case 'T_VOLUME_INFOS':	// 64 champs
            $champs = 'PK_VOLUMEINFOS_VIF¤TX_ISBN_VIF¤NM_NUMERO_COLLECTION_VIF¤TX_NUMCOLL_COMPLEMENT_VIF';
            $champs .= '¤TX_NUMCOLL_ANCIEN_VIF¤TX_NUMCOLL_NOUVEAU_VIF¤TX_SOUS_COLLECTION_VIF';
            $champs .= '¤TX_SOUSCOLL_NUMERO_VIF¤TX_PAGETITRE_TITRE_VIF¤TX_PAGETITRE_SSTITRE_VIF';
            $champs .= '¤DT_PARUTION_AN_VIF¤DT_DERNIERE_REIMP_AN_VIF¤NM_NOMBRE_REIMP_VIF';
            $champs .= '¤TX_NOMBRE_REIMP_CPLT_VIF¤TX_DOMAINE_VIF¤BL_DEVIS_DEMANDE_VIF';
            $champs .= '¤DT_DEVIS_DEMANDE_VIF¤BL_DEVIS_RECU_VIF¤DT_DEVIS_RECU_VIF';
            $champs .= '¤BL_SUBVENTION_DEMANDE_VIF¤DT_SUBVENTION_DEMANDE_VIF';
            $champs .= '¤BL_SUBVENTION_RECUE_VIF¤DT_SUBVENTION_RECUE_VIF';
            $champs .= '¤BL_PLACARD_TXTANCIEN_DEMANDE_VIF¤DT_PLACARD_TXTANCIEN_DEMANDE_VIF';
            $champs .= '¤BL_PLACARD_TXTANCIEN_RECU_VIF¤DT_PLACARD_TXTANCIEN_RECU_VIF';
            $champs .= '¤BL_CERF_1ERE_VERSION_ENVOYEE_VIF¤DT_CERF_1ERE_VERSION_ENVOYEE_VIF';
            $champs .= '¤BL_IMP_1ERE_VERSION_ENVOYEE_VIF¤DT_IMP_1ERE_VERSION_ENVOYEE_VIF';
            $champs .= '¤BL_1ERES_EPREUVES_RECUES_VIF¤DT_1ERES_EPREUVES_RECUES_VIF';
            $champs .= '¤BL_1ERES_EPREUVES_RENVOYEES_VIF¤DT_1ERES_EPREUVES_RENVOYEES_VIF';
            $champs .= '¤BL_2EMES_EPREUVES_RECUES_VIF¤DT_2EMES_EPREUVES_RECUES_VIF';
            $champs .= '¤BL_2EMES_EPREUVES_RENVOYEES_VIF¤DT_2EMES_EPREUVES_RENVOYEES_VIF';
            $champs .= '¤BL_3EMES_EPREUVES_RECUES_VIF¤DT_3EMES_EPREUVES_RECUES_VIF';
            $champs .= '¤BL_JAQUETTE_DEMANDEE_VIF¤DT_JAQUETTE_DEMANDEE_VIF¤BL_JAQUETTE_RECUE_VIF';
            $champs .= '¤DT_JAQUETTE_RECUE_VIF¤BL_COUVERTURE_ENVOYEE_VIF¤DT_COUVERTURE_ENVOYEE_VIF';
            $champs .= '¤BL_COUVERTURE_RECUE_VIF¤DT_COUVERTURE_RECUE_VIF¤DT_BAT_AN_VIF';
            $champs .= '¤TX_BAT_MOIS_VIF¤TX_BAT_JOUR_VIF¤DT_SORTIE_AN_VIF¤TX_SORTIE_MOIS_VIF';
            $champs .= '¤DT_PREVUE_PARUTION_AN_VIF¤TX_PREVUE_PARUTION_MOIS_VIF¤TX_PREVUE_PARUTION_JOUR_VIF';
            $champs .= '¤NM_PRIX_EUROS_VIF¤NM_NOMBRE_PAGES_VIF¤NM_TIRAGE_VIF¤NM_ETAT_STOCK_VIF';
            $champs .= '¤DT_ETAT_STOCK_DERNIER_AN¤TX_ETAT_STOCK_DERNIER_MOIS¤TX_REMARQUES_VIF';
            break;
        case 'T_VOLUME_REIMPRESSION':	// 16 champs
            $champs = 'PK_VOLREIMPR_VRP¤FK_VRP_REF_VOL¤TX_ISBN_VRP¤DT_DECISION_REIMP_AN_VRP';
            $champs .= '¤TX_DECISION_REIMP_MOIS_VRP¤DT_SORTIE_PREVUE_AN_VRP';
            $champs .= '¤TX_SORTIE_PREVUE_MOIS_VRP¤DT_BAT_AN_VRP¤TX_BAT_MOIS_VRP¤TX_BAT_JOUR_VRP';
            $champs .= '¤DT_SORTIE_AN_VRP¤TX_SORTIE_MOIS_VRP¤NM_PRIX_VRP¤NM_NOMBRE_PAGES_VRP';
            $champs .= '¤TX_REMARQUES_VRP¤TX_REMARQUES_CONFIDENTIELLES_VRP';
            break;
        case 'T_ZONE_GEO':	// 7 champs
            $champs = 'PK_ZONEGEO_ZNG¤FK_ZNG_REF_ZNT¤TX_NOM_FR_ACTUEL_ZNG¤TX_NOM_POSTAL_ZNG';
            $champs .= '¤TX_NOM_ANTIQUITE_ZNG¤TX_NOM_MOYENAGE_ZNG¤NM_PARENT_ZNG';
            break;
        case 'T_ZONE_TYPE':	// 3 champs
            $champs = 'PK_ZONETYPE_ZNT¤LB_ZONETYPE_ZNT¤NM_NIVEAU_ZNT';
            break;
    }

    return $champs;
}

function _recupContraintes($_table)
{
    // on fait l'inverse, nom de table en majuscule (pour éviter de tout changer...)
    // d.goudard 19/08/2015
    // et "SHOW COLUMNS $_table" tu connais mec ?
    $_table = strtoupper((string) $_table);

    $contraintes = '';
    // Les clefs primaires sont omises !
    switch ($_table) {
        // case "t_assoc_ovr_omc":
        case 'T_ASSOC_OVR_OMC':
            $contraintes = 'not null¤not null¤not null';
            break;
            /*
        case "T_ASSOC_AAN_BEL":
            $contraintes = "not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_AAN_BIB":
            $contraintes = "not null¤not null¤not null";
            break;
            */
        case 'T_ASSOC_AAN_DOC':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_AAN_HPL':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_AAN_IMG':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_AAN_ORH':
            $contraintes = 'not null¤not null¤not null';
            break;
        case 'T_ASSOC_AAN_USR':
            $contraintes = 'not null¤not null¤not null¤null';
            break;
            /*
        case "T_ASSOC_AGL_DOC":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_AGL_PML":
            $contraintes = "not null¤not null¤not null¤not null¤null";
            break;
            */
            /*
        case "T_ASSOC_AGL_UAS_NPT":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_AUD_AAN":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_AUD_USR":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
        case 'T_ASSOC_CLQ_AAN':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_CLQ_DOC':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_CLQ_HPL':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_CLQ_PML':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_CLQ_USR_NPT':
            $contraintes = 'not null¤not null¤not null¤not null¤null';
            break;
        case 'T_ASSOC_CRD_USR':
            $contraintes = 'not null¤not null¤not null';
            break;
            /*
        case "T_ASSOC_DOC_USR_NPT":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_ECH_AAN":
            $contraintes = "not null¤not null¤not null¤null";
            break;
            */
            /*
        case "T_ASSOC_ECH_DOC":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_ECH_OVR":
            $contraintes = "not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_ECH_POL":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_ECH_USR_NPT":
            $contraintes = "not null¤not null¤not null¤not null¤not null¤not null";
            break;
             */
            /*
        case "T_ASSOC_FMT_DOC":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_FMT_PML":
            $contraintes = "not null¤not null¤not null¤not null¤null";
            break;
            */
            /*
        case "T_ASSOC_FMT_USR_NPT":
            $contraintes = "not null¤not null¤not null¤not null¤null";
            break;
             */
        case 'T_ASSOC_HPL_HTC':
            $contraintes = 'not null¤not null¤not null';
            break;
        case 'T_ASSOC_HPL_HTM':
            $contraintes = 'not null¤not null¤not null';
            break;
        case 'T_ASSOC_OVR_OGR':
            $contraintes = 'not null¤not null¤not null¤not null¤null';
            break;
        case 'T_ASSOC_OVR_OTM':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_OVR_USR_TCH':	// 27 champs
            $contraintes = 'not null¤not null¤not null¤not null¤not null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_ASSOC_PFL_DRT':
            $contraintes = 'not null¤not null¤not null';
            break;
            /*
        case "T_ASSOC_PML_HPL":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
        case 'T_ASSOC_PML_PMP':
            $contraintes = 'not null¤not null¤not null¤not null¤not null¤null';
            break;
        case 'T_ASSOC_POL_AAN':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_ASSOC_POL_USR':
            $contraintes = 'not null¤not null¤not null¤null¤null¤null';
            break;
            /*
        case "T_ASSOC_USR_BEL":
            $contraintes = "not null¤not null¤not null";
            break;
            */
            /*
        case "T_ASSOC_USR_BIB":
            $contraintes = "not null¤not null¤not null¤null¤null¤null";
            break;
            */
        case 'T_ASSOC_USR_PFL':
            $contraintes = 'not null¤not null¤not null¤not null¤not null¤null';
            break;
        case 'T_ASSOC_USR_PML':
            $contraintes = 'not null¤not null¤not null¤not null¤not null¤null¤null';
            break;
            /*
        case "T_ASSOC_USR_USR":
            $contraintes = "not null¤not null¤not null¤not null";
            break;
            */
        case 'T_ASSOC_VOL_CRD':
            $contraintes = 'not null¤not null¤not null';
            break;
        case 'T_ASSOC_VOL_OVR':
            $contraintes = 'not null¤not null¤not null¤not null¤null';
            break;
            // case "T_ASSOC_VRP_USR":
            //	$contraintes = "not null¤not null¤not null¤not null";
            //	break;
        case 'T_AUTEUR_ANCIEN':
            $contraintes = 'not null¤null¤null¤null¤null¤not null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_AXE':
            $contraintes = 'not null¤not null¤null';
            break;
            /*
        case "T_BIBLIO_ELEMENT":
            $contraintes = "not null¤null¤null¤null¤null¤not null¤null";
            break;
            */
            /*
            case "T_BIBLIOELT_ARTREVUE":
                $contraintes = "not null¤null¤not null¤null¤null¤null¤null";
                $contraintes .= "¤null¤null¤null¤null¤null¤null";
                break;
             */
            /*
            case "T_BIBLIOELT_LIVRE":
                $contraintes = "not null¤not null¤null¤not null¤null¤null";
                $contraintes .= "¤null¤null¤null¤null¤null¤null¤null¤null";
                break;
             */
            /*
        case "T_BIBLIOELT_SSLIVRE":
            $contraintes = "not null¤not null¤not null";
            $contraintes .= "¤null¤null¤null";
            $contraintes .= "¤null¤null¤null";
            break;
            */
            /*
        case "T_BIBLIOELT_TYPE":
            $contraintes = "not null¤not null¤not null";
            break;
            */
        case 'ASSOC_BIBDATA_VERSET':
            $contraintes = 'not null¤not null¤not null';
            break;
            /*
        case "T_BIB_CORRESPONDANCE":
            $contraintes = "not null¤null¤null¤null¤null¤null¤null";
            break;
            */
        case 'T_BIB_CHAPITRE':
            $contraintes = 'not null¤not null¤not null¤not null';
            break;
        case 'T_BIB_PRECISION':
            $contraintes = 'not null¤not null¤null¤not null';
            break;
        case 'T_BIB_TYPE':
            $contraintes = 'not null¤not null';
            break;
        case 'T_BIB_VERSET':
            $contraintes = 'not null¤not null¤not null¤null';
            break;
        case 'T_BIBDATA':   // 25 champs
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤not null¤not null¤null';
            // no break
        case 'T_BIBDATA2':  // 17 champs
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null';
            break;
            // case "t_bib_liste":
        case 'T_BIB_LISTE':
            $contraintes = 'not null¤not null¤not null¤not null¤null¤null¤null¤not null¤not null¤not null';
            break;
        case 'T_COLLOQUE':
            $contraintes = 'not null¤null¤null¤not null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤not null¤not null¤not null¤null¤null¤null';
            break;
            /*
        case "T_COMPTEUR":
            $contraintes = "not null¤not null¤not null¤not null¤not null¤not null¤null¤null";
            break;
            */
        case 'T_COMPTE_RENDU':	// 21 champs
            $contraintes = 'not null¤null¤not null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤not null¤not null¤null¤null¤null';
            break;
            /*
        case "T_COTISATION_AASC":
            $contraintes = "not null¤null¤null¤not null¤not null¤not null¤null";
            break;
             */
        case 'T_DOCUMENT':
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤not null¤null¤not null¤not null¤not null';
            $contraintes .= '¤null¤null¤null¤null¤null';
            break;
        case 'T_DOCUMENT_TYPE':
            $contraintes = 'not null¤not null¤null';
            break;
        case 'T_EDITION_REFERENCE':
            $contraintes = 'not null¤not null¤not null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_ERRATUM':
            $contraintes = 'not null¤not null¤not null¤null¤null¤null¤null';
            break;
        case 'T_HYPERLIEN':
            $contraintes = 'not null¤not null¤null¤not null¤not null¤not null¤not null¤not null¤null';
            break;
        case 'T_HYPERLIEN_THEME':
            $contraintes = 'not null¤not null¤null';
            break;
        case 'T_HYPERLIEN_TYPE_CONTENU':
            $contraintes = 'not null¤not null';
            break;
        case 'T_HYPERLIEN_TYPE_SITE':
            $contraintes = 'not null¤not null';
            break;
        case 'T_IMAGE':
            $contraintes = 'not null¤null¤null¤not null¤not null¤not null';
            break;
        case 'T_IP_AUTORISEE':
            $contraintes = 'not null¤not null¤null';
            break;
        case 'T_OEUVRE':
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤not null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_OUTILS_RECHERCHE':
            $contraintes = 'not null¤not null¤not null¤null¤null¤null¤not null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_OUTILS_RECHERCHE_TYPE':
            $contraintes = 'not null¤not null';
            break;
        case 'T_OUTILS_RECHERCHE_RUBRIQUE':
            $contraintes = 'not null¤not null¤null';
            break;
        case 'T_OEUVRE_GENRE':
            $contraintes = 'not null¤not null¤null';
            break;
        case 'T_OEUVRE_MOTCLEF':
            $contraintes = 'not null¤not null';
            break;
        case 'T_OEUVRE_THEME':
            $contraintes = 'not null¤not null¤null¤null¤null¤null';
            break;
        case 'T_PERSONNE_MORALE':	// 18 champs
            $contraintes = 'not null¤not null¤null¤null¤null¤not null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_PERSMORALE_AASC':
            $contraintes = 'not null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_PERSMORALE_TYPE':
            $contraintes = 'not null¤not null';
            break;
        case 'T_POLE':
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_TRACE':
            $contraintes = 'not null¤not null¤not null¤not null¤not null¤not null¤not null';
            break;
        case 'T_UTILISATEUR':
            $contraintes = 'not null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤not null¤not null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_UTILISATEUR_AASC':
            $contraintes = 'not null¤null¤not null¤null¤null¤null¤null¤null';
            break;
        case 'T_UTILISATEUR_COLLAB':
            $contraintes = 'not null¤null¤null¤null¤null';
            break;
        case 'T_UTILISATEUR_ETUDIANT':
            $contraintes = 'not null¤null¤null¤null¤null';
            break;
        case 'T_UTILISATEUR_IMP':
            $contraintes = 'not null¤not null¤null¤null';
            break;
        case 'T_UTILISATEUR_SC':
            $contraintes = 'not null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_VOLUME':
            $contraintes = 'not null¤null¤null';
            break;
        case 'T_VOLUME_INFOS': // 64 champs
            $contraintes = 'not null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            break;
        case 'T_VOLUME_REIMPRESSION':
            $contraintes = 'not null¤not null¤null¤null¤null¤null¤null¤null¤null¤null¤null¤null';
            $contraintes .= '¤null¤null¤null¤null';
            break;
        case 'T_ZONE_GEO':
            $contraintes = 'not null¤not null¤null¤not null¤null¤null¤null';
            break;
        case 'T_ZONE_TYPE':
            $contraintes = 'not null¤not null¤not null';
            break;
    }

    return $contraintes;
}

function _recupTypes($_table)
{
    // on fait l'inverse, nom de table en majuscule (pour éviter de tout changer...)
    // d.goudard 19/08/2015
    // et "SHOW COLUMNS $_table" tu connais mec ?
    $_table = strtoupper((string) $_table);

    $types = '';	// date || datetime || int || float || string
    // Les clefs primaires sont omises !
    switch ($_table) {
        // case "t_assoc_ovr_omc":
        case 'T_ASSOC_OVR_OMC':
            $types = 'int¤int¤int';
            break;
            /*
        case "T_ASSOC_AAN_BEL":
            $types = "int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_AAN_BIB":
            $types = "int¤int¤int";
            break;
            */
        case 'T_ASSOC_AAN_DOC':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_AAN_HPL':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_AAN_IMG':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_AAN_ORH':
            $types = 'int¤int¤int';
            break;
        case 'T_ASSOC_AAN_USR':
            $types = 'int¤int¤int¤string';
            break;
            /*
        case "T_ASSOC_AGL_DOC":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_AGL_PML":
            $types = "int¤int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_AGL_UAS_NPT":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_AUD_AAN":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_AUD_USR":
            $types = "int¤int¤int¤int";
            break;
            */
        case 'T_ASSOC_CLQ_AAN':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_CLQ_DOC':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_CLQ_HPL':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_CLQ_PML':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_CLQ_USR_NPT':
            $types = 'int¤int¤int¤int¤string';
            break;
        case 'T_ASSOC_CRD_USR':
            $types = 'int¤int¤int';
            break;
            /*
        case "T_ASSOC_DOC_USR_NPT":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_ECH_AAN":
            $types = "int¤int¤int¤string";
            break;
            */
            /*
        case "T_ASSOC_ECH_DOC":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_ECH_OVR":
            $types = "int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_ECH_POL":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_ECH_USR_NPT":
            $types = "int¤int¤int¤int¤int¤int";
            break;
             *
             */
            /*
        case "T_ASSOC_FMT_DOC":
            $types = "int¤int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_FMT_PML":
            $types = "int¤int¤int¤int¤string";
            break;
            */
            /*
        case "T_ASSOC_FMT_USR_NPT":
            $types = "int¤int¤int¤int¤string";
            break;
             */
        case 'T_ASSOC_HPL_HTC':
            $types = 'int¤int¤int';
            break;
        case 'T_ASSOC_HPL_HTM':
            $types = 'int¤int¤int';
            break;
        case 'T_ASSOC_OVR_OGR':
            $types = 'int¤int¤int¤int¤string';
            break;
        case 'T_ASSOC_OVR_OTM':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_OVR_USR_TCH':	// 27 champs
            $types = 'int¤int¤int¤int¤int¤int¤int¤string¤int¤int¤string¤int¤string¤int';
            $types .= '¤string¤string¤int¤string';
            $types .= '¤int¤int¤string¤string¤int¤int¤string¤string¤string';
            break;
        case 'T_ASSOC_PFL_DRT':
            $types = 'int¤int¤int';
            break;
            /*
        case "T_ASSOC_PML_HPL":
            $types = "int¤int¤int¤int";
            break;
            */
        case 'T_ASSOC_PML_PMP':
            $types = 'int¤int¤int¤int¤int¤string';
            break;
        case 'T_ASSOC_POL_AAN':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_ASSOC_POL_USR':
            $types = 'int¤int¤int¤int¤int¤string';
            break;
            /*
        case "T_ASSOC_USR_BEL":
            $types = "int¤int¤int";
            break;
            */
            /*
        case "T_ASSOC_USR_BIB":
            $types = "int¤int¤int¤int¤int¤int";
            break;
            */
        case 'T_ASSOC_USR_PFL':
            $types = 'int¤int¤int¤int¤int¤string';
            break;
        case 'T_ASSOC_USR_PML':
            $types = 'int¤int¤int¤int¤int¤string¤string';
            break;
            /*
        case "T_ASSOC_USR_USR":
            $types = "int¤int¤int¤int";
            break;
            */
        case 'T_ASSOC_VOL_CRD':
            $types = 'int¤int¤int';
            break;
        case 'T_ASSOC_VOL_OVR':
            $types = 'int¤int¤int¤int¤string';
            break;
        case 'T_AUTEUR_ANCIEN':
            $types = 'int¤int¤int¤int¤int¤string¤string¤int¤int¤int¤int¤int¤string¤int¤int¤int';
            $types .= '¤string¤string¤string¤string¤string¤string¤int';
            break;
        case 'T_AXE':
            $types = 'int¤string¤string';
            break;
            /*
        case "T_BIBLIO_ELEMENT":
            $types = "int¤int¤int¤int¤string¤int¤int";
            break;
            */
            /*
            case "T_BIBLIOELT_ARTREVUE":
                $types = "int¤int¤string¤string¤int¤string¤string¤int¤string¤string";
                $types .= "¤float¤string¤string";
                break;
             */
            /*
            case "T_BIBLIOELT_LIVRE":
                $types = "int¤int¤int¤string¤string¤int¤string¤int";
                $types .= "¤int¤string¤float¤string¤string¤string";
                break;
             */
            /*
        case "T_BIBLIOELT_SSLIVRE":
            $types = "int¤int¤int¤string¤string¤int¤string¤string¤string";
            break;
            */
            /*
        case "T_BIBLIOELT_TYPE":
            $types = "int¤string¤string";
            break;
            */
        case 'ASSOC_BIBDATA_VERSET':
            $types = 'int¤int¤int';
            break;
            /*
        case "T_BIB_CORRESPONDANCE":
            $types = "int¤int¤int¤int¤int¤int¤int";
            break;
            */
        case 'T_BIB_CHAPITRE':
            $types = 'int¤int¤int¤int';
            break;
        case 'T_BIB_PRECISION':
            $types = 'int¤string¤string¤int';
            break;
        case 'T_BIB_TYPE':
            $types = 'int¤string';
            break;
        case 'T_BIB_VERSET':
            $types = 'int¤int¤int¤string';
            break;
        case 'T_BIBDATA':
            $types = 'int¤int¤int¤int¤int¤int¤int¤string¤string¤string¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string¤string¤string¤string¤string¤date¤string¤int';
            break;
        case 'T_BIBDATA2':
            $types = 'int¤int¤int¤int¤int¤int¤string¤string¤string¤string¤string¤string';
            $types .= '¤string¤string¤date¤string¤int';
            break;
            // case "t_bib_liste":
        case 'T_BIB_LISTE':
            $types = 'int¤int¤string¤string¤string¤string¤string¤int¤int¤int';
            break;
        case 'T_COLLOQUE':
            $types = 'int¤int¤int¤string¤int¤string¤string¤string¤string¤string¤string¤string';
            $types .= '¤int¤int¤int¤int¤string¤string';
            break;
            /*
        case "T_COMPTEUR":
            $types = "int¤datetime¤string¤string¤string¤string¤string¤string";
            break;
            */
        case 'T_COMPTE_RENDU':	// 21 champs
            $types = 'int¤int¤int¤int¤int¤string¤string¤int¤int¤string¤int¤int¤string¤string¤string';
            $types .= '¤string¤int¤int¤string¤string¤int';
            break;
            /*
        case "T_COTISATION_AASC":
            $types = "int¤int¤int¤date¤float¤string¤int";
            break;
             */
        case 'T_DOCUMENT':
            $types = 'int¤int¤int¤int¤int¤int¤string¤string¤int¤int¤int¤string¤date¤string¤string¤string';
            break;
        case 'T_DOCUMENT_TYPE':
            $types = 'int¤string¤string';
            break;
        case 'T_EDITION_REFERENCE':
            $types = 'int¤int¤string¤string¤string¤string¤string¤string¤int¤string¤string¤string¤string¤string¤string';
            break;
        case 'T_ERRATUM':
            $types = 'int¤int¤int¤string¤string¤string¤string';
            break;
        case 'T_HYPERLIEN':
            $types = 'int¤int¤int¤string¤string¤int¤int¤int¤string';
            break;
        case 'T_HYPERLIEN_THEME':
            $types = 'int¤string¤int';
            break;
        case 'T_HYPERLIEN_TYPE_CONTENU':
            $types = 'int¤string';
            break;
        case 'T_HYPERLIEN_TYPE_SITE':
            $types = 'int¤int';
            break;
        case 'T_IMAGE':
            $types = 'int¤string¤string¤string¤int¤int';
            break;
        case 'T_IP_AUTORISEE':
            $types = 'int¤string¤string';
            break;
        case 'T_OEUVRE':
            $types = 'int¤int¤int¤int¤int¤int¤int¤int¤string¤int¤string¤string¤string¤int¤string¤string¤string¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string¤string¤string¤string¤string¤string¤string¤int¤int¤string¤int¤int';
            $types .= '¤string¤string¤string¤int¤int¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string¤string¤string¤string¤int¤string¤string¤string¤int¤int¤int¤string¤string';
            break;
        case 'T_OEUVRE_GENRE':
            $types = 'int¤string¤string';
            break;
        case 'T_OEUVRE_MOTCLEF':
            $types = 'int¤string';
            break;
        case 'T_OEUVRE_THEME':
            $types = 'int¤string¤string¤int¤int¤int';
            break;
        case 'T_OUTILS_RECHERCHE':
            $types = 'int¤int¤int¤int¤int¤int¤string¤string¤string¤string¤string¤string¤string¤string';
            break;
        case 'T_OUTILS_RECHERCHE_TYPE':
            $types = 'int¤string';
            break;
        case 'T_OUTILS_RECHERCHE_RUBRIQUE':
            $types = 'int¤string¤string';
            break;
        case 'T_PERSONNE_MORALE':	// 18 champs
            $types = 'int¤int¤int¤int¤int¤string¤string¤string¤string¤string¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string';
            break;
        case 'T_PERSMORALE_AASC':
            $types = 'int¤string¤int¤string¤int¤int¤string';
            break;
        case 'T_PERSMORALE_TYPE':
            $types = 'int¤string';
            break;
        case 'T_POLE':
            $types = 'int¤int¤string¤string¤string¤int¤int¤int';
            break;
        case 'T_TRACE':
            $types = 'int¤string¤int¤string¤string¤datetime¤int';
            break;
        case 'T_UTILISATEUR':
            $types = 'int¤int¤int¤int¤int¤int¤int¤int¤int¤int¤int¤string';
            $types .= '¤string¤datetime';
            $types .= '¤string¤string';
            $types .= '¤string¤string¤string¤int¤int¤string¤string¤int¤string¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string¤string¤string¤string¤string¤string¤string';
            $types .= '¤string¤string¤string¤string¤string¤string¤int¤int¤string¤int¤string¤string¤datetime';
            break;
        case 'T_UTILISATEUR_AASC':
            $types = 'int¤int¤int¤string¤string¤int¤int¤string';
            break;
        case 'T_UTILISATEUR_COLLAB':
            $types = 'int¤string¤string¤string¤string';
            break;
        case 'T_UTILISATEUR_ETUDIANT':
            $types = 'int¤string¤string¤string¤int';
            break;
        case 'T_UTILISATEUR_IMP':
            $types = 'int¤int¤string¤string';
            break;
        case 'T_UTILISATEUR_SC':
            $types = 'int¤int¤date¤string¤string¤string¤string¤string¤string¤string';
            break;
        case 'T_VOLUME':
            $types = 'int¤int¤int';
            break;
        case 'T_VOLUME_INFOS':
            $types = 'int¤string¤int¤string¤string¤string¤string¤string¤string¤string¤int¤int¤int';
            $types .= '¤string¤string¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date';
            $types .= '¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date¤int¤date';
            $types .= '¤int¤date¤int¤date¤int¤string¤string¤int¤string¤int¤string¤string¤float¤int¤int¤int¤int';
            $types .= '¤string¤string';
            break;
        case 'T_VOLUME_REIMPRESSION':
            $types = 'int¤int¤string¤int¤string¤int¤string¤int¤string¤string¤int¤string¤int¤int¤string¤string';
            break;
        case 'T_ZONE_GEO':
            $types = 'int¤int¤string¤string¤string¤string¤int';
            break;
        case 'T_ZONE_TYPE':
            $types = 'int¤string¤int';
            break;
    }

    return $types;
}

// On récupère les clefs enfants pour la table considérée
function _recupClefsEnfants($_table)
{
    $enfants = '';

    // on fait l'inverse, nom de table en majuscule (pour éviter de tout changer...)
    // d.goudard 19/08/2015
    // et "SHOW COLUMNS $_table" tu connais mec ?
    $_table = strtoupper((string) $_table);

    switch ($_table) {
        case 'T_AUTEUR_ANCIEN':
            // $enfants = "T_OEUVRE.FK_OVR_REF_AAN¤T_BIBDATA.FK_BDT_REF_AAN";
            $enfants = 't_oeuvre.FK_OVR_REF_AAN¤t_bibdata.FK_BDT_REF_AAN';
            break;
        case 'T_AXE':
            // $enfants = "T_POLE.FK_POL_REF_AXE";
            $enfants = 't_pole.FK_POL_REF_AXE';
            break;
            /*
            case "T_BIBLIOELT_ARTREVUE":
                $enfants = "T_BIBLIO_ELEMENT.FK_BEL_REF_BAR";
                break;
             */
            /*
            case "T_BIBLIOELT_LIVRE":
                $enfants = "T_BIBLIO_ELEMENT.FK_BEL_REF_BER¤T_BIBLIOELT_SSLIVRE.FK_BSL_REF_BER";
                break;
             */
            /*
        case "T_BIBLIOELT_SSLIVRE":
            $enfants = "T_BIBLIO_ELEMENT.FK_BEL_REF_BSL";
            break;
            */
            /*
        case "T_BIBLIOELT_TYPE":
            $enfants = "T_BIBLIOELT_LIVRE.FK_BER_REF_BET";
            $enfants .= "¤T_BIBLIOELT_SSLIVRE.FK_BSL_REF_BET";
            break;
            */
        case 'T_BIB_CHAPITRE':
            // $enfants = "T_BIB_VERSET.FK_BVT_REF_BCH";
            $enfants = 't_bib_verset.FK_BVT_REF_BCH';
            // $enfants .= "¤T_BIB_CORRESPONDANCE.FK1_BCP_REF_BCH¤T_BIB_CORRESPONDANCE.FK2_BCP_REF_BCH";
            break;
        case 'T_BIB_PRECISION':
            // $enfants = "T_BIBDATA.FK_BDT_REF_BBP";
            $enfants = 't_bibdata.FK_BDT_REF_BBP';
            break;
        case 'T_BIB_VERSET':
            // $enfants = "ASSOC_BIBDATA_VERSET.FK_ABV_REF_BVT";
            $enfants = 'assoc_bibdata_verset.FK_ABV_REF_BVT';
            // $enfants .= "¤T_BIB_CORRESPONDANCE.FK1_BCP_REF_BVT¤T_BIB_CORRESPONDANCE.FK2_BCP_REF_BVT";
            break;
        case 'T_BIB_TYPE':
            $enfants = 't_bib_liste.FK_BLT_REF_BTP';
            break;
        case 'T_BIBDATA':
            // $enfants = "ASSOC_BIBDATA_VERSET.FK_ABV_REF_BDT";
            $enfants = 'assoc_bibdata_verset.FK_ABV_REF_BDT';
            break;
        case 'T_BIBDATA2':
            // $enfants = "ASSOC_BIBDATA_VERSET.FK_ABV_REF_BDT";
            $enfants = 'assoc_bibdata_verset.FK_ABV_REF_BDT';
            break;
            // case "t_bib_liste":
        case 'T_BIB_LISTE':
            // $enfants = "T_BIB_CHAPITRE.FK_BCH_REF_BLT¤T_BIBDATA.FK_BDT_REF_BLT";
            $enfants = 't_bib_chapitre.FK_BCH_REF_BLT¤t_bibdata.FK_BDT_REF_BLT';
            // $enfants .= "¤T_BIB_CORRESPONDANCE.FK1_BCP_REF_BLT¤T_BIB_CORRESPONDANCE.FK2_BCP_REF_BLT";
            break;
        case 'T_COLLOQUE':
            $enfants = '';
            break;
        case 'T_COMPTE_RENDU':
            // $enfants = "T_ASSOC_CRD_USR.FK_ADR_REF_CRD¤T_ASSOC_VOL_CRD.FK_AVC_REF_CRD";
            $enfants = 't_assoc_crd_usr.FK_ADR_REF_CRD¤t_assoc_vol_crd.FK_AVC_REF_CRD';
            break;
        case 'T_DOCUMENT':
            // $enfants = "T_COMPTE_RENDU.FK_CRD_REF_DOC";
            $enfants = 't_compte_rendu.FK_CRD_REF_DOC';
            break;
        case 'T_EDITION_REFERENCE':
            // $enfants = "T_OEUVRE.FK1_OVR_REF_ERF¤T_OEUVRE.FK2_OVR_REF_ERF";
            $enfants = 't_oeuvre.FK1_OVR_REF_ERF¤t_oeuvre.FK2_OVR_REF_ERF';
            break;
        case 'T_ERRATUM':
            $enfants = '';
            break;
        case 'T_HYPERLIEN':
            $enfants = '';
            break;
        case 'T_HYPERLIEN_TYPE_SITE':
            // $enfants = "T_HYPERLIEN.FK_HPL_REF_HTS";
            $enfants = 't_hyperlien.FK_HPL_REF_HTS';
            break;
        case 'T_OEUVRE':
            // $enfants = "T_BIBDATA2.FK_BDT_REF_OVR¤T_OEUVRE.NM_PARENT_OVR";
            $enfants = 't_bibdata2.FK_BDT_REF_OVR¤t_oeuvre.NM_PARENT_OVR';
            break;
        case 'T_OUTILS_RECHERCHE':
            // $enfants = "T_ASSOC_AAN_ORH.FK_AAO_REF_ORH";
            $enfants = 't_assoc_aan_orh.FK_AAO_REF_ORH';
            break;
        case 'T_PERSONNE_MORALE':
            // $enfants = "T_EDITION_REFERENCE.FK_ERF_REF_PML¤T_VOLUME.FK_VOL_REF_PML";
            // $enfants .= "¤T_COMPTE_RENDU.FK_CRD_REF_PML";
            $enfants = 't_edition_reference.FK_ERF_REF_PML¤t_volume.FK_VOL_REF_PML';
            $enfants .= '¤t_compte_rendu.FK_CRD_REF_PML';

            // //$enfants .= "¤T_PERSMORALE_REVUE.FK_PMR_REF_PML"; /* ¤T_BIBLIOELT_LIVRE.FK_BER_REF_PML */
            // //$enfants .= "¤T_BIBLIOELT_ARTREVUE.FK_BAR_REF_PML";
            break;
        case 'T_UTILISATEUR':
            // $enfants = "T_UTILISATEUR.FK_POL_REF_USR1¤T_UTILISATEUR.FK_POL_REF_USR2¤T_UTILISATEUR.FK_POL_REF_USR3";
            // $enfants .= "¤T_BIBDATA.FK_BDT_REF_USR";
            $enfants = 't_utilisateur.FK_POL_REF_USR1¤t_utilisateur.FK_POL_REF_USR2¤t_utilisateur.FK_POL_REF_USR3';
            $enfants .= '¤t_bibdata.FK_BDT_REF_USR';
            break;
        case 'T_VOLUME':
            // $enfants = "T_ASSOC_VOL_OVR.FK_AVO_REF_VOL¤T_ERRATUM.FK_ERT_REF_VOL";
            // $enfants .= "¤T_VOLUME_REIMPRESSION.FK_VRP_REF_VOL¤T_ASSOC_VOL_CRD.FK_AVC_REF_VOL";
            $enfants = 't_assoc_vol_ovr.FK_AVO_REF_VOL¤t_erratum.FK_ERT_REF_VOL';
            $enfants .= '¤t_volume_reimpression.FK_VRP_REF_VOL¤t_assoc_vol_crd.FK_AVC_REF_VOL';
            break;
        case 'T_VOLUME_INFOS':
            // $enfants = "T_VOLUME.FK_VOL_REF_VIF";
            $enfants = 't_volume.FK_VOL_REF_VIF';
            break;
        case 'T_VOLUME_REIMPRESSION':
            $enfants = '';
            break;
        case 'T_ZONE_GEO':
            // $enfants = "T_AUTEUR_ANCIEN.FK1_AAN_REF_ZNG¤T_AUTEUR_ANCIEN.FK2_AAN_REF_ZNG";
            // $enfants .= "¤T_AUTEUR_ANCIEN.FK3_AAN_REF_ZNG¤T_AUTEUR_ANCIEN.FK4_AAN_REF_ZNG";
            // $enfants .= "¤T_COLLOQUE.FK1_CLQ_REF_ZNG¤T_COLLOQUE.FK2_CLQ_REF_ZNG";
            // $enfants .= "¤T_PERSONNE_MORALE.FK1_PML_REF_ZNG¤T_PERSONNE_MORALE.FK2_PML_REF_ZNG";
            // $enfants .= "¤T_UTILISATEUR.FK1_USR_REF_ZNG¤T_UTILISATEUR.FK2_USR_REF_ZNG";
            // $enfants .= "¤T_UTILISATEUR.FK3_USR_REF_ZNG¤T_UTILISATEUR.FK4_USR_REF_ZNG";
            $enfants = 't_auteur_ancien.FK1_AAN_REF_ZNG¤t_auteur_ancien.FK2_AAN_REF_ZNG';
            $enfants .= '¤t_auteur_ancien.FK3_AAN_REF_ZNG¤t_auteur_ancien.FK4_AAN_REF_ZNG';
            $enfants .= '¤t_colloque.FK1_CLQ_REF_ZNG¤t_colloque.FK2_CLQ_REF_ZNG';
            $enfants .= '¤t_personne_morale.FK1_PML_REF_ZNG¤t_personne_morale.FK2_PML_REF_ZNG';
            $enfants .= '¤t_utilisateur.FK1_USR_REF_ZNG¤t_utilisateur.FK2_USR_REF_ZNG';
            $enfants .= '¤t_utilisateur.FK3_USR_REF_ZNG¤t_utilisateur.FK4_USR_REF_ZNG';
            break;
    }

    return $enfants;
}
