<?php
$oConnexion = DbConnection();

// PASSWORDS ENCRYPTION
$req1 = <<<SQL
    SELECT TX_PASSWORD_USR
    FROM sc_t_utilisateur as u;
SQL;
set_time_limit(0);
$oRs1 = DbExecRequete($req1, $oConnexion);
$passwords = [];
$hashes = [];
while($fetch = DbEnregSuivantTab($oRs1)){
    $passwords[] = $fetch['TX_PASSWORD_USR'];
    $hashes[] = password_hash($fetch['TX_PASSWORD_USR'], PASSWORD_ARGON2ID, ["cost" => 13]);
}

$queries = array_map(static function ($hash, $password): string{
    return <<<SQL
        UPDATE sc_t_utilisateur
        SET TX_PASSWORD_USR_HASH = '{$hash}'
        WHERE TX_PASSWORD_USR = '{$password}';
    SQL;
}, $hashes, $passwords);

array_walk($queries, static function (string $query) use ($oConnexion) {
    DbExecRequete($query, $oConnexion);
});


