<?php

const PAGINATION_LIMIT = 50;

$oConnexion = DbConnection();

if (!preg_match('/^\/api\/volume(.*)/', $_SERVER['REQUEST_URI'] ?? '', $matches)) {
    throw new \InvalidArgumentException('Resource not found');
}
[$id, $query_string] = explode('?', trim($matches[1], '/'));
$parts = explode('&', $query_string);
$params = [];
foreach ($parts as $part) {
    [$name, $value] = explode('=', $part);
    $params[$name] = urldecode($value);
}
$id = urldecode($id);

$page = $params['page'] ?? 1;
if ($page < 1) {
    throw new \InvalidArgumentException('Invalid page range');
}
$limit = PAGINATION_LIMIT;
$offset = ($page - 1) * $limit;
$status = $params['status'] ?? null;
switch (true) {
    case !$id && 'chantier' === $status:
        // volumes en chantier
        $sql = <<<SQL
        SELECT
            V.TX_PAGETITRE_TITRE_VIF,
            V.NM_NUMERO_COLLECTION_VIF,
            V.PK_VOLUMEINFOS_VIF,
            A.TX_NOM_FRANCAIS_AAN
        FROM sc_t_volume AS V
        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
            ON V.`PK_VOLUMEINFOS_VIF` = ot.`FK_OUT_REF_VOL`
        LEFT JOIN sc_t_tache AS T
            ON T.PK_TACHE_TCH = ot.FK_OUT_REF_TCH
        LEFT JOIN sc_t_oeuvre AS O
            ON O.`PK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
        LEFT JOIN sc_t_assoc_au_ovr AS ao
            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
        LEFT JOIN sc_t_auteur AS A
            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
        WHERE ot.FK_OUT_REF_USR IS NOT NULL
            AND vo.FK_AVO_REF_VOL IS NULL
            AND O.`BL_OEUVRE_INTERNE_OVR`=1
        GROUP BY V.TX_PAGETITRE_TITRE_VIF
        ORDER BY V.NM_NUMERO_COLLECTION_VIF DESC
        LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRs = DbExecRequete($sql, $oConnexion, true);
        $arr_chantier = [];
        while ($fetch = DbEnregSuivantTab($oRs)) {
            $arr_chantier[] = [
                'Num. Coll.' => $fetch['NM_NUMERO_COLLECTION_VIF'],
                'Auteur ancien' => $fetch['TX_NOM_FRANCAIS_AAN'],
                'Titre du volume' => $fetch['TX_PAGETITRE_TITRE_VIF'],
            ];
        }
        echo json_encode($arr_chantier, \JSON_UNESCAPED_UNICODE);
        break;

    case !$id && 'revision' === $status:
        // volumes réimprimés
        $sql = <<<SQL
            SELECT
                V.NM_NUMERO_COLLECTION_VIF,
                V.TX_PAGETITRE_TITRE_VIF,
                A.TX_NOM_FRANCAIS_AAN,
                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates
            FROM sc_t_volume AS V
            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                ON V.`PK_VOLUMEINFOS_VIF` = ot.`FK_OUT_REF_VOL`
            LEFT JOIN sc_t_tache AS T
                ON T.PK_TACHE_TCH = ot.FK_OUT_REF_TCH
            LEFT JOIN sc_t_oeuvre AS O
                ON O.`PK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
            LEFT JOIN sc_t_assoc_au_ovr AS ao
                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
            LEFT JOIN sc_t_auteur AS A
                ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
            LEFT JOIN sc_t_volume_reimpr AS VR
                ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
            WHERE (V.TX_ISBN_VIF IS NULL OR V.TX_ISBN_VIF='')
            AND T.LB_TACHE_TCH LIKE '%Révision%'
            GROUP BY V.NM_NUMERO_COLLECTION_VIF
            LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRs = DbExecRequete($sql, $oConnexion, true);
        $arr_reimpr = [];
        while ($fetch = DbEnregSuivantTab($oRs)) {
            $arr_reimpr[] = [
                'Num. Coll.' => $fetch['NM_NUMERO_COLLECTION_VIF'],
                'Auteur ancien' => $fetch['TX_NOM_FRANCAIS_AAN'],
                'Titre du volume' => $fetch['TX_PAGETITRE_TITRE_VIF'],
                'Dates de réimpression' => $fetch['dates'],
            ];
        }
        echo json_encode($arr_reimpr, \JSON_UNESCAPED_UNICODE);
        break;

    case !$id:
        // volumes list
        $sql = <<<SQL
        SELECT
            V.NM_NUMERO_COLLECTION_VIF,
            V.TX_PAGETITRE_TITRE_VIF,
            A.TX_NOM_FRANCAIS_AAN,
            VR.DT_SORTIE_VRP
        FROM sc_t_volume AS V
        LEFT JOIN sc_t_volume_reimpr AS VR
            ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
        LEFT JOIN sc_t_assoc_au_ovr AS ao
            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
        LEFT JOIN sc_t_auteur AS A
            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
        GROUP BY V.NM_NUMERO_COLLECTION_VIF
        ORDER BY V.NM_NUMERO_COLLECTION_VIF
        LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRs = DbExecRequete($sql, $oConnexion, true);
        $arr_reimpr = [];
        while ($fetch = DbEnregSuivantTab($oRs)) {
            $arr_reimpr[] = [
                'Num. Coll.' => $fetch['NM_NUMERO_COLLECTION_VIF'],
                'Auteur ancien' => $fetch['TX_NOM_FRANCAIS_AAN'],
                'Titre du volume' => $fetch['TX_PAGETITRE_TITRE_VIF'],
                'Date de réimpression' => $fetch['DT_SORTIE_VRP'],
            ];
        }
        echo json_encode($arr_reimpr, \JSON_UNESCAPED_UNICODE);
        break;

    case $id:
        if (!filter_var($id, FILTER_VALIDATE_INT)) {
            throw new \InvalidArgumentException("Invalid id format: '{$id}', expected an integer");
        }
        // volume
        $sql = <<<SQL
        SELECT V.*,
               ot.FK_OUT_REF_USR,
               ot.FK_OUT_REF_TCH,
               ot.LB_OUT_AVT_TCH,
               ot.DT_DEBUT_TCH,
               ot.DT_FIN_TCH,
               ot.TX_REMARQUES_OUT,
               ot.BL_MANUSCRIT_REMIS,
                A.TX_NOM_FRANCAIS_AAN,
                O.TX_TITRE_FRANCAIS_OVR,
                O.PK_OEUVRE_OVR
            FROM sc_t_volume AS V
            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                ON V.PK_VOLUMEINFOS_VIF = vo.FK_AVO_REF_VOL
            LEFT JOIN sc_t_oeuvre AS O
                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
            LEFT JOIN sc_t_assoc_au_ovr AS ao
                ON O.PK_OEUVRE_OVR =  ao.FK_OEUVRE_OVR
            LEFT JOIN sc_t_auteur AS A
                ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                ON V.PK_VOLUMEINFOS_VIF = ot.FK_OUT_REF_VOL
            WHERE V.PK_VOLUMEINFOS_VIF = {$id}
            GROUP BY V.PK_VOLUMEINFOS_VIF;
        SQL;
        $oRs = DbExecRequete($sql, $oConnexion, true);
        $arr_vol_id = [];
        while ($fetch = DbEnregSuivantTab($oRs)) {
            $arr_vol_id[] = [
                'Auteur ancien' => $fetch['TX_NOM_FRANCAIS_AAN'],
                'Oeuvre' => $fetch['TX_TITRE_FRANCAIS_OVR'],
                'ISBN' => $fetch['TX_ISBN_VIF'],
                'Num. Coll.' => $fetch['NM_NUMERO_COLLECTION_VIF'],
                'Titre du volume' => $fetch['TX_PAGETITRE_TITRE_VIF'],
                'Sous-titre (page de titre)' => $fetch['TX_PAGETITRE_SSTITRE_VIF'],
                'Complément (num. coll.)' => $fetch['TX_NUMCOLL_COMPLEMENT_VIF'],
                'Ancien num. coll.' => $fetch['TX_NUMCOLL_ANCIEN_VIF'],
                'Nouveau num. coll.' => $fetch['TX_NUMCOLL_NOUVEAU_VIF'],
                'Sous-collection' => $fetch['TX_SOUS_COLLECTION_VIF'],
                'Numéro (sous-collection)' => $fetch['TX_SOUSCOLL_NUMERO_VIF'],
                'Date prévue de parution' => $fetch['DT_SORTIE_VIF'],
                'Année de réimpression' => $fetch['DT_SORTIE_VRP'],
                'Nombre de réédition(s) et/ou réimpression(s)' => $fetch['NM_NOMBRE_REIMP_VIF'],
                'Précision(s) sur les rééditions/réimpressions' => $fetch['TX_NOMBRE_REIMP_VIF'],
                'Domaine' => $fetch['TX_DOMAINE_VIF'],
                'Subvention reçue' => $fetch['BL_SUBVENTION_VIF'],
                'Date de réception de la subvention' => $fetch['DT_SUBVENTION_ACCORDEE'],
                'Mécène' => $fetch['TX_MECENE_VIF'],
                'Mise en page' => $fetch['TX_IMPRIMEUR_VIF'],
                'Jaquette' => $fetch['BL_JAQUETTE_VIF'],
                'Droits demandés' => $fetch['BL_DEMANDE_DROITS'],
                'Date de demande des droits' => $fetch['DT_DEMANDE_DROITS'],
                'Demande des droits' => $fetch['TX_DEMANDE_DROITS'],
                'Devis' => $fetch['NM_MONTANT_DEVIS'],
                'Prestations complémentaires' => $fetch['NM_MONTANTS_PRESTATIONS'],
                'Date pour le bon à tirer' => $fetch['DT_BAT_VIF'],
                'Prix du volume en euros' => $fetch['NM_PRIX_VIF'],
                'Nombre de pages' => $fetch['NM_NOMBRE_PAGES_VIF'],
                'Tirage' => $fetch['NM_TIRAGE_VIF'],
                'État du stock' => $fetch['NM_ETAT_STOCK_VIF'],
                'Date du dernier état du stock' => $fetch['DT_ETAT_STOCK_VIF'],
                'Remarques' => $fetch['TX_REMARQUES_VIF'],
            ];
        }
        echo json_encode($arr_vol_id, \JSON_UNESCAPED_UNICODE);
        break;

    default:
        throw new \InvalidArgumentException('Resource not found');
}
