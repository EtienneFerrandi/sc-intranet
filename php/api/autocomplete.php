<?php

const PAGINATION_LIMIT = 50;

$oConnexion = DbConnection();

if (!preg_match('/^\/api\/autocomplete(.*)/', $_SERVER['REQUEST_URI'] ?? '', $matches)) {
    throw new \InvalidArgumentException('Resource not found');
}
[$entity, $query_string] = explode('?', trim($matches[1], '/'));
$parts = explode('&', $query_string);
$params = [];
foreach ($parts as $part) {
    [$name, $value] = explode('=', $part);
    $params[$name] = $value;
}

if (!empty($_SERVER['HTTP_REFERER']) && ($components = parse_url($_SERVER['HTTP_REFERER']))) {
    $parts = explode('&', $components['query']);
    $referer_params = [];
    foreach ($parts as $part) {
        [$name, $value] = explode('=', $part);
        $referer_params[$name] = $value;
    }
}

$q = $params['q'] ?? '';
$page = $params['page'] ?? 1;
if ($page < 1) {
    throw new \InvalidArgumentException('Invalid page range');
}
$limit = PAGINATION_LIMIT;
$offset = ($page - 1) * $limit;
switch ($entity) {
    case 'user':
        $sQueryUser = <<<SQL
        SELECT
            PK_UTILISATEUR_USR as id,
            CONCAT (IF(BL_DECEDE_USR = 1, '† ', ''), TX_PRENOM_USR, ' ', TX_NOM_USR) as text
        FROM sc_t_utilisateur
        WHERE ((TX_PRENOM_USR IS NOT NULL AND TX_PRENOM_USR != '') AND (TX_NOM_USR IS NOT NULL AND TX_NOM_USR != ''))
            AND (TX_PRENOM_USR LIKE '{$q}%' OR TX_NOM_USR LIKE '{$q}%' OR CONCAT(TX_PRENOM_USR, ' ', TX_NOM_USR) LIKE '{$q}%')
        ORDER BY TX_NOM_USR
        LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRecordsetUser = DbExecRequete($sQueryUser, $oConnexion);
        $users = DbEnregTab($oRecordsetUser);
        echo json_encode([
            'results' => $users,
            'pagination' => [
                'more' => PAGINATION_LIMIT === count($users)
            ],
        ], \JSON_UNESCAPED_UNICODE);
        break;

    case 'volume':
        $sQueryTask = $sQueryTask = empty($referer_params['id_oeuvre']) ? <<<SQL
        SELECT
            PK_VOLUMEINFOS_VIF as id,
            NM_NUMERO_COLLECTION_VIF as text
        FROM sc_t_volume
        WHERE ((NM_NUMERO_COLLECTION_VIF IS NOT NULL AND NM_NUMERO_COLLECTION_VIF != '') AND (TX_PAGETITRE_TITRE_VIF IS NOT NULL AND TX_PAGETITRE_TITRE_VIF != ''))
           AND (NM_NUMERO_COLLECTION_VIF LIKE '{$q}%' OR TX_PAGETITRE_TITRE_VIF LIKE '{$q}%')
        ORDER BY NM_NUMERO_COLLECTION_VIF
        LIMIT {$limit} OFFSET {$offset};
        SQL : <<<SQL
        SELECT
            PK_VOLUMEINFOS_VIF as id,
            NM_NUMERO_COLLECTION_VIF as text
        FROM sc_t_volume
        LEFT JOIN sc_t_assoc_vol_ovr ON sc_t_assoc_vol_ovr.FK_AVO_REF_VOL = sc_t_volume.PK_VOLUMEINFOS_VIF
        WHERE ((NM_NUMERO_COLLECTION_VIF IS NOT NULL AND NM_NUMERO_COLLECTION_VIF != '') AND (TX_PAGETITRE_TITRE_VIF IS NOT NULL AND TX_PAGETITRE_TITRE_VIF != ''))
            AND (NM_NUMERO_COLLECTION_VIF LIKE '{$q}%' OR TX_PAGETITRE_TITRE_VIF LIKE '{$q}%')
            AND sc_t_assoc_vol_ovr.FK_AVO_REF_OVR = {$referer_params['id_oeuvre']}
        ORDER BY NM_NUMERO_COLLECTION_VIF
        LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
        $volumes = DbEnregTab($oRecordsetTask);
        echo json_encode([
            'results' => $volumes,
            'pagination' => [
                'more' => PAGINATION_LIMIT === count($volumes)
            ],
        ], \JSON_UNESCAPED_UNICODE);
        break;

    case 'work':
        $sQueryTask = empty($referer_params['id_volume']) ? <<<SQL
        SELECT
            sc_t_oeuvre.PK_OEUVRE_OVR as id,
            CONCAT(
                GROUP_CONCAT(DISTINCT sc_t_auteur.TX_NOM_FRANCAIS_AAN SEPARATOR ', '),
                ' - ',
                sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
            ) AS text
        FROM sc_t_oeuvre
        LEFT JOIN sc_t_assoc_au_ovr ON sc_t_assoc_au_ovr.FK_OEUVRE_OVR = sc_t_oeuvre.PK_OEUVRE_OVR
        LEFT JOIN sc_t_auteur ON sc_t_assoc_au_ovr.FK_AUTEUR_ANCIEN_AAN = sc_t_auteur.PK_AUTEUR_ANCIEN_AAN
        WHERE ((sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR IS NOT NULL AND sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR != '') AND sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR LIKE '{$q}%')
            OR ((sc_t_auteur.TX_NOM_FRANCAIS_AAN IS NOT NULL AND sc_t_auteur.TX_NOM_FRANCAIS_AAN != '') AND sc_t_auteur.TX_NOM_FRANCAIS_AAN LIKE '{$q}%')
        GROUP BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
        ORDER BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
        LIMIT {$limit} OFFSET {$offset};
        SQL : <<<SQL
        SELECT
            sc_t_oeuvre.PK_OEUVRE_OVR as id,
            CONCAT(
                GROUP_CONCAT(DISTINCT sc_t_auteur.TX_NOM_FRANCAIS_AAN SEPARATOR ', '),
                ' - ',
                sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
            ) AS text
        FROM sc_t_oeuvre
        LEFT JOIN sc_t_assoc_vol_ovr ON sc_t_assoc_vol_ovr.FK_AVO_REF_OVR = sc_t_oeuvre.PK_OEUVRE_OVR
        LEFT JOIN sc_t_assoc_au_ovr ON sc_t_assoc_au_ovr.FK_OEUVRE_OVR = sc_t_oeuvre.PK_OEUVRE_OVR
        LEFT JOIN sc_t_auteur ON sc_t_assoc_au_ovr.FK_AUTEUR_ANCIEN_AAN = sc_t_auteur.PK_AUTEUR_ANCIEN_AAN
        WHERE (((sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR IS NOT NULL AND sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR != '') AND sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR LIKE '{$q}%')
            OR ((sc_t_auteur.TX_NOM_FRANCAIS_AAN IS NOT NULL AND sc_t_auteur.TX_NOM_FRANCAIS_AAN != '') AND sc_t_auteur.TX_NOM_FRANCAIS_AAN LIKE '{$q}%'))
            AND sc_t_assoc_vol_ovr.FK_AVO_REF_VOL = {$referer_params['id_volume']}
        GROUP BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
        ORDER BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
        LIMIT {$limit} OFFSET {$offset};
        SQL;
        $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
        $works = DbEnregTab($oRecordsetTask);
        echo json_encode([
            'results' => $works,
            'pagination' => [
                'more' => PAGINATION_LIMIT === count($works)
            ],
        ], \JSON_UNESCAPED_UNICODE);
        break;

    default:
        throw new \InvalidArgumentException('Resource not found');
}
