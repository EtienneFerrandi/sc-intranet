<?php

$bErreur = false;
$bEndOfSession = false;
$sEndOfSessionAuto = '';
$bAncienMotdepasseFaux = false;

$oConnexion = DbConnection();

if (!isset($_SESSION['PK_UTILISATEUR_USR']) && !empty($_POST)) {
    // if ($_POST != null)
    // {
    $password = trim($_POST['password']);
    $sRequete = <<<SQL
    SELECT u.PK_UTILISATEUR_USR,
        u.TX_PRENOM_USR,
        u.TX_NOM_USR,
        u.TX_PASSWORD_USR_HASH
    FROM sc_t_utilisateur as u
    WHERE u.TX_LOGIN_USR = '{$_POST['login']}'
SQL;
    $oRecordset = DbExecRequete($sRequete, $oConnexion);

    if (1 == DbNbreEnreg($oRecordset)) {
        $utilisateur = DbEnregSuivant($oRecordset);
        $password_hash = trim($utilisateur->TX_PASSWORD_USR_HASH);

        if(password_verify($password,$password_hash)){
            $PK_UTILISATEUR_USR = $utilisateur->PK_UTILISATEUR_USR;
            $TX_PRENOM_USR = $utilisateur->TX_PRENOM_USR;
            $TX_NOM_USR = $utilisateur->TX_NOM_USR;

            $_SESSION['PK_UTILISATEUR_USR'] = $PK_UTILISATEUR_USR;
            ('' == $TX_PRENOM_USR) ? ($_SESSION['TX_PRENOM_USR'] = '') : ($_SESSION['TX_PRENOM_USR'] = $TX_PRENOM_USR);
            ('' == $TX_NOM_USR) ? ($_SESSION['TX_NOM_USR'] = '') : ($_SESSION['TX_NOM_USR'] = $TX_NOM_USR);
        }
        else {
            $bErreur = true;
        }
    }
// }
} elseif (!empty($_GET)) {
    if (isset($_GET['endofsession']) && isset($_GET['auto'])) {
        if ('ok' == $_GET['endofsession']) {
            session_destroy();
            if (isset($PHPSESSID)) {
                session_start($PHPSESSID);
            } else {
                session_name('sources');
                session_start();
            }
            $bEndOfSession = true;
            $sEndOfSessionAuto = $_GET['auto'];	// "ok" : déconnexion automatique ; "no" : déconnexion volontaire
        }
    } elseif (isset($_GET['changer_motdepasse'])) {
        $sQuery = <<<SQL
            SELECT TX_PASSWORD_USR,
                   TX_PASSWORD_USR_HASH
            FROM sc_t_utilisateur
            WHERE PK_UTILISATEUR_USR={$_SESSION['PK_UTILISATEUR_USR']};
        SQL;

        $req = DbExecRequete($sQuery, $oConnexion);
        $chp = DbEnregSuivant($req);
        $current_pw = $chp->TX_PASSWORD_USR;
        $old_pw = $_POST['password_ancien'];
        $new_pw = $_POST['password_nouveau'];
        $password_hash = $chp->TX_PASSWORD_USR_HASH;

        if ($current_pw == $old_pw && password_verify($old_pw, $password_hash)) {
            $sQueryPw = <<<SQL
                UPDATE sc_t_utilisateur
                SET TX_PASSWORD_USR = '{$new_pw}'
                WHERE TX_PASSWORD_USR
                    LIKE '{$old_pw}';
            SQL;
            $oRsPw = DbExecRequete($sQueryPw, $oConnexion);

            $hash = password_hash($new_pw, PASSWORD_ARGON2ID, ["cost" => 13]);

            $sQueryHash = <<<SQL
                UPDATE sc_t_utilisateur
                SET TX_PASSWORD_USR_HASH = '{$hash}'
                WHERE TX_PASSWORD_USR = '{$new_pw}'
                AND PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
            SQL;
            $oRsHash = DbExecRequete($sQueryHash, $oConnexion);
        } else {
            $bAncienMotdepasseFaux = true;
        }
    }
}

DbClose($oConnexion);
