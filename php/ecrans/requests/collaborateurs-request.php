<?php

$id_oeuvre = '';
$type = '';

$id = '';

if (isset($_GET['id_oeuvre']) && isset($_GET['type'])) {	// 1er type d'accès possible : collaborateurs attachés à une oeuvre
    $id_oeuvre = $_GET['id_oeuvre'];
    $type = $_GET['type'];	// !!!!! Pour l'instant, ce paramètre n'est pas exploité !!!!!
} elseif (isset($_GET['id'])) {	// 2nd type d'accès possible : détail d'un collaborateur
    $id = $_GET['id'];
}
