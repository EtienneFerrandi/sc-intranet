<?php

if (!isset($_SESSION['PK_UTILISATEUR_USR'])) {
    header('Location: /?rubriqueid=intranet&pageid=connexion');
}
// else if (!strpos($client_navigateur, "Firefox") && !strpos($client_navigateur, "MSIE") && !strpos($client_navigateur, "Gecko"))
// {
//	header("Location: /?rubriqueid=intranet&pageid=connexion&navigateurincompatible=ok");
// }

$sectionid = '';
if (isset($_REQUEST['ddlsectionid'])) {
    $sectionid = $_REQUEST['ddlsectionid'];
} elseif (isset($_REQUEST['sectionid'])) {
    $sectionid = $_REQUEST['sectionid'];
}

$detail = '';
if (isset($_REQUEST['detail'])) {
    $detail = $_REQUEST['detail'];
}

$id = '';
if (isset($_GET['id'])) {	// Le GET prime sur le POST si un même paramètre est représenté 2 fois dans la page
    $id = $_GET['id'];
} elseif (isset($_POST['id'])) {
    $id = $_POST['id'];
}
