<?php $oConnexion = DbConnection(); ?>
<form name="frmGestionVolumesPreparation" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
    'rubriqueid' => 'intranet',
    'pageid' => 'volumes_preparation'
]))) ?>">
<div class="alert alert-info">
    Pour plus de précisions, écrire à <a href="mailto:webmaster.sc@mom.fr">webmaster.sc@mom.fr</a>.
</div>
<?php
$sQuery = <<<SQL
        SELECT V.*,
            A.TX_NOM_FRANCAIS_AAN,
            O.TX_URL_BIBLINDEX_OVR
        FROM sc_t_volume AS V
        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
            ON V.`PK_VOLUMEINFOS_VIF` = ot.`FK_OUT_REF_VOL`
        LEFT JOIN sc_t_tache AS T
            ON T.PK_TACHE_TCH = ot.FK_OUT_REF_TCH
        LEFT JOIN sc_t_oeuvre AS O
            ON O.`PK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
        LEFT JOIN sc_t_assoc_au_ovr AS ao
            ON ao.`FK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
        LEFT JOIN sc_t_auteur AS A
            ON ao.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
        WHERE (V.TX_ISBN_VIF IS NULL OR V.TX_ISBN_VIF = '')
        AND T.LB_TACHE_TCH LIKE '%Révision%'
        GROUP BY V.`PK_VOLUMEINFOS_VIF`, A.TX_NOM_FRANCAIS_AAN
        ORDER BY A.TX_NOM_FRANCAIS_AAN;
        SQL;

$oRecordset = DbExecRequete($sQuery, $oConnexion);
if (DbNbreEnreg($oRecordset) > 0) { ?>
    <table class="table table-sm table-header-fixed">
        <thead>
        <tr>
            <th colspan="2">
                <p align=left class="font-big" style="margin-left: 20px">
                    <b>Volumes en cours de révision à <i>Sources chrétiennes</i></b>
                </p>
            </th>
        </tr>
        <tr>
            <th class="title_gestion" width="25%">Auteur ancien</th>
            <th style="background-color: #C90063; color: #ffffff;text-align: center">Titre du volume</th>
        </tr>
        </thead>
        <tbody>
        <?php while ($prepasc = DbEnregSuivant($oRecordset)): ?>
        <?php
        $id_vol = $prepasc->PK_VOLUMEINFOS_VIF;
        $auteur = $prepasc->TX_NOM_FRANCAIS_AAN;
        $titre = $prepasc->TX_PAGETITRE_TITRE_VIF;
        $vol_sstitre = $prepasc->TX_PAGETITRE_SSTITRE_VIF;
        $lien_biblindex = !empty($pmt->TX_URL_BIBLINDEX_OVR) ? $pmt->TX_URL_BIBLINDEX_OVR : '';
        $vol_pk = $prepasc->PK_VOLUMEINFOS_VIF; ?>
        <tr style="font-size:13px;" >
            <td align="left" class="datagrid-lignesA" style="font-weight: bold; line-height: 14px;">
                <?php echo $auteur; ?>
            </td>
            <td align="left" class="datagrid-lignesA" style="font-weight: bold; line-height: 14px;">
                <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
                    <?php echo $titre; ?>
                <?php else: ?>
                    <?php
                    $url = sprintf('/?%s', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'gestion',
                        'sectionid' => 'volumes',
                        'detail' => 'ok',
                        'sourcerub' => 'intranet',
                        'sourcepg' => 'gestion',
                        'param1' => 'sectionid',
                        'vparam1' => 'volumes_preparation',
                        'signet' => $id_vol,
                        'id_volume' => $id_vol,
                    ]));
            ?>
                    <a href="<?php echo $url; ?>">
                        <?php echo $titre; ?>
                    </a>
                <?php endif; ?>
            </td>
        </tr>
        <?php endwhile ?>
        </tbody>
    </table>
    <?php
}

// Oeuvres en chantier :
// T_OEUVRE.BL_OEUVRE_INTERNE_OVR = 1
// T_ASSOC_VOL_OVR.PK_ASSOC_VOLOVR IS NULL
// T_ASSOC_OVR_USR_TCH.PK_ASSOC_OVRUSRTCH IS NOT NULL (au moins 1 collaborateur (sinon c'est une oeuvre libre))

$sQuery = <<<SQL
            SELECT
				A.`PK_AUTEUR_ANCIEN_AAN` AS 'author_id',
                A.TX_NOM_FRANCAIS_AAN AS 'author_name',
                O.PK_OEUVRE_OVR AS 'work_id',
                IF(O.TX_TITRE_LATIN_OVR IS NULL OR O.TX_TITRE_LATIN_OVR = '', O.TX_TITRE_FRANCAIS_OVR, O.TX_TITRE_LATIN_OVR) AS 'work_title',
                O.TX_CLAVIS_OVR AS 'clavis',
                O.TX_URL_BIBLINDEX_OVR AS 'biblindex_url'
            FROM sc_t_oeuvre AS O
            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
            LEFT JOIN sc_t_assoc_au_ovr AS au
                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
            LEFT JOIN sc_t_auteur AS A
                ON A.PK_AUTEUR_ANCIEN_AAN = au.FK_AUTEUR_ANCIEN_AAN
            LEFT JOIN sc_t_volume AS V
                ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
            INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
            WHERE ot.FK_OUT_REF_USR IS NOT NULL
                AND vo.FK_AVO_REF_VOL IS NULL
                AND O.`BL_OEUVRE_INTERNE_OVR`=1
            GROUP BY O.PK_OEUVRE_OVR, A.`TX_NOM_FRANCAIS_AAN`, O.TX_TITRE_LATIN_OVR
            ORDER BY A.`TX_NOM_FRANCAIS_AAN`, O.TX_TITRE_LATIN_OVR;
SQL;

$oRecordset = DbExecRequete($sQuery, $oConnexion);

if (DbNbreEnreg($oRecordset) > 0) { ?>
    <br>
    <table class="table table-sm table-header-fixed" style="width: 100%;">
        <thead>
        <tr>
            <th colspan="2">
                <p align=left class="font-big" style="margin-left: 20px">
                    <b>&#338;uvres en chantier pour la collection <i>Sources chrétiennes</i></b>
                </p>
                <p style="font-weight: normal; margin-left: 30px">
                    Sont ici listées les œuvres pour lesquelles un ou plusieurs collaborateurs sont déjà engagés.
                </p>
            </th>
        </tr>
        <tr>
            <th class="title_gestion" width="25%">Auteur ancien</th>
            <th style="background-color: #C90063; color: #ffffff;" class="text-center">Titre de l'&#339;uvre</th>
        </tr>
        </thead>
        <tbody>
        <?php $previous_author_id = null; ?>
        <?php while ($pmt = DbEnregSuivantTab($oRecordset)): ?>
        <?php
        $author_id = $pmt['author_id'];
        $work_url = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'oeuvres',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'volumes_preparation',
            'signet' => 'ovr'.$pmt['work_id'],
            'id_oeuvre' => $pmt['work_id'],
        ]));

        $lien_biblindex = !empty($pmt['biblindex_url']) ? $pmt['biblindex_url'] : '';
        $authors = $pmt['author_name'];
        ?>
        <?php if($author_id !== $previous_author_id):?>
            <?php if($previous_author_id !== null): ?>
                </ul>
            </td>
        </tr>
            <?php endif; ?>
        <tr class="datagrid-lignesA" style="line-height: 14px;">
            <td align="left" style="overflow-wrap: break-word; white-space: pre-wrap; font-weight: bold; width: 20%; font-size:13px;"><?php echo $authors; ?></td>
            <td align="left" style="overflow-wrap: break-word; white-space: normal; font-weight: bold; font-size:13px;">
                <ul style="column-count: 3">
        <?php endif; ?>
                    <li>
                        <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
                            <?php echo $pmt['work_title']; ?>
                        <?php else: ?>
                            <a href="<?php echo $work_url; ?>" class="work_title">
                                <?php echo $pmt['work_title']; ?>
                            </a>
                        <?php endif; ?>
                    </li>
        <?php $previous_author_id = $pmt['author_id']; ?>
        <?php endwhile; ?>
                </ul>
            </td>
            </tr>
        </tbody>
    </table>
<?php }

/**
 * Œuvres en chantier nécessitant une ou plusieurs collaborations supplémentaires :
 * Les critères sont les suivants :
 * BL_OEUVRE_INTERNE_OVR est à 1
 * ET il y a au moins un collaborateur engagé sur une tâche associée
 * ET le booléen “Recherche de collaborations complémentaires” est à 1
 */
$sQuery = <<<SQL
            SELECT
				A.`PK_AUTEUR_ANCIEN_AAN` AS 'author_id',
                A.TX_NOM_FRANCAIS_AAN AS 'author_name',
                O.PK_OEUVRE_OVR AS 'work_id',
                IF(O.TX_TITRE_LATIN_OVR IS NULL OR O.TX_TITRE_LATIN_OVR = '', O.TX_TITRE_FRANCAIS_OVR, O.TX_TITRE_LATIN_OVR) AS 'work_title',
                O.TX_CLAVIS_OVR AS 'clavis',
                O.TX_URL_BIBLINDEX_OVR AS 'biblindex_url'
            FROM sc_t_oeuvre AS O
            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
            LEFT JOIN sc_t_assoc_au_ovr AS au
                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
            LEFT JOIN sc_t_auteur AS A
                ON A.PK_AUTEUR_ANCIEN_AAN = au.FK_AUTEUR_ANCIEN_AAN
            LEFT JOIN sc_t_volume AS V
                ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
            INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
            WHERE ot.FK_OUT_REF_USR IS NOT NULL
                AND O.`BL_OEUVRE_INTERNE_OVR`=1
                AND O.`BL_COLLAB_SUPPL_OVR`=1
            GROUP BY O.PK_OEUVRE_OVR, A.`TX_NOM_FRANCAIS_AAN`, O.TX_TITRE_LATIN_OVR
            ORDER BY A.`TX_NOM_FRANCAIS_AAN`, O.TX_TITRE_LATIN_OVR;
SQL;

$oRecordset = DbExecRequete($sQuery, $oConnexion);

if (DbNbreEnreg($oRecordset) > 0) { ?>
    <br>
    <table class="table table-sm table-header-fixed" style="width: 100%;">
        <thead>
        <tr>
            <th colspan="2">
                <p align=left class="font-big" style="margin-left: 20px">
                    <b>&#338;uvres en chantier nécessitant une ou plusieurs collaborations supplémentaires
                </p>
            </th>
        </tr>
        <tr>
            <th class="title_gestion" width="25%">Auteur ancien</th>
            <th style="background-color: #C90063; color: #ffffff;" class="text-center">Titre de l'&#339;uvre</th>
        </tr>
        </thead>
        <tbody>
        <?php $previous_author_id = null; ?>
        <?php while ($pmt = DbEnregSuivantTab($oRecordset)): ?>
        <?php
        $author_id = $pmt['author_id'];
        $work_url = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'oeuvres',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'volumes_preparation',
            'signet' => 'ovr'.$pmt['work_id'],
            'id_oeuvre' => $pmt['work_id'],
        ]));

        $lien_biblindex = !empty($pmt['biblindex_url']) ? $pmt['biblindex_url'] : '';
        $authors = $pmt['author_name'];
        ?>
        <?php if($author_id !== $previous_author_id):?>
            <?php if($previous_author_id !== null): ?>
                </ul>
            </td>
        </tr>
            <?php endif; ?>
        <tr class="datagrid-lignesA" style="line-height: 14px;">
            <td align="left" style="overflow-wrap: break-word; white-space: pre-wrap; font-weight: bold; width: 20%; font-size:13px;"><?php echo $authors; ?></td>
            <td align="left" style="overflow-wrap: break-word; white-space: normal; font-weight: bold; font-size:13px;">
                <ul style="column-count: 3">
        <?php endif; ?>
                    <li>
                        <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
                            <?php echo $pmt['work_title']; ?>
                        <?php else: ?>
                            <a href="<?php echo $work_url; ?>" class="work_title">
                                <?php echo $pmt['work_title']; ?>
                            </a>
                        <?php endif; ?>
                    </li>
        <?php $previous_author_id = $pmt['author_id']; ?>
        <?php endwhile; ?>
                </ul>
            </td>
        </tr>
        </tbody>
    </table>
<?php }

// ==================== Oeuvres envisageables ==============================
$sQuery = <<<SQL
SELECT
    A.`TX_NOM_FRANCAIS_AAN`,
    O.PK_OEUVRE_OVR,
    GROUP_CONCAT(DISTINCT CONCAT('<a href="?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=volumes_preparation&signet=ovr',O.PK_OEUVRE_OVR, '&id_oeuvre=', O.PK_OEUVRE_OVR,'">', O.TX_TITRE_LATIN_OVR, '</a>') ORDER BY O.TX_TITRE_LATIN_OVR SEPARATOR '</li><li>') AS la_works_links,
    GROUP_CONCAT(DISTINCT CONCAT('<a href="?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=volumes_preparation&signet=ovr',O.PK_OEUVRE_OVR, '&id_oeuvre=', O.PK_OEUVRE_OVR,'">', O.`TX_TITRE_FRANCAIS_OVR`, '</a>') ORDER BY O.TX_TITRE_FRANCAIS_OVR SEPARATOR '</li><li>') AS fr_works_links,
    O.TX_CLAVIS_OVR,
    O.TX_URL_BIBLINDEX_OVR
FROM sc_t_oeuvre AS O
LEFT JOIN sc_t_assoc_au_ovr AS ao
    ON O.`PK_OEUVRE_OVR` = ao.FK_OEUVRE_OVR
LEFT JOIN sc_t_auteur AS A
    ON A.`PK_AUTEUR_ANCIEN_AAN` = ao.`FK_AUTEUR_ANCIEN_AAN`
LEFT JOIN sc_t_assoc_vol_ovr AS vo
    ON O.`PK_OEUVRE_OVR` = vo.`FK_AVO_REF_OVR`
LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
    ON O.`PK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
WHERE O.`BL_OEUVRE_INTERNE_OVR`=1
AND O.PK_OEUVRE_OVR NOT IN (SELECT FK_OUT_REF_OVR FROM sc_t_assoc_ovr_usr_tch)
GROUP BY A.`TX_NOM_FRANCAIS_AAN`
ORDER BY A.TX_NOM_FRANCAIS_AAN;
SQL;

$oRecordset = DbExecRequete($sQuery, $oConnexion);
?>
<?php if (DbNbreEnreg($oRecordset) > 0): ?>
    <br>
    <table class="table table-sm table-header-fixed">
        <thead>
        <tr>
            <th colspan="2">
                <p align=left class="font-big" style="margin-left: 20px">
                    <b>&#338;uvres envisageables</b>
                </p>
                <span style="font-weight: normal; margin-left: 30px">
                    Cette liste ne se veut pas exhaustive.
                </span>
            </th>
        </tr>
        <tr>
            <th class="title_gestion" width="25%">Auteur ancien</th>
            <th style="background-color: #C90063; color: #ffffff;" class="text-center">Titre de l'&#339;uvre</th>
        </tr>
        </thead>
        <tbody>
        <?php
    while ($chantiers = DbEnregSuivantTab($oRecordset)) {
        $works = $chantiers['la_works_links'] ?: $chantiers['fr_works_links'];
        $lien_biblindex = !empty($pmt['TX_URL_BIBLINDEX_OVR']) ? $pmt['TX_URL_BIBLINDEX_OVR'] : '';
        $authors = $chantiers['TX_NOM_FRANCAIS_AAN'];
        ?>
            <tr class="datagrid-lignesA" style="line-height: 14px;">
                <td align="left" style="overflow-wrap: break-word; white-space: -moz-pre-wrap; font-weight: bold; font-size:13px;"><?php echo $authors; ?></td>
                <td align="left" style="overflow-wrap: break-word; white-space: -moz-pre-wrap; font-weight: bold; font-size:13px;">
                    <ul style="column-count: 3">
                        <li>
                            <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
                                <?php echo strip_tags($works, '<li>'); ?>
                            <?php else: ?>
                                <?php echo $works; ?>
                            <?php endif; ?>
                        </li>
                    </ul>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php endif; ?>

<?php if (isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
<?php
// ==================== Oeuvres non envisagées ==============================
$sQuery = <<<SQL
    SELECT
    A.`TX_NOM_FRANCAIS_AAN`,
    O.PK_OEUVRE_OVR,
    GROUP_CONCAT(DISTINCT CONCAT('<a href="?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=volumes_preparation&signet=ovr',O.PK_OEUVRE_OVR, '&id_oeuvre=', O.PK_OEUVRE_OVR,'">', O.TX_TITRE_LATIN_OVR, '</a>') ORDER BY O.TX_TITRE_LATIN_OVR SEPARATOR '</li><li>') AS la_works_links,
GROUP_CONCAT(DISTINCT CONCAT('<a href="?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=volumes_preparation&signet=ovr',O.PK_OEUVRE_OVR, '&id_oeuvre=', O.PK_OEUVRE_OVR,'">', O.`TX_TITRE_FRANCAIS_OVR`, '</a>') ORDER BY O.TX_TITRE_FRANCAIS_OVR SEPARATOR '</li><li>') AS fr_works_links,
O.TX_CLAVIS_OVR,
O.TX_URL_BIBLINDEX_OVR
FROM sc_t_oeuvre AS O
LEFT JOIN sc_t_assoc_au_ovr AS ao
ON O.`PK_OEUVRE_OVR` = ao.FK_OEUVRE_OVR
LEFT JOIN sc_t_auteur AS A
ON A.`PK_AUTEUR_ANCIEN_AAN` = ao.`FK_AUTEUR_ANCIEN_AAN`
LEFT JOIN sc_t_assoc_vol_ovr AS vo
ON O.`PK_OEUVRE_OVR` = vo.`FK_AVO_REF_OVR`
LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
ON O.`PK_OEUVRE_OVR` = ot.`FK_OUT_REF_OVR`
WHERE O.`BL_OEUVRE_INTERNE_OVR`=0
GROUP BY A.`TX_NOM_FRANCAIS_AAN`
ORDER BY A.TX_NOM_FRANCAIS_AAN;
SQL;

$oRecordset = DbExecRequete($sQuery, $oConnexion);
?>
<?php if (DbNbreEnreg($oRecordset) > 0): ?>
    <br>
    <table class="table table-sm table-header-fixed">
        <thead>
        <tr>
            <th colspan="2">
                <p align=left class="font-big" style="margin-left: 20px">
                    <b>&#338;uvres non envisagées</font></b>
                </p>
            </th>
        </tr>
        <tr>
            <th class="title_gestion" width="25%">Auteur ancien</th>
            <th style="background-color: #C90063; color: #ffffff;" class="text-center">Titre de l'&#339;uvre</th>
        </tr>
        </thead>
        <tbody>
        <?php
        while ($chantiers = DbEnregSuivantTab($oRecordset)) {
            $works = $chantiers['la_works_links'] ?: $chantiers['fr_works_links'];
            $lien_biblindex = !empty($pmt['TX_URL_BIBLINDEX_OVR']) ? $pmt['TX_URL_BIBLINDEX_OVR'] : '';
            $authors = $chantiers['TX_NOM_FRANCAIS_AAN'];
            ?>
            <tr class="datagrid-lignesA" style="line-height: 14px;">
                <td align="left" style="overflow-wrap: break-word; white-space: -moz-pre-wrap; font-weight: bold; font-size:13px;"><?php echo $authors; ?></td>
                <td align="left" style="overflow-wrap: break-word; white-space: -moz-pre-wrap; font-weight: bold; font-size:13px;">
                    <ul style="column-count: 3">
                        <li>
                            <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
                                <?php echo strip_tags($works, '<li>'); ?>
                            <?php else: ?>
                                <?php echo $works; ?>
                            <?php endif; ?>
                        </li>
                    </ul>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php endif; ?>
<?php endif; ?>

</form>
<?php DbClose($oConnexion);
