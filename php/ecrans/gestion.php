<?php
// ..
?>
    <form name="frmGestion" method="post" action="/?rubriqueid=intranet&pageid=gestion" enctype="multipart/form-data" accept-charset="UTF-8">
        <script language="javascript" src="js/gestion.js"></script>
        <table border="0" cellpadding="4" cellspacing="4" width="100%">
            <?php

            // extrait de la page gestion-request.php
            $detail = '';
if (isset($_REQUEST['detail'])) {
    $detail = $_REQUEST['detail'];
}

if ('ok' != $detail && 'ok2' != $detail && 'ok3' != $detail && 'ok41' != $detail && 'ok42' != $detail && 'ok5' != $detail && 'ok6' != $detail) {
    ?>
                <!-- ==================== Menu déroulant des modules / DEBUT ==================== -->

                <tr><td align="center">
                            <table align="center" border="2" class="table">
                                <tr>
                                    <td width= "30%" align="center">
                                            <a href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=autanciens" class="std">
                                                <font class="font-big">Auteurs anciens</font></a>
                                    </td>
                                    <td><font class="font-large">éléments biographiques et bibliographiques, rattachement à un pôle</font></td>
                                </tr>

                                <tr>
                                    <td align="center">
                                            <a href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=oeuvres" class="std">
                                                <font class="font-big">&#140;uvres</font></a>
                                    </td>
                                    <td><font class="font-large"><b>chantiers des oeuvres projetées</b>, éditions antérieures, résumés, <b>informations confidentielles sur un dossier en cours non encore relié à un volume</b>; chantiers(<img src="img/chantier.gif" BORDER="0">)</font></td>
                                </tr>

                                <tr>
                                    <td align="center">
                                            <a href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=utilisateurs" class="std">
                                                <font class="font-big">Utilisateurs</font></a>
                                    </td>
                                    <td><font class="font-large">collaborateurs de la collection, membres de l'équipe (fiches personnelles), membres de l'association</font></td>
                                </tr>

                                <tr>
                                    <td align="center">
                                            <a href="/?rubriqueid=intranet&pageid=gestion&ddlsectionid=volumes" class="std">
                                                <font class="font-big">Volumes</font></a>
                                            <br>
                                    </td>
                                    <td><font class="font-large"><b>fiches de circulation des volumes en préparation</b> ou parus (n° du volume) ; chantiers des volumes (<img src="img/chantier.gif" BORDER="0">) ;<br> errata des volumes parus (<img src="img/errata.gif"  BORDER="0">) ; comptes rendus (<img src="img/cr.gif"  BORDER="0">) ; réimpressions (<img src="img/reimp.gif" BORDER="0">)</font></td>
                                </tr>
                            </table>
                    </td>
                </tr>
                <!-- ==================== Menu déroulant des modules / FIN ==================== -->
                <?php
}

if (isset($_REQUEST['err'])) {
    ?>
                <tr>
                    <td align="center">
                        <font class="font-normal"><font class="font-couleur-erreur"><b>Une erreur d'exécution est survenue. L'administrateur du site a été prévenu.</b></font></font>
                    </td>
                </tr>
                <?php
}

// echo "***".$sectionid.' '.$detail;

if ('autanciens' == $sectionid) {
    if ('ok' == $detail) {
        include 'gestion/detail/detail-autanciens.php';
    } else {
        include 'gestion/gestion-autanciens.php';
    }
} elseif ('oeuvres' == $sectionid) {
    if ('ok' == $detail) {
        include 'gestion/detail/detail-oeuvres.php';
    } elseif ('ok2' == $detail) {
        include 'gestion/detail/detail2-oeuvres.php';
    } elseif ('ok3' == $detail) {
        include 'gestion/detail/detail3-oeuvres.php';
    } else {
        //		echo "###";
        include 'gestion/gestion-oeuvres.php';
    }
} elseif ('utilisateurs' == $sectionid) {
    if ('ok' == $detail) {
        include 'gestion/detail/detail-utilisateurs.php';
    } else {
        include 'gestion/gestion-utilisateurs.php';
    }
} elseif ('volumes' == $sectionid) {
    if ('ok' == $detail) {
        include 'gestion/detail/detail-volumes.php';
    } elseif ('ok2' == $detail) {
        include 'gestion/detail/detail2-volumes.php';
    }
    // elseif ($detail == "ok21")
    // {
    //	include('gestion/detail/detail21-volumes.php');
    // }
    elseif ('ok3' == $detail) {
        include 'gestion/detail/detail3-volumes.php';
    } elseif ('ok41' == $detail) {
        include 'gestion/detail/detail41-volumes.php';
    } elseif ('ok42' == $detail) {
        include 'gestion/detail/detail42-volumes.php';
    } elseif ('ok5' == $detail) {
        include 'gestion/detail/detail5-volumes.php';
    } elseif ('ok6' == $detail) {
        include 'gestion/detail/detail6-volumes.php';
    } else {
        include 'gestion/gestion-volumes.php';
    }
}

?>
        </table>
    </form>
<?php
