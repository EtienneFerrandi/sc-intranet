<?php
$lettre = 'az';

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

if(isset($_REQUEST['vparam3']) && empty($_REQUEST['lettre'])){
    $lettre = $_REQUEST['vparam3'];
}
else if (isset($_REQUEST['lettre'])) {
    $lettre = $_REQUEST['lettre'];
}

$oConnexion = DbConnection();

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}
if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%auteurs anciens%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC' || $usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%auteurs anciens%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%auteurs anciens%'
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (isset($_REQUEST['action']) && !empty($id)) {
    begin($oConnexion);

    switch ($_REQUEST['action']) {
        case 'suppr':
            if ($rights_admin) {
                // delete association author work
                $sReq1 = <<<SQL
                        SELECT FK_AUTEUR_ANCIEN_AAN
                        FROM sc_t_assoc_au_ovr
                        WHERE FK_AUTEUR_ANCIEN_AAN = {$id};
                        SQL;
                $result1 = DbExecRequete($sReq1, $oConnexion);
                if (0 != $result1->num_rows) {
                    $oRecordset1 = DbExecRequete($sReq1, $oConnexion);
                }
                $sQuery2 = <<<SQL
                        DELETE FROM sc_t_assoc_au_ovr
                        WHERE FK_AUTEUR_ANCIEN_AAN = {$id};
                        SQL;

                // insert trace of the deletion
                $req = <<<SQL
                SELECT TX_NOM_FRANCAIS_AAN
                FROM sc_t_auteur
                WHERE PK_AUTEUR_ANCIEN_AAN = {$id};
                SQL;
                $oRecordset = DbExecRequete($req, $oConnexion);
                while ($fetch = DbEnregSuivant($oRecordset)) {
                    $au_name = $fetch->TX_NOM_FRANCAIS_AAN;
                }

                $str_auteur = addslashes("l'auteur");
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('AUTEUR_ANCIEN',
                       {$id},
                       'Suppression de {$str_auteur} {$au_name} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-D',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);

                // delete author
                $sQuery = <<<SQL
                        DELETE
                        FROM sc_t_auteur
                        WHERE PK_AUTEUR_ANCIEN_AAN={$id};
                        SQL;
                $oRecordset = DbExecRequete($sQuery, $oConnexion);

                addNotice('success', 'Auteur correctement supprimé !');

                redirect(sprintf('/?%s', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => 'autanciens',
                    'ddlsectionid' => 'autanciens',
                ])));
            }
            break;
    }
    commit($oConnexion);
}
switch ($lettre){
    case 'az' :
        $sQuery = <<<SQL
        SELECT
            A.PK_AUTEUR_ANCIEN_AAN,
            A.TX_NOM_FRANCAIS_AAN,
            A.TX_NOM_CLAVIS_AAN
        FROM sc_t_auteur AS A
        ORDER BY A.TX_NOM_FRANCAIS_AAN
        SQL;
        break;
        default :
        $sQuery = <<<SQL
        SELECT
            A.PK_AUTEUR_ANCIEN_AAN,
            A.TX_NOM_FRANCAIS_AAN,
            A.TX_NOM_CLAVIS_AAN
        FROM sc_t_auteur AS A
        WHERE TX_NOM_FRANCAIS_AAN LIKE '{$lettre}%'
        ORDER BY A.TX_NOM_FRANCAIS_AAN
        SQL;
        break;
}


$oRecordset = DbExecRequete($sQuery, $oConnexion);
$iNbreEnreg = DbNbreEnreg($oRecordset);

?>
    <table>
        <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_trace
        WHERE
            TX_DOMAINE_TRC = 'AUTEUR_ANCIEN'
        ORDER BY DT_TRACE_TRC DESC LIMIT 3;
    SQL;
        $oRs = DbExecRequete($sQuery, $oConnexion);
        while ($trace = DbEnregSuivant($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
            </td>
        </tr>
        HTML;
        }
        if (0 == DbNbreEnreg($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                Aucun historique des dernières modifications apportées à un auteur ancien
            </td>
        </tr>
        HTML;
        }
        ?>
    </table>
    <form name="frmGestionAutAnciens" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => 'autanciens',
        'ddlsectionid' => 'autanciens',
        'param3' => 'lettre',
        'vparam3' => $lettre
    ]))) ?>">
        <script language="javascript" src="js/gestion.js"></script>
        <table border="0" width="100%" cellspacing="2" cellpadding="2" class="table table-sm">
        <thead>
            <tr>
                <th class="title_gestion">
                    LISTE DES AUTEURS ANCIENS
                    <acronym title="ajouter un auteur ancien">
                        <?php
                        $url_add = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'autanciens',
                            'detail' => 'ok',
                            'sourcerub' => 'intranet',
                            'sourcepg' => 'gestion',
                            'param1' => 'sectionid',
                            'vparam1' => 'autanciens',
                            'param3' => 'lettre',
                            'vparam3' => $lettre
                        ]));
                        ?>
                        <input type="button" value="&nbsp;+&nbsp;" class="btn"
                               onclick="javascript:location.href='<?php echo $url_add; ?>'">
                    </acronym>
                </th>
                <th style="background-color: #C90063; text-align: center">
                    <select size="1" name="lettre" onchange="javascript:document.forms['frmGestionAutAnciens'].submit();">
                        <option value="az"<?php if ('az' == $lettre ) {
                            echo ' selected';
                        } ?>>A-Z</option>
                        <option value="a"<?php if ('a' == $lettre ) {
                            echo ' selected';
                        } ?>>A</option>
                        <option value="b"<?php if ('b' == $lettre ) {
                            echo ' selected';
                        } ?>>B</option>
                        <option value="c"<?php if ('c' == $lettre ) {
                            echo ' selected';
                        } ?>>C</option>
                        <option value="d"<?php if ('d' == $lettre ) {
                            echo ' selected';
                        } ?>>D</option>
                        <option value="e"<?php if ('e' == $lettre ) {
                            echo ' selected';
                        } ?>>E</option>
                        <option value="f"<?php if ('f' == $lettre ) {
                            echo ' selected';
                        } ?>>F</option>
                        <option value="g"<?php if ('g' == $lettre ) {
                            echo ' selected';
                        } ?>>G</option>
                        <option value="h"<?php if ('h' == $lettre ) {
                            echo ' selected';
                        } ?>>H</option>
                        <option value="i"<?php if ('i' == $lettre ) {
                            echo ' selected';
                        } ?>>I</option>
                        <option value="j"<?php if ('j' == $lettre ) {
                            echo ' selected';
                        } ?>>J</option>
                        <option value="k"<?php if ('k' == $lettre ) {
                            echo ' selected';
                        } ?>>K</option>
                        <option value="l"<?php if ('l' == $lettre ) {
                            echo ' selected';
                        } ?>>L</option>
                        <option value="m"<?php if ('m' == $lettre ) {
                            echo ' selected';
                        } ?>>M</option>
                        <option value="n"<?php if ('n' == $lettre ) {
                            echo ' selected';
                        } ?>>N</option>
                        <option value="o"<?php if ('o' == $lettre ) {
                            echo ' selected';
                        } ?>>O</option>
                        <option value="p"<?php if ('p' == $lettre ) {
                            echo ' selected';
                        } ?>>P</option>
                        <option value="q"<?php if ('q' == $lettre ) {
                            echo ' selected';
                        } ?>>Q</option>
                        <option value="r"<?php if ('r' == $lettre ) {
                            echo ' selected';
                        } ?>>R</option>
                        <option value="s"<?php if ('s' == $lettre ) {
                            echo ' selected';
                        } ?>>S</option>
                        <option value="t"<?php if ('t' == $lettre ) {
                            echo ' selected';
                        } ?>>T</option>
                        <option value="u"<?php if ('u' == $lettre ) {
                            echo ' selected';
                        } ?>>U</option>
                        <option value="v"<?php if ('v' == $lettre ) {
                            echo ' selected';
                        } ?>>V</option>
                        <option value="w"<?php if ('w' == $lettre ) {
                            echo ' selected';
                        } ?>>W</option>
                        <option value="x"<?php if ('x' == $lettre ) {
                            echo ' selected';
                        } ?>>X</option>
                        <option value="y"<?php if ('y' == $lettre ) {
                            echo ' selected';
                        } ?>>Y</option>
                        <option value="z"<?php if ('z' == $lettre ) {
                            echo ' selected';
                        } ?>>Z</option>
                    </select>
                </th>
                <th class="enregistrements">
                    <b><?php echo $iNbreEnreg.' enregistrement(s)'; ?></b>&nbsp;&nbsp;&nbsp;<br>
                </th>
            </tr>
            <tr>
                <th class="text-center" width="30%" scope="col" class="datagrid-ligne1">Nom français</th>
                <th class="text-center" width="30%" scope="col" class="datagrid-ligne1"
                    <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>Nom clavis</th>
                <th class="text-center" width="30%" scope="col" class="datagrid-ligne1"
                    <?php if (!$rights_admin) { ?> style="display: none"<?php } ?>>
                    Action
                </th>
            </tr>
        </thead>
            <tbody>
<?php

$ligneType = 'B';
while ($autanc = DbEnregSuivant($oRecordset)) {

    $aan_pk = $autanc->PK_AUTEUR_ANCIEN_AAN;

    ('' == $autanc->TX_NOM_FRANCAIS_AAN) ? ($nom_francais = '-') : ($nom_francais = $autanc->TX_NOM_FRANCAIS_AAN);
    ('' == $autanc->TX_NOM_CLAVIS_AAN) ? ($nom_clavis = '-') : ($nom_clavis = $autanc->TX_NOM_CLAVIS_AAN);

    ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A');
    ?>
            <tr style="line-height: 15px;">
                <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                    <a href="/?rubriqueid=intranet&pageid=gestion&sectionid=autanciens&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=autanciens&param3=lettre&vparam3=<?php echo $lettre; ?>&signet=<?php echo 'autanc'.$aan_pk; ?>&id_autancien=<?php echo $aan_pk; ?>"
                       class="dtg-lignes"><?php echo $nom_francais; ?>
                    </a>
                </td>
                <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                    <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                    <span class="clavis" name="<?php echo 'autanc'.$aan_pk; ?>"><?php echo $nom_clavis; ?></span>
                </td>
                <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                    <?php if (!$rights_admin) { ?> style="display: none;"<?php } ?>>
                        <a href="javascript:if (confirm('Etes-vous sûr de vouloir supprimer cet auteur ancien (et les &#339;uvres associées) ? '))
    {location.href='/?rubriqueid=intranet&pageid=gestion&sectionid=autanciens&ddlsectionid=autanciens&action=suppr&id=<?php echo $aan_pk; ?>';}"
                           class="dtg-lignes">Suppr.
                        </a>
                </td>
            </tr>

<?php
}

DbClose($oConnexion);
?>
            </tbody>
</table>
</form>
</td>
</tr>