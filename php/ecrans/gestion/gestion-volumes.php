<?php
$oConnexion = DbConnection();

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

/* get the rights of the logged user */
$usr_pfl = null;
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
            SELECT
                FK_APT_REF_PFL
            FROM sc_t_utilisateur
            WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
        SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}
if($usr_pfl === 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%Volumes%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl === 'membre SC' || $usr_pfl === 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Volumes%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl === 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Volumes%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

$vol_population = 'T';	// <T>ous les volumes (<P>arus et en <C>hantier)
$vol_oeuvre = '';	// Toutes les oeuvres
$vol_autancien = '';	// Tous les auteurs anciens
$vol_utilisateur = '';	// Tous les collaborateurs
$vol_numero = '';	// Tous les numéros de volume

$ordre = 'DESC';
$tri = 'NM_NUMERO_COLLECTION_VIF';

if (isset($_REQUEST['vol_numero'])) {
    $vol_numero = $_REQUEST['vol_numero'];
    $vol_population = 'T';
}elseif (isset($_REQUEST['vparam10']) && !array_key_exists('vol_numero', $_REQUEST)){
    $vol_numero = $_REQUEST['vparam10'];
    $vol_population = 'T';
}else{
    $vol_numero = '';
}

if (empty($vol_numero)) {
    if (isset($_REQUEST['vol_population']) && !empty($_REQUEST['vol_population'])) {
        $vol_population = $_REQUEST['vol_population'];
    }
    elseif (isset($_REQUEST['vparam2']) && !array_key_exists('vol_population', $_REQUEST)){
        $vol_population = $_REQUEST['vparam2'];
    }else{
        $vol_population = 'T';
    }
    if (isset($_REQUEST['vol_oeuvre']) && !empty($_REQUEST['vol_oeuvre'])) {
        $vol_oeuvre = $_REQUEST['vol_oeuvre'];
    }
    elseif (isset($_REQUEST['vparam3']) && !array_key_exists('vol_oeuvre', $_REQUEST)){
        $vol_oeuvre = $_REQUEST['vparam3'];
    }else{
        $vol_oeuvre = '';
    }
    if (isset($_REQUEST['vol_autancien']) && !empty($_REQUEST['vol_autancien'])) {
        $vol_autancien = $_REQUEST['vol_autancien'];
    }
    elseif (isset($_REQUEST['vparam4']) && !array_key_exists('vol_autancien', $_REQUEST)){
        $vol_autancien = $_REQUEST['vparam4'];
    }else{
        $vol_autancien = '';
    }
    if (isset($_REQUEST['vol_utilisateur']) && !empty($_REQUEST['vol_utilisateur'])) {
        $vol_utilisateur = $_REQUEST['vol_utilisateur'];
    }
    elseif (isset($_REQUEST['vparam5']) && !array_key_exists('vol_utilisateur', $_REQUEST)){
        $vol_utilisateur = $_REQUEST['vparam5'];
    }
    else{
        $vol_utilisateur = '';
    }
    if (isset($_REQUEST['ordre']) && !empty($_REQUEST['ordre'])) {
        $ordre = $_REQUEST['ordre'];
    }
    if (isset($_REQUEST['tri']) && !empty($_REQUEST['tri'])) {
        $tri = $_REQUEST['tri'];
    }
}
if ('DESC' == $ordre) {
    $nouvordre = 'ASC';
} else {
    $nouvordre = 'DESC';
}

if (isset($_REQUEST['action']) && !empty($id)) {
    begin($oConnexion);

    switch ($_REQUEST['action']) {
        case 'suppr':
            if ($rights_admin) {
                $sReq1 = <<<SQL
                        SELECT FK_AVO_REF_OVR
                        FROM sc_t_assoc_vol_ovr
                        WHERE FK_AVO_REF_VOL = {$id};
                        SQL;
                $sReq2 = <<<SQL
                        SELECT FK_OUT_REF_OVR
                        FROM sc_t_assoc_ovr_usr_tch
                        WHERE FK_OUT_REF_VOL = {$id};
                        SQL;
                $result1 = DbExecRequete($sReq1, $oConnexion);
                $result2 = DbExecRequete($sReq2, $oConnexion);

                $sQuery1 = <<<SQL
                        DELETE FROM sc_t_volume
                        WHERE PK_VOLUMEINFOS_VIF = {$id}
                        SQL;

                $sQuery2 = <<<SQL
                        DELETE FROM sc_t_assoc_vol_ovr
                        WHERE FK_AVO_REF_VOL = {$id};
                        SQL;

                $sQuery3 = <<<SQL
                        DELETE FROM sc_t_assoc_ovr_usr_tch
                        WHERE FK_OUT_REF_VOL = {$id};
                        SQL;

                if (0 != $result1->num_rows) {
                    $oRecordset1 = DbExecRequete($sQuery2, $oConnexion);
                } elseif (0 != $result2->num_rows) {
                    $oRecordset2 = DbExecRequete($sQuery3, $oConnexion);
                }

                // insert trace of the deletion
                $req = <<<SQL
                SELECT NM_NUMERO_COLLECTION_VIF
                FROM sc_t_volume
                WHERE PK_VOLUMEINFOS_VIF = {$id};
                SQL;
                $oRecordset = DbExecRequete($req, $oConnexion);
                while ($fetch = DbEnregSuivant($oRecordset)) {
                    $vol_num = $fetch->NM_NUMERO_COLLECTION_VIF;
                }
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('VOLUME  ',
                       {$id},
                       'Suppression du volume {$vol_num} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-D',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);

                // delete the volume
                $oRecordset3 = DbExecRequete($sQuery1, $oConnexion);

                addNotice('success', 'Volume correctement supprimé !');
                redirect(sprintf('/?%s', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => 'volumes',
                    'ddlsectionid' => 'volumes',
                    'param2' => 'vol_population',
                    'vparam2' => $vol_population,
                    'param3' => 'vol_oeuvre',
                    'vparam3' => $vol_oeuvre,
                    'param4' => 'vol_autancien',
                    'vparam4' => $vol_autancien,
                    'param5' => 'vol_population',
                    'vparam5' => $vol_utilisateur,
                ])));

                break;
            }
    }
    commit($oConnexion);
}

if (!empty($tri)) {
    if (!empty($vol_numero)) {
        if ('T' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres sélectionnées */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            } /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                /* selon l'oeuvre séléctionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes parus */
        if ('P' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND V.TX_ISBN_VIF != "" AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                    V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes en préparation */
        if ('Pr' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            INNER JOIN sc_t_volume AS V
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NULL AND
                                  au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            INNER JOIN sc_t_volume AS V
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NULL AND
                                  au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            INNER JOIN sc_t_volume AS V
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NULL AND
                                  au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }
    } elseif (empty($vol_numero)) {
        if ('T' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres sélectionnées */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                        	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            } /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                /* selon l'oeuvre séléctionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                        	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes parus */
        if ('P' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND V.TX_ISBN_VIF != ""
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes en préparation */
        if ('Pr' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            INNER JOIN sc_t_volume AS V
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NULL AND
                                  au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                            SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            INNER JOIN sc_t_volume AS V
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NULL AND
                                  au.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON vo.FK_AVO_REF_OVR=au.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NULL OR V.TX_ISBN_VIF = ""
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                        ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }
    }
} elseif (empty($tri)) {
    if (!empty($vol_numero)) {
        /* tous les volumes */
        if ('T' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres sélectionnées */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            } /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                /* selon l'oeuvre séléctionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                        	ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes parus */
        if ('P' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionnée */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND V.TX_ISBN_VIF != "" AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                    /* tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
                /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionnée */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                    /* tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionnée */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                    /* tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
                /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionnée */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                    /* tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                            	GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            	V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;
                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes en préparation */
        if ('Pr' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur} AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              V.NM_NUMERO_COLLECTION_VIF = {$vol_numero}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }
    } elseif (empty($vol_numero)) {
        if ('T' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                /* selon l'oeuvre sélectionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres sélectionnées */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            } /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                /* selon l'oeuvre séléctionnée */
                if (!empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                                  ot.FK_OUT_REF_USR = {$vol_utilisateur}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } /* toutes les oeuvres */
                elseif (empty($vol_oeuvre)) {
                    /* selon l'utilisateur sélectionné */
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE ot.FK_OUT_REF_USR = {$vol_utilisateur};
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } /* selon tous les utilisateurs */
                    elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes parus */
        if ('P' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND V.TX_ISBN_VIF != "" AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                                SELECT
                                    V.TX_PAGETITRE_TITRE_VIF,
                                    V.NM_NUMERO_COLLECTION_VIF,
                                    GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                    V.PK_VOLUMEINFOS_VIF
                                FROM sc_t_volume AS V
                                LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                    ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                                LEFT JOIN sc_t_assoc_au_ovr AS ao
                                    ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                                LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                    ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                                LEFT JOIN sc_t_volume_reimpr AS VR
                                    ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                                WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                      ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                      ot.FK_OUT_REF_USR = {$vol_utilisateur}
                                GROUP BY V.PK_VOLUMEINFOS_VIF
                                ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL
                        GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }

        /* volumes en préparation */
        if ('Pr' == $vol_population) {
            /* selon l'auteur selectionné */
            if (!empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                                SELECT
                                    V.TX_PAGETITRE_TITRE_VIF,
                                    V.NM_NUMERO_COLLECTION_VIF,
                                    GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                    V.PK_VOLUMEINFOS_VIF
                                FROM sc_t_volume AS V
                                LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                    ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                                LEFT JOIN sc_t_assoc_au_ovr AS ao
                                    ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                                LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                    ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                                LEFT JOIN sc_t_volume_reimpr AS VR
                                    ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                                WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                      ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien} AND
                                      ot.FK_OUT_REF_USR = {$vol_utilisateur}
                                GROUP BY V.PK_VOLUMEINFOS_VIF
                                ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                            SELECT
                                V.TX_PAGETITRE_TITRE_VIF,
                                V.NM_NUMERO_COLLECTION_VIF,
                                GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                                V.PK_VOLUMEINFOS_VIF
                            FROM sc_t_volume AS V
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_volume_reimpr AS VR
                            	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                            WHERE V.TX_ISBN_VIF IS NOT NULL AND
                                  ao.FK_AUTEUR_ANCIEN_AAN = {$vol_autancien}
                          	GROUP BY V.PK_VOLUMEINFOS_VIF
                            ORDER BY {$tri} {$ordre};
                        SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
            /* tous les auteurs */
            elseif (empty($vol_autancien)) {
                if (!empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre} AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              vo.FK_AVO_REF_OVR = {$vol_oeuvre}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                } elseif (empty($vol_oeuvre)) {
                    if (!empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL AND
                              ot.FK_OUT_REF_USR = {$vol_utilisateur}
                      	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    } elseif (empty($vol_utilisateur)) {
                        $sQuery = <<<SQL
                        SELECT
                            V.TX_PAGETITRE_TITRE_VIF,
                            V.NM_NUMERO_COLLECTION_VIF,
                            GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
                            V.PK_VOLUMEINFOS_VIF
                        FROM sc_t_volume AS V
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_volume_reimpr AS VR
                        	ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                        WHERE V.TX_ISBN_VIF IS NOT NULL
                    	GROUP BY V.PK_VOLUMEINFOS_VIF
                    	ORDER BY {$tri} {$ordre};
                    SQL;

                        $oRecordset = DbExecRequete($sQuery, $oConnexion);
                        $iNbreEnreg = DbNbreEnreg($oRecordset);
                    }
                }
            }
        }
    }
}
?>

<tr>
    <td width="100%">
        <input type="hidden" name="tri" value="<?php echo $tri; ?>">
        <input type="hidden" name="ordre" value="<?php echo $ordre; ?>">
    </td>
</tr>

    <?php if (hasNotice()): ?>
        <tr>
            <td align="center"><?php printNotice(); ?></td>
        </tr>
    <?php endif; ?>

    <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_trace
        WHERE
            TX_DOMAINE_TRC = 'VOLUME'
        ORDER BY DT_TRACE_TRC DESC LIMIT 3;
    SQL;
        $oRs = DbExecRequete($sQuery, $oConnexion);
        while ($trace = DbEnregSuivant($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
            </td>
        </tr>
        HTML;
        }
        if (0 == DbNbreEnreg($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                Aucun historique des dernières modifications apportées à un volume
            </td>
        </tr>
        HTML;
        }
        ?>
            <form name="frmGestionVolumes" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'volumes',
                'ddlsectionid' => 'volumes',
                'param2' => 'vol_population',
                'vparam2' => $vol_population,
                'param10' => 'vol_numero',
                'vparam10' => $vol_numero,
                'param3' => "vol_oeuvre",
                'vparam3' => $vol_oeuvre,
                'param4' => 'vol_autancien',
                'vparam4' => $vol_autancien,
                'param5' => 'vol_utilisateur',
                'vparam5' => $vol_utilisateur
            ]))) ?>">
                <script language="javascript" src="js/gestion.js"></script>
                <table class="table table-sm" border="0" width="100%" cellpadding="2" class="datagrid-entete">
                <thead>
                    <tr>
                        <th class="title_gestion" colspan="4">
                                LISTE DES VOLUMES
                            <?php
                            $url_add = sprintf('/?%s', http_build_query([
                                'rubriqueid' => 'intranet',
                                'pageid' => 'gestion',
                                'sectionid' => 'volumes',
                                'detail' => 'ok',
                                'sourcerub' => 'intranet',
                                'sourcepg' => 'gestion',
                                'param1' => 'sectionid',
                                'vparam1' => 'volumes',
                                'param2' => 'vol_population',
                                'vparam2' => $vol_population,
                                'param3' => 'vol_oeuvre',
                                'vparam3' => $vol_oeuvre,
                                'param4' => 'vol_autancien',
                                'vparam4' => $vol_autancien,
                                'param5' => 'vol_population',
                                'vparam5' => $vol_utilisateur
                            ]));
                            ?>
                            <input type="button" value="&nbsp;+&nbsp;" class="btn"
                                       onclick="javascript:location.href='<?php echo $url_add; ?>'">
                            <br>
                            <div style="text-align: center">
                                <u>numéro</u> permet d'accéder à la fiche signalétique et de circulation du volume.<br>
                                <img src="img/reimp.gif"> permet d'entrer dans la gestion des réimpressions.
                            </div>
                        </th>
                    </tr>
                    <tr>
                        <th style="background-color: #C90063; text-align: center">
                            <select size="1" name="vol_population"
                        onchange="javascript:document.forms['frmGestionVolumes'].vol_numero.selectedIndex=0;document.forms['frmGestionVolumes'].submit();">
                        <option value="T"<?php if ('T' == $vol_population) {
                            echo ' selected';
                        } ?>>=== Tous les volumes === </option>
                        <option value="P"<?php if ('P' == $vol_population) {
                            echo ' selected';
                        } ?>>Volumes parus</option>
                        <option value="Pr"<?php if ('Pr' == $vol_population) {
                            echo ' selected';
                        } ?>>Volumes en préparation</option>
                    </select>

                            <select size="1" name="vol_numero"  onchange="javascript:document.forms['frmGestionVolumes'].submit();">
                                <option value=""<?php if (empty($vol_numero)) {
                                    echo ' selected';
                                } ?>>=== Tous les numéros de volume ===
                                </option>
                                <?php

                                $sQuery = <<<SQL
                    SELECT
                        NM_NUMERO_COLLECTION_VIF
                    FROM sc_t_volume
                    ORDER BY NM_NUMERO_COLLECTION_VIF ASC;
                    SQL;

                $oRs_VolNum = DbExecRequete($sQuery, $oConnexion);
                while ($volnum = DbEnregSuivant($oRs_VolNum)) {
                    echo '<option value="'.$volnum->NM_NUMERO_COLLECTION_VIF.'"';
                    if ($vol_numero == $volnum->NM_NUMERO_COLLECTION_VIF) {
                        echo ' selected';
                    }
                    echo '>'.$volnum->NM_NUMERO_COLLECTION_VIF.'
                                </option>';
                }
                ?>
                            </select>

                        </th>
                        <th style="background-color: #C90063; text-align: center">

                            <select size="1" name="vol_oeuvre"
                                    onchange="javascript:document.forms['frmGestionVolumes'].vol_numero.selectedIndex=0;document.forms['frmGestionVolumes'].submit();">
                                <option value=""<?php if ('' == $vol_oeuvre) {
                                    echo ' selected';
                                } ?>>=== Toutes les &#339;uvres === </option>
                            <?php

                            $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        O.PK_OEUVRE_OVR,
                                        O.TX_TITRE_FRANCAIS_OVR
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                        ON O.PK_OEUVRE_OVR = vo.FK_AVO_REF_OVR
                                    WHERE vo.PK_ASSOC_VOLOVR IS NOT NULL
                                    ORDER BY O.TX_TITRE_FRANCAIS_OVR;
                                    SQL;

$oRecordset_2 = DbExecRequete($sQuery, $oConnexion);
while ($ovr = DbEnregSuivant($oRecordset_2)) {
    ('' == $ovr->TX_TITRE_FRANCAIS_OVR) ? ($vol_ovr_francais = '-') : ($vol_ovr_francais = $ovr->TX_TITRE_FRANCAIS_OVR);
    if ('-' != $vol_ovr_francais) {
        echo '<option value="'.$ovr->PK_OEUVRE_OVR.'"';
        if ($vol_oeuvre == $ovr->PK_OEUVRE_OVR) {
            echo ' selected';
        }
        echo '>'._raccourcirChaine(45, $vol_ovr_francais).'</option>';
    }
}
?>
                            </select>

                            <select size="1" name="vol_autancien"
        onchange="javascript:document.forms['frmGestionVolumes'].vol_numero.selectedIndex=0;document.forms['frmGestionVolumes'].submit();">
    <option value=""<?php if (empty($vol_autancien)) {
        echo ' selected';
    } ?>>=== Tous les auteurs anciens === </option>
<?php

$sQuery = <<<SQL
        SELECT DISTINCT
            A.PK_AUTEUR_ANCIEN_AAN,
            A.TX_NOM_FRANCAIS_AAN
        FROM sc_t_auteur AS A
        LEFT JOIN sc_t_assoc_au_ovr AS ao
            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
        LEFT JOIN sc_t_oeuvre AS O
            ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
        WHERE vo.PK_ASSOC_VOLOVR IS NOT NULL
        ORDER BY A.TX_NOM_FRANCAIS_AAN;
       SQL;

$oRecordset_2 = DbExecRequete($sQuery, $oConnexion);
while ($aan = DbEnregSuivant($oRecordset_2)) {
    $vol_autancien_nomfrancais = $aan->TX_NOM_FRANCAIS_AAN;
    echo '<option value="'.$aan->PK_AUTEUR_ANCIEN_AAN.'"';
    if ($vol_autancien == $aan->PK_AUTEUR_ANCIEN_AAN) {
        echo ' selected';
    }
    echo '>'._raccourcirChaine(45, $vol_autancien_nomfrancais).'</option>';
}
?>
</select>&nbsp;
                        </th>
                        <th style="background-color: #C90063; text-align: center">
                            <select size="1" name="vol_utilisateur" onchange="javascript:document.forms['frmGestionVolumes'].vol_numero.selectedIndex=0;document.forms['frmGestionVolumes'].submit();">
    <option value=""<?php if ('' == $vol_utilisateur) {
        echo ' selected';
    } ?>>=== Tous les collaborateurs ===
    </option>
<?php

$sQuery = <<<SQL
        SELECT DISTINCT
            PK_UTILISATEUR_USR,
            TX_NOM_USR,
            TX_PRENOM_USR,
            BL_DECEDE_USR
        FROM sc_t_utilisateur AS U
        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
            ON ot.FK_OUT_REF_USR = U.PK_UTILISATEUR_USR
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
            ON vo.FK_AVO_REF_OVR = ot.FK_OUT_REF_OVR
        WHERE vo.PK_ASSOC_VOLOVR IS NOT NULL
        ORDER BY U.TX_NOM_USR, U.TX_PRENOM_USR;
        SQL;

$oRecordset_2 = DbExecRequete($sQuery, $oConnexion);

while ($usr = DbEnregSuivant($oRecordset_2)) {
    ('' == $usr->TX_NOM_USR) ? ($vol_utilisateur_nom = '') : ($vol_utilisateur_nom = $usr->TX_NOM_USR);
    ('' == $usr->TX_PRENOM_USR) ? ($vol_utilisateur_prenom = '') : ($vol_utilisateur_prenom = $usr->TX_PRENOM_USR);
    $decede = $usr->BL_DECEDE_USR;?>

    <option value="<?php echo $usr->PK_UTILISATEUR_USR; ?>"
    <?php if ($vol_utilisateur == $usr->PK_UTILISATEUR_USR) {?> selected <?php } ?>>
    <?php if ($decede) { echo '† '; } echo $vol_utilisateur_nom; echo ' '; echo $vol_utilisateur_prenom; ?>
    </option>
<?php } ?>
</select>&nbsp;

                        </th>
                        <th style="background-color: #C90063; text-align: center" class="enregistrements">
                            <b><?php echo $iNbreEnreg; ?> enregistrement(s)</b>
                        </th>
                    </tr>
                    <tr>
                    <!-------------------------------------->
                    <!--N°coll / Chantier/Réimp.-->
                    <!-------------------------------------->
                    <th class="text-center" width="30%" scope="col" class="datagrid-ligne1">
                        <?php
                        $url = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'volumes',
                            'ddlsectionid' => 'volumes',
                            'vol_oeuvre' => $vol_oeuvre,
                            'vol_population' => $vol_population,
                            'tri' => 'NM_NUMERO_COLLECTION_VIF',
                            'ordre' => ('NM_NUMERO_COLLECTION_VIF' == $tri) ? $nouvordre : $ordre,
                        ]));
                        ?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>N° Coll.</u>
                            <?php
                        if ('NM_NUMERO_COLLECTION_VIF' == $tri && 'ASC' == $ordre) {
                            echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
                        } elseif ('NM_NUMERO_COLLECTION_VIF' == $tri && 'DESC' == $ordre) {
                            echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
                        }
                    ?>
                        </a> / Réimp.
                    </th>
                    <!------------------->
                    <!--auteurs anciens-->
                    <!------------------->
                    <th class="text-center" width="30%" scope="col" class="datagrid-ligne1">
                                Auteur(s) ancien(s)
                    </th>
                    <!------------------->
                    <!--titre du volume-->
                    <!------------------->
                    <th class="text-center" width="30%" scope="col" class="datagrid-ligne1"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                        <?php
                        $url = sprintf('/?%s', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'gestion',
                        'sectionid' => 'volumes',
                        'ddlsectionid' => 'volumes',
                        'vol_oeuvre' => $vol_oeuvre,
                        'vol_population' => $vol_population,
                        'tri' => 'TX_PAGETITRE_TITRE_VIF',
                        'ordre' => ('TX_PAGETITRE_TITRE_VIF' == $tri) ? $nouvordre : $ordre,
                        ]));
                        ?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>Titre du volume</u>
                            <?php
                        if ('TX_PAGETITRE_TITRE_VIF' == $tri && 'ASC' == $ordre) {
                            echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
                        } elseif ('TX_PAGETITRE_TITRE_VIF' == $tri && 'DESC' == $ordre) {
                            echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
                        }
                        ?>
                        </a>

                    </th>
                    <!---------->
                    <!--Action-->
                    <!---------->
                    <th class="text-center" width="30%" scope="col" class="datagrid-ligne1"
                        <?php if (!$rights_admin) { ?> style="display: none"<?php } ?>>Action
                    </th>
                </tr>
                </thead>


                <?php
                $ligneType = 'B';
$vol_date_reimpr = [];
while ($vol = DbEnregSuivantTab($oRecordset)) {
    $vol_pk = $vol['PK_VOLUMEINFOS_VIF'];
    (empty($vol['NM_NUMERO_COLLECTION_VIF'])) ? ($vol_numcollection = '-') : ($vol_numcollection = $vol['NM_NUMERO_COLLECTION_VIF']);
    (empty($vol['TX_ISBN_VIF'])) ? ($vol_isbn = '-') : ($vol_isbn = $vol['TX_ISBN_VIF']);
    (empty($vol['TX_PAGETITRE_TITRE_VIF'])) ? ($vol_titre = '-') : ($vol_titre = $vol['TX_PAGETITRE_TITRE_VIF']);
    $annees = $vol['dates'];
    $vol_autancien_lb = '';
    $sQuery = <<<SQL
            SELECT DISTINCT
                A.TX_NOM_FRANCAIS_AAN,
                A.PK_AUTEUR_ANCIEN_AAN,
                O.PK_OEUVRE_OVR
            FROM sc_t_auteur AS A
            LEFT JOIN sc_t_assoc_au_ovr AS ao
                ON ao.FK_AUTEUR_ANCIEN_AAN = A.PK_AUTEUR_ANCIEN_AAN
            LEFT JOIN sc_t_oeuvre AS O
                ON ao.FK_OEUVRE_OVR = O.PK_OEUVRE_OVR
            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
            WHERE vo.FK_AVO_REF_VOL = {$vol_pk}
            GROUP BY A.TX_NOM_FRANCAIS_AAN
            ORDER BY A.TX_NOM_FRANCAIS_AAN;
            SQL;

    $oRecordset_2 = DbExecRequete($sQuery, $oConnexion);
    if (DbNbreEnreg($oRecordset_2) > 0) {
        while ($aan = DbEnregSuivant($oRecordset_2)) {
            if (!empty($vol_autancien_lb)) {
                $vol_autancien_lb .= '<br>';
            }
            $vol_autancien_lb .= '<a target="_blank"
            href="/?rubriqueid=intranet&pageid=gestion&sectionid=autanciens&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=volumes&param2=vol_population&vparam2='.$vol_population.'&param3=vol_oeuvre&vparam3='.$vol_oeuvre.'&param4=vol_autancien&vparam4='.$vol_autancien.'&param5=vol_utilisateur&vparam5='.$vol_utilisateur.'&signet=vol'.$vol_pk.'&id_autancien='.$aan->PK_AUTEUR_ANCIEN_AAN.'" class="dtg-lignes">'.$aan->TX_NOM_FRANCAIS_AAN.'</a>';
        }
    } else {
        $vol_autancien_lb = '-';
    }

    ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A');
    ?>
                <tbody>
                <tr style="line-height: 15px">
                    <td style="text-align: left;" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <a name="<?php echo 'vol'.$vol_pk; ?>">
                            <table border="0">
                                <tr>
                                    <td width="20">
                                        <?php
                                        $url_vol =
                                            sprintf('/?%s', http_build_query([
                                                'rubriqueid' => 'intranet',
                                                'pageid' => 'gestion',
                                                'sectionid' => 'volumes',
                                                'detail' => 'ok',
                                                'param1' => 'sectionid',
                                                'vparam1' => 'volumes',
                                                'param2' => 'vol_population',
                                                'vparam2' => $vol_population,
                                                'param10' => 'vol_numero',
                                                'vparam10' => $vol_numero,
                                                'param3' => "vol_oeuvre",
                                                'vparam3' => $vol_oeuvre,
                                                'param4' => 'vol_autancien',
                                                'vparam4' => $vol_autancien,
                                                'param5' => 'vol_utilisateur',
                                                'vparam5' => $vol_utilisateur,
                                                'signet' => 'vol'.$vol_pk,
                                                'id_volume' => $vol_pk,
                                                'action' => 'creationreimpression'
                                            ]));
                                        ?>
                                        <a href="<?php echo $url_vol; ?>" class="dtg-lignes" style="margin-left: 180px">
                                            <?php echo $vol_numcollection; ?>
                                        </a>
                                    </td>
                                    <td width="20" nowrap align="center">
                                        <?php
                                        $url_img_reimp = sprintf('/?%s', http_build_query([
                                            'rubriqueid' => 'intranet',
                                            'pageid' => 'gestion',
                                            'sectionid' => 'volumes',
                                            'detail' => 'ok6',
                                            'signet' => 'vol'.$vol_pk,
                                            'id_volume' => $vol_pk,
                                            'action' => 'creationreimpression'
                                        ]));
                                        ?>
                                        <a href="<?php echo $url_img_reimp; ?>" class="dtg-lignes">
                                            <img src="img/reimp.gif" align="absmiddle" border="0" alt="Réimpressions de volume">
                                        </a>
                                    </td>
                                    <td>
                                        <?php
                            $years = explode(',', $annees ?? '');
                            $arr_years = [];
                            foreach ($years as $year) {
                                $arr_years[] = _trim(explode('-', $year)[0]);
                            }
                            $arr_annees = [];
                            foreach ($years as $year) {
                                $arr_annees[] = _trim(explode(',', $year)[0]);
                            }
                            $url_annees = [];
                            foreach ($arr_annees as $annee) {
                                $url_annees[] = sprintf('/?%s', http_build_query([
                                    'rubriqueid' => 'intranet',
                                    'pageid' => 'gestion',
                                    'sectionid' => 'volumes',
                                    'detail' => 'ok6',
                                    'sourcerub' => 'intranet',
                                    'sourcepg' => 'gestion',
                                    'param1' => 'sectionid',
                                    'vparam1' => 'volumes',
                                    'signet' => 'vol'.$vol_pk,
                                    'id_volume' => $vol_pk,
                                    'id_vol_reimpr' => $annee,
                                    'action' => 'chargerreimpression'
                                ]));
                            }
                            $arr_both = array_filter(array_combine($url_annees, $arr_years));
                            foreach ($arr_both as $url_annee => $year) { ?>
                                            <a href="<?php echo $url_annee; ?>">
                                                <?php echo $year; ?>
                                            </a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </a>
                    </td>
                    <td class="datagrid-lignes<?php echo $ligneType; ?>" style="text-align: center">
                                <?php echo $vol_autancien_lb; ?>
                    </td>
                    <td  class="datagrid-lignes<?php echo $ligneType; ?>" style="text-align: center"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                                <?php echo $vol_titre; ?>
                    </td>
                    <td class="datagrid-lignes<?php echo $ligneType; ?>"
                        <?php if (!$rights_admin) { ?> style="display: none;"<?php } ?>>
                            <a href="javascript:if (confirm('Etes-vous sûr de vouloir supprimer ce volume ?'))
                {location.href='/?rubriqueid=intranet&pageid=gestion&sectionid=volumes&action=suppr&id=<?php echo $vol_pk; ?>&vol_population=<?php echo $vol_population; ?>';}" class="dtg-lignes">Suppr.
                            </a>
                    </td>
                </tr>
                </tbody>
                <?php }?>
        </table>
        </form>
    </td>
</tr>

<?php

DbClose($oConnexion);
?>



