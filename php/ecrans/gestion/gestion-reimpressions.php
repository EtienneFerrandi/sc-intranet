<?php
$oConnexion = DbConnection();

$todayA = date('Y');

// Selection de 3 années à partir de cette année
$rq_annee = <<<SQL
    SELECT SUBSTRING_INDEX(DT_SORTIE_VRP, '-',1)
    FROM sc_t_volume_reimpr
    GROUP BY SUBSTRING_INDEX(DT_SORTIE_VRP, '-',1)
    ORDER BY DT_SORTIE_VRP DESC;
    SQL;
$oRecordset_annee = DbExecRequete($rq_annee, $oConnexion);

$annees = [];
$i = 1;
while (($an = DbEnregSuivantTab($oRecordset_annee)) && $i <= 3) {
    $annees[] = $an["SUBSTRING_INDEX(DT_SORTIE_VRP, '-',1)"];
    ++$i;
}
?>
<tr>
    <td>
        <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_trace
        WHERE
            TX_DOMAINE_TRC = 'VOLUME_REIMPRESSION'
        ORDER BY DT_TRACE_TRC DESC LIMIT 3;
    SQL;
        $oRs = DbExecRequete($sQuery, $oConnexion);
        while ($trace = DbEnregSuivant($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
            </td>
        </tr>
        HTML;
        }
        if (0 == DbNbreEnreg($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                Aucun historique des dernières modifications apportées à une réimpression de volume
            </td>
        </tr>
        HTML;
        }
        ?>
        <form name="frmGestionReimpressions" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'reimpressions',
            'ddlsectionid' => 'reimpressions'
        ]))) ?>">
        <table border="0" width="100%" class="table table-sm table-borderless" style="border-top: solid; border-bottom: solid;">
            <tbody>
<?php
// /CLASSEMENT PAR ANNEE
$list_annees = implode(',', $annees);
$rq = <<<SQL
        SELECT
            V.PK_VOLUMEINFOS_VIF,
            VR.PK_VOLREIMPR_VRP,
            V.NM_NUMERO_COLLECTION_VIF,
            V.TX_NUMCOLL_COMPLEMENT_VIF,
            V.TX_PAGETITRE_TITRE_VIF,
            VR.DT_SORTIE_VRP,
            A.TX_NOM_FRANCAIS_AAN
        FROM sc_t_volume_reimpr AS VR
        INNER JOIN sc_t_volume AS V
            ON VR.FK_VRP_REF_VOL = V.PK_VOLUMEINFOS_VIF
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
        LEFT JOIN sc_t_assoc_au_ovr AS ao
            ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
        LEFT JOIN sc_t_auteur AS A
            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
        WHERE DT_SORTIE_VRP IN ($list_annees)
        GROUP BY V.NM_NUMERO_COLLECTION_VIF
        ORDER BY SUBSTRING_INDEX(VR.DT_SORTIE_VRP, "-", 1) DESC, V.NM_NUMERO_COLLECTION_VIF ASC;
SQL;
$oRecordset = DbExecRequete($rq, $oConnexion);
if (DbNbreEnreg($oRecordset) > 0) {
    $anc_annee = '';
    while ($vrp = DbEnregSuivant($oRecordset)) {
        $id_vol = $vrp->PK_VOLUMEINFOS_VIF;
        $auteur = mb_strtoupper((string) $vrp->TX_NOM_FRANCAIS_AAN);
        $id_vrp = $vrp->PK_VOLREIMPR_VRP;
        $annee = $vrp->DT_SORTIE_VRP;
        $titre = $vrp->TX_PAGETITRE_TITRE_VIF;
        $num = $vrp->NM_NUMERO_COLLECTION_VIF;
        if (null != $num && null != $vrp->TX_NUMCOLL_COMPLEMENT_VIF) {
            $num .= ' '.$vrp->TX_NUMCOLL_COMPLEMENT_VIF;
        }

        $url_reimpr = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok6',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'reimpressions',
            'signet' => 'vol'.$id_vol,
            'id_volume' => $id_vol
        ]));
        $url_vol = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'reimpressions',
            'signet' => $id_vol,
            'id_volume' => $id_vol
        ]));
        ?>
                <tr style="line-height: 15px">
                    <td class="datagrid-lignesB" style="text-align: center">
                        <?php if (explode('-', $annee)[0] !== $anc_annee) {
                            $anc_annee = explode('-', $annee)[0]; ?>
                            <span style="color: #95004a; font-size: large;"><b>Réimpressions réalisées en <?php echo explode('-', $annee)[0]; ?></b></span>
                        <?php } ?>
                    </td>
                    <td align="left" class="datagrid-lignesB">
                        <a href='<?php echo $url_reimpr; ?>'>
                            <img src="img/loupe.gif" border="0">
                        </a>
                        <a href='<?php echo $url_vol; ?>' class="std"><?php echo $num; ?>. <?php echo $auteur; ?>, <?php echo $titre; ?></a>
                    </td>
                </tr>
            <?php
    }
} ?>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table border="0" width="100%" class="table table-sm" style="border-top: solid;">

<?php

// /CLASSEMENT PAR NUMERO
$rq2 = <<<SQL
    SELECT
        V.PK_VOLUMEINFOS_VIF,
        VR.PK_VOLREIMPR_VRP,
        V.NM_NUMERO_COLLECTION_VIF,
        V.TX_NUMCOLL_COMPLEMENT_VIF,
        V.TX_PAGETITRE_TITRE_VIF,
        GROUP_CONCAT(DISTINCT VR.DT_SORTIE_VRP SEPARATOR ', ') AS dates,
        A.TX_NOM_FRANCAIS_AAN
    FROM sc_t_volume_reimpr AS VR
    LEFT JOIN sc_t_volume AS V
        ON VR.FK_VRP_REF_VOL = V.PK_VOLUMEINFOS_VIF
    LEFT JOIN sc_t_assoc_vol_ovr AS vo
        ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
    LEFT JOIN sc_t_assoc_au_ovr AS ao
        ON vo.FK_AVO_REF_OVR = ao.FK_OEUVRE_OVR
    LEFT JOIN sc_t_auteur AS A
        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
    WHERE
        DT_SORTIE_VRP IS NOT NULL
    AND V.PK_VOLUMEINFOS_VIF IS NOT NULL
    GROUP BY FK_VRP_REF_VOL
    ORDER BY V.NM_NUMERO_COLLECTION_VIF, VR.DT_SORTIE_VRP DESC;
SQL;
$oRecordset2 = DbExecRequete($rq2, $oConnexion);

if (DbNbreEnreg($oRecordset2) > 0) { ?>
            <thead style="background-color: #C90063;">
                <tr >
                    <th style="text-align:center; color: white; font-size: large">Volumes (<?php echo DbNbreEnreg($oRecordset2); ?>)</th>
                    <th style="text-align:center; color: white; font-size: large">Année de réimpression</th>
                </tr>
            </thead>
            <tbody>
<?php
    while ($vrp = DbEnregSuivantTab($oRecordset2)) {
        $id_vol = $vrp['PK_VOLUMEINFOS_VIF'];
        $annees = $vrp['dates'];
        $auteur = mb_strtoupper((string) $vrp['TX_NOM_FRANCAIS_AAN']);
        $id_vrp = $vrp['PK_VOLREIMPR_VRP'];
        $titre = $vrp['TX_PAGETITRE_TITRE_VIF'];
        $num = $vrp['NM_NUMERO_COLLECTION_VIF'];
        if (null != $num && null != $vrp['TX_NUMCOLL_COMPLEMENT_VIF']) {
            $num .= ' '.$vrp['TX_NUMCOLL_COMPLEMENT_VIF'];
        }

        $url_reimpr = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok6',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'reimpressions',
            'signet' => 'vol'.$id_vol,
            'id_volume' => $id_vol,
        ]));
        $url_vol = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'reimpressions',
            'signet' => $id_vol,
            'id_volume' => $id_vol,
        ]));
        ?>
            <tr style="line-height: 15px">
                <td align="center" class="datagrid-lignesB">
                    <a href='<?php echo $url_reimpr; ?>'>
                        <img src="img/loupe.gif" border="0">
                    </a>
                    <a href='<?php echo $url_vol; ?>' class="std"><?php echo $num; ?>. <?php echo $auteur; ?>, <?php echo $titre; ?></a>
                </td>
                <td class="datagrid-lignesB" style="text-align: center">
                    <?php
                    $years = explode(',', $annees);
        $arr_years = [];
        foreach ($years as $year) {
            $arr_years[] = _trim(explode('-', $year)[0]);
        }
        $arr_annees = [];
        foreach ($years as $year) {
            $arr_annees[] = _trim(explode(',', $year)[0]);
        }
        $url_annees = [];
        foreach ($arr_annees as $annee) {
            $url_annees[] = sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'volumes',
                'detail' => 'ok6',
                'sourcerub' => 'intranet',
                'sourcepg' => 'gestion',
                'param1' => 'sectionid',
                'vparam1' => 'reimpressions',
                'signet' => 'vol'.$id_vol,
                'id_volume' => $id_vol,
                'id_vol_reimpr' => $annee,
                'action' => 'chargerreimpression',
            ]));
        }
        $arr_both = array_filter(array_combine($url_annees, $arr_years));
        foreach ($arr_both as $url_annee => $year) {?>
                        <a href="<?php echo $url_annee; ?>">
                            <b><?php echo $year; ?></b>
                        </a>
                    <?php } ?>
                </td>
            </tr>
<?php } ?>
        </tbody>
        </table>
        </form>
    </td>
</tr>
<?php }
DbClose($oConnexion); ?>
