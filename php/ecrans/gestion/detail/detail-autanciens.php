<?php
$oConnexion = DbConnection();

$id_autancien = '';
$nom_francais = '';
$nom_clavis = '';
$lien_biblindex = '';
$remarques = '';

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}

if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%auteurs anciens%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC' || $usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%auteurs anciens%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%auteurs anciens%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (!isset($_POST['recupform']) && !empty($_GET['id_autancien'])) {
    $id_autancien = $_GET['id_autancien'];

    $sQuery = <<<SQL
        SELECT *
        FROM sc_t_auteur
        WHERE PK_AUTEUR_ANCIEN_AAN = {$id_autancien};
        SQL;

    $oRecordset = DbExecRequete($sQuery, $oConnexion);

    if (1 == DbNbreEnreg($oRecordset)) {
        $autanc = DbEnregSuivant($oRecordset);
        $nom_francais = $autanc->TX_NOM_FRANCAIS_AAN;
        $nom_clavis = $autanc->TX_NOM_CLAVIS_AAN;
        $lien_biblindex = $autanc->TX_URL_BIBLINDEX_AAN;
        $remarques = $autanc->TX_REMARQUES_AAN;
    } else { // Erreur : aucun auteur ancien ne correspond à cet identifiant
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
    }
} elseif (null != $_POST) {
    // L'utilisateur a soumis le formulaire et il dispose du droit "Gestion des auteurs anciens - Edition"
    $letter = $_POST['vparam3'];
    $id_autancien = $_POST['id_autancien'];
    $nom_francais = _trim($_POST['nom_francais']);
    $nom_clavis = _trim($_POST['nom_clavis']);
    $lien_biblindex = _trim($_POST['lien_biblindex']);
    $remarques = _trim($_POST['remarques']);
    // ========================== MISE A JOUR EN BASE DE DONNEES ================================

    if (empty($id_autancien)) {// On insère un nouvel auteur ancien dans la base
        $remarques = mysqli_real_escape_string($oConnexion, $remarques);
        $sRequete = <<<SQL
            INSERT INTO sc_t_auteur(TX_NOM_FRANCAIS_AAN,
                TX_NOM_CLAVIS_AAN,
                TX_URL_BIBLINDEX_AAN,
                TX_REMARQUES_AAN)
            VALUES("{$nom_francais}","{$nom_clavis}","{$lien_biblindex}","{$remarques}");
            SQL;
        $oRecordset = DbExecRequete($sRequete, $oConnexion);

        $aut_id = mysqli_insert_id($oConnexion);
        global $id_aut;
        $id_aut = $aut_id;

        if (mysqli_affected_rows($oConnexion)) {
            // insert a trace of the addition

            $str_auteur = addslashes("l'auteur");
            $nom_francais = addslashes($nom_francais);
            $today = date('Y-m-d H:i:s');
            $sQueryTrace = <<<SQL
            INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
            VALUES('AUTEUR_ANCIEN',
                   {$aut_id},
                   'Ajout de {$str_auteur} ancien {$nom_francais} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                   'SQL-I',
                   "{$today}",
                   {$_SESSION['PK_UTILISATEUR_USR']}
                   );
            SQL;
            $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
        }

        addNotice('success', 'Auteur correctement ajouté !');
        commit($oConnexion);
        redirect(sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'autanciens',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'autanciens',
            'param3' => 'lettre',
            'vparam3' => $letter,
            'signet' => 'autanc'.$id_aut,
            'id_autancien' => $id_aut,
        ])));
    } else { // On met à jour l'auteur ancien dans la base
        $remarques = mysqli_real_escape_string($oConnexion, $remarques);
        $sRequete = <<<SQL
            UPDATE sc_t_auteur
            SET TX_NOM_FRANCAIS_AAN = "{$nom_francais}",
                TX_NOM_CLAVIS_AAN = "{$nom_clavis}",
                TX_URL_BIBLINDEX_AAN = "{$lien_biblindex}",
                TX_REMARQUES_AAN = "{$remarques}"
            WHERE PK_AUTEUR_ANCIEN_AAN = {$id_autancien}
            SQL;
        $oRecordset = DbExecRequete($sRequete, $oConnexion);

        if (mysqli_affected_rows($oConnexion)) {
            // insert a trace of the update
            $str_auteur = addslashes("l'auteur");
            $nom_francais = addslashes($nom_francais);
            $today = date('Y-m-d H:i:s');

            $req = <<<SQL
                DELETE FROM sc_t_trace
                WHERE
                    DT_TRACE_TRC = "{$today}"
                AND NM_UTILISATEUR_TRC = {$_SESSION['PK_UTILISATEUR_USR']}
                AND TX_DOMAINE_TRC = 'AUTEUR_ANCIEN'
                AND NM_ITEMID_TRC = {$id_autancien};
            SQL;
            $exec = DbExecRequete($req, $oConnexion);

            $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('AUTEUR_ANCIEN',
                       {$id_autancien},
                       'Mise à jour de {$str_auteur} ancien {$nom_francais} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-U',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
            $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
        }

        addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');
        commit($oConnexion);

        redirect(sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'autanciens',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'autanciens',
            'param3' => 'lettre',
            'vparam3' => $letter,
            'signet' => 'autanc'.$id_autancien,
            'id_autancien' => $id_autancien,
        ])));
    }
}

// Doit-on inhiber l'édition car on accède à l'auteur ancien depuis une autre page que celle de gestion des auteurs anciens ?
$inhiber = false;
if (isset($_REQUEST['vparam1'])) {
    if ('autanciens' != $_REQUEST['vparam1']) {
        $inhiber = true;
    }
} elseif (isset($_REQUEST['oldvparam1'])) {
    if ('autanciens' != $_REQUEST['oldvparam1']) {
        $inhiber = true;
    }
}
?>
<script type="text/javascript" src="js/tiny_mce_v349/tiny_mce_gzip.js"></script>
<script type="text/javascript" src="js/tinymce-launcher_v349_20120420.js"></script>
<script language="javascript" src="js/detail-autanciens.js"></script>
<input type="hidden" name="recupform" value="ok">
<input type="hidden" name="id_autancien" value="<?php echo $id_autancien; ?>">
<!-- ===== Bouton 'Retour' / DEBUT ===== -->
<input type="hidden" name="sourcerub" value="intranet">
<input type="hidden" name="sourcepg" value="gestion">
<input type="hidden" name="param1" value="sectionid">
<input type="hidden" name="vparam1" value="<?php if (isset($_REQUEST['vparam1'])) {
    echo $_REQUEST['vparam1'];
} elseif (isset($_REQUEST['oldvparam1'])) {
    echo $_REQUEST['oldvparam1'];
} ?>">
<input type="hidden" name="param2" value="<?php if (isset($_REQUEST['param2'])) {
    echo $_REQUEST['param2'];
} ?>">
<input type="hidden" name="vparam2" value="<?php if (isset($_REQUEST['vparam2'])) {
    echo $_REQUEST['vparam2'];
} elseif (isset($_REQUEST['oldvparam2'])) {
    echo $_REQUEST['oldvparam2'];
} ?>">
<input type="hidden" name="param3" value="<?php if (isset($_REQUEST['param3'])) {
    echo $_REQUEST['param3'];
} ?>">
<input type="hidden" name="vparam3" value="<?php if (isset($_REQUEST['vparam3'])) {
    echo $_REQUEST['vparam3'];
} elseif (isset($_REQUEST['oldvparam3'])) {
    echo $_REQUEST['oldvparam3'];
} ?>">
<input type="hidden" name="param4" value="<?php if (isset($_REQUEST['param4'])) {
    echo $_REQUEST['param4'];
} ?>">
<input type="hidden" name="vparam4" value="<?php if (isset($_REQUEST['vparam4'])) {
    echo $_REQUEST['vparam4'];
} elseif (isset($_REQUEST['oldvparam4'])) {
    echo $_REQUEST['oldvparam4'];
} ?>">
<input type="hidden" name="param5" value="<?php if (isset($_REQUEST['param5'])) {
    echo $_REQUEST['param5'];
} ?>">
<input type="hidden" name="vparam5" value="<?php if (isset($_REQUEST['vparam5'])) {
    echo $_REQUEST['vparam5'];
} elseif (isset($_REQUEST['oldvparam5'])) {
    echo $_REQUEST['oldvparam5'];
} ?>">
<input type="hidden" name="param6" value="<?php if (isset($_REQUEST['param6'])) {
    echo $_REQUEST['param6'];
} ?>">
<input type="hidden" name="vparam6" value="<?php if (isset($_REQUEST['vparam6'])) {
    echo $_REQUEST['vparam6'];
} elseif (isset($_REQUEST['oldvparam6'])) {
    echo $_REQUEST['oldvparam6'];
} ?>">
<input type="hidden" name="param7" value="<?php if (isset($_REQUEST['param7'])) {
    echo $_REQUEST['param7'];
} ?>">
<input type="hidden" name="vparam7" value="<?php if (isset($_REQUEST['vparam7'])) {
    echo $_REQUEST['vparam7'];
} elseif (isset($_REQUEST['oldvparam7'])) {
    echo $_REQUEST['oldvparam7'];
} ?>">
<input type="hidden" name="param8" value="<?php if (isset($_REQUEST['param8'])) {
    echo $_REQUEST['param8'];
} ?>">
<input type="hidden" name="vparam8" value="<?php if (isset($_REQUEST['vparam8'])) {
    echo $_REQUEST['vparam8'];
} elseif (isset($_REQUEST['oldvparam8'])) {
    echo $_REQUEST['oldvparam8'];
} ?>">
<input type="hidden" name="param9" value="<?php if (isset($_REQUEST['param9'])) {
    echo $_REQUEST['param9'];
} ?>">
<input type="hidden" name="vparam9" value="<?php if (isset($_REQUEST['vparam9'])) {
    echo $_REQUEST['vparam9'];
} elseif (isset($_REQUEST['oldvparam9'])) {
    echo $_REQUEST['oldvparam9'];
} ?>">

<?php if (isset($_REQUEST['signet'])) { ?>
    <input type="hidden" name="signet" value="<?php if (isset($_REQUEST['signet'])) {
        echo $_REQUEST['signet'];
    } elseif (isset($_REQUEST['oldsignet'])) {
        echo $_REQUEST['oldsignet'];
    } ?>">
<?php } ?>
<!-- ===== Bouton 'Retour' / FIN ===== -->



<!-------------------><!-------------------><!------------------->
<!-------------------><!-------------------><!------------------->
<!-------------------><!--Début affichage--><!------------------->
<!-------------------><!-------------------><!------------------->
<!-------------------><!-------------------><!------------------->

<tr>
    <td align="center">
        <font class="font-titre1">
            <?php if (empty($id_autancien)) {
                echo 'Ajout d\'un nouvel auteur ancien';
            } else {
                echo 'Modification d\'un auteur ancien';
            } ?></font>
    </td>
</tr>
<?php if (hasNotice()): ?>
    <tr>
        <td align="center">
            <?php printNotice(); ?>
        </td>
    </tr>
<?php endif; ?>


<tr>
    <td width="100%">
        <div align="center">
            <table border="0" width="100%" cellpadding="4" cellspacing="0">


        <?php
        if (!empty($id_autancien)) {
            $sQuery = <<<SQL
                SELECT *
                FROM sc_t_trace
                WHERE
                    TX_DOMAINE_TRC = 'AUTEUR_ANCIEN'
                AND NM_ITEMID_TRC = {$id_autancien}
                ORDER BY DT_TRACE_TRC DESC LIMIT 3;
            SQL;
            $oRs = DbExecRequete($sQuery, $oConnexion);
            while ($trace = DbEnregSuivant($oRs)) {
                echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
                    </td>
                </tr>
                HTML;
            }
            if (0 == DbNbreEnreg($oRs)) {
                echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        Aucun historique des dernières modifications apportées à cet auteur ancien
                    </td>
                </tr>
                HTML;
            }
            ?>
        <?php } ?>

            <?php
            $url = sprintf('/?%s#back', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'autanciens',
                'ddlsectionid' => 'autanciens',
                'param3' => 'lettre',
                'vparam3' => $_GET['vparam3']
            ]));
            ?>
        <tr>
            <td colspan="3" align="center">
                <a href="<?php echo $url; ?>" id="return">
                    <img alt="Retour" src="img/common/retour.gif" width="91" height="24" border="0">
                </a>
            </td>
        </tr>
    <?php $ligneType = 'B'; ?>
    <tr>
        <td colspan="3" align="center">
            <font class="font-small">Les champs dont les libellés sont en caractères gras sont obligatoires.</font>
        </td>
    </tr>


    <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
    <tr class="datagrid-lignes<?php echo $ligneType; ?>">
        <td align="left" nowrap>
            <font class="font-normal"><b>Nom français</b>&nbsp;:</font><br><font class="font-small"><em>Quand il s’agit d’un Pseudo,
                    mettre « nom de l’auteur (Pseudo-) »</em></font>
        </td>
        <td width="25">&nbsp;</td>
        <td align="center">
            <input type="text" maxlength="100" name="nom_francais" value="<?php echo $nom_francais; ?>" size="40">
        </td>
    </tr>

    <?php ('A' === $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
    <tr class="datagrid-lignes<?php echo $ligneType; ?>">
        <td align="left" nowrap>
            <font class="font-normal">Nom clavis&nbsp;:</font><br>
                <font class="font-small">
                    <em>donc nom latin même pour les auteurs grecs.</em></font><font class="font-small">
                    <em>Quand il s’agit d’un Pseudo, mettre « nom de l'auteur (Ps.) »</em>
            </font>
        </td>
        <td width="25">&nbsp;</td>
        <td align="center">
            <input type="text" maxlength="100" name="nom_clavis" value="<?php echo $nom_clavis; ?>" size="40">
        </td>
    </tr>


    <?php ('A' === $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
    <tr class="datagrid-lignes<?php echo $ligneType; ?>">
        <td align="left" nowrap>
            <font class="font-normal">Lien Biblindex : </font><br>
                <font class="font-small"><em>le lien vers l'auteur sur le site Biblindex</em></font>
        </td>
        <td width="25">&nbsp;</td>
        <td align="center">
            <input type="text" maxlength="100" name="lien_biblindex" value="<?php echo $lien_biblindex; ?>" size="40">
        </td>
    </tr>

    <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
    <tr class="datagrid-lignes<?php echo $ligneType; ?>">
        <td align="left" nowrap>
            <font class="font-normal">Remarques générales :</font><br><font class="font-small"><em>Pour indiquer notamment les &#339;uvres de cet auteur dont la publication n’est pas souhaitée à SC</em></font>
        </td>
        <td width="25">&nbsp;</td>
        <td align="center">
            <textarea id="full-featured-non-premium"
                      name="remarques" rows="10" cols="90" class="mceEditor"><?php echo $remarques; ?></textarea>
        </td>
    </tr>

<!------------------>
<!--Collaborateurs-->
<!------------------>

    <?php if (!empty($id_autancien)) { ?>

                        <td colspan="3" align="center">
                            <table border="0" width="100%" class="datagrid-entete">
                                <tr>
                                    <td width="75%">
                                        <font class="font-normal"><font class="font-datagrid-entete">
                                                <a name="utilisateur">
                                                    <b>Collaborateur(s) associé(s) à cet auteur ancien</b>
                                                </a>
                                            </font></font><br>

                                    </td>
                                    <td width="25%" align="right">
                                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                                    </td>
                                </tr>
                            </table>
                        </td>



                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td style='text-align:center;' colspan="3">
                        <ul style="column-count: 4">
                            <?php
            $sQuery = <<<SQL
                        SELECT DISTINCT U.PK_UTILISATEUR_USR,
                            U.TX_NOM_USR,
                            U.TX_PRENOM_USR,
                            U.BL_DECEDE_USR
                        FROM sc_t_auteur AS A
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                        LEFT JOIN sc_t_oeuvre AS O
                            ON O.PK_OEUVRE_OVR= ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_utilisateur AS U
                            ON ot.FK_OUT_REF_USR = U.PK_UTILISATEUR_USR
                        WHERE A.PK_AUTEUR_ANCIEN_AAN={$id_autancien} AND
                            U.TX_NOM_USR IS NOT NULL AND
                            U.TX_PRENOM_USR IS NOT NULL
                        ORDER BY U.TX_NOM_USR;
                        SQL;

        $oRecordset = DbExecRequete($sQuery, $oConnexion);
        $iNbreEnreg = DbNbreEnreg($oRecordset);

        if ($iNbreEnreg > 0) {
            while ($usr = DbEnregSuivant($oRecordset)) {
                $url = sprintf('/?%s', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => 'utilisateurs',
                    'detail' => 'ok',
                    'sourcerub' => 'intranet',
                    'sourcepg' => 'gestion',
                    'param1' => 'sectionid',
                    'vparam1' => 'utilisateurs',
                    'signet' => 'uti'.$usr->PK_UTILISATEUR_USR,
                    'id_utilisateur' => $usr->PK_UTILISATEUR_USR,
                ])); ?>
                <li>
                    <a href="<?php echo $url; ?>" target="_blank">
                        <?php if ($usr->BL_DECEDE_USR) {
                            echo '† ';
                        }
                        echo "{$usr->TX_NOM_USR} {$usr->TX_PRENOM_USR}"; ?>
                    </a>
                </li>
            <?php } ?>
            </ul>
        </td>
    </tr>
<?php } ?>

<!----------------------->
<!--Oeuvres de l'auteur-->
<!----------------------->

    <?php if (!empty($id_autancien)) {
        ?>

                    <tr>
                        <td colspan="3" align="center">
                            <table border="0" width="100%" class="datagrid-entete">
                                <tr>
                                    <td width="75%">
                                        <font class="font-normal"><font class="font-datagrid-entete"><b>&#338;uvres de cet auteur ancien</b></font></font>
                                    </td>
                                    <td width="25%" align="right">
                                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                                    </td>
                            </table>
                        </td>
                    </tr>


                    <?php ('A' === $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                    <?php
                    $sQuery = <<<SQL
                        SELECT DISTINCT O.PK_OEUVRE_OVR,
                        O.TX_TITRE_LATIN_OVR,
                        O.TX_TITRE_FRANCAIS_OVR,
                        O.TX_CLAVIS_OVR,
                        V.NM_NUMERO_COLLECTION_VIF,
                        V.TX_NUMCOLL_COMPLEMENT_VIF,
                        V.DT_PARUTION_VIF,
                        O.TX_URL_BIBLINDEX_OVR
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                        ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                        LEFT JOIN sc_t_volume AS V
                        ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                        ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        WHERE au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                    SQL;

                    $oRs_ttes = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregTtes = DbNbreEnreg($oRs_ttes);

                    $sQuery = <<<SQL
                       SELECT DISTINCT O.PK_OEUVRE_OVR,
                            O.TX_TITRE_LATIN_OVR,
                            O.`TX_TITRE_FRANCAIS_OVR`,
                            O.TX_CLAVIS_OVR,
                            V.NM_NUMERO_COLLECTION_VIF,
                            V.TX_NUMCOLL_COMPLEMENT_VIF,
                            V.DT_PARUTION_VIF,
                            O.TX_URL_BIBLINDEX_OVR
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        LEFT JOIN sc_t_volume AS V
                            ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                        WHERE vo.FK_AVO_REF_VOL IS NOT NULL AND
                              V.TX_ISBN_VIF IS NOT NULL AND
                              au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                    SQL;

                    $oRs_OPC = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregPar = DbNbreEnreg($oRs_OPC);


                    $sQuery = <<<SQL
                        SELECT DISTINCT
                            O.TX_TITRE_LATIN_OVR,
                            O.TX_TITRE_FRANCAIS_OVR
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON vo.FK_AVO_REF_OVR = O.PK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        INNER JOIN sc_t_volume AS V
                            ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
                        WHERE V.TX_ISBN_VIF IS NULL AND
                              au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                        SQL;

                    $oRs_OEP = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregPrep = DbNbreEnreg($oRs_OEP);

                    $sQuery = <<<SQL
                        SELECT DISTINCT O.PK_OEUVRE_OVR,
                            O.TX_TITRE_LATIN_OVR,
                            O.`TX_TITRE_FRANCAIS_OVR`,
                            O.TX_CLAVIS_OVR,
                            O.TX_URL_BIBLINDEX_OVR
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        LEFT JOIN sc_t_volume AS V
                            ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                        INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                        WHERE vo.FK_AVO_REF_VOL IS NULL
                            AND O.`BL_OEUVRE_INTERNE_OVR`=1
                            AND au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                        SQL;

                    $oRs_OPMT = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregCha = DbNbreEnreg($oRs_OPMT);

                    $sQuery = <<<SQL
                        SELECT DISTINCT O.PK_OEUVRE_OVR,
                            O.BL_OEUVRE_INTERNE_OVR,
                            O.TX_TITRE_LATIN_OVR,
                            O.`TX_TITRE_FRANCAIS_OVR`,
                            O.TX_CLAVIS_OVR,
                            O.`TX_URL_BIBLINDEX_OVR`
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        LEFT JOIN sc_t_volume AS V
                            ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                        WHERE vo.FK_AVO_REF_VOL IS NULL
                            AND O.`BL_OEUVRE_INTERNE_OVR`=0
                            AND au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                SQL;

                    $oRs_OPA = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregNPrev = DbNbreEnreg($oRs_OPA);

                    $sQuery = <<<SQL
                        SELECT DISTINCT O.PK_OEUVRE_OVR,
                            O.TX_TITRE_LATIN_OVR,
                            O.`TX_TITRE_FRANCAIS_OVR`,
                            O.TX_CLAVIS_OVR,
                            O.`TX_URL_BIBLINDEX_OVR`
                        FROM sc_t_oeuvre AS O
                        LEFT JOIN sc_t_assoc_vol_ovr AS vo
                            ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                        LEFT JOIN sc_t_assoc_au_ovr AS au
                            ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                        LEFT JOIN sc_t_volume AS V
                            ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                        WHERE vo.FK_AVO_REF_VOL IS NULL
                            AND O.`BL_OEUVRE_INTERNE_OVR`=1
                            AND au.FK_AUTEUR_ANCIEN_AAN={$id_autancien}
                            AND ot.`FK_OUT_REF_OVR` IS NULL
                        ORDER BY O.`TX_TITRE_LATIN_OVR`, O.`TX_TITRE_FRANCAIS_OVR`;
                    SQL;

                    $oRs_OL = DbExecRequete($sQuery, $oConnexion);
                    $nbEnregEnv = DbNbreEnreg($oRs_OL);
                    ?>
                    <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                        <td colspan="3" align="center">
                            <table border="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td style='text-align:center;' colspan="3" id="ottes">
                                            <ul class="list_works_au">
                                                <!--Toutes les oeuvres de l'auteur-->
                                                <?php
                                                    echo <<<HTML
                                                        <h5>&#338;uvres parues dans SC ({$nbEnregPar})</h5>
                                                    HTML;
                                                    while ($par = DbEnregSuivant($oRs_OPC)) {
                                                        $url = sprintf('/?%s', http_build_query([
                                                            'rubriqueid' => 'intranet',
                                                            'pageid' => 'gestion',
                                                            'sectionid' => 'oeuvres',
                                                            'detail' => 'ok',
                                                            'sourcerub' => 'intranet',
                                                            'sourcepg' => 'gestion',
                                                            'param1' => 'sectionid',
                                                            'vparam1' => 'oeuvres',
                                                            'signet' => 'ovr'.$par->PK_OEUVRE_OVR,
                                                            'id_oeuvre' => $par->PK_OEUVRE_OVR,
                                                        ]));
                                                        if (!empty($par->TX_TITRE_LATIN_OVR)) {
                                                            echo <<<HTML
                                                                <li>
                                                                    <a href="{$url}" target="_blank">{$par->TX_TITRE_LATIN_OVR}</a>
                                                                </li>
                                                            HTML;
                                                        } else {
                                                            echo <<<HTML
                                                                <li>
                                                                    <a href="{$url}" target="_blank">{$par->TX_TITRE_FRANCAIS_OVR}</a>
                                                                </li>
                                                            HTML;
                                                        }
                                                    }
                                                ?>
                                            </ul>
                                            <hr>
                                            <ul class="list_works_au">
                                                <?php
                                                    echo <<<HTML
                                                        <h5>&#338;uvres en cours de révision à SC ({$nbEnregPrep})</h5>
                                                    HTML;
                                                    while ($prepasc = DbEnregSuivant($oRs_OEP)) {
                                                        $url = sprintf('/?%s', http_build_query([
                                                            'rubriqueid' => 'intranet',
                                                            'pageid' => 'gestion',
                                                            'sectionid' => 'oeuvres',
                                                            'detail' => 'ok',
                                                            'sourcerub' => 'intranet',
                                                            'sourcepg' => 'gestion',
                                                            'param1' => 'sectionid',
                                                            'vparam1' => 'oeuvres',
                                                            'signet' => 'ovr'.$prepasc->PK_OEUVRE_OVR,
                                                            'id_oeuvre' => $prepasc->PK_OEUVRE_OVR,
                                                        ]));
                                                        if (!empty($prepasc->TX_TITRE_LATIN_OVR)) {
                                                            echo <<<HTML
                                                                <li>
                                                                    <a href="{$url}" target="_blank">{$prepasc->TX_TITRE_LATIN_OVR}</a>
                                                                </li>
                                                            HTML;
                                                        } else {
                                                            echo <<<HTML
                                                                <li>
                                                                    <a href="{$url}" target="_blank">{$prepasc->TX_TITRE_FRANCAIS_OVR}</a>
                                                                </li>
                                                            HTML;
                                                        }
                                                    }
                                                ?>
                                            </ul>
                                            <hr>
                                            <ul class="list_works_au">
                                                <?php
                                                    echo <<<HTML
                                                        <h5>&#338;uvres en chantier ({$nbEnregCha})</h5>
                                                    HTML;
                                                    while ($prevmt = DbEnregSuivant($oRs_OPMT)) {
                                                        $url = sprintf('/?%s', http_build_query([
                                                            'rubriqueid' => 'intranet',
                                                            'pageid' => 'gestion',
                                                            'sectionid' => 'oeuvres',
                                                            'detail' => 'ok',
                                                            'sourcerub' => 'intranet',
                                                            'sourcepg' => 'gestion',
                                                            'param1' => 'sectionid',
                                                            'vparam1' => 'oeuvres',
                                                            'signet' => 'ovr'.$prevmt->PK_OEUVRE_OVR,
                                                            'id_oeuvre' => $prevmt->PK_OEUVRE_OVR,
                                                        ]));
                                                        if (!empty($prevmt->TX_TITRE_LATIN_OVR)) {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$prevmt->TX_TITRE_LATIN_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        } else {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$prevmt->TX_TITRE_FRANCAIS_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        }
                                                    }
                                                ?>
                                            </ul>
                                            <hr>
                                            <ul class="list_works_au">
                                                <?php
                                                    echo <<<HTML
                                                        <h5>&#338;uvres non prévues ({$nbEnregNPrev})</h5>
                                                    HTML;
                                                    while ($opa = DbEnregSuivant($oRs_OPA)) {
                                                        $url = sprintf('/?%s', http_build_query([
                                                            'rubriqueid' => 'intranet',
                                                            'pageid' => 'gestion',
                                                            'sectionid' => 'oeuvres',
                                                            'detail' => 'ok',
                                                            'sourcerub' => 'intranet',
                                                            'sourcepg' => 'gestion',
                                                            'param1' => 'sectionid',
                                                            'vparam1' => 'oeuvres',
                                                            'signet' => 'ovr'.$opa->PK_OEUVRE_OVR,
                                                            'id_oeuvre' => $opa->PK_OEUVRE_OVR,
                                                        ]));
                                                        if (!empty($opa->TX_TITRE_LATIN_OVR)) {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$opa->TX_TITRE_LATIN_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        } else {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$opa->TX_TITRE_FRANCAIS_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        }
                                                    }
                                                ?>
                                            </ul>
                                            <hr>
                                            <ul class="list_works_au">
                                                <?php
                                                if ($nbEnregEnv > 0) {
                                                    echo <<<HTML
                                                        <h5>&#338;uvres envisageables ({$nbEnregEnv})</h5>
                                                    HTML;
                                                    while ($ol = DbEnregSuivant($oRs_OL)) {
                                                        $url = sprintf('/?%s', http_build_query([
                                                            'rubriqueid' => 'intranet',
                                                            'pageid' => 'gestion',
                                                            'sectionid' => 'oeuvres',
                                                            'detail' => 'ok',
                                                            'sourcerub' => 'intranet',
                                                            'sourcepg' => 'gestion',
                                                            'param1' => 'sectionid',
                                                            'vparam1' => 'oeuvres',
                                                            'signet' => 'ovr'.$ol->PK_OEUVRE_OVR,
                                                            'id_oeuvre' => $ol->PK_OEUVRE_OVR,
                                                        ]));
                                                        if (!empty($ol->TX_TITRE_LATIN_OVR)) {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$ol->TX_TITRE_LATIN_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        } else {
                                                            echo <<<HTML
                                                            <li>
                                                                <a href="{$url}" target="_blank">{$ol->TX_TITRE_FRANCAIS_OVR}</a>
                                                            </li>
                                                            HTML;
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
        <?php } ?>
    <?php } ?>

<!------------------------>
<!--Bouton de validation-->
<!------------------------>
    <tr>
                    <td colspan="3" align="center">
                        <a name="validation">
                                <input
                                        type="button"
                                        name="btn_valider"
                                        class="btn"
                                        value="&nbsp;&nbsp;<?php echo empty($id_autancien) ?
                                        "Créer l'auteur ancien" : 'Valider les modifications'; ?>&nbsp;&nbsp;"
                                        onclick="javascript:testFormulaireAutancien();"
                                        <?php if ($rights_ext) { ?> style="display: none" <?php } ?>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </td>
</tr>

<?php DbClose($oConnexion); ?>
