<?php
$oConnexion = DbConnection();
$id_volume = '';

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

$vol_numcollection = '';

$reimp_supprimee = false;
$isbn = '';

$ddl_reimpression = '';
$vrp_isbn = '';
$vrp_date_decision = '';
$vrp_date_bat = '';
$vrp_date_sortie = '';
$vrp_prix = '';
$vrp_nb_pages = '';

$vrpremarques = '';
$vrpremarques_confidentielles = '';

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}

if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%Réimpressions de volume%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC' || $usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Réimpressions de volume%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Réimpressions de volume%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if ('creationreimpression' == $_GET['action']) {
    // On charge les informations de la réimpression de volume
    $id_volume = $_GET['id_volume'];
    $sQuery = <<<SQL
                SELECT NM_NUMERO_COLLECTION_VIF,
                       TX_NUMCOLL_COMPLEMENT_VIF
                FROM sc_t_volume AS V
                WHERE PK_VOLUMEINFOS_VIF = {$id_volume};
            SQL;
    $oRecordset = DbExecRequete($sQuery, $oConnexion);
    while($vrp = DbEnregSuivant($oRecordset)){
        $vol_numcollection = $vrp->NM_NUMERO_COLLECTION_VIF;
        $vol_numcoll_complt = $vrp->TX_NUMCOLL_COMPLEMENT_VIF;
    }
}

if (!isset($_POST['recupform']) && isset($_GET['id_volume']) && $_GET['action'] != 'creationreimpression') {
    $id_volume = $_GET['id_volume'];

    $sQuery = <<<SQL
        SELECT 
            NM_NUMERO_COLLECTION_VIF,
            TX_NUMCOLL_COMPLEMENT_VIF
        FROM sc_t_volume
        WHERE PK_VOLUMEINFOS_VIF = {$id_volume};
    SQL;
    $oRecordset = DbExecRequete($sQuery, $oConnexion);

    if (1 == DbNbreEnreg($oRecordset)) {
        $vol = DbEnregSuivant($oRecordset);
        (empty($vol->NM_NUMERO_COLLECTION_VIF)) ? ($vol_numcollection = '???') : ($vol_numcollection = $vol->NM_NUMERO_COLLECTION_VIF);
        $vol_numcoll_complt = $vol->TX_NUMCOLL_COMPLEMENT_VIF;
    } else { // Erreur : aucun volume ne correspond à cet identifiant
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
    }

    if (isset($_GET['id_vol_reimpr'])) {
        $sQueryDate = <<<SQL
        SELECT *
        FROM sc_t_volume_reimpr
        WHERE
            FK_VRP_REF_VOL = {$id_volume}
        AND DT_SORTIE_VRP = "{$_GET['id_vol_reimpr']}";
        SQL;
        $oRecordset = DbExecRequete($sQueryDate, $oConnexion);
        while ($fetch = DbEnregSuivant($oRecordset)) {
            $ddl_reimpression = $fetch->PK_VOLREIMPR_VRP;
            $vrp_isbn = $fetch->TX_ISBN_VRP;
            $vrp_date_decision = $fetch->DT_DECISION_REIMP_VRP;
            $vrp_date_sortie = $fetch->DT_SORTIE_VRP;
            $vrp_date_bat = $fetch->DT_BAT_VRP;
            $vrp_prix = $fetch->NM_PRIX_VRP;
            $vrp_nb_pages = $fetch->NM_NOMBRE_PAGES_VRP;
            $vrpremarques = $fetch->TX_REMARQUES1_VRP;
            $vrpremarques_confidentielles = $fetch->TX_REMARQUES2_VRP;
        }
    } else {
        $sQuery = <<<SQL
            SELECT *
            FROM sc_t_volume_reimpr
            WHERE FK_VRP_REF_VOL = {$id_volume}
            ORDER BY DT_SORTIE_VRP DESC, DT_BAT_VRP DESC
        SQL;

        $oRecordset = DbExecRequete($sQuery, $oConnexion);
        if (DbNbreEnreg($oRecordset) > 0) {
            // Par défaut, on se positionne sur le dernier
            $vrp = DbEnregSuivant($oRecordset);

            $ddl_reimpression = $vrp->PK_VOLREIMPR_VRP;

            $vrp_isbn = $vrp->TX_ISBN_VRP;
            $vrp_date_decision = $vrp->DT_DECISION_REIMP_VRP;
            $vrp_date_bat = $vrp->DT_BAT_VRP;
            $vrp_date_sortie = $vrp->DT_SORTIE_VRP;
            $vrp_prix = $vrp->NM_PRIX_VRP;
            $vrp_nb_pages = $vrp->NM_NOMBRE_PAGES_VRP;

            $vrpremarques = $vrp->TX_REMARQUES2_VRP;
            $vrpremarques_confidentielles = $vrp->TX_REMARQUES1_VRP;
        }
    }
} elseif (null != $_POST) { 	// L'utilisateur a soumis le formulaire
    $id_volume = $_POST['id_volume'];
    $vol_numcollection = $_POST['vol_numcollection'];
    $vol_numcoll_complt = $_POST['vol_numcoll_complt'];

    if (isset($_POST['ddl_reimpression'])) {
        $ddl_reimpression = $_POST['ddl_reimpression'];
    }

    if (isset($_GET['action'])) {
        if ('chargerreimpression' == $_GET['action']) {
            // On charge les informations de la réimpression de volume
            $sQuery = <<<SQL
                SELECT *
                FROM sc_t_volume_reimpr
                WHERE PK_VOLREIMPR_VRP = {$ddl_reimpression};
            SQL;
            $oRecordset = DbExecRequete($sQuery, $oConnexion);
            if (1 == DbNbreEnreg($oRecordset)) {
                $vrp = DbEnregSuivant($oRecordset);

                $vrp_isbn = $vrp->TX_ISBN_VRP;
                $vrp_date_decision = $vrp->DT_DECISION_REIMP_VRP;
                $vrp_date_bat = $vrp->DT_BAT_VRP;
                $vrp_date_sortie = $vrp->DT_SORTIE_VRP;
                $vrp_prix = $vrp->NM_PRIX_VRP;
                $vrp_nb_pages = $vrp->NM_NOMBRE_PAGES_VRP;

                $vrpremarques = $vrp->TX_REMARQUES2_VRP;
                $vrpremarques_confidentielles = $vrp->TX_REMARQUES1_VRP;
            }
        } elseif ('supprimerreimpression' == $_GET['action']) {
            if ($rights_admin) {
                $reimp_supprimee = true;
                $req = <<<SQL
                DELETE FROM sc_t_volume_reimpr
                WHERE PK_VOLREIMPR_VRP = {$ddl_reimpression};
            SQL;
                $exec = DbExecRequete($req, $oConnexion);

                if (mysqli_affected_rows($oConnexion)) {
                    // insert a trace of the addition
                    $today = date('Y-m-d H:i:s');
                    $sQueryTrace = <<<SQL
                    INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                    VALUES('VOLUME_REIMPRESSION',
                           {$ddl_reimpression},
                           'Suppression de la réimpression {$ddl_reimpression} pour le volume n°{$vol_numcollection} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                           'SQL-D',
                           "{$today}",
                           {$_SESSION['PK_UTILISATEUR_USR']}
                           );
                    SQL;
                    $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
                }

                $ddl_reimpression = '';

                addNotice('success', 'La réimpression de volume a été supprimée avec succès !');

                redirect(sprintf('/?%s', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => $_REQUEST['sectionid'],
                    'detail' => 'ok6',
                    'param1' => 'sectionid',
                    'vparam1' => 'reimpressions',
                    'signet' => 'vol'.$id_volume,
                    'id_volume' => $id_volume
                ])));
            }
        } elseif ('nouvellereimpression' == $_GET['action']) {
            $ddl_reimpression = '';
        }
    } else {
        $vrp_isbn = _trim($_POST['vrp_isbn']);
        if (isset($_POST['vrp_date_decision'])) {
            $vrp_date_decision = $_POST['vrp_date_decision'];
        }
        if (isset($_POST['vrp_date_bat'])) {
            $vrp_date_bat = $_POST['vrp_date_bat'];
        }
        if (isset($_POST['vrp_date_sortie'])) {
            $vrp_date_sortie = $_POST['vrp_date_sortie'];
        }

        (empty(_trim($_POST['vrp_prix']))) ? $vrp_prix = 'NULL' : $vrp_prix = _trim($_POST['vrp_prix']);
        (empty(_trim($_POST['vrp_nb_pages']))) ? $vrp_nb_pages = 'NULL' : $vrp_nb_pages = _trim($_POST['vrp_nb_pages']);

        $vrpremarques = _trim($_POST['vrpremarques']);
        $vrpremarques_confidentielles = _trim($_POST['vrpremarques_confidentielles']);

        begin($oConnexion);

        if (empty($ddl_reimpression)) {	// On insère une nouvelle réimpression de volume
            // check if the volume already exists in database
            $req = <<<SQL
                SELECT
                    FK_VRP_REF_VOL,
                    TX_ISBN_VRP,
                    DT_DECISION_REIMP_VRP,
                    DT_SORTIE_VRP,
                    DT_BAT_VRP,
                    NM_PRIX_VRP,
                    NM_NOMBRE_PAGES_VRP,
                    TX_REMARQUES1_VRP,
                    TX_REMARQUES2_VRP
                FROM sc_t_volume_reimpr
                WHERE
                    FK_VRP_REF_VOL = {$id_volume} AND
                    TX_ISBN_VRP = "{$vrp_isbn}" AND
                    DT_DECISION_REIMP_VRP = "{$vrp_date_decision}" AND
                    DT_SORTIE_VRP = "{$vrp_date_bat}" AND
                    DT_BAT_VRP = "{$vrp_date_sortie}" AND
                    NM_PRIX_VRP = {$vrp_prix} AND
                    NM_NOMBRE_PAGES_VRP = {$vrp_nb_pages} AND
                    TX_REMARQUES1_VRP = "{$vrpremarques}" AND
                    TX_REMARQUES2_VRP = "{$vrpremarques_confidentielles}";
            SQL;
            $exec = DbExecRequete($req, $oConnexion);
            $volume_exists = (bool) mysqli_num_rows($exec);

            // create the volume if it does not exist in database
            if (!$volume_exists) {
                $sQuery = <<<SQL
                INSERT INTO sc_t_volume_reimpr(FK_VRP_REF_VOL,
                                               TX_ISBN_VRP,
                                               DT_DECISION_REIMP_VRP,
                                               DT_SORTIE_VRP,
                                               DT_BAT_VRP,
                                               NM_PRIX_VRP,
                                               NM_NOMBRE_PAGES_VRP,
                                               TX_REMARQUES1_VRP,
                                               TX_REMARQUES2_VRP)
                VALUES(
                    {$id_volume},
                    "{$vrp_isbn}",
                    "{$vrp_date_decision}",
                    "{$vrp_date_bat}",
                    "{$vrp_date_sortie}",
                    {$vrp_prix},
                    {$vrp_nb_pages},
                    "{$vrpremarques}",
                    "{$vrpremarques_confidentielles}");
            SQL;

                $oRecordset = DbExecRequete($sQuery, $oConnexion);

                $reimpr_id = mysqli_insert_id($oConnexion);
                global $id_reimpr;
                $id_reimpr = $reimpr_id;

                if (mysqli_affected_rows($oConnexion)) {
                    // insert a trace of the addition
                    $req = <<<SQL
                    SELECT TX_NUMCOLL_NOUVEAU_VIF
                    FROM sc_t_volume AS V
                    LEFT JOIN sc_t_volume_reimpr AS VR
                        ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                    WHERE VR.FK_VRP_REF_VOL = {$id_volume};
                    SQL;
                    $oRecordset = DbExecRequete($req, $oConnexion);
                    while ($fetch = DbEnregSuivant($oRecordset)) {
                        $num_vol = $fetch->TX_NUMCOLL_NOUVEAU_VIF;
                    }

                    $today = date('Y-m-d H:i:s');
                    $sQueryTrace = <<<SQL
                    INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                    VALUES('VOLUME_REIMPRESSION',
                           {$id_reimpr},
                           'Ajout de la réimpression {$id_reimpr} pour le volume n°{$vol_numcollection} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                           'SQL-I',
                           "{$today}",
                           {$_SESSION['PK_UTILISATEUR_USR']}
                           );
                    SQL;
                    $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
                }

                commit($oConnexion);

                addNotice('success', 'Réimpression correctement ajoutée');

                if ('reimpressions' == $_REQUEST['vparam1']) {
                    redirect(sprintf('/?%s', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'gestion',
                        'sectionid' => $_REQUEST['sectionid'],
                        'detail' => 'ok6',
                        'param1' => 'sectionid',
                        'vparam1' => 'reimpressions',
                        'signet' => 'vol'.$id_volume,
                        'id_volume' => $id_volume
                    ])));
                } else {
                    redirect(sprintf('/?%s', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'gestion',
                        'sectionid' => $_REQUEST['sectionid'],
                        'detail' => 'ok6',
                        'param1' => 'sectionid',
                        'vparam1' => 'volumes',
                        'signet' => 'vol'.$id_volume,
                        'id_volume' => $id_volume
                    ])));
                }
            } else {
                addNotice('danger', 'Réimpression déjà existante !');
            }
        } else { // On met à jour la réimpression de volume existante
            $sQuery = <<<SQL
                UPDATE sc_t_volume_reimpr
                SET
                    TX_ISBN_VRP = "{$vrp_isbn}",
                    DT_DECISION_REIMP_VRP = "{$vrp_date_decision}",
                    DT_SORTIE_VRP = "{$vrp_date_sortie}",
                    DT_BAT_VRP = "{$vrp_date_bat}",
                    NM_PRIX_VRP = {$vrp_prix},
                    NM_NOMBRE_PAGES_VRP = {$vrp_nb_pages},
                    TX_REMARQUES1_VRP = "{$vrpremarques}",
                    TX_REMARQUES2_VRP = "{$vrpremarques_confidentielles}"
                WHERE PK_VOLREIMPR_VRP = {$ddl_reimpression};
            SQL;
            $oRecordset = DbExecRequete($sQuery, $oConnexion);

            if (mysqli_affected_rows($oConnexion)) {
                // insert a trace of the update
                $today = date('Y-m-d H:i:s');

                $req = <<<SQL
                DELETE FROM sc_t_trace
                WHERE
                    DT_TRACE_TRC = "{$today}"
                AND NM_UTILISATEUR_TRC = {$_SESSION['PK_UTILISATEUR_USR']}
                AND TX_DOMAINE_TRC = 'VOLUME_REIMPRESSION'
                AND NM_ITEMID_TRC = {$ddl_reimpression};
            SQL;
                $exec = DbExecRequete($req, $oConnexion);

                $req = <<<SQL
                    SELECT TX_NUMCOLL_NOUVEAU_VIF
                    FROM sc_t_volume AS V
                    LEFT JOIN sc_t_volume_reimpr AS VR
                        ON V.PK_VOLUMEINFOS_VIF = VR.FK_VRP_REF_VOL
                    WHERE VR.FK_VRP_REF_VOL = {$id_volume};
                    SQL;

                $oRecordset = DbExecRequete($req, $oConnexion);
                while ($fetch = DbEnregSuivant($oRecordset)) {
                    $num_vol = $fetch->TX_NUMCOLL_NOUVEAU_VIF;
                }

                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('VOLUME_REIMPRESSION',
                       {$ddl_reimpression},
                       'Mise à jour de la réimpression {$ddl_reimpression} du volume n°{$vol_numcollection} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-U',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
            }
        }

        commit($oConnexion);

        addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');

        if ('reimpressions' == $_REQUEST['vparam1']) {
            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => $_REQUEST['sectionid'],
                'detail' => 'ok6',
                'param1' => 'sectionid',
                'vparam1' => 'reimpressions',
                'signet' => 'vol'.$id_volume,
                'id_volume' => $id_volume
            ])));
        } else {
            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => $_REQUEST['sectionid'],
                'detail' => 'ok6',
                'param1' => 'sectionid',
                'vparam1' => 'reimpressions',
                'signet' => 'vol'.$id_volume,
                'id_volume' => $id_volume
            ])));
        }
    }
} else { // Mode création - Premier chargement de la page
    // ...
}
?>
<script type="text/javascript" src="js/tiny_mce_v349/tiny_mce_gzip.js"></script>
<script type="text/javascript" src="js/tinymce-launcher_v349_20120420.js"></script>
<script language="javascript" src="js/detail6-volumes.js"></script>
<input type="hidden" name="recupform" value="ok">
<input type="hidden" name="id_volume" value="<?php echo $id_volume; ?>">
<input type="hidden" name="vol_numcollection" value="<?php echo $vol_numcollection; ?>">
<input type="hidden" name="vol_numcoll_complt" value="<?php echo $vol_numcoll_complt; ?>">
<!-- ===== Bouton 'Retour' / DEBUT ===== -->
<input type="hidden" name="sourcerub" value="intranet">
<input type="hidden" name="sourcepg" value="gestion">
<input type="hidden" name="param1" value="sectionid">
<?php if ('reimpressions' == $_GET['vparam1']) { ?>
    <input type="hidden" name="vparam1" value="reimpressions">
<?php } else {
    ?>
    <input type="hidden" name="vparam1" value="volumes">
<?php } ?>
<?php if (isset($_GET['id_vol_reimpr'])) { ?>
    <input type="hidden" name="ddl_reimpression" value="<?php echo $ddl_reimpression; ?>">
<?php } ?>
<?php if (isset($_REQUEST['signet'])) { ?>
<input type="hidden" name="signet" value="<?php if (isset($_REQUEST['signet'])) {
    echo $_REQUEST['signet'];
} elseif (isset($_REQUEST['oldsignet'])) {
    echo $_REQUEST['oldsignet'];
} ?>">
<?php } ?>
<!-- ===== Bouton 'Retour' / FIN ===== -->


<?php
if (!empty($ddl_reimpression)) {
    $sQuery = <<<SQL
                SELECT *
                FROM sc_t_trace
                WHERE
                    TX_DOMAINE_TRC = 'VOLUME_REIMPRESSION'
                AND LB_TRACE_TRC LIKE '%n°{$vol_numcollection}%'
                ORDER BY DT_TRACE_TRC DESC LIMIT 3;
            SQL;

    $oRs = DbExecRequete($sQuery, $oConnexion);
    while ($trace = DbEnregSuivant($oRs)) {
        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
                    </td>
                </tr>
                HTML;
    }
    if (0 == DbNbreEnreg($oRs)) {
        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        Historique non exhaustif, en particulier pour les dernières réimpressions (informations non communiquées par les Éditions du Cerf)
                    </td>
                </tr>
                HTML;
    }
    ?>
<?php } ?>


<?php
if ('reimpressions' == $_REQUEST['vparam1'] && 'nouvellereimpression' != $_REQUEST['action']) {
    $url = sprintf('/?%s#back', http_build_query([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => $_REQUEST['sectionid'],
        'ddlsectionid' => 'reimpressions',
    ]));
} elseif('volumes' == $_REQUEST['vparam1']) {
    $url = sprintf('/?%s#back', http_build_query([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => $_REQUEST['sectionid'],
        'ddlsectionid' => 'volumes',
    ]));
} elseif ('creationreimpression' == $_REQUEST['action']){
    $url = sprintf('/?%s#back', http_build_query([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => $_REQUEST['sectionid'],
        'ddlsectionid' => 'volumes'
    ]));
} elseif ('nouvellereimpression' == $_REQUEST['action']){
    $url = sprintf('/?%s#back', http_build_query([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => $_REQUEST['sectionid'],
        'detail' => 'ok6',
        'param1' => 'sectionid',
        'vparam1' => 'reimpressions',
        'signet' => 'vol'.$id_volume,
        'id_volume' => $id_volume
    ]));
}

?>
    <tr>
        <td colspan="3" align="center">
            <a href="<?php echo $url; ?>">
                <img src="img/common/retour.gif" width="91" height="24" border="0" style="cursor:hand"></a>
        </td>
    </tr>

<tr>
    <td align="center">
        <?php
if (isset($_GET['id_vol_reimpr']) || !empty($ddl_reimpression) || $reimp_supprimee) { ?>
    Réimpression de volume
    <select name="ddl_reimpression" style="font-size: medium" onchange="javascript:chargerReimpression();">
        <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_volume_reimpr
        WHERE FK_VRP_REF_VOL = {$id_volume}
        ORDER BY DT_SORTIE_VRP DESC, DT_BAT_VRP DESC;
    SQL;

    $oRecordset = DbExecRequete($sQuery, $oConnexion);
    while ($vrp = DbEnregSuivant($oRecordset)) {?>
            <option value="<?php echo $vrp->PK_VOLREIMPR_VRP; ?>" <?php if ($vrp->PK_VOLREIMPR_VRP == $ddl_reimpression) { ?>
                selected
            <?php }?>>
                <?php
                $date = $vrp->DT_SORTIE_VRP ?? null;
                $year = preg_match('/([0-9]{4})/', $date, $matches) ? $matches[1] : '????';
                echo $year;
        ('' == $vrp->TX_ISBN_VRP) ? ($isbn = 'non renseigné') : ($isbn = $vrp->TX_ISBN_VRP); ?>
                - ISBN :
                <?php echo $isbn; ?>
            </option>
        <?php } ?>
    </select> du volume
    <b><?php echo $vol_numcollection; echo $vol_numcoll_complt; ?></b>
    <br><input type="button" name="btn_nouveau"
               value="Créer une nouvelle réimpression pour ce volume"
               onclick="javascript:nouvelleReimpression();"
               <?php if ($rights_ext) { ?> style="display: none" <?php } ?>>
<?php } else { ?>
        Création d'une réimpression du volume <b><?php echo $vol_numcollection; echo $vol_numcoll_complt; ?></b>
<?php } ?>
    </td>
</tr>


<?php if (hasNotice()): ?>
    <tr>
        <td align="center">
            <?php printNotice(); ?>
        </td>
    </tr>
<?php endif; ?>
    <tr>
        <td width="100%">
            <table border="0" width="100%" cellpadding="4" cellspacing="0">
                <?php
                $ligneType = 'B';

$url_reimpr = sprintf('/?%s', http_build_query([
    'rubriqueid' => 'intranet',
    'pageid' => 'gestion',
    'ddlsectionid' => 'reimpressions',
]));
?>
                <tr>
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal"><font class="font-datagrid-entete"><b>Identité</b></font></font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                                </td>
                        </table>
                    </td>
                </tr>

                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">ISBN :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <input type="text" name="vrp_isbn" value="<?php echo $vrp_isbn; ?>" size="40" maxlength="13">
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Date de la décision :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <?php if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])$/', $vrp_date_decision)) { ?>
                            <input type="month" name="vrp_date_decision"
                                   value="<?php echo $vrp_date_decision; ?>"
                        <?php } elseif (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $vrp_date_decision)) { ?>
                            <input type="date" name="vrp_date_decision"
                                   value="<?php echo $vrp_date_decision; ?>"
                        <?php } else { ?>
                            <input type="text" name="vrp_date_decision"
                                   value="<?php echo $vrp_date_decision; ?>"
                        <?php } ?>
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Date du BAT :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <?php if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])$/', $vrp_date_bat)) { ?>
                            <input type="month" name="vrp_date_bat"
                                   value="<?php echo $vrp_date_bat; ?>"
                        <?php } elseif (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $vrp_date_bat)) { ?>
                            <input type="date" name="vrp_date_bat"
                                   value="<?php echo $vrp_date_bat; ?>"
                        <?php } else { ?>
                            <input type="text" name="vrp_date_bat"
                                   value="<?php echo $vrp_date_bat; ?>"
                        <?php } ?>
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Date de sortie effective :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <?php ?>
                    <td align="center">
                        <?php if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])$/', $vrp_date_sortie)) { ?>
                        <input type="month" name="vrp_date_sortie"
                               value="<?php echo $vrp_date_sortie; ?>"
                        <?php } elseif (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $vrp_date_sortie)) { ?>
                        <input type="date" name="vrp_date_sortie"
                               value="<?php echo $vrp_date_sortie; ?>"
                        <?php } else { ?>
                        <input type="text" name="vrp_date_sortie"
                               value="<?php echo $vrp_date_sortie; ?>"
                        <?php } ?>
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Prix (en euros) :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <input type="text" name="vrp_prix" value="<?php echo $vrp_prix; ?>" size="40" maxlength="10">
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Nombre de pages <font class="font-small">(<em>y compris pages non numérotées</em>) :</font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <input type="text" name="vrp_nb_pages" value="<?php echo $vrp_nb_pages; ?>" size="40" maxlength="10">
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Remarques :<br><font class="font-small"><em>données publiques<br>Ce champ permet notamment de préciser<br> quelles ont été les modifications apportées par la réimpression.</em></font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <textarea id="vrpremarques" name="vrpremarques" rows="10" cols="90" class="mceEditor"><?php echo $vrpremarques; ?></textarea>
                    </td>
                </tr>
                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" nowrap>
                        <font class="font-normal">Remarques confidentielles :<br><font class="font-small"><em>données privées</em></font>
                    </td>
                    <td width="25">&nbsp;</td>
                    <td align="center">
                        <textarea id="vrpremarques_confidentielles" name="vrpremarques_confidentielles" rows="10" cols="90" class="mceEditor"><?php echo $vrpremarques_confidentielles; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <font class="font-normal">&nbsp;</font>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <a name="validation">
                            <input type="button" name="btn_valider" class="btn"
                                   value="<?php if (!empty($ddl_reimpression)) { ?>Valider les modifications<?php } else { ?>Créer la réimpression de volume<?php } ?>"
                                   onclick="javascript:majReimpression();"
                                   <?php if ($rights_ext) { ?> style="display: none" <?php } ?>>
                        </a>
                        <?php
        if (!empty($ddl_reimpression) && $rights_admin) {?>
                            <br><br><input type="button" name="btn_supprimer" class="btn" value="&nbsp;&nbsp;<?php echo 'Supprimer cette réimpression de volume'; ?>&nbsp;&nbsp;" onclick="javascript:if(confirm('Êtes-vous sûr de vouloir supprimer cette réimpression de volume ?')) { supprimerReimpression(); }">
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </td>
</tr>
<?php
DbClose($oConnexion);
?>