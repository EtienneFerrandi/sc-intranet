<?php
$oConnexion = DbConnection();

$identifiant_deja_pris = false;

$civilite = '';
$nom = '';
$prenom = '';
$profil = '';
$fonction = '';
$identifiant = '';
$motdepasse = '';

$naissance_date = '';
$deces_date = '';
$decede = '';

$utilisateur_sc_numint = '';

$perso_adresse = '';
$perso_tel_fixe = '';
$perso_tel_mobile = '';
$perso_email = '';

$pro_adresse = '';
$pro_tel_fixe = '';
$pro_tel_mobile = '';
$pro_email = '';
$pro_url = '';
$remarques = '';

$id_utilisateur = '';
$motdepasse_actuel = '';

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

$reqPfl = <<<SQL
    SELECT FK_APT_REF_PFL
    FROM sc_t_utilisateur
    WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
$oRsPfl = DbExecRequete($reqPfl, $oConnexion);
while ($fetchPfl = DbEnregSuivant($oRsPfl)) {
    $usr_pfl = $fetchPfl->FK_APT_REF_PFL;
}

if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%utilisateurs%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC' || $usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT *
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%utilisateurs%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT *
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%utilisateurs%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (!isset($_POST['recupform']) && isset($_GET['id_utilisateur'])) {
    if (!empty($_GET['id_utilisateur'])) {
        $id_utilisateur = $_GET['id_utilisateur'];

        // Données utilisateur

        $sQueryVol = <<<SQL
            SELECT
                ot.FK_OUT_REF_USR,
                ot.FK_OUT_REF_VOL,
                V.NM_NUMERO_COLLECTION_VIF
            FROM sc_t_assoc_ovr_usr_tch AS ot
            LEFT JOIN sc_t_volume AS V
                ON ot.FK_OUT_REF_VOL = V.PK_VOLUMEINFOS_VIF
            WHERE ot.FK_OUT_REF_USR = {$id_utilisateur}
            AND ot.FK_OUT_REF_VOL IS NOT NULL
            GROUP BY ot.FK_OUT_REF_VOL
            ORDER BY V.NM_NUMERO_COLLECTION_VIF;
            SQL;
        $oRecordsetVol = DbExecRequete($sQueryVol, $oConnexion);

        $sQuery = <<<SQL
                SELECT *
                FROM sc_t_utilisateur
                WHERE PK_UTILISATEUR_USR = {$id_utilisateur};
                SQL;

        $oRecordset = DbExecRequete($sQuery, $oConnexion);
        if (1 == DbNbreEnreg($oRecordset)) {
            $usr = DbEnregSuivant($oRecordset);

            $profil = $usr->FK_APT_REF_PFL;

            $motdepasse = $usr->TX_PASSWORD_USR;
            $motdepasse_actuel = $motdepasse;

            $identifiant = $usr->TX_LOGIN_USR;
            $civilite = $usr->TX_CIVILITE_USR;
            $nom = $usr->TX_NOM_USR;
            $prenom = $usr->TX_PRENOM_USR;
            $fonction = $usr->TX_FONCTION_USR;

            (empty($usr->DT_NAISSANCE_USR)) ? ($naissance_date = '') : ($naissance_date = $usr->DT_NAISSANCE_USR);
            (empty($usr->DT_DECES_USR)) ? ($deces_date = '') : ($deces_date = $usr->DT_DECES_USR);

            if (!empty($deces_date)) { // Si l'année du décès est renseignée, l'utilisateur est nécessairement décédé ...
                $decede = '1';
            } else {
                $decede = $usr->BL_DECEDE_USR;
            }

            $utilisateur_sc_numint = $usr->TX_TEL_NUMERO_INTERNE_USC;

            $perso_adresse = $usr->TX_PERSO_ADRESSE_USR;
            $perso_tel_fixe = $usr->TX_PERSO_TEL_FIXE_USR;
            $perso_tel_mobile = $usr->TX_PERSO_TEL_MOBILE_USR;
            $perso_email = $usr->TX_PERSO_EMAIL_USR;

            $pro_adresse = $usr->TX_PRO_ADRESSE_USR;

            $pro_tel_fixe = $usr->TX_PRO_TEL_FIXE_USR;
            $pro_tel_mobile = $usr->TX_PRO_TEL_MOBILE_USR;
            $pro_email = $usr->TX_PRO_EMAIL_USR;
            $pro_url = $usr->TX_PRO_URL_USR;

            $remarques = $usr->TX_REMARQUES_USR;
        } else { // Erreur : aucun utilisateur ne correspond à cet identifiant
            header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
        }
    }
} elseif (null != $_POST) { // L'utilisateur a soumis le formulaire
    $letter = $_POST['vparam3'];
    $id_utilisateur = $_POST['id_utilisateur'];

    $profil = $_POST['profil'];

    $civilite = (string) _trim($_POST['civilite']);
    $nom = mb_strtoupper((string) _trim($_POST['nom']));
    $fonction = (string) _trim($_POST['fonction']);

    if (!empty(_trim($_POST['prenom']))) {
        $prenom = _prenom(mb_strtoupper(mb_substr((string) _trim($_POST['prenom']), 0, 1))
                .mb_strtolower(mb_substr((string) _trim($_POST['prenom']), 1)));

        $pos1 = mb_strpos((string) $prenom, '.');

        $pos2 = mb_strpos((string) $prenom, '.', $pos1 + 1);
        if (null != $pos2 && $pos2 > 1) {
            $prenom = mb_substr((string) $prenom, 0, $pos2 - 1).mb_strtoupper(mb_substr((string) $prenom, $pos2 - 1, 1)).mb_substr((string) $prenom, $pos2);
        }
    }

    if (isset($_POST['identifiant'])) {
        $identifiant = _trim($_POST['identifiant']);
    }
    if (isset($_POST['motdepasse'])) {
        $motdepasse = trim($_POST['motdepasse']);
    }
    if(isset($_POST['motdepasse'])){
        $hash_password = password_hash($motdepasse, PASSWORD_ARGON2ID, ["cost" => 13]);
    }
    if (isset($_POST['motdepasse_actuel'])) {
        $motdepasse_actuel = trim($_POST['motdepasse_actuel']);
    }
    if(isset($_POST['motdepasse_actuel'])){
        $hash_password_actuel = password_hash($motdepasse_actuel, PASSWORD_ARGON2ID, ["cost" => 13]);
    }
    if (isset($_POST['naissance_date'])) {
        $naissance_date = $_POST['naissance_date'];
    }

    $deces_date = $_POST['deces_date'] ?? null;
    $decede = $deces_date || !empty($_POST['decede']) ? '1' : '0';

    if (!empty($deces_date)) {
        $decede = '1';
    } else {
        $decede = empty($_POST['decede']) ? '0' : $_POST['decede'];
    }

    if (isset($_POST['utilisateur_sc_numint'])) {
        $utilisateur_sc_numint = _trim($_POST['utilisateur_sc_numint']);
    }
    if (isset($_POST['perso_adresse'])) {
        $perso_adresse = str_replace("\r\n", ' ', (string) _trim($_POST['perso_adresse']));
    }
    if (isset($_POST['perso_tel_fixe'])) {
        $perso_tel_fixe = _trim($_POST['perso_tel_fixe']);
    }
    if (isset($_POST['perso_tel_mobile'])) {
        $perso_tel_mobile = _trim($_POST['perso_tel_mobile']);
    }
    if (isset($_POST['perso_email'])) {
        $perso_email = _trim($_POST['perso_email']);
    }
    if (isset($_POST['pro_adresse'])) {
        $pro_adresse = _trim($_POST['pro_adresse']);
    }
    if (isset($_POST['pro_tel_fixe'])) {
        $pro_tel_fixe = _trim($_POST['pro_tel_fixe']);
    }
    if (isset($_POST['pro_tel_mobile'])) {
        $pro_tel_mobile = _trim($_POST['pro_tel_mobile']);
    }
    if (isset($_POST['pro_email'])) {
        $pro_email = _trim($_POST['pro_email']);
    }
    if (isset($_POST['pro_url'])) {
        $pro_url = _trim($_POST['pro_url']);
    }
    if (isset($_POST['remarques'])) {
        $remarques = _trim($_POST['remarques']);
    }

    if (empty($id_utilisateur)) { // INSERT...
        // On s'assure de l'unicité de l'identifiant ...
        $sQuery = <<<SQL
                    SELECT PK_UTILISATEUR_USR
                    FROM sc_t_utilisateur
                    WHERE TX_LOGIN_USR = "{$identifiant}";
                    SQL;
        $oRecordset = DbExecRequete($sQuery, $oConnexion);

        if (0 == DbNbreEnreg($oRecordset)) {
            begin($oConnexion);

            $motdepasse_actuel = $motdepasse;
            $hash_password_actuel = $hash_password;

            // ====================== INSERTION DANS T_UTILISATEUR =========================
            $remarques = mysqli_real_escape_string($oConnexion, $remarques);
            $motdepasse = mysqli_real_escape_string($oConnexion, $motdepasse);
            $hash_password = mysqli_real_escape_string($oConnexion, $hash_password);
            $sQuery = <<<SQL
                        INSERT INTO sc_t_utilisateur(TX_NOM_USR,
                                                     TX_PRENOM_USR,
                                                     TX_CIVILITE_USR,
                                                     TX_FONCTION_USR,
                                                     TX_LOGIN_USR,
                                                     TX_PASSWORD_USR,
                                                     TX_PASSWORD_USR_HASH,
                                                     FK_APT_REF_PFL,
                                                     DT_NAISSANCE_USR,
                                                     DT_DECES_USR,
                                                     TX_PERSO_ADRESSE_USR,
                                                     TX_PERSO_TEL_FIXE_USR,
                                                     TX_PERSO_TEL_MOBILE_USR,
                                                     TX_PERSO_EMAIL_USR,
                                                     TX_PRO_ADRESSE_USR,
                                                     TX_PRO_TEL_FIXE_USR,
                                                     TX_PRO_TEL_MOBILE_USR,
                                                     TX_PRO_EMAIL_USR,
                                                     TX_PRO_URL_USR,
                                                     TX_REMARQUES_USR,
                                                     TX_TEL_NUMERO_INTERNE_USC)
                        VALUES("{$nom}",
                                "{$prenom}",
                                "{$civilite}",
                                "{$fonction}",
                                "{$identifiant}",
                                "{$motdepasse}",
                                "{$hash_password}",
                                "{$profil}",
                                "{$naissance_date}",
                                "{$deces_date}",
                                "{$perso_adresse}",
                                "{$perso_tel_fixe}",
                                "{$perso_tel_mobile}",
                                "{$perso_email}",
                                "{$pro_adresse}",
                                "{$pro_tel_fixe}",
                                "{$pro_tel_mobile}",
                                "{$pro_email}",
                                "{$pro_url}",
                                "{$remarques}",
                                "{$utilisateur_sc_numint}");
                        SQL;

            $oRecordset = DbExecRequete($sQuery, $oConnexion);

            $usr_id = mysqli_insert_id($oConnexion);
            global $id_usr;
            $id_usr = $usr_id;

            if (mysqli_affected_rows($oConnexion)) {
                // insert a trace of the addition
                $str_user = addslashes("l'utilisateur");
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
            INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
            VALUES('UTILISATEUR',
                   {$id_usr},
                   'Ajout de {$str_user} {$prenom} {$nom} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                   'SQL-I',
                   "{$today}",
                   {$_SESSION['PK_UTILISATEUR_USR']}
                   );
            SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
            }
            commit($oConnexion);

            addNotice('success', 'Utilisateur correctement ajouté !');

            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'utilisateurs',
                'detail' => 'ok',
                'sourcerub' => 'intranet',
                'sourcepg' => 'gestion',
                'param1' => 'sectionid',
                'vparam1' => 'utilisateurs',
                'param3' => 'lettre',
                'vparam3' => $letter,
                'signet' => $id_usr,
                'id_utilisateur' => $id_usr
            ])));
        } else {
            if (isset($_POST['anc_identifiant'])) {
                $identifiant = $_POST['anc_identifiant'];
            } else {
                $identifiant = '';
            }

            addNotice('danger', 'Désolé, mais cet identifiant est déjà pris !');
            $identifiant_deja_pris = true;
        }
    } else { // UPDATE...
        $remarques = mysqli_real_escape_string($oConnexion, $remarques);
        $motdepasse = mysqli_real_escape_string($oConnexion, $motdepasse);
        $hash_password = mysqli_real_escape_string($oConnexion, $hash_password);
        $sQuery = <<<SQL
                    SELECT PK_UTILISATEUR_USR
                    FROM sc_t_utilisateur
                    WHERE TX_LOGIN_USR = "{$identifiant}" AND
                          PK_UTILISATEUR_USR != {$id_utilisateur};
                    SQL;

        $oRecordset = DbExecRequete($sQuery, $oConnexion);

        if (0 == DbNbreEnreg($oRecordset)) {
            begin($oConnexion);

            $motdepasse_actuel = $motdepasse;
            $hash_password_actuel = $hash_password;

            $sQuery = <<<SQL
                    UPDATE sc_t_utilisateur
                    SET FK_APT_REF_PFL = "{$profil}",
                        TX_LOGIN_USR="{$identifiant}",
                        TX_PASSWORD_USR="{$motdepasse}",
                        TX_PASSWORD_USR_HASH="{$hash_password}",
                        TX_NOM_USR="{$nom}",
                        TX_PRENOM_USR="{$prenom}",
                        TX_CIVILITE_USR="{$civilite}",
                        TX_FONCTION_USR="{$fonction}",
                        BL_DECEDE_USR="{$decede}",
                        DT_NAISSANCE_USR="{$naissance_date}",
                        DT_DECES_USR="{$deces_date}",
                        TX_PERSO_ADRESSE_USR="{$perso_adresse}",
                        TX_PERSO_TEL_FIXE_USR="{$perso_tel_fixe}",
                        TX_PERSO_TEL_MOBILE_USR="{$perso_tel_mobile}",
                        TX_PERSO_EMAIL_USR="{$perso_email}",
                        TX_PRO_ADRESSE_USR="{$pro_adresse}",
                        TX_PRO_TEL_FIXE_USR="{$pro_tel_fixe}",
                        TX_PRO_TEL_MOBILE_USR="{$pro_tel_mobile}",
                        TX_PRO_EMAIL_USR="{$pro_email}",
                        TX_PRO_URL_USR="{$pro_url}",
                        TX_REMARQUES_USR="{$remarques}",
                        TX_TEL_NUMERO_INTERNE_USC="{$utilisateur_sc_numint}"
                    WHERE PK_UTILISATEUR_USR = {$id_utilisateur};
                    SQL;

            $oRecordset = DbExecRequete($sQuery, $oConnexion);

            if (mysqli_affected_rows($oConnexion)) {
                // insert a trace of the update
                $today = date('Y-m-d H:i:s');
                $str_user = addslashes("l'utilisateur");

                $req = <<<SQL
                DELETE FROM sc_t_trace
                WHERE
                    DT_TRACE_TRC = "{$today}"
                AND NM_UTILISATEUR_TRC = {$_SESSION['PK_UTILISATEUR_USR']}
                AND TX_DOMAINE_TRC = 'UTILISATEUR'
                AND NM_ITEMID_TRC = {$id_utilisateur};
            SQL;
                $exec = DbExecRequete($req, $oConnexion);

                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('UTILISATEUR',
                       {$id_utilisateur},
                       'Mise à jour de {$str_user} {$prenom} {$nom} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-U',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;

                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
            }

            commit($oConnexion);

            addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');

            redirect(sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'utilisateurs',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'utilisateurs',
            'param3' => 'lettre',
            'vparam3' => $letter,
            'signet' => $id_utilisateur,
            'id_utilisateur' => $id_utilisateur
        ])));
        } else {
            if (isset($_POST['anc_identifiant'])) {
                $identifiant = $_POST['anc_identifiant'];
            } else {
                $identifiant = '';
            }

            addNotice('danger', 'Désolé, mais cet identifiant est déjà pris !');
            $identifiant_deja_pris = true;
        }
    }
} else { // Mode création - Premier chargement de la page
    $motdepasse = 'password';
}
?>
<script type="text/javascript" src="js/tiny_mce_v349/tiny_mce_gzip.js"></script>
<script type="text/javascript" src="js/tinymce-launcher_v349_20120420.js"></script>
<script language="javascript" src="js/detail-utilisateurs.js"></script>
<input type="hidden" name="recupform" value="ok">
<input type="hidden" name="id_utilisateur" value="<?php echo $id_utilisateur; ?>">
<input type="hidden" name="anc_identifiant" value="<?php echo $identifiant; ?>">
<input type="hidden" name="motdepasse_actuel" value="<?php echo $motdepasse_actuel; ?>">
<!-- ===== Bouton 'Retour' / DEBUT ===== -->
<input type="hidden" name="sourcerub" value="intranet">
<input type="hidden" name="sourcepg" value="gestion">
<input type="hidden" name="param1" value="sectionid">
<input type="hidden" name="vparam1" value="<?php echo $_REQUEST['vparam1']; ?>">
<input type="hidden" name="vparam3" value="<?php echo $_REQUEST['vparam3']; ?>">

<?php
if (isset($_REQUEST['param4'])) {
    echo '<input type="hidden" name="param4" value="'.$_REQUEST['param4'].'">
<input type="hidden" name="vparam4" value="'.$_REQUEST['vparam4'].'">
<input type="hidden" name="param5" value="'.$_REQUEST['param5'].'">
<input type="hidden" name="vparam5" value="'.$_REQUEST['vparam5'].'">';
}
?>

<input type="hidden" name="oldsourcerub" value="intranet">
<input type="hidden" name="oldsourcepg" value="gestion">
<?php
if (isset($_REQUEST['oldvparam1'])) {
    ?>
    <input type="hidden" name="oldparam1" value="sectionid">
    <input type="hidden" name="oldvparam1" value="<?php if (isset($_REQUEST['oldvparam1'])) {
        echo $_REQUEST['oldvparam1'];
    } ?>">
    <?php
}
    if (isset($_REQUEST['oldparam2']) && isset($_REQUEST['oldvparam2'])) {
        ?>
    <input type="hidden" name="oldparam2" value="<?php if (isset($_REQUEST['oldparam2'])) {
        echo $_REQUEST['oldparam2'];
    } ?>">
    <input type="hidden" name="oldvparam2" value="<?php if (isset($_REQUEST['oldvparam2'])) {
        echo $_REQUEST['oldvparam2'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam3']) && isset($_REQUEST['oldvparam3'])) {
        ?>
    <input type="hidden" name="oldparam3" value="<?php if (isset($_REQUEST['oldparam3'])) {
        echo $_REQUEST['oldparam3'];
    } ?>">
    <input type="hidden" name="oldvparam3" value="<?php if (isset($_REQUEST['oldvparam3'])) {
        echo $_REQUEST['oldvparam3'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam4']) && isset($_REQUEST['oldvparam4'])) {
        ?>
    <input type="hidden" name="oldparam4" value="<?php if (isset($_REQUEST['oldparam4'])) {
        echo $_REQUEST['oldparam4'];
    } ?>">
    <input type="hidden" name="oldvparam4" value="<?php if (isset($_REQUEST['oldvparam4'])) {
        echo $_REQUEST['oldvparam4'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam5']) && isset($_REQUEST['oldvparam5'])) {
        ?>
    <input type="hidden" name="oldparam5" value="<?php if (isset($_REQUEST['oldparam5'])) {
        echo $_REQUEST['oldparam5'];
    } ?>">
    <input type="hidden" name="oldvparam5" value="<?php if (isset($_REQUEST['oldvparam5'])) {
        echo $_REQUEST['oldvparam5'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam6']) && isset($_REQUEST['oldvparam6'])) {
        ?>
    <input type="hidden" name="oldparam6" value="<?php if (isset($_REQUEST['oldparam6'])) {
        echo $_REQUEST['oldparam6'];
    } ?>">
    <input type="hidden" name="oldvparam6" value="<?php if (isset($_REQUEST['oldvparam6'])) {
        echo $_REQUEST['oldvparam6'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam7']) && isset($_REQUEST['oldvparam7'])) {
        ?>
    <input type="hidden" name="oldparam7" value="<?php if (isset($_REQUEST['oldparam7'])) {
        echo $_REQUEST['oldparam7'];
    } ?>">
    <input type="hidden" name="oldvparam7" value="<?php if (isset($_REQUEST['oldvparam7'])) {
        echo $_REQUEST['oldvparam7'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldparam8']) && isset($_REQUEST['oldvparam8'])) {
        ?>
    <input type="hidden" name="oldparam8" value="<?php if (isset($_REQUEST['oldparam8'])) {
        echo $_REQUEST['oldparam8'];
    } ?>">
    <input type="hidden" name="oldvparam8" value="<?php if (isset($_REQUEST['oldvparam8'])) {
        echo $_REQUEST['oldvparam8'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['oldsignet'])) {
        ?>
    <input type="hidden" name="oldsignet" value="<?php if (isset($_REQUEST['oldsignet'])) {
        echo $_REQUEST['oldsignet'];
    } ?>">
    <?php
    }
    if (isset($_REQUEST['utiliserold'])) {
        ?>
    <input type="hidden" name="utiliserold" value="<?php if (isset($_REQUEST['utiliserold'])) {
        echo $_REQUEST['utiliserold'];
    } ?>">
    <?php }

    if (isset($_REQUEST['signet'])) {
        ?>
    <input type="hidden" name="signet" value="<?php echo $_REQUEST['signet']; ?>">
<?php } ?>
<!-- ===== Bouton 'Retour' / FIN ===== -->
<tr>
    <td align="center">
        <font class="font-titre1"><?php if ('' == $id_utilisateur) {
            echo 'Ajout d\'un nouvel utilisateur';
        } else {
            echo 'Modification d\'un utilisateur';
        } ?></font>
    </td>
</tr>

<?php if (hasNotice()): ?>
    <tr>
        <td align="center">
            <?php printNotice(); ?>
        </td>
    </tr>
<?php endif; ?>
<tr>
    <td width="100%">
        <div align="center">
            <table border="0" width="100%" cellpadding="4" cellspacing="0">


                <?php
                if (!empty($id_utilisateur)) {
                    $sQuery = <<<SQL
                SELECT *
                FROM sc_t_trace
                WHERE
                    TX_DOMAINE_TRC = 'UTILISATEUR'
                AND NM_ITEMID_TRC = {$id_utilisateur}
                ORDER BY DT_TRACE_TRC DESC LIMIT 3;
            SQL;
                    $oRs = DbExecRequete($sQuery, $oConnexion);
                    while ($trace = DbEnregSuivant($oRs)) {
                        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
                    </td>
                </tr>
                HTML;
                    }
                    if (0 == DbNbreEnreg($oRs)) {
                        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        Aucun historique des dernières modifications apportées à cet utilisateur
                    </td>
                </tr>
                HTML;
                    }
                    ?>
                <?php } ?>

                <?php
                $url = sprintf('/?%s#back', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => 'utilisateurs',
                    'ddlsectionid' => 'utilisateurs',
                    'param3' => 'lettre',
                    'vparam3' => $_GET['vparam3']
                ]));

?>

                <?php $ligneType = 'B'; ?>
                <tr>
                    <td colspan="3" align="center">
                        <a href="<?php echo $url; ?>">
                            <img src="img/common/retour.gif" width="91" height="24" border="0" style="cursor:hand"></a>
                    </td>
                </tr>


<?php $ligneType = 'B'; ?>
    <tr>
        <td colspan="3">
    <font class="font-small"><u>Attention&nbsp;:</u> les champs dont les libellés sont en caractères <b>gras</b> sont obligatoires.
        </td>
    </tr>


    <?php if (!empty($id_utilisateur)) {?>
    <tr>
        <td colspan="3" align="center">
            <table border="0" width="100%" class="datagrid-entete">
                <tr>
                    <td width="75%">
                        <font class="font-normal"><font class="font-datagrid-entete"><b>Volumes liés à ce collaborateur</b></font></font>
                    </td>
                    <td width="25%" align="right">
                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                    </td>
            </table>
        </td>
    </tr>
        <tr>
            <td colspan="3" align="left">
                <ul style="column-count: 4">
                <?php
if (0 == DbNbreEnreg($oRecordsetVol)) {
    echo 'Ce collaborateur n\'est lié à aucun volume.';
} else {
    while ($fetchVol = DbEnregSuivant($oRecordsetVol)) {
        $url = sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'volumes',
            'signet' => 'vol'.$fetchVol->FK_OUT_REF_VOL,
            'id_volume' => $fetchVol->FK_OUT_REF_VOL,
        ]));

        echo <<<HTML
                        <li>
                            <a href="{$url}" target="_blank">Volume N°{$fetchVol->NM_NUMERO_COLLECTION_VIF}</a>
                        </li>
                        HTML;
    }
}
        ?>
                </ul>
            </td>
        </tr>
    <?php } ?>



    <tr>
        <td colspan="3" align="center">
            <table border="0" width="100%" class="datagrid-entete">
                <tr>
                    <td width="75%">
                        <font class="font-normal"><font class="font-datagrid-entete"><b>Etat civil</b></font></font>
                    </td>
                    <td width="25%" align="right">
                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                    </td>
            </table>
        </td>
    </tr>



        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>"
                <?php if (empty($id_utilisateur) || empty($decede)) { ?> style="display: none"<?php } ?>>
                <td colspan="3" align="center">
                    <span style="color: #95004A; text-align: center" id="msg_deces"><b>Cette personne est décédée</b></span>
                </td>
            </tr>


        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left" nowrap>
                    <font class="font-normal"><b>Nom</b> <font class="font-small">(<em>mettre la particule avec le nom de famille</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" maxlength="50" name="nom" id="nom" value="<?php echo $nom; ?>" size="40"
                           onKeyup="javascript:creerIdentifiant();"
                           oninput="this.value = this.value.toUpperCase()"/>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left">
                <font class="font-normal">Pr&eacute;nom :</font><br><font class="font-small"><em>Si possible, mettre le prénom complet, avec le tiret pour les prénoms composés français.
                    <br> Sinon, mettre l'initiale suivie d'un point;si plusieurs lettres, mettre un espace après le point (ex. M. A. Botti)</em></font>
            </td>
            <td width="25">&nbsp;</td>
            <td align="center">
                <input type="text" maxlength="25" name="prenom" value="<?php echo $prenom; ?>" size="40"
                    onKeyup="javascript:creerIdentifiant();">
            </td>
        </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" class="font-normal" colspan="2">
                Civilit&eacute; :
            </td>
            <td align="center">
                <input list="civilities" type="text" maxlength="25" name="civilite" value="<?php echo $civilite; ?>" size="40">
                <?php
                $sQuery = <<<SQL
                SELECT DISTINCT TX_CIVILITE_USR
                FROM sc_t_utilisateur
                WHERE TX_CIVILITE_USR IS NOT NULL AND TX_CIVILITE_USR != ''
                ORDER BY TX_CIVILITE_USR
                SQL;
                $civilities = DbEnregColonne(DbExecRequete($sQuery, $oConnexion));
                ?>
                <datalist id="civilities">
                    <?php foreach($civilities as $civility): ?>
                        <option value="<?php echo $civility ?>">
                    <?php endforeach; ?>
                </datalist>
            </td>
        </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr id="identifiant" class="datagrid-lignes<?php echo $ligneType; ?>"
                <?php if ('extérieur' == $profil) { ?> style="display: none"<?php } ?>>
                <td align="left">
                    <font class="font-normal"><b>Identifiant </b></font><font class="font-small">(<em>données confidentielles</em>) :</font>
                    <br><font class="font-small">
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="identifiant" value="<?php echo $identifiant; ?>" maxlength="50" size="40">
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr id="motdepasse" class="datagrid-lignes<?php echo $ligneType; ?>"
                <?php if ('extérieur' == $profil) { ?> style="display: none"<?php } ?>>
                <td align="left">
                    <font class="font-normal"><b>Mot de passe </b></font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="<?php if (empty($id_utilisateur)) {
                        echo 'text';
                    } else {
                        echo 'password';
                    } ?>" name="motdepasse" value="<?php echo $motdepasse; ?>" maxlength="10" size="40"<?php if ('1' == $decede) {
                        echo ' disabled';
                    } ?>>
                </td>
            </tr>

            <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Profil </font><font class="font-small">
                        (<em>seul admin parmi les profils dispose du droit de suppression</em>) : </font>
                </td>
                <td width="25"></td>
                <td align="center">
                    <label <?php if (!$rights_admin) { ?> style="display: none" <?php } ?>>
                        <input type="radio" name="profil" value="admin"<?php if ('admin' == $profil) { ?> checked<?php } ?>> admin
                    </label>
                    <label><input type="radio" name="profil" value="extérieur"<?php if ('extérieur' == $profil) { ?> checked<?php } ?>> extérieur</label>
                    <label><input type="radio" name="profil" value="membre CS"<?php if ('membre CS' == $profil) { ?> checked<?php } ?>> membre CS</label>
                    <label><input type="radio" name="profil" value="membre SC"<?php if ('membre SC' == $profil) { ?> checked<?php } ?>> membre SC</label>
                    <script>listenToProfileChange()</script>
                </td>
            </tr>

                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" class="font-normal" colspan="2">
                        Fonction :
                    </td>
                    <td align="center">
                        <input list="functions" type="text" maxlength="25" name="fonction" value="<?php echo $fonction; ?>" size="40">
                        <?php
                        $sQuery = <<<SQL
                        SELECT DISTINCT TX_FONCTION_USR
                        FROM sc_t_utilisateur
                        WHERE TX_FONCTION_USR IS NOT NULL AND TX_FONCTION_USR != ''
                        ORDER BY TX_FONCTION_USR
                        SQL;
                        $functions = DbEnregColonne(DbExecRequete($sQuery, $oConnexion));
                        ?>
                        <datalist id="functions">
                            <?php foreach($functions as $function): ?>
                            <option value="<?php echo $function ?>">
                                <?php endforeach; ?>
                        </datalist>
                    </td>
                </tr>

                <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Date de naissance </font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="date" id="dateNaissance" name="naissance_date" value="<?php echo $naissance_date; ?>" max="<?php echo date('Y-m-d'); ?>"/>
                </td>
            </tr>


        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Date de décès :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="date" id="dateDeces" name="deces_date" value="<?php echo $deces_date; ?>" max="<?php echo date('Y-m-d'); ?>">
                    <br>
                    <input type="checkbox" name="decede" onclick="champsDecede();" value="1"
                        <?php if ('1' == $decede || !empty($deces_date)) { ?> checked<?php } ?>
                    >
                    <font class="font-normal">décédé(e)</font><br>
                    <font class="font-small"><em>Cocher la case "décédé(e)" entraîne automatiquement la mise en grisé d'un certain nombre de champs.</em></font>
                </td>
            </tr>


    <tr>
        <td colspan="3" align="center">
            <table border="0" width="100%" class="datagrid-entete">
                <tr>
                    <td width="75%">
                        <font class="font-normal"><font class="font-datagrid-entete"><b>Coordonnées personnelles</b></font></font>
                    </td>
                    <td width="25%" align="right">
                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                    </td>
            </table>
        </td>
    </tr>


        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Adresse </font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <textarea name="perso_adresse" cols="38"<?php if ('1' == $decede): ?> disabled<?php endif; ?>><?php echo $perso_adresse; ?></textarea>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Tél. fixe </font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="perso_tel_fixe" value="<?php echo $perso_tel_fixe; ?>" maxlength="25" size="40"
                        <?php if ('1' == $decede) {?> disabled="disabled" <?php } ?>>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Tél. mobile </font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="perso_tel_mobile" value="<?php echo $perso_tel_mobile; ?>" maxlength="25" size="40"
                        <?php if ('1' == $decede) {?> disabled="disabled" <?php } ?>>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">E-mail </font><font class="font-small">(<em>données confidentielles</em>) :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="perso_email" value="<?php echo $perso_email; ?>" maxlength="75" size="40"
                        <?php if ('1' == $decede) { ?> disabled="disabled" <?php } ?>>
                </td>
            </tr>


    <tr>
        <td colspan="3" align="center">
            <table border="0" width="100%" class="datagrid-entete">
                <tr>
                    <td width="75%">
                        <font class="font-normal"><font class="font-datagrid-entete"><b>Coordonnées professionnelles</b></font></font>
                    </td>
                    <td width="25%" align="right">
                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                    </td>
            </table>
        </td>
    </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Adresse :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <textarea name="pro_adresse" cols="38"<?php if ('1' == $decede): ?> disabled="disabled" <?php endif; ?>><?php echo $pro_adresse; ?></textarea>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Tél. fixe :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="pro_tel_fixe" value="<?php echo $pro_tel_fixe; ?>" maxlength="25" size="40"
                        <?php if ('1' == $decede) { ?> disabled="disabled" <?php } ?>>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">Tél. mobile :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="pro_tel_mobile" value="<?php echo $pro_tel_mobile; ?>" maxlength="25" size="40"
                        <?php if ('1' == $decede) {?> disabled="disabled" <?php } ?>>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
            <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                <td align="left">
                    <font class="font-normal">E-mail :</font>
                </td>
                <td width="25">&nbsp;</td>
                <td align="center">
                    <input type="text" name="pro_email" value="<?php echo $pro_email; ?>" maxlength="75" size="40"
                        <?php if ('1' == $decede) {?> disabled="disabled" <?php } ?>>
                </td>
            </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left">
                <font class="font-normal">URL du site professionnel :</font>
            </td>
            <td width="25">&nbsp;</td>
            <td align="center">
                <input type="text" name="pro_url" value="<?php echo $pro_url; ?>" maxlength="100" size="40"
                    <?php if ('1' == $decede) { ?> disabled="disabled" <?php } ?>>
            </td>
        </tr>

        <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left">
                <font class="font-normal">Tél. interne aux SC :</font>
            </td>
            <td width="25">&nbsp;</td>
            <td align="center">
                <input type="text" name="utilisateur_sc_numint" maxlength="25" size="40"
                       value="<?php echo $utilisateur_sc_numint; ?>"
                    <?php if ('1' == $decede) { ?> disabled="disabled" <?php } ?>>
            </td>
        </tr>


    <tr>
        <td colspan="3" align="center">
            <table border="0" width="100%" class="datagrid-entete">
                <tr>
                    <td width="75%">
                        <font class="font-normal"><font class="font-datagrid-entete"><b>Divers</b></font></font>
                    </td>
                    <td width="25%" align="right">
                        <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                    </td>
            </table>
        </td>
    </tr>

    <?php ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A'); ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left">
                <font class="font-normal">Remarques </font><font class="font-small">(<em>données confidentielles</em>) :</font>
            </td>
            <td width="25">&nbsp;</td>
            <td align="center">
                <textarea id="remarques" name="remarques" rows="10" cols="90" class="mceEditor">
                    <?php echo $remarques; ?>
                </textarea>
            </td>
        </tr>


    <tr>
        <td colspan="3">
            <font class="font-normal">&nbsp;</font>
        </td>
    </tr>


    <tr>
        <td colspan="3" align="center">
            <a name="validation">
                    <input type="button" name="btn_valider" class="btn" value="<?php if (empty($id_utilisateur)) {
                        echo 'Créer l\'utilisateur';
                    } else {
                        echo 'Valider les modifications';
                    } ?> " onclick="testFormulaireUtilisateur();"
                        <?php if ($rights_ext) { ?> style="display: none" <?php } ?>>
            </a>
        </td>
    </tr>


            </table>
        </div>
    </td>
</tr>

    <?php
    if ($identifiant_deja_pris && '1' != $decede) {
        echo '<script language="javascript">document.getElementById("identifiant").focus();</script>';
    } elseif (!isset($_GET['champ'])) {
        echo '<script language="javascript">document.getElementById("nom").focus();</script>';
    }
?>
    <?php
DbClose($oConnexion);

?>
