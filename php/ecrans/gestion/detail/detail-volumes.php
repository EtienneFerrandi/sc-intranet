<?php
$oConnexion = DbConnection();

$id_volume = '';
$id_volume_infos = '';

$rights_admin = [];
$rights_member_sc = [];
$rights_member_cs = [];
$rights_ext = [];

$vol_tch = [];
$vol_usr = [];
$vol_ovr = [];
$vol_lvl = [];
$vol_begin = [];
$vol_end = [];
$vol_rmq = [];
$vol_ms = [];
$volume_works = [];
$vol_isbn = '';
$vol_numcollection = '';
$vol_numcollection_cplt = '';
$vol_numcollection_ancien = '';
$vol_numcollection_nouveau = '';
$vol_sscollection = '';
$vol_sscollection_num = '';
$vol_pgtitre_titre = '';
$vol_pgtitre_sstitre = '';
$vol_dateparution_an = '';
$vol_dateparution_mois = '';
$vol_datedernierereimp_an = '';
$vol_reimp_nb = '';
$vol_reimp_nb_cplt = '';
$vol_domaine = '';
$vol_subvention_recue = '';
$vol_mecene = '';
$vol_jaquette_recue = '';
$vol_remarques = '';
$vol_imprimeur = '';
$vol_devis = '';
$vol_prestations = '';

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}

if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member_sc[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member_cs[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression' AND LB_DROIT_DRT NOT LIKE '%Edition');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (!isset($_POST['recupform']) && isset($_GET['id_volume'])) {
    $id_vol = $_GET['id_volume'];
    global $id_volume;
    $id_volume = $id_vol;

    /* delete association task / user */
    if (isset($_REQUEST['action'])) {
        $fk_user = empty($_GET['fk_usr']) ? 'IS NULL' : "= {$_GET['fk_usr']}";
        $fk_work = empty($_GET['fk_ovr']) ? 'IS NULL' : "= {$_GET['fk_ovr']}";
        $fk_task = empty($_GET['fk_tch']) ? 'IS NULL' : "= {$_GET['fk_tch']}";
        if ($_REQUEST['action'] == 'suppr') {
            begin($oConnexion);
            $sQuery = <<<SQL
            DELETE FROM sc_t_assoc_ovr_usr_tch
            WHERE
                FK_OUT_REF_VOL = {$id_volume}
            AND FK_OUT_REF_USR {$fk_user}
            AND FK_OUT_REF_OVR {$fk_work}
            AND FK_OUT_REF_TCH {$fk_task};
            SQL;
            $oRecordset = DbExecRequete($sQuery, $oConnexion);
            commit($oConnexion);
            addNotice('success', 'Tâche supprimée');
        }
    }

    $sQuery = <<<SQL
            SELECT V.*,
                   ot.FK_OUT_REF_USR,
                   ot.FK_OUT_REF_OVR,
                   ot.FK_OUT_REF_TCH,
                   ot.LB_OUT_AVT_TCH,
                   ot.DT_DEBUT_TCH,
                   ot.DT_FIN_TCH,
                   ot.TX_REMARQUES_OUT,
                   ot.BL_MANUSCRIT_REMIS
            FROM sc_t_volume AS V
            LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                ON V.PK_VOLUMEINFOS_VIF = ot.FK_OUT_REF_VOL
            WHERE V.PK_VOLUMEINFOS_VIF = {$id_volume}
            GROUP BY V.PK_VOLUMEINFOS_VIF;
            SQL;
    $oRecordset = DbExecRequete($sQuery, $oConnexion);

    if (1 == DbNbreEnreg($oRecordset)) {
        $vol = DbEnregSuivant($oRecordset);

        $vol_tch = $vol->FK_OUT_REF_TCH;
        $vol_usr = $vol->FK_OUT_REF_USR;
        $vol_ovr = $vol->FK_OUT_REF_OVR;
        $vol_lvl = $vol->LB_OUT_AVT_TCH;
        $vol_begin = $vol->DT_DEBUT_TCH;
        $vol_end = $vol->DT_FIN_TCH;
        $vol_rmq = $vol->TX_REMARQUES_OUT;
        $vol_ms = $vol->BL_MANUSCRIT_REMIS;

        // fetch works of the volume
        $sQueryWorks = <<<SQL
        SELECT vo.FK_AVO_REF_OVR
        FROM sc_t_volume AS V
        LEFT JOIN sc_t_assoc_vol_ovr AS vo
        ON vo.FK_AVO_REF_VOL = V.PK_VOLUMEINFOS_VIF
        WHERE V.PK_VOLUMEINFOS_VIF = {$id_volume}
        GROUP BY vo.FK_AVO_REF_OVR;
        SQL;
        $oRecordsetWorks = DbExecRequete($sQueryWorks, $oConnexion);
        while ($work = DbEnregSuivant($oRecordsetWorks)) {
            $volume_works[] = $work->FK_AVO_REF_OVR;
        }

        $id_volume_infos = $vol->PK_VOLUMEINFOS_VIF;
        $vol_isbn = $vol->TX_ISBN_VIF;
        $vol_numcollection = $vol->NM_NUMERO_COLLECTION_VIF;
        $vol_numcollection_cplt = $vol->TX_NUMCOLL_COMPLEMENT_VIF;
        $vol_numcollection_ancien = $vol->TX_NUMCOLL_ANCIEN_VIF;
        $vol_numcollection_nouveau = $vol->TX_NUMCOLL_NOUVEAU_VIF;
        $vol_sscollection = $vol->TX_SOUS_COLLECTION_VIF;
        $vol_sscollection_num = $vol->TX_SOUSCOLL_NUMERO_VIF;
        $vol_pgtitre_titre = $vol->TX_PAGETITRE_TITRE_VIF;
        $vol_pgtitre_sstitre = $vol->TX_PAGETITRE_SSTITRE_VIF;
        $vol_dateparution_an = $vol->DT_PARUTION_VIF;
        $vol_dateparution_mois = $vol->DT_MOIS_PARUTION_VIF;
        $vol_datedernierereimp_an = $vol->DT_DERNIERE_REIMP_AN_VIF;
        $vol_reimp_nb = $vol->NM_NOMBRE_REIMP_VIF;
        $vol_reimp_nb_cplt = $vol->TX_NOMBRE_REIMP_VIF;
        $vol_domaine = $vol->TX_DOMAINE_VIF;
        $vol_subvention_recue = $vol->BL_SUBVENTION_VIF;
        $vol_mecene = $vol->TX_MECENE_VIF;
        $vol_jaquette_recue = $vol->BL_JAQUETTE_VIF;
        $vol_prevue_parution_annee = $vol->DT_SORTIE_VIF;
        $vol_remarques = $vol->TX_REMARQUES_VIF;
        $vol_imprimeur = $vol->TX_IMPRIMEUR_VIF;
        $vol_devis = $vol->NM_MONTANT_DEVIS;
        $vol_prestations = $vol->NM_MONTANTS_PRESTATIONS;
    } else { // Erreur : aucun volume ne correspond à cet identifiant
        header('Location: /?rubriqueid=intranet&pageid=gestion&err=ok');
    }
} elseif (null != $_POST) { 	// L'utilisateur a soumis le formulaire
    $vol_population = $_POST['vparam2'];
    $vol_oeuvre = $_POST['vparam3'];
    $vol_autancien = $_POST['vparam4'];
    $vol_utilisateur = $_POST['vparam5'];
    $id_volume = $_POST['id_volume'];
    $id_volume_infos = $_POST['id_volume_infos'];

    if (!empty($_POST['vol_tch'] || !empty($_POST['vol_usr']))) {
        if (count($_POST['vol_tch']) !== count($_POST['vol_usr'])) {
            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'volumes',
                'detail' => 'ok',
                'sourcerub' => 'intranet',
                'sourcepg' => 'gestion',
                'param1' => 'sectionid',
                'vparam1' => 'volumes',
                'param2' => 'vol_population',
                'vparam2' => $vol_population,
                'param3' => 'vol_oeuvre',
                'vparam3' => $vol_oeuvre,
                'param4' => 'vol_autancien',
                'vparam4' => $vol_autancien,
                'param5' => 'vol_population',
                'vparam5' => $vol_utilisateur,
                'signet' => $id_volume,
                'id_volume' => $id_volume
            ])));
            throw new \InvalidArgumentException('Works users and tasks numbers mismatch');
        }
    }

    $vol_usr = $_POST['vol_usr'] ?? [];
    $vol_ovr = $_POST['vol_ovr'] ?? [];
    $vol_tch = $_POST['vol_tch'] ?? [];
    $vol_lvl = $_POST['vol_lvl'] ?? [];
    $vol_begin = $_POST['vol_begin'] ?? [];
    $vol_end = $_POST['vol_end'] ?? [];
    $vol_rmq = $_POST['vol_rmq'] ?? [];
    $vol_ms = $_POST['vol_ms'] ?? [];
    $volume_works = array_filter($_POST['volume_works']);
    $vol_isbn = $_POST['vol_isbn'];
    $vol_numcollection = $_POST['vol_numcollection'];
    $vol_numcollection_cplt = $_POST['vol_numcollection_cplt'];
    $vol_numcollection_ancien = $_POST['vol_numcollection_ancien'];
    $vol_numcollection_nouveau = $_POST['vol_numcollection_nouveau'];
    $vol_sscollection = $_POST['vol_sscollection'];
    $vol_sscollection_num = $_POST['vol_sscollection_num'];
    $vol_pgtitre_titre = $_POST['vol_pgtitre_titre'];
    $vol_pgtitre_sstitre = $_POST['vol_pgtitre_sstitre'];
    $vol_dateparution_an = $_POST['vol_dateparution_an'];
    $vol_dateparution_mois = $_POST['vol_dateparution_mois'];
    $vol_datedernierereimp_an = $_POST['vol_datedernierereimp_an'];
    $vol_reimp_nb = empty($_POST['vol_reimp_nb']) ? 'NULL' :  $_POST['vol_reimp_nb'];
    $vol_reimp_nb_cplt = $_POST['vol_reimp_nb_cplt'];
    $vol_domaine = $_POST['vol_domaine'];
    $vol_subvention_recue = empty($_POST['vol_subvention_recue']) ? 'NULL' : $_POST['vol_subvention_recue'];
    $vol_mecene = $_POST['vol_mecene'];
    $vol_jaquette_recue = empty($_POST['vol_jaquette_recue']) ? 'NULL' : $_POST['vol_jaquette_recue'];
    $vol_remarques = $_POST['vol_remarques'];
    $vol_imprimeur = $_POST['vol_imprimeur'];
    $vol_devis = empty($_POST['vol_devis']) ? 'NULL' : $_POST['vol_devis'];
    $vol_prestations = empty($_POST['vol_prestations']) ? 'NULL' : $_POST['vol_prestations'];

    begin($oConnexion);

    $e_vol_isbn = mysqli_real_escape_string($oConnexion, $vol_isbn);
    $e_vol_numcollection = mysqli_real_escape_string($oConnexion, $vol_numcollection);
    $e_vol_numcollection_cplt = mysqli_real_escape_string($oConnexion, $vol_numcollection_cplt);
    $e_vol_numcollection_ancien = mysqli_real_escape_string($oConnexion, $vol_numcollection_ancien);
    $e_vol_numcollection_nouveau = mysqli_real_escape_string($oConnexion, $vol_numcollection_nouveau);
    $e_vol_sscollection = mysqli_real_escape_string($oConnexion, $vol_sscollection);
    $e_vol_sscollection_num = mysqli_real_escape_string($oConnexion, $vol_sscollection_num);
    $e_vol_pgtitre_titre = mysqli_real_escape_string($oConnexion, $vol_pgtitre_titre);
    $e_vol_pgtitre_sstitre = mysqli_real_escape_string($oConnexion, $vol_pgtitre_sstitre);
    $e_vol_dateparution_an = mysqli_real_escape_string($oConnexion, $vol_dateparution_an);
    $e_vol_dateparution_mois = mysqli_real_escape_string($oConnexion, $vol_dateparution_mois);
    $e_vol_datedernierereimp_an = mysqli_real_escape_string($oConnexion, $vol_datedernierereimp_an);
    $e_vol_reimp_nb = mysqli_real_escape_string($oConnexion, $vol_reimp_nb);
    $e_vol_reimp_nb_cplt = mysqli_real_escape_string($oConnexion, $vol_reimp_nb_cplt);
    $e_vol_domaine = mysqli_real_escape_string($oConnexion, $vol_domaine);
    $e_vol_subvention_recue = mysqli_real_escape_string($oConnexion, $vol_subvention_recue);
    $e_vol_mecene = mysqli_real_escape_string($oConnexion, $vol_mecene);
    $e_vol_jaquette_recue = mysqli_real_escape_string($oConnexion, $vol_jaquette_recue);
    $e_vol_remarques = mysqli_real_escape_string($oConnexion, $vol_remarques);
    $e_vol_imprimeur = mysqli_real_escape_string($oConnexion, $vol_imprimeur);
    $e_vol_devis = mysqli_real_escape_string($oConnexion, $vol_devis);
    $e_vol_prestations = mysqli_real_escape_string($oConnexion, $vol_prestations);

    if (empty($id_volume)) { 	// On insère un nouvel ...
        // check if the volume already exists in database
        $req = <<<SQL
            SELECT NM_NUMERO_COLLECTION_VIF
            FROM sc_t_volume
            WHERE NM_NUMERO_COLLECTION_VIF LIKE {$vol_numcollection};
        SQL;
        $exec = DbExecRequete($req, $oConnexion);
        $volume_exists = (bool) mysqli_num_rows($exec);

        // create the volume if it does not exist in database
        if (!$volume_exists) {
            // save volume
            $sQuery = <<<SQL
            INSERT INTO  sc_t_volume(
                TX_ISBN_VIF,
                NM_NUMERO_COLLECTION_VIF,
                TX_NUMCOLL_COMPLEMENT_VIF,
                TX_NUMCOLL_ANCIEN_VIF,
                TX_NUMCOLL_NOUVEAU_VIF,
                TX_SOUS_COLLECTION_VIF,
                TX_SOUSCOLL_NUMERO_VIF,
                TX_PAGETITRE_TITRE_VIF,
                TX_PAGETITRE_SSTITRE_VIF,
                DT_PARUTION_VIF,
                DT_MOIS_PARUTION_VIF,
                DT_DERNIERE_REIMP_AN_VIF,
                NM_NOMBRE_REIMP_VIF,
                TX_NOMBRE_REIMP_VIF,
                TX_DOMAINE_VIF,
                BL_SUBVENTION_VIF,
                TX_MECENE_VIF,
                BL_JAQUETTE_VIF,
                TX_REMARQUES_VIF,
                TX_IMPRIMEUR_VIF,
                NM_MONTANT_DEVIS,
                NM_MONTANTS_PRESTATIONS)
            VALUES(
                "{$e_vol_isbn}",
                {$e_vol_numcollection},
                "{$e_vol_numcollection_cplt}",
                "{$e_vol_numcollection_ancien}",
                "{$e_vol_numcollection_nouveau}",
                "{$e_vol_sscollection}",
                "{$e_vol_sscollection_num}",
                "{$e_vol_pgtitre_titre}",
                "{$e_vol_pgtitre_sstitre}",
                {$e_vol_dateparution_an},
                {$e_vol_dateparution_mois},
                {$e_vol_datedernierereimp_an},
                {$e_vol_reimp_nb},
                "{$e_vol_reimp_nb_cplt}",
                "{$e_vol_domaine}",
                {$e_vol_subvention_recue},
                "{$e_vol_mecene}",
                {$e_vol_jaquette_recue},
                "{$e_vol_remarques}",
                "{$e_vol_imprimeur}",
                {$e_vol_devis},
                {$e_vol_prestations}
            );
            SQL;

            $oRecordset = DbExecRequete($sQuery, $oConnexion);

            $vol_id = mysqli_insert_id($oConnexion);
            global $volume_id;
            $volume_id = $vol_id;

            // save volume's works
            $queries = array_map(static function ($work_id) use ($volume_id): string {
                return <<<SQL
                INSERT INTO sc_t_assoc_vol_ovr(
                    FK_AVO_REF_VOL,
                    FK_AVO_REF_OVR
                ) VALUES (
                    {$volume_id},
                    {$work_id}
                );
                SQL;
            }, $volume_works);
            array_walk($queries, static function (string $query) use ($oConnexion): void {
                DbExecRequete($query, $oConnexion);
            });

            // update works tasks
            $queries = array_map(static function ($work_id) use ($volume_id): string {
                return <<<SQL
                UPDATE sc_t_assoc_ovr_usr_tch
                SET FK_OUT_REF_VOL={$volume_id}
                WHERE FK_OUT_REF_OVR={$work_id} AND FK_OUT_REF_VOL IS NULL;
                SQL;
            }, $volume_works);
            array_walk($queries, static function (string $query) use ($oConnexion): void {
                DbExecRequete($query, $oConnexion);
            });

            /* ajout lien utilisateur tâche */
            $wuts_ids = array_unique(array_merge(
                array_keys($vol_tch),
                array_keys($vol_usr),
                array_keys($vol_ovr)
            ));
            if (count(array_diff_key($vol_tch, $vol_usr)) || count(array_diff_key($vol_usr, $vol_tch))) {
                addNotice('danger', "Certaines tâches n'ont pas pu être enregistrées : la tâche ou l'utilisateur sont manquants.");
            }

            $queries = [];
            foreach ($wuts_ids as $index) {
                $tasks_ids = $vol_tch[$index] ?? []; // field is required
                $users_ids = $vol_usr[$index] ?? []; // field is required
                $works_ids = $vol_ovr[$index] ?? [null]; // field is optional
                foreach($tasks_ids as $task_id) {
                    foreach ($users_ids as $user_id) {
                        foreach ($works_ids as $work_id) {
                            $id_ovr = $id_ovr ?? 'NULL';
                            $task_id = $task_id ?? 'NULL';
                            $user_id = $user_id ?? 'NULL';
                            $work_id = $work_id ?? 'NULL';
                            $lvl_id = $vol_lvl[$index] ?? null;
                            $begin_id = $vol_begin[$index] ?? null;
                            $end_id = $vol_end[$index] ?? null;
                            $rmq_id = $vol_rmq[$index] ?? null;
                            $ms_id = '1' === ($vol_ms[$index] ?? '0') ? '1' : '0';

                            $queries[] = <<<SQL
                            INSERT INTO sc_t_assoc_ovr_usr_tch(
                                FK_OUT_REF_OVR,
                                FK_OUT_REF_VOL,
                                FK_OUT_REF_TCH,
                                FK_OUT_REF_USR,
                                LB_OUT_AVT_TCH,
                                DT_DEBUT_TCH,
                                DT_FIN_TCH,
                                TX_REMARQUES_OUT,
                                BL_MANUSCRIT_REMIS
                            ) VALUES (
                                {$work_id},
                                {$volume_id},
                                {$task_id},
                                {$user_id},
                                "{$lvl_id}",
                                "{$begin_id}",
                                "{$end_id}",
                                "{$rmq_id}",
                                {$ms_id}
                            );
                            SQL;
                        }
                    }
                }
                array_walk($queries1[0], static function (string $query) use ($oConnexion): void {
                    DbExecRequete($query, $oConnexion);
                });
            }
            /* FIN ajout lien utilisateur tâche */

            if (mysqli_affected_rows($oConnexion)) {
                // insert a trace of the addition
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
            INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
            VALUES('VOLUME',
                   {$volume_id},
                   'Ajout du volume n°{$e_vol_numcollection} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                   'SQL-I',
                   "{$today}",
                   {$_SESSION['PK_UTILISATEUR_USR']}
                   );
            SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
            }

            commit($oConnexion);

            addNotice('success', 'Volume correctement ajouté !');

            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'volumes',
                'detail' => 'ok',
                'sourcerub' => 'intranet',
                'sourcepg' => 'gestion',
                'param1' => 'sectionid',
                'vparam1' => 'volumes',
                'param2' => 'vol_population',
                'vparam2' => $vol_population,
                'param3' => 'vol_oeuvre',
                'vparam3' => $vol_oeuvre,
                'param4' => 'vol_autancien',
                'vparam4' => $vol_autancien,
                'param5' => 'vol_population',
                'vparam5' => $vol_utilisateur,
                'signet' => $volume_id,
                'id_volume' => $volume_id
            ])));
        } else {
            addNotice('danger', 'Volume déjà existant !');
        }
    } else { // On met à jour ... existant
        //get the previous volume elements
        $req = <<<SQL
        SELECT
            TX_ISBN_VIF,
            NM_NUMERO_COLLECTION_VIF,
            TX_NUMCOLL_COMPLEMENT_VIF,
            TX_NUMCOLL_ANCIEN_VIF,
            TX_NUMCOLL_NOUVEAU_VIF,
            TX_SOUS_COLLECTION_VIF,
            TX_SOUSCOLL_NUMERO_VIF,
            TX_PAGETITRE_TITRE_VIF,
            TX_PAGETITRE_SSTITRE_VIF,
            DT_PARUTION_VIF,
            DT_MOIS_PARUTION_VIF,
            DT_DERNIERE_REIMP_AN_VIF,
            NM_NOMBRE_REIMP_VIF,
            TX_NOMBRE_REIMP_VIF,
            TX_DOMAINE_VIF,
            BL_SUBVENTION_VIF,
            TX_MECENE_VIF,
            BL_JAQUETTE_VIF,
            TX_REMARQUES_VIF,
            TX_IMPRIMEUR_VIF,
            NM_MONTANT_DEVIS,
            NM_MONTANTS_PRESTATIONS
        FROM sc_t_volume
        WHERE PK_VOLUMEINFOS_VIF = {$id_volume}
        ORDER BY TX_ISBN_VIF, NM_NUMERO_COLLECTION_VIF, TX_NUMCOLL_COMPLEMENT_VIF, TX_NUMCOLL_ANCIEN_VIF, TX_NUMCOLL_NOUVEAU_VIF, TX_SOUS_COLLECTION_VIF, TX_SOUSCOLL_NUMERO_VIF, TX_PAGETITRE_TITRE_VIF, TX_PAGETITRE_SSTITRE_VIF, DT_PARUTION_VIF, DT_MOIS_PARUTION_VIF, DT_DERNIERE_REIMP_AN_VIF, NM_NOMBRE_REIMP_VIF, TX_NOMBRE_REIMP_VIF, TX_DOMAINE_VIF, BL_SUBVENTION_VIF, TX_MECENE_VIF, BL_JAQUETTE_VIF, TX_REMARQUES_VIF, TX_IMPRIMEUR_VIF, NM_MONTANT_DEVIS, NM_MONTANTS_PRESTATIONS
        SQL;
        $oRecordset = DbExecRequete($req, $oConnexion);
        $existing_vol_elements = [];
        while ($result = DbEnregSuivantTab($oRecordset)) {
            $existing_vol_elements[] = [
                $result['TX_ISBN_VIF'], $result['NM_NUMERO_COLLECTION_VIF'], $result['TX_NUMCOLL_COMPLEMENT_VIF'], $result['TX_NUMCOLL_ANCIEN_VIF'], $result['TX_NUMCOLL_NOUVEAU_VIF'], $result['TX_SOUS_COLLECTION_VIF'], $result['TX_SOUSCOLL_NUMERO_VIF'], $result['TX_PAGETITRE_TITRE_VIF'], $result['TX_PAGETITRE_SSTITRE_VIF'], $result['DT_PARUTION_VIF'], $result['DT_MOIS_PARUTION_VIF'], $result['DT_DERNIERE_REIMP_AN_VIF'], $result['NM_NOMBRE_REIMP_VIF'], $result['TX_NOMBRE_REIMP_VIF'], $result['TX_DOMAINE_VIF'], $result['BL_SUBVENTION_VIF'], $result['TX_MECENE_VIF'], $result['BL_JAQUETTE_VIF'], $result['TX_REMARQUES_VIF'], $result['TX_IMPRIMEUR_VIF'], $result['NM_MONTANT_DEVIS'], $result['NM_MONTANTS_PRESTATIONS']
            ];
        }
        $existing_vol_elements = array_merge(...$existing_vol_elements);

        $sQuery = <<<SQL
        UPDATE sc_t_volume
        SET
            TX_ISBN_VIF = "{$e_vol_isbn}",
            NM_NUMERO_COLLECTION_VIF = {$e_vol_numcollection},
            TX_NUMCOLL_COMPLEMENT_VIF = "{$e_vol_numcollection_cplt}",
            TX_NUMCOLL_ANCIEN_VIF = "{$e_vol_numcollection_ancien}",
            TX_NUMCOLL_NOUVEAU_VIF = "{$e_vol_numcollection_nouveau}",
            TX_SOUS_COLLECTION_VIF = "{$e_vol_sscollection}",
            TX_SOUSCOLL_NUMERO_VIF = "{$e_vol_sscollection_num}",
            TX_PAGETITRE_TITRE_VIF = "{$e_vol_pgtitre_titre}",
            TX_PAGETITRE_SSTITRE_VIF = "{$e_vol_pgtitre_sstitre}",
            DT_PARUTION_VIF = {$e_vol_dateparution_an},
            DT_MOIS_PARUTION_VIF = {$e_vol_dateparution_mois},
            DT_DERNIERE_REIMP_AN_VIF = {$e_vol_datedernierereimp_an},
            NM_NOMBRE_REIMP_VIF = {$e_vol_reimp_nb},
            TX_NOMBRE_REIMP_VIF = "{$e_vol_reimp_nb_cplt}",
            TX_DOMAINE_VIF = "{$e_vol_domaine}",
            BL_SUBVENTION_VIF = {$e_vol_subvention_recue},
            TX_MECENE_VIF = "{$e_vol_mecene}",
            BL_JAQUETTE_VIF = {$e_vol_jaquette_recue},
            TX_REMARQUES_VIF = "{$e_vol_remarques}",
            TX_IMPRIMEUR_VIF = "{$e_vol_imprimeur}",
            NM_MONTANT_DEVIS = {$e_vol_devis},
            NM_MONTANTS_PRESTATIONS = {$e_vol_prestations}
        WHERE PK_VOLUMEINFOS_VIF = {$id_volume};
        SQL;
        $oRecordset = DbExecRequete($sQuery, $oConnexion);

        // save volume's works if the user adds a new one or remove an old one
        // get the id of the association in order to replace it if exists
        $sQuery = <<<SQL
            SELECT FK_AVO_REF_OVR
            FROM sc_t_assoc_vol_ovr
            WHERE FK_AVO_REF_VOL = {$id_volume};
            SQL;
        $oRecordset = DbExecRequete($sQuery, $oConnexion);

        $existing_work_ids = [];
        while ($result = DbEnregSuivant($oRecordset)) {
            $existing_work_ids[] = $result->FK_AVO_REF_OVR;
        }

        $queries_vo = array_merge(
            array_map(static function ($work_id) use ($id_volume): string {
                return <<<SQL
                DELETE FROM `sc_t_assoc_vol_ovr`
                WHERE
                    `FK_AVO_REF_VOL`={$id_volume}
                AND `FK_AVO_REF_OVR`={$work_id};
                SQL;
            }, $existing_work_ids),
            array_map(static function ($work_id) use ($id_volume): string {
                return <<<SQL
                INSERT INTO `sc_t_assoc_vol_ovr`(
                    `FK_AVO_REF_VOL`,
                    `FK_AVO_REF_OVR`
                ) VALUES (
                    {$id_volume},
                    {$work_id}
                );
                SQL;
            }, $volume_works)
        );
        array_walk($queries_vo, static function (string $query) use ($oConnexion): void {
            DbExecRequete($query, $oConnexion);
        });

        /* mise à jour de la table association oeuvre utilisateur tâche */
        $existing_assoc_fields = [];
        if($vol_tch || $vol_usr){
            $req = <<<SQL
            SELECT FK_OUT_REF_OVR,FK_OUT_REF_USR,FK_OUT_REF_TCH,LB_OUT_AVT_TCH,BL_MANUSCRIT_REMIS,TX_REMARQUES_OUT,BL_ENGAGEMENT_COLLAB_OUT,DT_DEBUT_TCH,DT_FIN_TCH
            FROM `sc_t_assoc_ovr_usr_tch`
            WHERE `FK_OUT_REF_VOL`={$id_volume}
            ORDER BY FK_OUT_REF_USR,FK_OUT_REF_TCH,LB_OUT_AVT_TCH,BL_MANUSCRIT_REMIS,TX_REMARQUES_OUT,BL_ENGAGEMENT_COLLAB_OUT,DT_DEBUT_TCH,DT_FIN_TCH;
            SQL;
            $oRecordset = DbExecRequete($req, $oConnexion);
            while ($result = DbEnregSuivantTab($oRecordset)) {
                $existing_assoc_fields[] = [
                    $result['FK_OUT_REF_OVR'],
                    $result['FK_OUT_REF_USR'],
                    $result['FK_OUT_REF_TCH'],
                    $result['LB_OUT_AVT_TCH'],
                    $result['BL_MANUSCRIT_REMIS'],
                    $result['TX_REMARQUES_OUT'],
                    $result['BL_ENGAGEMENT_COLLAB_OUT'],
                    $result['TX_REMARQUES_OUT'],
                    $result['DT_DEBUT_TCH'],
                    $result['DT_FIN_TCH']
                    ];
            }
            $existing_assoc_fields = array_merge(...$existing_assoc_fields);

            /* ajout lien utilisateur tâche */
            $wuts_ids = array_unique(array_merge(
                array_keys($vol_tch),
                array_keys($vol_usr),
                array_keys($vol_ovr)
            ));
            if (count(array_diff_key($vol_tch, $vol_usr)) || count(array_diff_key($vol_usr, $vol_tch))) {
                addNotice('danger', "Certaines tâches n'ont pas pu être enregistrées : la tâche ou l'utilisateur sont manquants.");
            }

            $queries = [
                <<<SQL
                DELETE FROM `sc_t_assoc_ovr_usr_tch`
                WHERE `FK_OUT_REF_VOL`={$id_volume};
                SQL,
            ];
            foreach ($wuts_ids as $index) {
                $tasks_ids = $vol_tch[$index] ?? []; // field is required
                $users_ids = $vol_usr[$index] ?? []; // field is required
                $works_ids = $vol_ovr[$index] ?? [null]; // field is optional
                foreach($tasks_ids as $task_id) {
                    foreach ($users_ids as $user_id) {
                        foreach ($works_ids as $work_id) {
                            $id_ovr = $id_ovr ?? 'NULL';
                            $task_id = $task_id ?? 'NULL';
                            $user_id = $user_id ?? 'NULL';
                            $work_id = $work_id ?? 'NULL';
                            $lvl_id = $vol_lvl[$index] ?? null;
                            $begin_id = $vol_begin[$index] ?? null;
                            $end_id = $vol_end[$index] ?? null;
                            $rmq_id = $vol_rmq[$index] ?? null;
                            $ms_id = '1' === ($vol_ms[$index] ?? '0') ? '1' : '0';
                            $queries[] = <<<SQL
                            INSERT INTO sc_t_assoc_ovr_usr_tch(
                                FK_OUT_REF_OVR,
                                FK_OUT_REF_VOL,
                                FK_OUT_REF_TCH,
                                FK_OUT_REF_USR,
                                LB_OUT_AVT_TCH,
                                DT_DEBUT_TCH,
                                DT_FIN_TCH,
                                TX_REMARQUES_OUT,
                                BL_MANUSCRIT_REMIS
                            ) VALUES (
                                {$work_id},
                                {$id_volume},
                                {$task_id},
                                {$user_id},
                                "{$lvl_id}",
                                "{$begin_id}",
                                "{$end_id}",
                                "{$rmq_id}",
                                {$ms_id}
                            );
                            SQL;
                        }
                    }
                }
            }
            array_walk($queries, static function (string $query) use ($oConnexion) {
                DbExecRequete($query, $oConnexion);
            });
        }
        /* FIN mise à jour de la table association oeuvre utilisateur tâche */
        $req = <<<SQL
            SELECT
                FK_OUT_REF_USR,
                FK_OUT_REF_TCH,
                LB_OUT_AVT_TCH,
                BL_MANUSCRIT_REMIS,
                TX_REMARQUES_OUT,
                BL_ENGAGEMENT_COLLAB_OUT,
                DT_DEBUT_TCH,DT_FIN_TCH
            FROM `sc_t_assoc_ovr_usr_tch`
            WHERE FK_OUT_REF_VOL = {$id_volume}
            ORDER BY FK_OUT_REF_USR,FK_OUT_REF_TCH,LB_OUT_AVT_TCH,BL_MANUSCRIT_REMIS,TX_REMARQUES_OUT,BL_ENGAGEMENT_COLLAB_OUT,DT_DEBUT_TCH,DT_FIN_TCH
            SQL;
        $oRs = DbExecRequete($req, $oConnexion);
        $after_assoc_fields = [];
        while($result = DbEnregSuivantTab($oRs)){
            $after_assoc_fields[] = [
                $result['FK_OUT_REF_USR'],
                $result['FK_OUT_REF_TCH'],
                $result['LB_OUT_AVT_TCH'],
                $result['BL_MANUSCRIT_REMIS'],
                $result['TX_REMARQUES_OUT'],
                $result['BL_ENGAGEMENT_COLLAB_OUT'],
                $result['TX_REMARQUES_OUT'],
                $result['DT_DEBUT_TCH'],
                $result['DT_FIN_TCH']
            ];
        }
        $after_assoc_fields = array_merge(...$after_assoc_fields);

        // get the new works elements
        $req = <<<SQL
            SELECT
                TX_ISBN_VIF,
                NM_NUMERO_COLLECTION_VIF,
                TX_NUMCOLL_COMPLEMENT_VIF,
                TX_NUMCOLL_ANCIEN_VIF,
                TX_NUMCOLL_NOUVEAU_VIF,
                TX_SOUS_COLLECTION_VIF,
                TX_SOUSCOLL_NUMERO_VIF,
                TX_PAGETITRE_TITRE_VIF,
                TX_PAGETITRE_SSTITRE_VIF,
                DT_PARUTION_VIF,
                DT_MOIS_PARUTION_VIF,
                DT_DERNIERE_REIMP_AN_VIF,
                NM_NOMBRE_REIMP_VIF,
                TX_NOMBRE_REIMP_VIF,
                TX_DOMAINE_VIF,
                BL_SUBVENTION_VIF,
                TX_MECENE_VIF,
                BL_JAQUETTE_VIF,
                TX_REMARQUES_VIF,
                TX_IMPRIMEUR_VIF,
                NM_MONTANT_DEVIS,
                NM_MONTANTS_PRESTATIONS
            FROM sc_t_volume
            WHERE PK_VOLUMEINFOS_VIF = {$id_volume}
            ORDER BY TX_ISBN_VIF, NM_NUMERO_COLLECTION_VIF, TX_NUMCOLL_COMPLEMENT_VIF, TX_NUMCOLL_ANCIEN_VIF, TX_NUMCOLL_NOUVEAU_VIF, TX_SOUS_COLLECTION_VIF, TX_SOUSCOLL_NUMERO_VIF, TX_PAGETITRE_TITRE_VIF, TX_PAGETITRE_SSTITRE_VIF, DT_PARUTION_VIF, DT_MOIS_PARUTION_VIF, DT_DERNIERE_REIMP_AN_VIF, NM_NOMBRE_REIMP_VIF, TX_NOMBRE_REIMP_VIF, TX_DOMAINE_VIF, BL_SUBVENTION_VIF, TX_MECENE_VIF, BL_JAQUETTE_VIF, TX_REMARQUES_VIF, TX_IMPRIMEUR_VIF, NM_MONTANT_DEVIS, NM_MONTANTS_PRESTATIONS
        SQL;
        $oRecordset = DbExecRequete($req, $oConnexion);
        $after_vol_elements = [];
        while ($result = DbEnregSuivantTab($oRecordset)) {
            $after_vol_elements[] = [
                $result['TX_ISBN_VIF'], $result['NM_NUMERO_COLLECTION_VIF'], $result['TX_NUMCOLL_COMPLEMENT_VIF'], $result['TX_NUMCOLL_ANCIEN_VIF'], $result['TX_NUMCOLL_NOUVEAU_VIF'], $result['TX_SOUS_COLLECTION_VIF'], $result['TX_SOUSCOLL_NUMERO_VIF'], $result['TX_PAGETITRE_TITRE_VIF'], $result['TX_PAGETITRE_SSTITRE_VIF'], $result['DT_PARUTION_VIF'], $result['DT_MOIS_PARUTION_VIF'], $result['DT_DERNIERE_REIMP_AN_VIF'], $result['NM_NOMBRE_REIMP_VIF'], $result['TX_NOMBRE_REIMP_VIF'], $result['TX_DOMAINE_VIF'], $result['BL_SUBVENTION_VIF'], $result['TX_MECENE_VIF'], $result['BL_JAQUETTE_VIF'], $result['TX_REMARQUES_VIF'], $result['TX_IMPRIMEUR_VIF'], $result['NM_MONTANT_DEVIS'], $result['NM_MONTANTS_PRESTATIONS']
            ];
        }
        $after_vol_elements = array_merge(...$after_vol_elements);

        //get the differences between the new and the old
        $differences_assoc = array_diff($after_assoc_fields, $existing_assoc_fields);
        $differences_vol = array_diff($after_vol_elements, $existing_vol_elements);

        if ($differences_assoc || $differences_vol) {
            // insert a trace of the update
            $today = date('Y-m-d H:i:s');

            $req = <<<SQL
                DELETE FROM sc_t_trace
                WHERE
                    DT_TRACE_TRC = "{$today}"
                AND NM_UTILISATEUR_TRC = {$_SESSION['PK_UTILISATEUR_USR']}
                AND TX_DOMAINE_TRC = 'VOLUME'
                AND NM_ITEMID_TRC = {$id_volume};
            SQL;
            $exec = DbExecRequete($req, $oConnexion);

            $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(
                    TX_DOMAINE_TRC,
                    NM_ITEMID_TRC,
                    LB_TRACE_TRC,
                    TX_TYPE_TRC,
                    DT_TRACE_TRC,
                    NM_UTILISATEUR_TRC
                ) VALUES (
                    'VOLUME',
                    {$id_volume},
                    'Mise à jour du volume n°{$e_vol_numcollection} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                    'SQL-U',
                    "{$today}",
                    {$_SESSION['PK_UTILISATEUR_USR']}
                );
                SQL;
            $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
        }

        addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');
        commit($oConnexion);

        redirect(sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'volumes',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'volumes',
            'param2' => 'vol_population',
            'vparam2' => $vol_population,
            'param3' => 'vol_oeuvre',
            'vparam3' => $vol_oeuvre,
            'param4' => 'vol_autancien',
            'vparam4' => $vol_autancien,
            'param5' => 'vol_population',
            'vparam5' => $vol_utilisateur,
            'signet' => $id_volume,
            'id_volume' => $id_volume
        ])));
    }
} else { // Mode création - Premier chargement de la page
    // ...
}
?>
<script type="text/javascript" src="js/tiny_mce_v349/tiny_mce_gzip.js"></script>
<script type="text/javascript" src="js/tinymce-launcher_v349_20120420.js"></script>
<input type="hidden" name="recupform" value="ok">
<input type="hidden" name="id_volume" value="<?php echo $id_volume; ?>">
<input type="hidden" name="id_volume_infos" value="<?php echo $id_volume_infos; ?>">
<!-- ===== Bouton 'Retour' / DEBUT ===== -->
<input type="hidden" name="sourcerub" value="intranet">
<input type="hidden" name="sourcepg" value="gestion">
<input type="hidden" name="param1" value="sectionid">
<input type="hidden" name="vparam1" value="volumes">
<input type="hidden" name="param2" value="vol_population">
<input type="hidden" name="vparam2" value="<?php if (isset($_REQUEST['vparam2'])) {
    echo $_REQUEST['vparam2'];
} ?>">
<input type="hidden" name="vparam3" value="<?php if (isset($_REQUEST['vparam3'])) {
    echo $_REQUEST['vparam3'];
} ?>">
<input type="hidden" name="vparam4" value="<?php if (isset($_REQUEST['vparam4'])) {
    echo $_REQUEST['vparam4'];
} ?>">
<input type="hidden" name="vparam5" value="<?php if (isset($_REQUEST['vparam5'])) {
    echo $_REQUEST['vparam5'];
} ?>">
<input type="hidden" name="vparam6" value="<?php if (isset($_REQUEST['vparam6'])) {
    echo $_REQUEST['vparam6'];
} ?>">
<input type="hidden" name="vparam7" value="<?php if (isset($_REQUEST['vparam7'])) {
    echo $_REQUEST['vparam7'];
} ?>">
<input type="hidden" name="vparam8" value="<?php if (isset($_REQUEST['vparam8'])) {
    echo $_REQUEST['vparam8'];
} ?>">
<input type="hidden" name="vparam9" value="<?php if (isset($_REQUEST['vparam9'])) {
    echo $_REQUEST['vparam9'];
} ?>">
<input type="hidden" name="vparam10" value="<?php if (isset($_REQUEST['vparam10'])) {
    echo $_REQUEST['vparam10'];
} ?>">
<?php if (isset($_REQUEST['signet'])) { ?>
<input type="hidden" name="signet" value="<?php echo $_REQUEST['signet']; ?>">
<?php } ?>
<!-- ===== Bouton 'Retour' / FIN ===== -->
<tr>
	<td align="center">
		<font class="font-titre1"><?php if (empty($id_volume)) {
		    echo 'Ajout d\'un nouveau volume';
		} else {
		    echo 'Modification d\'un volume';
		} ?></font>
	</td>
</tr>
<?php if(hasNotice()): ?>
<tr>
	<td align="center">
		<?php printNotice(); ?>
	</td>
</tr>
<?php endif; ?>

<!-------------------->
<!--début du tableau-->
<!-------------------->

<tr>
	<td width="100%">
		<div align="center">
			<table border="0" width="100%" cellpadding="4" cellspacing="0">
                <?php
                if (!empty($id_volume)) {
                    $sQuery = <<<SQL
                SELECT *
                FROM sc_t_trace
                WHERE
                    TX_DOMAINE_TRC = 'VOLUME'
                AND NM_ITEMID_TRC = {$id_volume}
                ORDER BY DT_TRACE_TRC DESC LIMIT 3;
            SQL;
                    $oRs = DbExecRequete($sQuery, $oConnexion);
                    while ($trace = DbEnregSuivant($oRs)) {
                        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
                    </td>
                </tr>
                HTML;
                    }
                    if (0 == DbNbreEnreg($oRs)) {
                        echo <<<HTML
                <tr>
                    <td colspan="3" align="center">
                        Aucun historique des dernières modifications apportées à ce volume
                    </td>
                </tr>
                HTML;
                    }
                    ?>
                <?php } ?>

        <?php
        if ('reimpressions' == $_REQUEST['vparam1']) {
            $url = sprintf('/?%s#back', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => $_REQUEST['sectionid'],
                'ddlsectionid' => 'reimpressions'
            ]));
        } elseif ('volumes_preparation' == $_REQUEST['vparam1']) {
            $url = sprintf('/?%s#back', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'volumes_preparation'
            ]));
        } elseif('volumes' == $_REQUEST['vparam1']) {
            $url = sprintf('/?%s#back', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => $_REQUEST['sectionid'] ?? null,
                'ddlsectionid' => 'volumes',
                'param2' => 'vol_population',
                'vparam2' => $_GET['vparam2'] ?? null,
                'param3' => "vol_oeuvre",
                'vparam3' => $_GET['vparam3'] ?? null,
                'param4' => 'vol_autancien',
                'vparam4' => $_GET['vparam4'] ?? null,
                'param5' => 'vol_utilisateur',
                'vparam5' => $_GET['vparam5'] ?? null,
                'param10' => 'vol_numero',
                'vparam10' => $_GET['vparam10'] ?? null,
            ]));
        }
        ?>

        <?php $ligneType = 'B'; ?>
        <tr>
            <td colspan="3" align="center">
                <a href="<?php echo $url; ?>">
                    <img src="img/common/retour.gif" width="91" height="24" border="0" style="cursor:hand"></a>
            </td>
        </tr>

                <tr>
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal"><font class="font-datagrid-entete"><b>Caractéristiques du volume</b></font></font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                                </td>
                        </table>
                    </td>
                </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap >
                <font class="font-normal">Oeuvre(s) correspondante(s) :</font><br>
                <font class="font-small" style="margin-left: 10px"><em>si l'oeuvre ne figure pas dans la liste, vous pouvez ajouter <br>
                        l'oeuvre sur la page "gestion oeuvres" puis l'associer à votre nouveau volume.</em></font>
            </td>
            <td align="center" colspan="2">
                <select
                    class="form-select select2"
                    id="field_volume_works"
                    multiple
                    name="volume_works[]"
                    size="5"
                >
                    <?php
                    $sQuery = <<<SQL
                    SELECT
                        A.TX_NOM_FRANCAIS_AAN,
                        O.TX_TITRE_FRANCAIS_OVR,
                        O.PK_OEUVRE_OVR
                    FROM sc_t_oeuvre AS O
                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                        ON O.PK_OEUVRE_OVR =  ao.FK_OEUVRE_OVR
                    LEFT JOIN sc_t_auteur AS A
                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                    WHERE TX_TITRE_FRANCAIS_OVR IS NOT NULL AND
                        A.TX_NOM_FRANCAIS_AAN IS NOT NULL
                    GROUP BY O.PK_OEUVRE_OVR, A.TX_NOM_FRANCAIS_AAN
                    ORDER BY A.TX_NOM_FRANCAIS_AAN;
                    SQL;
                    $oRecordset = DbExecRequete($sQuery, $oConnexion);
                    while ($ovr = DbEnregSuivant($oRecordset)) {
                        echo sprintf(
                            '<option value="%s"%s>%s-%s</option>',
                            $ovr->PK_OEUVRE_OVR,
                            in_array($ovr->PK_OEUVRE_OVR, $volume_works, false) ? ' selected' : '',
                            $ovr->TX_NOM_FRANCAIS_AAN,
                            $ovr->TX_TITRE_FRANCAIS_OVR
                        );
                    }
                    ?>
                </select>
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">

          <td align="left" nowrap>
            <font class="font-normal"><font class="font-couleur-erreur">
                    <?php if (!empty($vol_isbn)) {
                        ?>Volume paru avec l'ISBN suivant<?php } else { ?>ISBN futur<?php } ?> :
            </font></font><br>
            <font class="font-small"><em>Vérifier l’ISBN sur le livre et sur la liste finale.
                    <ul>
                        <li>
                            pour les volumes anciens sans ISBN, mettre un ISBN fictif de 0000000000 ;
                        </li>
                        <li>
                            pour les volumes non encore parus, ne rien mettre : le champ ISBN futur n’est à remplir qu’une fois le volume paru,
                            <br> pour le faire basculer de la catégorie « volume en chantier » à la catégorie « volume paru ».
                        </li>
                    </ul>
                </em>
            </font>
            </td>
            <td align="center"  colspan="2" >
                <input type="text" maxlength="13" name="vol_isbn" value="<?php echo $vol_isbn; ?>" size="40">
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal"><b>Numéro de collection</b> :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="4" name="vol_numcollection" value="<?php echo $vol_numcollection; ?>" size="20">
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Complément (num. de coll.) :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="20" name="vol_numcollection_cplt" value="<?php echo $vol_numcollection_cplt; ?>" size="20">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Ancien numéro de collection :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="4" name="vol_numcollection_ancien" value="<?php echo $vol_numcollection_ancien; ?>" size="20">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Nouveau numéro de collection :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="4" name="vol_numcollection_nouveau" value="<?php echo $vol_numcollection_nouveau; ?>" size="20">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Sous-collection </font><font class="font-small"><em>(Textes monastiques, Série bernardine,..)</em> :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="50" name="vol_sscollection" value="<?php echo $vol_sscollection; ?>" size="20">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Numéro (sous-collection) </font><font class="font-small">(<em>en chiffres arabes</em>) :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="4" name="vol_sscollection_num" value="<?php echo $vol_sscollection_num; ?>" size="20">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
          <td align="left" nowrap>
                <font class="font-normal">Titre (page de titre) :</font><br>
              <font class="font-small"><em>- toujours sous la forme « .., tome X. Livres XX-XX » comme dans la liste finale<br>
                - indiquer les numéros des sermons etc. dans le titre, même s’ils ne figurent pas sur la page de titre</em>
              </font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="255" name="vol_pgtitre_titre" value="<?php echo $vol_pgtitre_titre; ?>" size="60">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Sous-titre (page de titre) :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="255" name="vol_pgtitre_sstitre" value="<?php echo $vol_pgtitre_sstitre; ?>" size="60">
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Année de parution :</font>
            </td>
            <td align="center" colspan="2" >
                <select name="vol_dateparution_an">
                    <option value=NULL>Ann&eacute;e....</option>
                    <?php
                    $anneecourante = date('Y');
for ($i = $anneecourante - 65; $i < $anneecourante + 4; ++$i) {
    echo '<option value="'.$i.'"';
    if ($vol_dateparution_an == $i) {
        echo ' selected';
    }
    echo '>'.$i.'</option>';
}
?>
                </select>
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Mois de parution :</font>
            </td>
            <td align="center" colspan="2" >
                <select name="vol_dateparution_mois">
                    <option value=NULL>Mois....</option>
                    <?php
                    $months = [
                        0 => null,
                        1 => 'Janvier',
                        2 => 'Février',
                        3 => 'Mars',
                        4 => 'Avril',
                        5 => 'Mai',
                        6 => 'Juin',
                        7 => 'Juillet',
                        8 => 'Août',
                        9 => 'Septembre',
                        10 => 'Octobre',
                        11 => 'Novembre',
                        12 => 'Décembre',
                    ];
                    for ($i = 1; $i <= 12; ++$i) {
                        echo '<option value="'.$i.'"';
                        if ($vol_dateparution_mois == $i) {
                            echo ' selected';
                        }
                        echo '>'.$months[$i].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Année de la dernière réimpression :</font>
            </td>
            <td align="center" colspan="2" >
                <select name="vol_datedernierereimp_an">
                    <option value=NULL>Ann&eacute;e....</option>
                    <?php
$anneecourante = date('Y');
for ($i = $anneecourante - 65; $i < $anneecourante + 4; ++$i) {
    echo '<option value="'.$i.'"';
    if ($vol_datedernierereimp_an == $i) {
        echo ' selected';
    }
    echo '>'.$i.'</option>';
}
?>
                </select>
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Nombre de réédition(s) et/ou réimpression(s) </font>
                <font class="font-small" style="margin-left: 5px">(<em>en l'absence de réimpression, mettre 0</em>) :</font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="2" name="vol_reimp_nb" value="<?php echo 'NULL' == $vol_reimp_nb ? '' : $vol_reimp_nb; ?>" size="4">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
          <td align="left" nowrap>
                <font class="font-normal">Précision(s) sur les rééditions/réimpressions :</font><br>
              <font class="font-small"><em>- indiquer la date des réimpressions intermédiaires<br>
                - pour les détails, créer une fiche pour chaque réimpression</em>
              </font>
            </td>
            <td align="center" colspan="2" >
                <input type="text" maxlength="50" name="vol_reimp_nb_cplt" value="<?php echo $vol_reimp_nb_cplt; ?>" size="40">
            </td>
        </tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Domaine </font><font class="font-small">(<em>G grec, L latin, M Moyen Age, O oriental</em>) :</font>
            </td>
            <td align="center" colspan="2" >
                <select name="vol_domaine" size="1">
                    <option value=""<?php if ('' == $vol_domaine) {
                        echo ' selected';
                    } ?>>---</option>
                    <option value="G"<?php if ('G' == $vol_domaine) {
                        echo ' selected';
                    } ?>>G</option>
                    <option value="L"<?php if ('L' == $vol_domaine) {
                        echo ' selected';
                    } ?>>L</option>
                    <option value="M"<?php if ('M' == $vol_domaine) {
                        echo ' selected';
                    } ?>>M</option>
                    <option value="O"<?php if ('O' == $vol_domaine) {
                        echo ' selected';
                    } ?>>O</option>
                </select>
            </td>
        </tr>


        <tr>
            <td colspan="3" align="center">
                <table border="0" width="100%" class="datagrid-entete">
                    <tr>
                        <td width="75%">
                            <font class="font-normal"><font class="font-datagrid-entete"><b>Subvention</b></font></font>
                        </td>
                        <td width="25%" align="right">
                            <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                        </td>
                </table>
            </td>
        </tr>
        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <font class="font-normal">Subvention reçue </font>(<font class="font-small"><em>données confidentielles</em></font>) :<br>
                <?php
                echo '<font class="font-normal" style="margin-left: 30px">';

echo '<input type="radio" name="vol_subvention_recue" value="1"';
if ('1' == $vol_subvention_recue) {
    echo ' checked';
}
echo '>&nbsp;oui';

echo '&nbsp;&nbsp;<input type="radio" name="vol_subvention_recue" value="0"';
if ('0' == $vol_subvention_recue) {
    echo ' checked';
}
echo '>&nbsp;non';

echo '&nbsp;&nbsp;<input type="radio" name="vol_subvention_recue" value=NULL';
if (null === $vol_subvention_recue) {
    echo ' checked';
}
echo '>&nbsp;non applicable</font><br>';
?>
            </td>
            <td>
                <div align="left">
                    <p class="font-normal" style="text-align: center; margin-bottom: 0px">Mécène :</p>
                    <input type="text" maxlength="255" name="vol_mecene" value="<?php echo $vol_mecene; ?>" size="40">
                </div>
            </td>
        </tr>


        <tr>
            <td colspan="3" align="center">
                <table border="0" width="100%" class="datagrid-entete">
                    <tr>
                        <td width="75%">
                            <font class="font-normal"><font class="font-datagrid-entete"><b>PAO</b></font></font>
                        </td>
                        <td width="25%" align="right">
                            <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                        </td>
                </table>
            </td>
        </tr>
        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left" nowrap>
                <div align="left">
                    <p class="font-normal" style="text-align: center; margin-bottom: 0px">Mise en page :</p>
                    <input type="text" maxlength="255" name="vol_imprimeur" value="<?php echo $vol_imprimeur; ?>" size="40">
                </div>
            </td>
            <td align="center" colspan="2">
                <font class="font-normal">Jaquette (<font class="font-small"><em>données confidentielles</em></font>) :</font><br>
                <?php
echo '<font class="font-normal">';

echo '<input type="radio" name="vol_jaquette_recue" value="1"';
if ('1' == $vol_jaquette_recue) {
    echo ' checked';
}
echo '>&nbsp;oui';

echo '&nbsp;&nbsp;<input type="radio" name="vol_jaquette_recue" value="0"';
if ('0' == $vol_jaquette_recue) {
    echo ' checked';
}
echo '>&nbsp;non';

echo '&nbsp;&nbsp;<input type="radio" name="vol_jaquette_recue" value=NULL';
if (null === $vol_jaquette_recue) {
    echo ' checked';
}
echo '>&nbsp;non renseigné</font><br>';
?>
            </td>
            <td width="25"></td>
        </tr>

        <tr>
            <td colspan="3" align="center">
                <table border="0" width="100%" class="datagrid-entete">
                    <tr>
                        <td width="75%" >
                            <font class="font-normal"><font class="font-datagrid-entete"><b>Devis et prestations</b></font></font>
                        </td>
                        <td width="25%" align="right">
                            <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                        </td>
                </table>
            </td>
        </tr>

        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr>
            <td>
                <div align="left">
                    <p class="font-normal" style="text-align: center; margin-bottom: 0px">Devis</p>
                    <input type="text" maxlength="255" name="vol_devis" value="<?php echo 'NULL' == $vol_devis ? '' : $vol_devis; ?>"
                           size="20" maxlength="4">
                </div>
            </td>
            <td colspan="2">
                <div align="left">
                    <p class="font-normal" style="text-align: center; margin-bottom: 0px">Prestations complémentaires</p>
                    <input type="text" maxlength="255" name="vol_prestations" value="<?php echo 'NULL' == $vol_prestations ? '' : $vol_prestations; ?>"
                           size="20" maxlength="4">
                </div>
            </td>
        </tr>

<tr>
    <td colspan="3" align="center">
        <table border="0" width="100%" class="datagrid-entete">
            <tr>
                <td width="75%">
                    <font class="font-normal"><font class="font-datagrid-entete"><b>Divers</b></font></font>
                </td>
                <td width="25%" align="right">
                    <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                </td>
        </table>
    </td>
</tr>


        <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
        <tr class="datagrid-lignes<?php echo $ligneType; ?>">
            <td align="left">
                <font class="font-normal">Remarques :</font><br><br>
                    <p style="margin-left: 10px" class="font-small"><em>le fil des événements relatifs à la préparation de ce volume, à partir des mails, tél, et données du CS</em></p>
            </td>
            <td align="center" colspan="2">
                <textarea id="vol_remarques" name="vol_remarques" rows="10" cols="90" class="mceEditor"><?php echo $vol_remarques; ?></textarea>
            </td>
        </tr>

                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal"><font class="font-datagrid-entete"><b>Tâche(s) et collaborateur(s) associés à cette oeuvre</b></font></font>								</td>
                                <td width="25%" align="right">
                                    <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>								</td>
                        </table>
                    </td>
                </tr>

                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3">
                        <table class="table table-striped table-sm" id="tbl_usr_task" >
                            <thead>
                            <tr>
                                <th scope="col" width="280px">&OElig;uvre</th>
                                <th scope="col" width="68px">Tâche</th>
                                <th scope="col" width="105px">Collaborateur</th>
                                <th scope="col" width="154px">Date de début</th>
                                <th scope="col" width="154px">Date de fin</th>
                                <th scope="col">Remarques</th>
                                <th scope="col" width="224px">Degré d'avancement de la tâche</th>
                                <th scope="col" width="80px">Manuscrit<br />remis</th>
                                <th scope="col" width="60px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            // Users/Tasks list
                            $wuts = [];
                            if ($id_volume) {
                                $sQuery = <<<SQL
                                SELECT
                                    PK_ASSOC_OVRUSRTCH,
                                    FK_OUT_REF_TCH,
                                    FK_OUT_REF_USR,
                                    FK_OUT_REF_OVR,
                                    LB_OUT_AVT_TCH,
                                    DT_DEBUT_TCH,
                                    DT_FIN_TCH,
                                    TX_REMARQUES_OUT,
                                    BL_MANUSCRIT_REMIS
                                FROM sc_t_assoc_ovr_usr_tch
                                WHERE FK_OUT_REF_VOL={$id_volume}
                                GROUP BY FK_OUT_REF_TCH, FK_OUT_REF_USR, LB_OUT_AVT_TCH, PK_ASSOC_OVRUSRTCH
                                ORDER BY PK_ASSOC_OVRUSRTCH;
                                SQL;
                                $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                $wuts = DbEnregTab($oRecordset);
                            }

                            // Works list
                            $works = [];
                            if (count($wuts)) {
                                $in = implode(', ', array_filter(array_map(function ($wut) {
                                    return $wut['FK_OUT_REF_OVR'] ?? null;
                                }, $wuts)));
                                if ($in) {
                                    $sQueryTask = <<<SQL
                                    SELECT
                                        sc_t_oeuvre.PK_OEUVRE_OVR,
                                        sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR,
                                        GROUP_CONCAT(DISTINCT sc_t_auteur.TX_NOM_FRANCAIS_AAN SEPARATOR ', ') AS authors
                                    FROM sc_t_oeuvre
                                    LEFT JOIN sc_t_assoc_au_ovr ON sc_t_assoc_au_ovr.FK_OEUVRE_OVR = sc_t_oeuvre.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur ON sc_t_assoc_au_ovr.FK_AUTEUR_ANCIEN_AAN = sc_t_auteur.PK_AUTEUR_ANCIEN_AAN
                                    WHERE sc_t_oeuvre.PK_OEUVRE_OVR IN ({$in})
                                    GROUP BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR
                                    ORDER BY sc_t_oeuvre.TX_TITRE_FRANCAIS_OVR;
                                    SQL;
                                    $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
                                    $works = DbEnregTab($oRecordsetTask);
                                }
                            }

                            // Tasks list
                            $sQueryTask = <<<SQL
                            SELECT
                                PK_TACHE_TCH,
                                LB_TACHE_TCH
                            FROM sc_t_tache
                            ORDER BY NM_ORDRE_TCH;
                            SQL;
                            $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
                            $tasks = DbEnregTab($oRecordsetTask);

                            // Users list
                            $users = [];
                            if (count($wuts)) {
                                $in = implode(', ', array_filter(array_map(function ($wut) {
                                    return $wut['FK_OUT_REF_USR'] ?? null;
                                }, $wuts)));
                                if ($in) {
                                    $sQueryUser = <<<SQL
                                    SELECT
                                        PK_UTILISATEUR_USR,
                                        TX_NOM_USR,
                                        TX_PRENOM_USR,
                                        BL_DECEDE_USR
                                    FROM sc_t_utilisateur
                                    WHERE PK_UTILISATEUR_USR IN ({$in})
                                    ORDER BY TX_NOM_USR;
                                    SQL;
                                    $oRecordsetUser = DbExecRequete($sQueryUser, $oConnexion);
                                    $users = DbEnregTab($oRecordsetUser);
                                }
                            }

                            // level of progress of the task list
                            $sQueryLevel = <<<SQL
                            SELECT
                                LB_OUT_AVT_TCH
                            FROM sc_t_assoc_ovr_usr_tch
                            GROUP BY LB_OUT_AVT_TCH
                            ORDER BY LB_OUT_AVT_TCH;
                            SQL;
                            $oRecordsetLevel = DbExecRequete($sQueryLevel, $oConnexion);
                            $levels = DbEnregTab($oRecordsetLevel);

                            // Dates list
                            $sQueryDates = <<<SQL
                            SELECT
                                DT_DEBUT_TCH,
                                DT_FIN_TCH
                            FROM sc_t_assoc_ovr_usr_tch
                            WHERE DT_DEBUT_TCH IS NOT NULL
                            GROUP BY DT_DEBUT_TCH;
                            SQL;
                            $oRecordsetDates = DbExecRequete($sQueryDates, $oConnexion);
                            $dates = DbEnregTab($oRecordsetDates);

                            // Remarques list
                            $sQueryRmk = <<<SQL
                            SELECT
                                TX_REMARQUES_OUT
                            FROM sc_t_assoc_ovr_usr_tch
                            WHERE TX_REMARQUES_OUT IS NOT NULL
                            GROUP BY TX_REMARQUES_OUT;
                            SQL;
                            $oRecordsetRmk = DbExecRequete($sQueryRmk, $oConnexion);
                            $rmks = DbEnregTab($oRecordsetRmk);

                            // Manuscrit remis list
                            $sQueryMs = <<<SQL
                            SELECT
                                BL_MANUSCRIT_REMIS
                            FROM sc_t_assoc_ovr_usr_tch
                            GROUP BY BL_MANUSCRIT_REMIS;
                            SQL;
                            $oRecordsetMs = DbExecRequete($sQueryMs, $oConnexion);
                            $mss = DbEnregTab($oRecordsetMs);
                            ?>
                            <?php foreach ($wuts as $wut) {
                                $fk_usr = $wut['FK_OUT_REF_USR'];
                                $fk_ovr = $wut['FK_OUT_REF_OVR'];
                                $fk_tch = $wut['FK_OUT_REF_TCH'];
                                $fk_avt = $wut['LB_OUT_AVT_TCH']; ?>
                                <tr data-id="wut_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>">
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-ajax--url="/api/autocomplete/work"
                                            data-ajax--cache="true"
                                            data-placeholder="Œuvre"
                                            id="field_vol_ovr_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="vol_ovr[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                            required
                                        >
                                            <?php foreach ($works as $work): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s - %s</option>',
                                                    $work['PK_OEUVRE_OVR'],
                                                    $wut['FK_OUT_REF_OVR'] === $work['PK_OEUVRE_OVR'] ? ' selected' : '',
                                                    $work['authors'],
                                                    $work['TX_TITRE_FRANCAIS_OVR']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-placeholder="Tâche"
                                            id="field_vol_tsk_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="vol_tch[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                            required
                                        >
                                            <?php foreach ($tasks as $task): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s</option>',
                                                    $task['PK_TACHE_TCH'],
                                                    $wut['FK_OUT_REF_TCH'] === $task['PK_TACHE_TCH'] ? ' selected' : '',
                                                    $task['LB_TACHE_TCH']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-ajax--url="/api/autocomplete/user"
                                            data-ajax--cache="true"
                                            data-placeholder="Utilisateur"
                                            id="field_vol_user_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="vol_usr[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                            required
                                        >
                                            <?php foreach ($users as $user): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s%s %s</option>',
                                                    $user['PK_UTILISATEUR_USR'],
                                                    $wut['FK_OUT_REF_USR'] == $user['PK_UTILISATEUR_USR'] ? ' selected' : '',
                                                    $user['BL_DECEDE_USR'] == 1 ? '† ' : '',
                                                    $user['TX_NOM_USR'],
                                                    $user['TX_PRENOM_USR']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input
                                            class="form-control"
                                            name="vol_begin[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                            placeholder="AAAA-MM(-DD)"
                                            title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                            type="text"
                                            value="<?php echo $wut['DT_DEBUT_TCH']; ?>"
                                        />
                                    </td>
                                    <td>
                                        <input
                                            class="form-control"
                                            name="vol_end[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                            placeholder="AAAA-MM(-DD)"
                                            title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                            type="text"
                                            value="<?php echo $wut['DT_FIN_TCH']; ?>"
                                        />
                                    </td>
                                    <td>
                                        <textarea
                                            class="form-control"
                                            name="vol_rmq[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            placeholder="Remarques"
                                            rows="1"
                                        ><?php echo strip_tags(html_entity_decode($wut['TX_REMARQUES_OUT'] ?? '')); ?></textarea>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            name="vol_lvl[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                        >
                                            <option
                                                <?php if(empty($wut['LB_OUT_AVT_TCH'])): ?>
                                                    selected
                                                <?php endif; ?>
                                                value=""
                                            >Niveau d'avancement</option>
                                            <?php foreach ($levels as $level): ?>
                                                <?php if(!empty($level['LB_OUT_AVT_TCH'])): ?>
                                                    <option
                                                        <?php if ($wut['LB_OUT_AVT_TCH'] == $level['LB_OUT_AVT_TCH']) { ?>
                                                            selected="selected"
                                                        <?php } ?>
                                                        value="<?php echo $level['LB_OUT_AVT_TCH']; ?>"
                                                    ><?php echo "{$level['LB_OUT_AVT_TCH']}"; ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input
                                                <?php if ('1' == $wut['BL_MANUSCRIT_REMIS']): ?>
                                                    checked
                                                <?php endif; ?>
                                                class="form-check-input"
                                                name="vol_ms[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                                type="checkbox"
                                                value="1"
                                            />
                                        </div>
                                    </td>
                                    <td>
                                        <?php
                                        $url_suppr = $url = sprintf('/?%s', http_build_query([
                                            'rubriqueid' => 'intranet',
                                            'pageid' => 'gestion',
                                            'sectionid' => 'volumes',
                                            'action' => 'suppr',
                                            'detail' => 'ok',
                                            'sourcerub' => 'intranet',
                                            'sourcepg' => 'gestion',
                                            'param1' => 'sectionid',
                                            'vparam1' => 'volumes',
                                            'signet' => $id_volume,
                                            'id_volume' => $id_volume,
                                            'fk_usr' => $fk_usr,
                                            'fk_ovr' => $fk_ovr,
                                            'fk_tch' => $fk_tch
                                        ]));
                                        ?>
                                        <button
                                            class="btn btn-secondary"
                                            <?php if ($rights_ext || $rights_member_cs): ?>
                                                disabled="disabled"
                                            <?php endif; ?>
                                            onclick="if (confirm('Êtes-vous sûr de vouloir supprimer cette association utilisateur/tâche ?')) location.href='<?php echo $url_suppr; ?>'"
                                            type="button"
                                        >
                                            Suppr.
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot id="foot_tbl_usr_task">
                            <tr>
                                <th colspan="9">
                                    Associer une ou plusieurs tâches à un ou plusieurs utilisateurs
                                </th>
                            </tr>
                            <tr data-id="wut_0">
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-ajax--url="/api/autocomplete/work"
                                        data-ajax--cache="true"
                                        data-placeholder="Œuvre"
                                        id="field_vol_ovr_0"
                                        multiple
                                        name="vol_ovr[0][]"
                                    ></select>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-placeholder="Tâche"
                                        id="field_vol_tch_0"
                                        multiple
                                        name="vol_tch[0][]"
                                    >
                                        <?php foreach ($tasks as $task) { ?>
                                            <option value="<?php echo $task['PK_TACHE_TCH']; ?>">
                                                <?php echo $task['LB_TACHE_TCH']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-ajax--url="/api/autocomplete/user"
                                        data-ajax--cache="true"
                                        data-placeholder="Utilisateur"
                                        id="field_vol_usr_0"
                                        multiple
                                        name="vol_usr[0][]"
                                    ></select>
                                </td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="vol_begin[0]"
                                        pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                        placeholder="AAAA-MM(-DD)"
                                        title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                        type="text"
                                    />
                                </td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="vol_end[0]"
                                        pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                        placeholder="AAAA-MM(-DD)"
                                        title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                        type="text"
                                    />
                                </td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="vol_rmq[0]"
                                        placeholder="Remarques"
                                        rows="1"
                                    ></textarea>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        name="vol_lvl[0]"
                                    >
                                        <option value="">
                                            Niveau d'avancement
                                        </option>
                                        <?php foreach ($levels as $level): ?>
                                            <?php if(!empty($level['LB_OUT_AVT_TCH'])): ?>
                                                <option value="<?php echo $level['LB_OUT_AVT_TCH']; ?>"><?php echo "{$level['LB_OUT_AVT_TCH']}"; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input
                                            class="form-check-input"
                                            name="vol_ms[0]"
                                            type="checkbox"
                                            value="1"
                                        />
                                    </div>
                                </td>
                                <td>
                                    <a
                                        class="btn btn-secondary"
                                        <?php if ($rights_ext): ?>
                                            disabled="disabled"
                                        <?php endif; ?>
                                        onclick="if(this.closest('tfoot').children.length > 2) this.closest('tr').remove()"
                                    >
                                        Suppr.
                                    </a>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <script>
                            $('select.select2').select2();
                            function addRow() {
                                const table = document.querySelector('#tbl_usr_task');
                                const tfoot = table.querySelector('tfoot');
                                const emptyRow = tfoot.querySelector('tr:nth-child(2)');
                                $(emptyRow).find('select.select2').select2('destroy');
                                const clone = emptyRow.cloneNode(true);
                                let index = 0;
                                while (document.querySelector(`tr[data-id="wut_${index}"]`)) {
                                    index += 1;
                                }
                                clone.dataset.id = `wut_${index}`;
                                clone.querySelectorAll('input, textarea, select, button').forEach(input => {
                                    input.setAttribute('id', (input.getAttribute('id') || '').replace(/_\d+$/, `_${index}`));
                                    input.setAttribute('name', (input.getAttribute('name') || '').replace(/\[\d+\]/, `[${index}]`));
                                    input.value = '';
                                });
                                tfoot.appendChild(clone);
                                $(emptyRow).find('select.select2').select2();
                                $(clone).find('select').select2();
                            }
                        </script>
                        <button
                            class="btn btn-light add-more"
                            <?php if ($rights_ext): ?>
                                disabled="disabled"
                            <?php endif; ?>
                            onclick="addRow();"
                            type="button"
                        >
                            Ajouter une autre tâche
                        </button>
                    </td>
                </tr>

        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>

        <tr>
            <td colspan="3" align="center">
                <a name="validation">
                <input type="button" name="btn_valider" class="btn"
                   <?php if ($rights_ext) { ?> style="display: none" <?php } ?>
                   value="&nbsp;&nbsp;<?php if (empty($id_volume)) {
                       echo 'Créer le volume';
                   } else {
                       echo 'Valider les modifications';
                   } ?>&nbsp;&nbsp;"
                onclick="javascript:testFormulaireVolume();">
                </a>
            </td>
        </tr>
			</table>
		</div>
	</td>
</tr>
<?php
DbClose($oConnexion);
?>