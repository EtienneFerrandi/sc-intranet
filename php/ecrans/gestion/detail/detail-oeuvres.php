<?php

$url_originale = $_POST['url_originale'] ?? "https://{$_SERVER['HTTP_HOST']}{$_SERVER['SCRIPT_NAME']}?{$_SERVER['QUERY_STRING']}";
$oConnexion = DbConnection();

$needsAdditionalCollaborations = '0';

$id_oeuvre = '';

$rights_admin = [];
$rights_member_sc = [];
$rights_member_cs = [];
$rights_ext = [];

$ovr_vol = [];
$ovr_tch = [];
$ovr_usr = [];
$ovr_lvl = [];
$ovr_begin = [];
$ovr_end = [];
$ovr_rmq = [];
$ovr_ms = [];

$ovr_fk_aan = '';

$ovr_latin = '';
$ovr_francais = '';
$ovr_clavis = '';

$ovr_interne = '';
$ovr_type_manuscrit_dispo = '';
$ovr_localisation = '';
$ovr_rqs_localisation = '';

$ovr_url_biblindex = '';

$ovr_remarques1 = '';
$ovr_remarques2 = '';

$id_source = '';

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}

if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%Oeuvres%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member_sc[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member_cs[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        (LB_DROIT_DRT LIKE '%Volumes%' OR LB_DROIT_DRT LIKE '%Tâches%') AND
        (LB_DROIT_DRT NOT LIKE '%Suppression' AND LB_DROIT_DRT NOT LIKE '%Edition');
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (!isset($_POST['recupform']) && isset($_GET['id_oeuvre'])) {
    $id_ovr = $_GET['id_oeuvre'];
    global $id_oeuvre;
    $id_oeuvre = $id_ovr;

    /* delete association task / user */
    if (isset($_REQUEST['action'])) {
        $fk_user = empty($_GET['fk_usr']) ? 'IS NULL' : "= {$_GET['fk_usr']}";
        $fk_volume = empty($_GET['fk_vol']) ? 'IS NULL' : "= {$_GET['fk_vol']}";
        $fk_task = empty($_GET['fk_tch']) ? 'IS NULL' : "= {$_GET['fk_tch']}";
        if ($_REQUEST['action'] == 'suppr') {
            begin($oConnexion);
            $sQuery = <<<SQL
            DELETE FROM sc_t_assoc_ovr_usr_tch
            WHERE
                FK_OUT_REF_OVR = {$id_oeuvre}
            AND FK_OUT_REF_VOL {$fk_volume}
            AND FK_OUT_REF_USR {$fk_user}
            AND FK_OUT_REF_TCH {$fk_task};
            SQL;
            $oRecordset = DbExecRequete($sQuery, $oConnexion);
            commit($oConnexion);
            addNotice('success', 'Tâche supprimée');
        }
    }

    $sQuery = <<<SQL
    SELECT
        O.*,
        au.FK_AUTEUR_ANCIEN_AAN,
        ot.FK_OUT_REF_VOL,
        ot.FK_OUT_REF_USR,
        ot.FK_OUT_REF_TCH,
        ot.LB_OUT_AVT_TCH,
        ot.DT_DEBUT_TCH,
        ot.DT_FIN_TCH,
        ot.TX_REMARQUES_OUT,
        ot.BL_MANUSCRIT_REMIS
    FROM sc_t_oeuvre AS O
    LEFT JOIN sc_t_assoc_au_ovr AS au
        ON O.PK_OEUVRE_OVR = au.FK_OEUVRE_OVR
    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
        ON O.PK_OEUVRE_OVR = ot.FK_OUT_REF_OVR
    WHERE O.PK_OEUVRE_OVR = {$id_oeuvre}
    GROUP BY O.PK_OEUVRE_OVR;
    SQL;

    $oRecordset = DbExecRequete($sQuery, $oConnexion);
    if (1 == DbNbreEnreg($oRecordset)) {
        $ovr = DbEnregSuivant($oRecordset);

        $needsAdditionalCollaborations = empty($ovr->BL_COLLAB_SUPPL_OVR) ? '0' : '1';
        $ovr_vol = $ovr->FK_OUT_REF_VOL;
        $ovr_tch = $ovr->FK_OUT_REF_TCH;
        $ovr_usr = $ovr->FK_OUT_REF_USR;
        $ovr_lvl = $ovr->LB_OUT_AVT_TCH;
        $ovr_begin = $ovr->DT_DEBUT_TCH;
        $ovr_end = $ovr->DT_FIN_TCH;
        $ovr_rmq = $ovr->TX_REMARQUES_OUT;
        $ovr_ms = $ovr->BL_MANUSCRIT_REMIS;

        $ovr_fk_aan = $ovr->FK_AUTEUR_ANCIEN_AAN;

        $ovr_latin = $ovr->TX_TITRE_LATIN_OVR;
        $ovr_francais = $ovr->TX_TITRE_FRANCAIS_OVR;
        $ovr_clavis = $ovr->TX_CLAVIS_OVR;

        $ovr_remarques1 = $ovr->TX_REMARQUES1_OVR;
        $ovr_remarques2 = $ovr->TX_REMARQUES2_OVR;

        $ovr_type_manuscrit_dispo = $ovr->TX_TYPE_MANUSCRIT_DISPO_OVR;
        $ovr_localisation = $ovr->TX_LOCALISATION_OVR;
        $ovr_rqs_localisation = $ovr->TX_RQS_LOCALISATION_OVR;

        $ovr_interne = $ovr->BL_OEUVRE_INTERNE_OVR;
        $ovr_url_biblindex = $ovr->TX_URL_BIBLINDEX_OVR;
    }
} elseif (null != $_POST) {// L'utilisateur a soumis le formulaire
    $needsAdditionalCollaborations = empty($_POST['needsAdditionalCollaborations']) ? '0' : '1';
    $letter = $_POST['vparam2'];
    $id_oeuvre = $_POST['id_oeuvre'];

    $ovr_vol = $_POST['ovr_vol'] ?? [];
    $ovr_usr = $_POST['ovr_usr'] ?? [];
    $ovr_tch = $_POST['ovr_tch'] ?? [];
    $ovr_lvl = $_POST['ovr_lvl'] ?? [];
    $ovr_begin = $_POST['ovr_begin'] ?? [];
    $ovr_end = $_POST['ovr_end'] ?? [];
    $ovr_rmq = $_POST['ovr_rmq'] ?? [];
    $ovr_ms = $_POST['ovr_ms'] ?? [];

    $ovr_fk_aan = $_POST['ovr_fk_aan'];

    $ovr_latin = _trim($_POST['ovr_latin']);
    $ovr_francais = _trim($_POST['ovr_francais']);
    $ovr_clavis = _trim($_POST['ovr_clavis']);

    $ovr_remarques1 = _trim($_POST['ovr_remarques1']);
    $ovr_remarques2 = _trim($_POST['ovr_remarques2']);

    $ovr_type_manuscrit_dispo = _trim($_POST['ovr_type_manuscrit_dispo']);
    $ovr_localisation = _trim($_POST['ovr_localisation']);
    $ovr_rqs_localisation = _trim($_POST['ovr_rqs_localisation']);

    $ovr_interne = $_POST['ovr_interne'];

    $ovr_url_biblindex = $_POST['ovr_url_biblindex'];

    begin($oConnexion);

    if (empty($id_oeuvre)) {    // On insère un nouvel ...
        $ovr_remarques1 = mysqli_real_escape_string($oConnexion, $ovr_remarques1);
        $ovr_remarques2 = mysqli_real_escape_string($oConnexion, $ovr_remarques2);
        $sQuery = <<<SQL
            INSERT INTO sc_t_oeuvre(
                TX_TITRE_LATIN_OVR,
                TX_TITRE_FRANCAIS_OVR,
                BL_OEUVRE_INTERNE_OVR,
                TX_CLAVIS_OVR,
                TX_REMARQUES1_OVR,
                TX_REMARQUES2_OVR,
                TX_URL_BIBLINDEX_OVR,
                TX_TYPE_MANUSCRIT_DISPO_OVR,
                TX_LOCALISATION_OVR,
                TX_RQS_LOCALISATION_OVR,
                BL_COLLAB_SUPPL_OVR
            ) VALUES (
                "{$ovr_latin}",
                "{$ovr_francais}",
                {$ovr_interne},
                "{$ovr_clavis}",
                "{$ovr_remarques1}",
                "{$ovr_remarques2}",
                "{$ovr_url_biblindex}",
                "{$ovr_type_manuscrit_dispo}",
                "{$ovr_localisation}",
                "{$ovr_rqs_localisation}",
                {$needsAdditionalCollaborations}
            );
            SQL;

        /* verification si l'oeuvre insérée n'existe pas déjà. Auquel cas, ajout d'une nouvelle oeuvre */
        $req = <<<SQL
                SELECT TX_TITRE_FRANCAIS_OVR
                FROM sc_t_oeuvre
                WHERE TX_TITRE_FRANCAIS_OVR LIKE "{$ovr_francais}";
                SQL;
        $exec = DbExecRequete($req, $oConnexion);

        if (0 == mysqli_num_rows($exec)) {
            $oRecordset = DbExecRequete($sQuery, $oConnexion);
            $ovr_id = mysqli_insert_id($oConnexion);
            global $id_ovr;
            $id_ovr = $ovr_id;

            /* ajout du lien auteur oeuvre */
            $sQuery1 = <<<SQL
                INSERT INTO sc_t_assoc_au_ovr(
                    FK_AUTEUR_ANCIEN_AAN,
                    FK_OEUVRE_OVR
                ) VALUES (
                    {$ovr_fk_aan},
                    {$id_ovr}
                );
                SQL;

            $oRecordset1 = DbExecRequete($sQuery1, $oConnexion);
            /* FIN ajout du lien auteur oeuvre */

            /* ajout lien utilisateur tâche */
            $wuts_ids = array_unique(array_merge(
                array_keys($ovr_tch),
                array_keys($ovr_usr),
                array_keys($ovr_vol)
            ));
            if (count(array_diff_key($ovr_tch, $ovr_usr)) || count(array_diff_key($ovr_usr, $ovr_tch))) {
                addNotice('danger', "Certaines tâches n'ont pas pu être enregistrées : la tâche ou l'utilisateur sont manquants.");
            }

            $queries = [];
            if ('0' == $ovr_interne && count($wuts_ids)) {
                addNotice('danger', "Il est impossible d’enregistrer des tâches pour une œuvre non prévue.");
            }
            foreach ($wuts_ids as $index) {
                $tasks_ids = $ovr_tch[$index] ?? []; // field is required
                $users_ids = $ovr_usr[$index] ?? []; // field is required
                $volumes_ids = $ovr_vol[$index] ?? [null]; // field is optional
                foreach ($tasks_ids as $task_id) {
                    foreach ($users_ids as $user_id) {
                        foreach ($volumes_ids as $volume_id) {
                            $id_ovr = $id_ovr ?? 'NULL';
                            $task_id = $task_id ?? 'NULL';
                            $user_id = $user_id ?? 'NULL';
                            $volume_id = $volume_id ?? 'NULL';
                            $lvl_id = $ovr_lvl[$index] ?? null;
                            $begin_id = $ovr_begin[$index] ?? null;
                            $end_id = $ovr_end[$index] ?? null;
                            $rmq_id = $ovr_rmq[$index] ?? null;
                            $ms_id = '1' === ($ovr_ms[$index] ?? '0') ? '1' : '0';

                            $queries[] = <<<SQL
                            INSERT INTO sc_t_assoc_ovr_usr_tch(
                                FK_OUT_REF_OVR,
                                FK_OUT_REF_TCH,
                                FK_OUT_REF_USR,
                                FK_OUT_REF_VOL,
                                LB_OUT_AVT_TCH,
                                DT_DEBUT_TCH,
                                DT_FIN_TCH,
                                TX_REMARQUES_OUT,
                                BL_MANUSCRIT_REMIS
                            ) VALUES (
                                {$id_ovr},
                                {$task_id},
                                {$user_id},
                                {$volume_id},
                                "{$lvl_id}",
                                "{$begin_id}",
                                "{$end_id}",
                                "{$rmq_id}",
                                {$ms_id}
                            );
                            SQL;
                        }
                    }
                }
            }
            array_walk($queries, static function (string $query) use ($oConnexion): void {
                DbExecRequete($query, $oConnexion);
            });
            /* FIN ajout lien utilisateur tâche */

            if (mysqli_affected_rows($oConnexion)) {
                // insert a trace of the addition
                $str_oeuvre = addslashes("l'oeuvre");
                $today = date('Y-m-d H:i:s');
                $ovr_francais = addslashes($ovr_francais);
                $sQueryTrace = <<<SQL
            INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
            VALUES('OEUVRE',
                   {$ovr_id},
                   'Ajout de {$str_oeuvre} {$ovr_francais} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                   'SQL-I',
                   "{$today}",
                   {$_SESSION['PK_UTILISATEUR_USR']}
                   );
            SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
            }

            commit($oConnexion);

            addNotice('success', '&#338;uvre correctement ajoutée !');

            redirect(sprintf('/?%s', http_build_query([
                'rubriqueid' => 'intranet',
                'pageid' => 'gestion',
                'sectionid' => 'oeuvres',
                'detail' => 'ok',
                'sourcerub' => 'intranet',
                'sourcepg' => 'gestion',
                'param1' => 'sectionid',
                'vparam1' => 'oeuvres',
                'param2' => 'lettre',
                'vparam2' => $letter,
                'signet' => $id_ovr,
                'id_oeuvre' => $id_ovr
            ])));

        } else {
            addNotice('danger', '&#338;uvre déjà existante !');
        }
    } else {// On met à jour ... existant
        //get the previous work elements
        $req = <<<SQL
            SELECT
                TX_TITRE_LATIN_OVR,
                TX_TITRE_FRANCAIS_OVR,
                TX_CLAVIS_OVR,
                BL_OEUVRE_INTERNE_OVR,
                TX_TYPE_MANUSCRIT_DISPO_OVR,
                TX_LOCALISATION_OVR,
                TX_RQS_LOCALISATION_OVR,
                TX_REMARQUES1_OVR,
                TX_REMARQUES2_OVR,
                TX_URL_BIBLINDEX_OVR
            FROM sc_t_oeuvre
            WHERE PK_OEUVRE_OVR = {$id_oeuvre}
            ORDER BY TX_TITRE_LATIN_OVR, TX_TITRE_FRANCAIS_OVR, TX_CLAVIS_OVR, BL_OEUVRE_INTERNE_OVR, TX_TYPE_MANUSCRIT_DISPO_OVR, TX_LOCALISATION_OVR, TX_RQS_LOCALISATION_OVR, TX_REMARQUES1_OVR, TX_REMARQUES2_OVR, TX_URL_BIBLINDEX_OVR;
        SQL;
        $oRecordset = DbExecRequete($req, $oConnexion);
        $existing_ovr_elements = [];
        while ($result = DbEnregSuivantTab($oRecordset)) {
            $existing_ovr_elements[] = [
                $result['TX_TITRE_LATIN_OVR'],
                $result['TX_TITRE_FRANCAIS_OVR'],
                $result['TX_CLAVIS_OVR'],
                $result['BL_OEUVRE_INTERNE_OVR'],
                $result['TX_TYPE_MANUSCRIT_DISPO_OVR'],
                $result['TX_LOCALISATION_OVR'],
                $result['TX_RQS_LOCALISATION_OVR'],
                $result['TX_REMARQUES1_OVR'],
                $result['TX_REMARQUES2_OVR'],
                $result['TX_URL_BIBLINDEX_OVR'],
            ];
        }
        $existing_ovr_elements = array_merge(...$existing_ovr_elements);

        $ovr_remarques1 = mysqli_real_escape_string($oConnexion, $ovr_remarques1);
        $ovr_remarques2 = mysqli_real_escape_string($oConnexion, $ovr_remarques2);
        $sQuery = <<<SQL
                UPDATE sc_t_oeuvre
                SET
                    TX_TITRE_LATIN_OVR = "{$ovr_latin}",
                    TX_TITRE_FRANCAIS_OVR = "{$ovr_francais}",
                    TX_CLAVIS_OVR = "{$ovr_clavis}",
                    BL_OEUVRE_INTERNE_OVR = {$ovr_interne},
                    TX_TYPE_MANUSCRIT_DISPO_OVR = "{$ovr_type_manuscrit_dispo}",
                    TX_LOCALISATION_OVR = "{$ovr_localisation}",
                    TX_RQS_LOCALISATION_OVR = "{$ovr_rqs_localisation}",
                    TX_REMARQUES1_OVR = "{$ovr_remarques1}",
                    TX_REMARQUES2_OVR = "{$ovr_remarques2}",
                    TX_URL_BIBLINDEX_OVR = "{$ovr_url_biblindex}",
                    BL_COLLAB_SUPPL_OVR = {$needsAdditionalCollaborations}
                WHERE PK_OEUVRE_OVR = {$id_oeuvre};
                SQL;
        $oRecordset = DbExecRequete($sQuery, $oConnexion);

        /* mise à jour de la table association auteur oeuvre */
        $sQuery1 = <<<SQL
            UPDATE sc_t_assoc_au_ovr
            SET
                FK_AUTEUR_ANCIEN_AAN = {$ovr_fk_aan}
            WHERE FK_OEUVRE_OVR = {$id_oeuvre};
            SQL;
        $oRecordset1 = DbExecRequete($sQuery1, $oConnexion);
        /* FIN mise à jour de la table association auteur oeuvre */

        /* mise à jour de la table association oeuvre utilisateur tâche */
        $req = <<<SQL
        SELECT
            ot.FK_OUT_REF_VOL,
            ot.FK_OUT_REF_USR,
            ot.FK_OUT_REF_TCH,
            ot.LB_OUT_AVT_TCH,
            ot.BL_MANUSCRIT_REMIS,
            ot.TX_REMARQUES_OUT,
            ot.BL_ENGAGEMENT_COLLAB_OUT,
            ot.DT_DEBUT_TCH,
            ot.DT_FIN_TCH
        FROM `sc_t_assoc_ovr_usr_tch` AS ot
        WHERE `FK_OUT_REF_OVR`= {$id_oeuvre}
        ORDER BY FK_OUT_REF_VOL, FK_OUT_REF_USR,FK_OUT_REF_TCH,LB_OUT_AVT_TCH,BL_MANUSCRIT_REMIS,TX_REMARQUES_OUT,BL_ENGAGEMENT_COLLAB_OUT,DT_DEBUT_TCH,DT_FIN_TCH;
        SQL;
        $oRecordset = DbExecRequete($req, $oConnexion);
        $existing_assoc_fields = [];
        while ($result = DbEnregSuivantTab($oRecordset)) {
            $existing_assoc_fields[] = [
                $result['FK_OUT_REF_VOL'],
                $result['FK_OUT_REF_USR'],
                $result['FK_OUT_REF_TCH'],
                $result['LB_OUT_AVT_TCH'],
                $result['BL_MANUSCRIT_REMIS'],
                $result['TX_REMARQUES_OUT'],
                $result['BL_ENGAGEMENT_COLLAB_OUT'],
                $result['DT_DEBUT_TCH'],
                $result['DT_FIN_TCH']
            ];
        }
        $existing_assoc_fields = array_merge(...$existing_assoc_fields);

        $wuts_ids = array_unique(array_merge(
            array_keys($ovr_tch),
            array_keys($ovr_usr),
            array_keys($ovr_vol)
        ));
        if (count(array_diff_key($ovr_tch, $ovr_usr)) || count(array_diff_key($ovr_usr, $ovr_tch))) {
            addNotice('danger', "Certaines tâches n'ont pas pu être enregistrées : la tâche ou l'utilisateur sont manquants.");
        }

        $queries = [
            <<<SQL
                DELETE FROM `sc_t_assoc_ovr_usr_tch`
                WHERE `FK_OUT_REF_OVR`={$id_oeuvre};
                SQL,
        ];
        if ('0' == $ovr_interne && count($wuts_ids)) {
            addNotice('danger', "Il est impossible d’enregistrer des tâches pour une œuvre non prévue.");
        }
        foreach ($wuts_ids as $index) {
            $tasks_ids = $ovr_tch[$index] ?? []; // field is required
            $users_ids = $ovr_usr[$index] ?? []; // field is required
            $volumes_ids = $ovr_vol[$index] ?? [null]; // field is optional
            foreach ($tasks_ids as $task_id) {
                foreach ($users_ids as $user_id) {
                    foreach ($volumes_ids as $volume_id) {
                        $id_ovr = $id_ovr ?? 'NULL';
                        $task_id = $task_id ?? 'NULL';
                        $user_id = $user_id ?? 'NULL';
                        $volume_id = $volume_id ?? 'NULL';
                        $lvl_id = $ovr_lvl[$index] ?? null;
                        $begin_id = $ovr_begin[$index] ?? null;
                        $end_id = $ovr_end[$index] ?? null;
                        $rmq_id = $ovr_rmq[$index] ?? null;
                        $ms_id = '1' === ($ovr_ms[$index] ?? '0') ? '1' : '0';

                        $queries[] = <<<SQL
                        INSERT INTO sc_t_assoc_ovr_usr_tch(
                            FK_OUT_REF_OVR,
                            FK_OUT_REF_TCH,
                            FK_OUT_REF_USR,
                            FK_OUT_REF_VOL,
                            LB_OUT_AVT_TCH,
                            DT_DEBUT_TCH,
                            DT_FIN_TCH,
                            TX_REMARQUES_OUT,
                            BL_MANUSCRIT_REMIS
                        ) VALUES (
                            {$id_oeuvre},
                            {$task_id},
                            {$user_id},
                            {$volume_id},
                            "{$lvl_id}",
                            "{$begin_id}",
                            "{$end_id}",
                            "{$rmq_id}",
                            {$ms_id}
                        );
                        SQL;
                    }
                }
            }
        }
        array_walk($queries, static function (string $query) use ($oConnexion): void {
            DbExecRequete($query, $oConnexion);
        });
        /* FIN mise à jour de la table association oeuvre utilisateur tâche */

        // get the new associations user/task
        $req = <<<SQL
                SELECT
                    ot.FK_OUT_REF_VOL,
                    ot.FK_OUT_REF_USR,
                    ot.FK_OUT_REF_TCH,
                    ot.LB_OUT_AVT_TCH,
                    ot.BL_MANUSCRIT_REMIS,
                    ot.TX_REMARQUES_OUT,
                    ot.BL_ENGAGEMENT_COLLAB_OUT,
                    ot.DT_DEBUT_TCH,
                    ot.DT_FIN_TCH
                FROM `sc_t_assoc_ovr_usr_tch` AS ot
                WHERE FK_OUT_REF_OVR = {$id_oeuvre}
                ORDER BY FK_OUT_REF_VOL, FK_OUT_REF_USR,FK_OUT_REF_TCH,LB_OUT_AVT_TCH,BL_MANUSCRIT_REMIS,TX_REMARQUES_OUT,BL_ENGAGEMENT_COLLAB_OUT,DT_DEBUT_TCH,DT_FIN_TCH
            SQL;
        $oRs = DbExecRequete($req, $oConnexion);
        $after_assoc_fields = [];
        while($result = DbEnregSuivantTab($oRs)){
            $after_assoc_fields[] = [
                $result['FK_OUT_REF_VOL'],
                $result['FK_OUT_REF_USR'],
                $result['FK_OUT_REF_TCH'],
                $result['LB_OUT_AVT_TCH'],
                $result['BL_MANUSCRIT_REMIS'],
                $result['TX_REMARQUES_OUT'],
                $result['BL_ENGAGEMENT_COLLAB_OUT'],
                $result['DT_DEBUT_TCH'],
                $result['DT_FIN_TCH']
            ];
        }
        $after_assoc_fields = array_merge(...$after_assoc_fields);

        // get the new works elements
        $req = <<<SQL
            SELECT
                TX_TITRE_LATIN_OVR,
                TX_TITRE_FRANCAIS_OVR,
                TX_CLAVIS_OVR,
                BL_OEUVRE_INTERNE_OVR,
                TX_TYPE_MANUSCRIT_DISPO_OVR,
                TX_LOCALISATION_OVR,
                TX_RQS_LOCALISATION_OVR,
                TX_REMARQUES1_OVR,
                TX_REMARQUES2_OVR,
                TX_URL_BIBLINDEX_OVR
            FROM sc_t_oeuvre
            WHERE PK_OEUVRE_OVR = {$id_oeuvre}
            ORDER BY TX_TITRE_LATIN_OVR, TX_TITRE_FRANCAIS_OVR, TX_CLAVIS_OVR, BL_OEUVRE_INTERNE_OVR, TX_TYPE_MANUSCRIT_DISPO_OVR, TX_LOCALISATION_OVR, TX_RQS_LOCALISATION_OVR, TX_REMARQUES1_OVR, TX_REMARQUES2_OVR, TX_URL_BIBLINDEX_OVR;
        SQL;
        $oRecordset = DbExecRequete($req, $oConnexion);
        $after_ovr_elements = [];
        while ($result = DbEnregSuivantTab($oRecordset)) {
            $after_ovr_elements[] = [
                $result['TX_TITRE_LATIN_OVR'],
                $result['TX_TITRE_FRANCAIS_OVR'],
                $result['TX_CLAVIS_OVR'],
                $result['BL_OEUVRE_INTERNE_OVR'],
                $result['TX_TYPE_MANUSCRIT_DISPO_OVR'],
                $result['TX_LOCALISATION_OVR'],
                $result['TX_RQS_LOCALISATION_OVR'],
                $result['TX_REMARQUES1_OVR'],
                $result['TX_REMARQUES2_OVR'],
                $result['TX_URL_BIBLINDEX_OVR']
            ];
        }
        $after_ovr_elements = array_merge(...$after_ovr_elements);

        //get the differences between the new and the old
        $differences_assoc = array_diff($after_assoc_fields, $existing_assoc_fields);
        $differences_work = array_diff($after_ovr_elements, $existing_ovr_elements);
        if ($differences_assoc || $differences_work) {
            // insert a trace of the update of the association user/task
            $today = date('Y-m-d H:i:s');
            $str_oeuvre = addslashes("l'oeuvre");
            $ovr_francais = addslashes($ovr_francais);

            $req = <<<SQL
                DELETE FROM sc_t_trace
                WHERE
                    DT_TRACE_TRC = "{$today}"
                AND NM_UTILISATEUR_TRC = {$_SESSION['PK_UTILISATEUR_USR']}
                AND TX_DOMAINE_TRC = 'OEUVRE'
                AND NM_ITEMID_TRC = {$id_oeuvre};
            SQL;
            $exec = DbExecRequete($req, $oConnexion);

            $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('OEUVRE',
                       {$id_oeuvre},
                       'Mise à jour de {$str_oeuvre} {$ovr_francais} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-U',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
            $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);
        }

        addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');
        commit($oConnexion);

        redirect(sprintf('/?%s', http_build_query([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'oeuvres',
            'detail' => 'ok',
            'sourcerub' => 'intranet',
            'sourcepg' => 'gestion',
            'param1' => 'sectionid',
            'vparam1' => 'oeuvres',
            'param2' => 'lettre',
            'vparam2' => $letter,
            'signet' => $id_oeuvre,
            'id_oeuvre' => $id_oeuvre
        ])));
    }
}
?>
<script type="text/javascript" src="js/tiny_mce_v349/tiny_mce_gzip.js"></script>
<script type="text/javascript" src="js/tinymce-launcher_v349_20120420.js"></script>
<input type="hidden" name="recupform" value="ok">
<input type="hidden" name="id_oeuvre" value="<?php echo $id_oeuvre; ?>">
<input type="hidden" name="id_source" value="<?php echo $id_source; ?>">

<input type="hidden" name="url_originale" value="<?php echo $url_originale; ?>">
<!-- ===== Bouton 'Retour' / DEBUT ===== -->
<input type="hidden" name="sourcerub" value="intranet">
<input type="hidden" name="sourcepg" value="gestion">
<input type="hidden" name="param1" value="sectionid">
<input type="hidden" name="vparam1" value="oeuvres">
<input type="hidden" name="param2" value="ddl_lettre_ovr">
<input type="hidden" name="vparam2" value="<?php if (isset($_REQUEST['vparam2'])) {
    echo $_REQUEST['vparam2'];
} elseif (isset($_REQUEST['oldvparam2'])) {
    echo $_REQUEST['oldvparam2'];
} ?>">
<input type="hidden" name="param3" value="ddl_autancien">
<input type="hidden" name="vparam3" value="<?php if (isset($_REQUEST['vparam3'])) {
    echo $_REQUEST['vparam3'];
} elseif (isset($_REQUEST['oldvparam3'])) {
    echo $_REQUEST['oldvparam3'];
} ?>">
<input type="hidden" name="param4" value="rdbAutAncien">
<input type="hidden" name="vparam4" value="<?php if (isset($_REQUEST['vparam4'])) {
    echo $_REQUEST['vparam4'];
} elseif (isset($_REQUEST['oldvparam4'])) {
    echo $_REQUEST['oldvparam4'];
} ?>">
<input type="hidden" name="param6" value="ddl_utilisateur">
<input type="hidden" name="vparam6" value="<?php if (isset($_REQUEST['vparam6'])) {
    echo $_REQUEST['vparam6'];
} elseif (isset($_REQUEST['oldvparam6'])) {
    echo $_REQUEST['oldvparam6'];
} ?>">
<input type="hidden" name="param8" value="ddl_titre_ovr">
<input type="hidden" name="vparam8" value="<?php if (isset($_REQUEST['vparam8'])) {
    echo $_REQUEST['vparam8'];
} elseif (isset($_REQUEST['oldvparam8'])) {
    echo $_REQUEST['oldvparam8'];
} ?>">
<?php if (isset($_REQUEST['signet'])) { ?>
    <input type="hidden" name="signet" value="<?php echo $_REQUEST['signet']; ?>">
<?php }
?>
<!-- ===== Bouton 'Retour' / FIN ===== -->
<tr>
    <td align="center">
        <font class="font-titre1"><?php if (empty($id_oeuvre)) {
                echo 'Ajout d\'une nouvelle &#156;uvre';
            } else {
                echo 'Modification d\'une &#156;uvre';
            } ?></font>
    </td>
</tr>
<?php if(hasNotice()): ?>
    <tr>
        <td align="center">
            <?php printNotice(); ?>
        </td>
    </tr>
<?php endif; ?>
<tr>
    <td width="100%">
        <div align="center">
            <table border="0" width="100%" cellpadding="4" cellspacing="0">
                <?php if (!empty($id_oeuvre)): ?>
                    <?php
                    $sQuery = <<<SQL
                    SELECT *
                    FROM sc_t_trace
                    WHERE TX_DOMAINE_TRC = 'OEUVRE' AND NM_ITEMID_TRC = {$id_oeuvre}
                    ORDER BY DT_TRACE_TRC DESC LIMIT 3;
                    SQL;
                    $oRs = DbExecRequete($sQuery, $oConnexion);
                    ?>
                    <?php while ($trace = DbEnregSuivant($oRs)): ?>
                        <tr>
                            <td colspan="3" align="center">
                                <b><?php echo $trace->DT_TRACE_TRC ?> : <?php echo $trace->LB_TRACE_TRC ?></b><br>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                    <?php if (0 == DbNbreEnreg($oRs)): ?>
                        <tr>
                            <td colspan="3" align="center">
                                Aucun historique des dernières modifications apportées à cette &#338;uvre
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endif; ?>
                <?php
                $url = null;
                if ('volumes_preparation' == $_REQUEST['vparam1']) {
                    $url = sprintf('/?%s#back', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'volumes_preparation',
                    ]));
                } elseif('oeuvres' == $_REQUEST['vparam1']) {
                    $url = sprintf('/?%s#back', http_build_query([
                        'rubriqueid' => 'intranet',
                        'pageid' => 'gestion',
                        'sectionid' => 'oeuvres',
                        'ddlsectionid' => 'oeuvres',
                        'param2' => 'ddl_lettre_ovr',
                        'vparam2' => $_GET['vparam2'] ?? null,
                        'param3' => 'ddl_autancien',
                        'vparam3' => $_GET['vparam3'] ?? null,
                        'param4' => 'rdbAutAncien',
                        'vparam4' => $_GET['vparam4'] ?? null,
                        'param6' => 'ddl_utilisateur',
                        'vparam6' => $_GET['vparam6'] ?? null
                    ]));
                }
                ?>
                <?php $ligneType = 'B'; ?>
                <tr>
                    <td colspan="3" align="center">
                        <a href="<?php echo $url; ?>">
                            <img src="img/common/retour.gif" width="91" height="24" border="0" style="cursor:hand"></a>
                    </td>
                </tr>
                <?php $ligneType = 'B'; ?>
                <tr>
                    <td colspan="3">
                        <u>Attention&nbsp;:</u> les champs dont les libellés sont en caractères <b>gras</b> sont obligatoires.<br>
                    </td>
                </tr>
                <?php if (!empty($id_oeuvre)): ?>
                    <tr>
                        <td colspan="3" align="center">
                            <table border="0" width="100%" class="datagrid-entete">
                                <tr>
                                    <td width="75%"><font class="font-normal"><font class="font-datagrid-entete"><b>Volume(s) auxquel(s) l'&#156;uvre est associée</b></font></font></td>
                                    <td width="25%" align="right"><font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>								</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php
                    $sQuery = <<<SQL
                    SELECT V.`NM_NUMERO_COLLECTION_VIF`,V.`PK_VOLUMEINFOS_VIF`
                    FROM sc_t_volume AS V
                    LEFT JOIN sc_t_assoc_vol_ovr as vo ON V.PK_VOLUMEINFOS_VIF = vo.FK_AVO_REF_VOL
                    WHERE vo.FK_AVO_REF_OVR = {$id_oeuvre}
                    ORDER BY V.NM_NUMERO_COLLECTION_VIF ASC
                    SQL;
                    $oRs = DbExecRequete($sQuery, $oConnexion);
                    ?>
                    <tr>
                        <td colspan="3" align="left">
                            <?php if (0 == DbNbreEnreg($oRs)): ?>
                                Cette &#156;uvre n'est associée à aucun volume.
                            <?php else: ?>
                                <?php while ($ovrvol = DbEnregSuivant($oRs)): ?>
                                    <?php
                                    $url = '/?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok&sourcerub=intranet&sourcepg=gestion';
                                    $url .= '&param1=sectionid&vparam1=volumes&param2=vol_population&vparam2=T&param3=vol_oeuvre&vparam3=';
                                    $url .= $id_oeuvre.'&param4=vol_autancien&vparam4=&param5=vol_utilisateur&vparam5=&param6=vol_domaine&vparam6=';
                                    $url .= '&param7=vol_pole&vparam7=&param8=deb&vparam8=0&param9=afficher&vparam9=25&signet=vol';
                                    $url .= $ovrvol->PK_VOLUMEINFOS_VIF.'&id_volume='.$ovrvol->PK_VOLUMEINFOS_VIF;
                                    ?>
                                    <a href="<?php echo $url ?>" target="_new">Volume N°<?php echo $ovrvol->NM_NUMERO_COLLECTION_VIF ?></a>&nbsp;&nbsp;&nbsp;
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal"><font class="font-datagrid-entete"><b>Auteur et titre de l'&#339;uvre</b></font></font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>
                                </td>
                        </table>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal"><b>AUTEUR ANCIEN</b> :</font>
                    </td>
                    <td align="left">
                        <select size="1" name="ovr_fk_aan">
                            <?php if ('' == $id_oeuvre) {
                                ?>
                            <option value=""
                                <?php if ('' == $ovr_fk_aan) {
                                    echo ' selected';
                                } ?>>Veuillez choisir un auteur ancien (nom français)...</option><?php } ?>
                            <?php
                            $sQuery = <<<SQL
                            SELECT
                                PK_AUTEUR_ANCIEN_AAN,
                                TX_NOM_FRANCAIS_AAN
                            FROM sc_t_auteur
                            ORDER BY TX_NOM_FRANCAIS_AAN;
                            SQL;
                            $oRecordset = DbExecRequete($sQuery, $oConnexion);
                            while ($aan = DbEnregSuivant($oRecordset)) {
                                echo '<option value="'.$aan->PK_AUTEUR_ANCIEN_AAN.'"';
                                if ($ovr_fk_aan == $aan->PK_AUTEUR_ANCIEN_AAN) {
                                    echo ' selected';
                                }
                                echo '>'.$aan->TX_NOM_FRANCAIS_AAN.'</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal"><b>TITRE FRANCAIS</b> :</font><br>
                        <font class="font-small"><em><strong> Ne pas mettre de guillemets</strong> dans le titre !<strong>Ne pas laisser deux titres exactement identiques</strong> pour un même auteur !<br />
                        A, Aux, De la/le/l', Du, Des, Sur le/la/les, nombre : à mettre à la fin du titre entre parenthèses. <br />
                        Mettre <strong>Oe</strong> et non œ en début de mot mais mettre les <strong>É</strong> ; Saint-Esprit et non Esprit saint.<br>
                        Indiquer <strong>l'intervalle</strong> des num&eacute;ros de sermons, lettres, etc, et non le nombre total.<br />
                        <strong>Si traduction incertaine, mettre un * à la fin </strong></em></font>
                    </td>
                    <td align="left">
                        <input type="text" id="ovr_francais" name="ovr_francais" size="80" value="<?php echo $ovr_francais; ?>" />
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">TITRE LATIN:</font><br />
                        <font class="font-small"><strong><em>Si titre latin incertain, mettre un * à la fin</em></strong></font>
                    </td>
                    <td align="left">
                        <input type="text" id="ovr_latin" name="ovr_latin" size="80" value="<?php echo $ovr_latin; ?>" />
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal font-datagrid-entete"><b>&#140;UVRE EDITEE OU PROJETEE A SC :</b></font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2">
                        <label for="needsAdditionalCollaborations">Besoin de collaborations supplémentaires</label>
                    </td>
                    <td align="left">
                        <div class="form-check">
                            <input
                                <?php if ('1' === $needsAdditionalCollaborations): ?>
                                    checked
                                <?php endif; ?>
                                class="form-check-input"
                                id="needsAdditionalCollaborations"
                                name="needsAdditionalCollaborations"
                                type="checkbox"
                                value="1"
                            >
                        </div>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-small"><em><strong>TRES IMPORTANT :</strong> sur la page de l'auteur ancien,
                                <ul>
                                    <li>Cocher <strong>« oui »</strong> fait apparaître l'&#339;uvre<br>
                                        - dans la catégorie des &#339;uvres <strong>en chantier</strong><u>s'il y a au moins un collaborateur associé</u> <br>
                                        - dans celle des &#339;uvres <strong>prévues</strong> <br><u>si aucun collaborateur n'est associé.</u>
                                    </li>
                                    <li>Cocher <strong>« non »</strong> fait basculer l’&#339;uvre<br>
                                        - dans la catégorie des <strong>travaux simplement connus</strong> <u>s'il y a au moins un collaborateur associé</u><br>
                                        - dans la catégorie des &#339;uvres <strong>non encore prévues</strong><br> <u>si aucun collaborateur n'est associé</u>.
                                    </li>
                                </ul>
                            </em>
                        </font>
                    </td>
                    <td align="left">
                        <input
                            <?php if ('1' == $ovr_interne): ?>
                                checked
                            <?php endif; ?>
                            name="ovr_interne"
                            type="radio"
                            value="1"
                        >
                        oui
                        <input
                            <?php if ('0' == $ovr_interne): ?>
                                checked
                            <?php endif; ?>
                            name="ovr_interne"
                            type="radio"
                            value="0"
                        >
                        non
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal font-datagrid-entete"><b>Référencement</b></font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font>
                                </td>
                        </table>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>" colspan="2">
                    <td align="left" colspan="2" nowrap>
                        <p>
                            <font class="font-normal">CLAVIS :<br />
                            <font class="font-small"><em>Indiquez "CPL" pour Clavis Patrum Latinorum ou "CPG" pour Clavis Patrum Graecorum<br />
                            suivi d'un numéro et éventuellement d'un complément. Ex. compl&eacute;ments : /2 ; et 1235; a ; .II <br />
                            Si aucun numéro, mettre type &quot;s. n.&quot;, num&eacute;ro &quot;0&quot;<br />
                            Si l'&#339;uvre ancienne ne figure pas dans la Clavis, indiquez le référencement concerné (ex : BHL 1).</em></font>
                        </p>
                    </td>
                    <td align="left">
                        <font class="font-normal">Clavis :</font>&nbsp;
                        <input type="text" name="ovr_clavis" maxlength="10" size="10" value="<?php echo $ovr_clavis; ?>" placeholder="CPL 1">&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr>
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%"><font class="font-normal"><font class="font-datagrid-entete"><b>Informations internes</b></font></font>								</td>
                                <td width="25%" align="right"><font class="font-normal"><font class="font-datagrid-entete"><a href="#validation" class="dtg-entete">Aller au bouton de validation</a></font></font>								</td>
                        </table>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">REMARQUES concernant les autres éditions de cette &#339;uvre :</font><br>
                    <td align="left">
                        <textarea id="ovr_remarques1" name="ovr_remarques1" rows="10" cols="90" class="mceEditor"><?php echo $ovr_remarques1; ?></textarea>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">REMARQUES concernant les collaborations :</font><br>
                    </td>
                    <td align="left">
                        <textarea id="ovr_remarques2" name="ovr_remarques2" rows="10" cols="90" class="mceEditor"><?php echo $ovr_remarques2; ?></textarea>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">LIEN BIBLINDEX :</font>
                    </td>
                    <td align="left">
                        <input type="url" maxlength="100" name="ovr_url_biblindex" value="<?php echo $ovr_url_biblindex; ?>" size="50" placeholder = "https://www.biblindex.org/fr/work-edition/1">
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">TYPE DE MANUSCRIT disponible :</font>
                    </td>
                    <td align="left">
                        <select name="ovr_type_manuscrit_dispo">
                            <option value="" <?php if ('' == $ovr_type_manuscrit_dispo) {
                                echo ' selected';
                            }?>>Non renseigné</option>
                            <option value="Papier" <?php if ('papier' == mb_strtolower((string) $ovr_type_manuscrit_dispo)) {
                                echo ' selected';
                            }?>>Papier</option>
                            <option value="Informatique" <?php if ('informatique' == mb_strtolower((string) $ovr_type_manuscrit_dispo)) {
                                echo ' selected';
                            }?>>Informatique</option>
                        </select>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">LOCALISATION DU MANUSCRIT :</font>
                    </td>
                    <td align="left">
                        <input type="text" maxlength="100" name="ovr_localisation" value="<?php echo $ovr_localisation; ?>" size="40">
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td align="left" colspan="2" nowrap>
                        <font class="font-normal">PRECISIONS SUR LA LOCALISATION :</font>
                    </td>
                    <td align="left">
                        <textarea id="ovr_rqs_localisation" name="ovr_rqs_localisation" rows="10" cols="90" class="mceEditor"><?php echo $ovr_rqs_localisation; ?></textarea>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3" align="center">
                        <table border="0" width="100%" class="datagrid-entete">
                            <tr>
                                <td width="75%">
                                    <font class="font-normal font-datagrid-entete">
                                        <b>Tâche(s) et collaborateur(s) associés à cette &#339;uvre</b>
                                    </font>
                                </td>
                                <td width="25%" align="right">
                                    <font class="font-normal font-datagrid-entete">
                                        <a href="#validation" class="dtg-entete">Aller au bouton de validation</a>
                                    </font>
                                </td>
                        </table>
                    </td>
                </tr>
                <?php $ligneType = 'A' === $ligneType ? 'A' : 'B'; ?>
                <tr class="datagrid-lignes<?php echo $ligneType; ?>">
                    <td colspan="3">
                        <table class="table table-striped table-sm" id="tbl_usr_task">
                            <thead>
                            <tr>
                                <th scope="col" width="68px">Volume</th>
                                <th scope="col" width="68px">Tâche</th>
                                <th scope="col" width="105px">Collaborateur</th>
                                <th scope="col" width="154px">Date de début</th>
                                <th scope="col" width="154px">Date de fin</th>
                                <th scope="col">Remarques</th>
                                <th scope="col" width="224px">Degré d'avancement de la tâche</th>
                                <th scope="col" width="80px">Manuscrit<br />remis</th>
                                <th scope="col" width="60px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            // Users/Tasks list
                            $wuts = [];
                            if ($id_oeuvre) {
                                $sQuery = <<<SQL
                                SELECT
                                    PK_ASSOC_OVRUSRTCH,
                                    FK_OUT_REF_TCH,
                                    FK_OUT_REF_USR,
                                    FK_OUT_REF_VOL,
                                    LB_OUT_AVT_TCH,
                                    DT_DEBUT_TCH,
                                    DT_FIN_TCH,
                                    TX_REMARQUES_OUT,
                                    BL_MANUSCRIT_REMIS
                                FROM sc_t_assoc_ovr_usr_tch
                                WHERE FK_OUT_REF_OVR={$id_oeuvre}
                                GROUP BY FK_OUT_REF_TCH, FK_OUT_REF_USR, LB_OUT_AVT_TCH, PK_ASSOC_OVRUSRTCH
                                ORDER BY PK_ASSOC_OVRUSRTCH;
                                SQL;
                                $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                $wuts = DbEnregTab($oRecordset);
                            }

                            // Volumes list
                            $volumes = [];
                            if (count($wuts)) {
                                $in = implode(', ', array_filter(array_map(function ($wut) {
                                    return $wut['FK_OUT_REF_VOL'] ?? null;
                                }, $wuts)));
                                if ($in) {
                                    $sQueryTask = <<<SQL
                                    SELECT
                                        PK_VOLUMEINFOS_VIF,
                                        NM_NUMERO_COLLECTION_VIF,
                                        TX_PAGETITRE_TITRE_VIF
                                    FROM sc_t_volume
                                    WHERE PK_VOLUMEINFOS_VIF IN ({$in})
                                    ORDER BY NM_NUMERO_COLLECTION_VIF;
                                    SQL;
                                    $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
                                    $volumes = DbEnregTab($oRecordsetTask);
                                }
                            }

                            // Tasks list
                            $sQueryTask = <<<SQL
                            SELECT
                                PK_TACHE_TCH,
                                LB_TACHE_TCH
                            FROM sc_t_tache
                            ORDER BY NM_ORDRE_TCH;
                            SQL;
                            $oRecordsetTask = DbExecRequete($sQueryTask, $oConnexion);
                            $tasks = DbEnregTab($oRecordsetTask);

                            // Users list
                            $users = [];
                            if (count($wuts)) {
                                $in = implode(', ', array_filter(array_map(function ($wut) {
                                    return $wut['FK_OUT_REF_USR'] ?? null;
                                }, $wuts)));
                                if ($in) {
                                    $sQueryUser = <<<SQL
                                    SELECT
                                        PK_UTILISATEUR_USR,
                                        TX_NOM_USR,
                                        TX_PRENOM_USR,
                                        BL_DECEDE_USR
                                    FROM sc_t_utilisateur
                                    WHERE PK_UTILISATEUR_USR IN ({$in})
                                    ORDER BY TX_NOM_USR;
                                    SQL;
                                    $oRecordsetUser = DbExecRequete($sQueryUser, $oConnexion);
                                    $users = DbEnregTab($oRecordsetUser);
                                }
                            }

                            // levels of progress of the task list
                            $sQueryLevel = <<<SQL
                            SELECT
                                LB_OUT_AVT_TCH
                            FROM sc_t_assoc_ovr_usr_tch
                            GROUP BY LB_OUT_AVT_TCH
                            ORDER BY LB_OUT_AVT_TCH;
                            SQL;
                            $oRecordsetLevel = DbExecRequete($sQueryLevel, $oConnexion);
                            $levels = DbEnregTab($oRecordsetLevel);

                            // Dates list
                            $sQueryDates = <<<SQL
                            SELECT
                                DT_DEBUT_TCH,
                                DT_FIN_TCH
                            FROM sc_t_assoc_ovr_usr_tch
                            WHERE DT_DEBUT_TCH IS NOT NULL
                            GROUP BY DT_DEBUT_TCH;
                            SQL;
                            $oRecordsetDates = DbExecRequete($sQueryDates, $oConnexion);
                            $dates = DbEnregTab($oRecordsetDates);

                            // Remarques list
                            $sQueryRmk = <<<SQL
                            SELECT
                                TX_REMARQUES_OUT
                            FROM sc_t_assoc_ovr_usr_tch
                            WHERE TX_REMARQUES_OUT IS NOT NULL
                            GROUP BY TX_REMARQUES_OUT;
                            SQL;
                            $oRecordsetRmk = DbExecRequete($sQueryRmk, $oConnexion);
                            $rmks = DbEnregTab($oRecordsetRmk);

                            // Manuscrit remis list
                            $sQueryMs = <<<SQL
                            SELECT
                                BL_MANUSCRIT_REMIS
                            FROM sc_t_assoc_ovr_usr_tch
                            GROUP BY BL_MANUSCRIT_REMIS;
                            SQL;
                            $oRecordsetMs = DbExecRequete($sQueryMs, $oConnexion);
                            $mss = DbEnregTab($oRecordsetMs);
                            ?>
                            <?php foreach ($wuts as $wut): ?>
                                <?php
                                $fk_vol = $wut['FK_OUT_REF_VOL'];
                                $fk_usr = $wut['FK_OUT_REF_USR'];
                                $fk_tch = $wut['FK_OUT_REF_TCH'];
                                $fk_avt = $wut['LB_OUT_AVT_TCH'];
                                ?>
                                <tr data-id="wut_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>">
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-ajax--url="/api/autocomplete/volume"
                                            data-ajax--cache="true"
                                            data-placeholder="Volume"
                                            id="field_ovr_vol_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="ovr_vol[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                        >
                                            <?php foreach ($volumes as $volume): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s</option>',
                                                    $volume['PK_VOLUMEINFOS_VIF'],
                                                    $wut['FK_OUT_REF_VOL'] === $volume['PK_VOLUMEINFOS_VIF'] ? ' selected' : '',
                                                    $volume['NM_NUMERO_COLLECTION_VIF']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-placeholder="Tâche"
                                            id="field_ovr_tsk_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="ovr_tch[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                            required
                                        >
                                            <?php foreach ($tasks as $task): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s</option>',
                                                    $task['PK_TACHE_TCH'],
                                                    $wut['FK_OUT_REF_TCH'] === $task['PK_TACHE_TCH'] ? ' selected' : '',
                                                    $task['LB_TACHE_TCH']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            data-ajax--url="/api/autocomplete/user"
                                            data-ajax--cache="true"
                                            data-placeholder="Utilisateur"
                                            id="field_ovr_user_<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>"
                                            multiple
                                            name="ovr_usr[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>][]"
                                            required
                                        >
                                            <?php foreach ($users as $user): ?>
                                                <?php echo sprintf(
                                                    '<option value="%s"%s>%s%s %s</option>',
                                                    $user['PK_UTILISATEUR_USR'],
                                                    $wut['FK_OUT_REF_USR'] == $user['PK_UTILISATEUR_USR'] ? ' selected' : '',
                                                    $user['BL_DECEDE_USR'] == 1 ? '† ' : '',
                                                    $user['TX_NOM_USR'],
                                                    $user['TX_PRENOM_USR']
                                                ); ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input
                                            class="form-control"
                                            name="ovr_begin[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                            placeholder="AAAA-MM(-DD)"
                                            title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                            type="text"
                                            value="<?php echo $wut['DT_DEBUT_TCH']; ?>"
                                        />
                                    </td>
                                    <td>
                                        <input
                                            class="form-control"
                                            name="ovr_end[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                            placeholder="AAAA-MM(-DD)"
                                            title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                            type="text"
                                            value="<?php echo $wut['DT_FIN_TCH']; ?>"
                                        />
                                    </td>
                                    <td>
                                        <textarea
                                            class="form-control"
                                            name="ovr_rmq[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                            placeholder="Remarques"
                                            rows="1"
                                        ><?php echo strip_tags(html_entity_decode($wut['TX_REMARQUES_OUT'] ?? '')); ?></textarea>
                                    </td>
                                    <td>
                                        <select
                                            class="form-select select2"
                                            name="ovr_lvl[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                        >
                                            <option
                                                <?php if(empty($wut['LB_OUT_AVT_TCH'])): ?>
                                                    selected
                                                <?php endif; ?>
                                                value=""
                                            >Niveau d'avancement</option>
                                            <?php foreach ($levels as $level): ?>
                                                <?php if(!empty($level['LB_OUT_AVT_TCH'])): ?>
                                                    <option
                                                        value="<?php echo $level['LB_OUT_AVT_TCH']; ?>"
                                                        <?php if ($wut['LB_OUT_AVT_TCH'] == $level['LB_OUT_AVT_TCH']): ?>
                                                            selected="selected"
                                                        <?php endif; ?>
                                                    ><?php echo "{$level['LB_OUT_AVT_TCH']}"; ?></option>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input
                                                <?php if ('1' == $wut['BL_MANUSCRIT_REMIS']): ?>
                                                    checked
                                                <?php endif; ?>
                                                class="form-check-input"
                                                name="ovr_ms[<?php echo $wut['PK_ASSOC_OVRUSRTCH']; ?>]"
                                                type="checkbox"
                                                value="1"
                                            />
                                        </div>
                                    </td>
                                    <td>
                                        <?php
                                        $url_suppr = $url = sprintf('/?%s', http_build_query([
                                            'rubriqueid' => 'intranet',
                                            'pageid' => 'gestion',
                                            'sectionid' => 'oeuvres',
                                            'action' => 'suppr',
                                            'detail' => 'ok',
                                            'sourcerub' => 'intranet',
                                            'sourcepg' => 'gestion',
                                            'param1' => 'sectionid',
                                            'vparam1' => 'oeuvres',
                                            'signet' => $id_oeuvre,
                                            'id_oeuvre' => $id_oeuvre,
                                            'fk_usr' => $fk_usr,
                                            'fk_tch' => $fk_tch,
                                            'fk_vol' => $fk_vol
                                        ]));
                                        ?>
                                        <button
                                            class="btn btn-secondary"
                                            <?php if ($rights_ext || $rights_member_cs): ?>
                                                disabled="disabled"
                                            <?php endif; ?>
                                            onclick="if (confirm('Êtes-vous sûr de vouloir supprimer cette association utilisateur/tâche ?')) location.href='<?php echo $url_suppr; ?>'"
                                            type="button"
                                        >
                                            Suppr.
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot id="foot_tbl_usr_task">
                            <tr>
                                <th colspan="5">
                                    Associer une ou plusieurs tâches à un ou plusieurs utilisateurs
                                </th>
                            </tr>
                            <tr data-id="wut_0">
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-ajax--url="/api/autocomplete/volume"
                                        data-ajax--cache="true"
                                        data-placeholder="Volume"
                                        id="field_ovr_vol_0"
                                        multiple
                                        name="ovr_vol[0][]"
                                    ></select>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-placeholder="Tâche"
                                        id="field_ovr_tch_0"
                                        multiple
                                        name="ovr_tch[0][]"
                                    >
                                        <?php foreach ($tasks as $task): ?>
                                            <option value="<?php echo $task['PK_TACHE_TCH']; ?>"><?php echo $task['LB_TACHE_TCH']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        data-ajax--url="/api/autocomplete/user"
                                        data-ajax--cache="true"
                                        data-placeholder="Utilisateur"
                                        id="field_ovr_usr_0"
                                        multiple
                                        name="ovr_usr[0][]"
                                    ></select>
                                </td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="ovr_begin[0]"
                                        pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                        placeholder="AAAA-MM(-DD)"
                                        title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                        type="text"
                                    />
                                </td>
                                <td>
                                    <input
                                        class="form-control"
                                        name="ovr_end[0]"
                                        pattern="(^20[0-2][0-9]-((0[1-9])|(1[0-2]))-(0[1-9]|[1-2][0-9]|3[0-1])$)|(^20[0-2][0-9]-((0[1-9])|(1[0-2]))$)"
                                        placeholder="AAAA-MM(-DD)"
                                        title="Entrez une date au format AAAA-MM-DD ou AAAA-MM"
                                        type="text"
                                    />
                                </td>
                                <td>
                                    <textarea
                                        class="form-control"
                                        name="ovr_rmq[0]"
                                        placeholder="Remarques"
                                        rows="1"
                                    ></textarea>
                                </td>
                                <td>
                                    <select
                                        class="form-select select2"
                                        name="ovr_lvl[0]"
                                    >
                                        <option value="">
                                            Niveau d'avancement
                                        </option>
                                        <?php foreach ($levels as $level): ?>
                                            <?php if(!empty($level['LB_OUT_AVT_TCH'])): ?>
                                                <option value="<?php echo $level['LB_OUT_AVT_TCH']; ?>"><?php echo $level['LB_OUT_AVT_TCH']; ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <div class="form-check">
                                        <input
                                            class="form-check-input"
                                            name="ovr_ms[0]"
                                            type="checkbox"
                                            value="1"
                                        />
                                    </div>
                                </td>
                                <td>
                                    <a
                                        class="btn btn-secondary"
                                        onclick="if(this.closest('tfoot').children.length > 2) this.closest('tr').remove()"
                                    >
                                        Suppr.
                                    </a>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                        <script>
                            $('select.select2').select2();
                            function addRow() {
                                const table = document.querySelector('#tbl_usr_task');
                                const tfoot = table.querySelector('tfoot');
                                const emptyRow = tfoot.querySelector('tr:nth-child(2)');
                                $(emptyRow).find('select.select2').select2('destroy');
                                const clone = emptyRow.cloneNode(true);
                                let index = 0;
                                while (document.querySelector(`tr[data-id="wut_${index}"]`)) {
                                    index += 1;
                                }
                                clone.dataset.id = `wut_${index}`;
                                clone.querySelectorAll('input, textarea, select, button').forEach(input => {
                                    input.setAttribute('id', (input.getAttribute('id') || '').replace(/_\d+$/, `_${index}`));
                                    input.setAttribute('name', (input.getAttribute('name') || '').replace(/\[\d+\]/, `[${index}]`));
                                    input.value = '';
                                });
                                tfoot.appendChild(clone);
                                $(emptyRow).find('select.select2').select2();
                                $(clone).find('select.select2').select2();
                            }
                        </script>
                        <button
                            class="btn btn-light add-more"
                            <?php if ($rights_ext): ?>
                                disabled="disabled"
                            <?php endif; ?>
                            onclick="addRow();"
                            type="button"
                        >
                            Ajouter une autre tâche
                        </button>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="50">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <a name="validation">
                            <input
                                class="btn"
                                id="btn_valider"
                                onclick="javascript:testFormulaireOeuvre();"
                                <?php if ($rights_ext): ?>
                                    style="display: none"
                                <?php endif; ?>
                                type="button"
                                value="&nbsp;&nbsp;<?php echo empty($id_oeuvre) ? 'Créer l\'&#156;uvre' : 'Valider les modifications'; ?>&nbsp;&nbsp;"
                            />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </td>
</tr>

<?php
DbClose($oConnexion);
?>
