<?php
$rights_admin = [];
$rights_member = [];
$rights_ext = [];

$export_mail = false;
if (isset($_REQUEST['export_mail'])) {
    $export_mail = true;
}

$lettre = 'a';

if(isset($_REQUEST['vparam3']) && empty($_REQUEST['lettre'])){
    $lettre = $_REQUEST['vparam3'];
}
else if (isset($_REQUEST['lettre'])) {
    $lettre = $_REQUEST['lettre'];
}

$oConnexion = DbConnection();

/* get the rights of the logged user */
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}
if($usr_pfl == 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%utilisateurs%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'membre SC' || $usr_pfl == 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%utilisateurs%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl == 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%utilisateurs%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (isset($_REQUEST['action']) && !empty($id)) {
    begin($oConnexion);

    switch ($_REQUEST['action']) {
        case 'suppr':
            if ($rights_admin) {
                // Association Utilisateur - Oeuvre - Tâche
                $sQuery = <<<SQL
                    DELETE FROM sc_t_assoc_ovr_usr_tch
                    WHERE FK_OUT_REF_USR = {$id};
                SQL;
                $oRecordset = DbExecRequete($sQuery, $oConnexion);

                // insert trace of the deletion
                $req = <<<SQL
                SELECT TX_NOM_USR, TX_PRENOM_USR
                FROM sc_t_utilisateur
                WHERE PK_UTILISATEUR_USR = {$id};
                SQL;
                $oRecordset = DbExecRequete($req, $oConnexion);
                while ($fetch = DbEnregSuivant($oRecordset)) {
                    $usr_name = $fetch->TX_NOM_USR;
                    $usr_fname = $fetch->TX_PRENOM_USR;
                }
                $str_user = addslashes("l'utilisateur");
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('UTILISATEUR',
                       {$id},
                       'Suppression de {$str_user} {$usr_fname} {$usr_name} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-D',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);

                // Utilisateur
                $sQuery = <<<SQL
                    DELETE FROM sc_t_utilisateur
                    WHERE PK_UTILISATEUR_USR = {$id};
                SQL;
                $oRecordset = DbExecRequete($sQuery, $oConnexion);
                break;
            }
    }

    commit($oConnexion);

    addNotice('success', 'Mise à jour de la base de données effectuée avec succès !');

    redirect(sprintf('/?%s', http_build_query([
        'rubriqueid' => 'intranet',
        'pageid' => 'gestion',
        'sectionid' => 'utilisateurs',
        'ddlsectionid' => 'utilisateurs',
        'param3' => 'lettre',
        'vparam3' => $lettre
    ])));
}

if ('az' == $lettre) {
    $sQuery = <<<SQL
        SELECT *
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR > 1 -- On omet l'administrateur principal du site
        SQL;
} elseif ('az' != $lettre) {
    $sQuery = <<<SQL
            SELECT *
            FROM sc_t_utilisateur
            WHERE TX_NOM_USR LIKE '{$lettre}%'
            ORDER BY TX_NOM_USR, TX_PRENOM_USR;
            SQL;
}
$oRecordset = DbExecRequete($sQuery, $oConnexion);
$iNbreEnreg = DbNbreEnreg($oRecordset);

?>

<?php if (hasNotice()): ?>
    <tr>
        <td align="center"><?php printNotice(); ?></td>
    </tr>
<?php endif; ?>



<tr>
    <td>
        <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_trace
        WHERE
            TX_DOMAINE_TRC = 'UTILISATEUR'
        ORDER BY DT_TRACE_TRC DESC LIMIT 3;
    SQL;
        $oRs = DbExecRequete($sQuery, $oConnexion);
        while ($trace = DbEnregSuivant($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
            </td>
        </tr>
        HTML;
        }
        if (0 == DbNbreEnreg($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                Aucun historique des dernières modifications apportées à un utilisateur
            </td>
        </tr>
        HTML;
        }
        ?>

        <form name="frmGestionUsers" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'utilisateurs',
            'ddlsectionid' => 'utilisateurs',
            'param2' => 'lettre',
            'vparam2' => $lettre
        ]))) ?>">
        <script language="javascript" src="js/gestion.js"></script>
        <table border="0" width="100%" cellspacing="2" cellpadding="2" class="table table-sm">
            <thead>
                <tr style="background-color: #C90063;" >
                    <th class="title_gestion" colspan="5">
                        <b>LISTE DES UTILISATEURS</b>
                        <?php
                            $url_add = sprintf('/?%s', http_build_query([
                                'rubriqueid' => 'intranet',
                                'pageid' => 'gestion',
                                'sectionid' => 'utilisateurs',
                                'detail' => 'ok',
                                'sourcerub' => 'intranet',
                                'sourcepg' => 'gestion',
                                'param1' => 'sectionid',
                                'vparam1' => 'utilisateurs',
                                'param3' => 'lettre',
                                'vparam3' => $lettre
                            ]));
                        ?>
                        <input type="button" value="&nbsp;+&nbsp;" class="btn"
                               onclick="javascript:location.href='<?php echo $url_add; ?>'">
                </tr>
                <tr>
                    <th class="title_gestion" colspan="2    ">
                        Sont ici potentiellement répertoriées toutes les personnes physiques susceptibles de collaborer avec Sources Chrétiennes sur le plan scientifique.
                    </th>
                    <th align="right" valign="middle" class="enregistrements" colspan="3">
                       <span><b><?php echo $iNbreEnreg; ?> enregistrement(s)</b></span><br>
                        <select size="1" name="lettre" onchange="javascript:document.forms['frmGestionUsers'].submit();">
                            <option value="az"<?php if ('az' == $lettre) {
                                echo ' selected';
                            } ?>>A-Z</option>
                            <option value="a"<?php if ('a' == $lettre) {
                                echo ' selected';
                            } ?>>A</option>
                            <option value="b"<?php if ('b' == $lettre) {
                                echo ' selected';
                            } ?>>B</option>
                            <option value="c"<?php if ('c' == $lettre) {
                                echo ' selected';
                            } ?>>C</option>
                            <option value="d"<?php if ('d' == $lettre) {
                                echo ' selected';
                            } ?>>D</option>
                            <option value="e"<?php if ('e' == $lettre) {
                                echo ' selected';
                            } ?>>E</option>
                            <option value="f"<?php if ('f' == $lettre) {
                                echo ' selected';
                            } ?>>F</option>
                            <option value="g"<?php if ('g' == $lettre) {
                                echo ' selected';
                            } ?>>G</option>
                            <option value="h"<?php if ('h' == $lettre) {
                                echo ' selected';
                            } ?>>H</option>
                            <option value="i"<?php if ('i' == $lettre) {
                                echo ' selected';
                            } ?>>I</option>
                            <option value="j"<?php if ('j' == $lettre) {
                                echo ' selected';
                            } ?>>J</option>
                            <option value="k"<?php if ('k' == $lettre) {
                                echo ' selected';
                            } ?>>K</option>
                            <option value="l"<?php if ('l' == $lettre) {
                                echo ' selected';
                            } ?>>L</option>
                            <option value="m"<?php if ('m' == $lettre) {
                                echo ' selected';
                            } ?>>M</option>
                            <option value="n"<?php if ('n' == $lettre) {
                                echo ' selected';
                            } ?>>N</option>
                            <option value="o"<?php if ('o' == $lettre) {
                                echo ' selected';
                            } ?>>O</option>
                            <option value="p"<?php if ('p' == $lettre) {
                                echo ' selected';
                            } ?>>P</option>
                            <option value="q"<?php if ('q' == $lettre) {
                                echo ' selected';
                            } ?>>Q</option>
                            <option value="r"<?php if ('r' == $lettre) {
                                echo ' selected';
                            } ?>>R</option>
                            <option value="s"<?php if ('s' == $lettre) {
                                echo ' selected';
                            } ?>>S</option>
                            <option value="t"<?php if ('t' == $lettre) {
                                echo ' selected';
                            } ?>>T</option>
                            <option value="u"<?php if ('u' == $lettre) {
                                echo ' selected';
                            } ?>>U</option>
                            <option value="v"<?php if ('v' == $lettre) {
                                echo ' selected';
                            } ?>>V</option>
                            <option value="w"<?php if ('w' == $lettre) {
                                echo ' selected';
                            } ?>>W</option>
                            <option value="x"<?php if ('x' == $lettre) {
                                echo ' selected';
                            } ?>>X</option>
                            <option value="y"<?php if ('y' == $lettre) {
                                echo ' selected';
                            } ?>>Y</option>
                            <option value="z"<?php if ('z' == $lettre) {
                                echo ' selected';
                            } ?>>Z</option>
                        </select>
                    </th>
                </tr>
            </thead>
            <thead>
                <tr style="background-color: #790a41">
                    <th class="text-center" scope="col" width="30%">Nom</th>
                    <th class="text-center" scope="col" width="25%">Prénom</th>
                    <th class="text-center" scope="col">Tél.</th>
                    <th class="text-center" scope="col"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>E-mail
                    </th>
                    <th class="text-center" scope="col"
                        <?php if (!$rights_admin) { ?> style="display: none"<?php } ?>>
                        Action
                    </th>
                </tr>
            </thead>
            <?php

            $ligneType = 'B';
while ($uti = DbEnregSuivant($oRecordset)) {
    $usr_pk = $uti->PK_UTILISATEUR_USR;

    $decede = $uti->BL_DECEDE_USR;

    (empty($uti->TX_NOM_USR)) ? ($nom = '-') : ($nom = $uti->TX_NOM_USR);
    (empty($uti->TX_PRENOM_USR)) ? ($prenom = '-') : ($prenom = $uti->TX_PRENOM_USR);

    // ordre de priorité : e-mail pro, e-mail perso, '-'
    (empty($uti->TX_PRO_EMAIL_USR)) ?
        ((empty($uti->TX_PERSO_EMAIL_USR)) ?
            ($email = '-') :
            ($email = $uti->TX_PERSO_EMAIL_USR)) :
        ($email = $uti->TX_PRO_EMAIL_USR);

    // ordre de priorité : tél pro fixe, tél pro mobile, tél perso fixe, tél perso mobile
    (empty($uti->TX_PRO_TEL_FIXE_USR)) ?
        ((empty($uti->TX_PRO_TEL_MOBILE_USR)) ?
            ((empty($uti->TX_PERSO_TEL_FIXE_USR)) ?
                ((empty($uti->TX_PERSO_TEL_MOBILE_USR)) ?
                    ($tel = '-') :
                    ($tel = $uti->TX_PERSO_TEL_MOBILE_USR)) :
                ($tel = $uti->TX_PERSO_TEL_FIXE_USR)) :
            ($tel = $uti->TX_PRO_TEL_MOBILE_USR)) :
        ($tel = $uti->TX_PRO_TEL_FIXE_USR);

    ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A');
    ?>
            <tbody style="line-height: 15px;">
                <tr>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <?php if ($decede) {
                            echo '† ';
                        }?>
                            <a href="/?rubriqueid=intranet&pageid=gestion&sectionid=utilisateurs&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=utilisateurs&param3=lettre&vparam3=<?php echo $lettre; ?>&signet=<?php echo 'uti'.$usr_pk; ?>&id_utilisateur=<?php echo $usr_pk; ?>"
                               class="dtg-lignes">
                                <?php echo $nom; ?>
                            </a>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <a name="<?php echo 'uti'.$usr_pk; ?>">
                            <?php echo $prenom; ?>
                        </a>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <?php echo $tel; ?>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                        <?php if ('-' != $email) { ?>
                            <a target="_blank" href="mailto:<?php echo $email; ?>" class="dtg-lignes"><?php } ?>
                                <?php echo $email; ?>
                                <?php if ('-' != $email) { ?>
                            </a>
                        <?php } ?>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                        <?php if (!$rights_admin) { ?> style="display: none;"<?php } ?>>
                        <a href="javascript:if (confirm('Etes-vous sûr de vouloir supprimer cet utilisateur ?')){
                            <?php
                            $rq = <<<SQL
                                SELECT
                                    O.TX_TITRE_FRANCAIS_OVR,
                                    O.TX_CLAVIS_OVR
                                FROM sc_t_oeuvre AS O
                                LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                    ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                WHERE ot.FK_OUT_REF_USR = {$usr_pk}
                                GROUP BY O.TX_TITRE_FRANCAIS_OVR;
                                SQL;
    $oRecordset_ovr = DbExecRequete($rq, $oConnexion);
    $nb_ovr = DbNbreEnreg($oRecordset_ovr);

    echo "if(confirm('Cet utilisateur est relié à ".$nb_ovr.' &#339;uvre(s) :\n ';
    while ($ovr = DbEnregSuivant($oRecordset_ovr)) {
        if (!empty($ovr->TX_TITRE_FRANCAIS_OVR)) {
            echo addslashes((string) $ovr->TX_TITRE_FRANCAIS_OVR);
        }
        if (!empty($ovr->TX_CLAVIS_OVR)) {
            echo '('.$ovr->TX_CLAVIS_OVR.')\n ';
        }
    } ?>
                        \u00cates vous sûr de vouloir le supprimer ainsi que ses tâches ?'))
            location.href='/?rubriqueid=intranet&pageid=gestion&sectionid=utilisateurs&action=suppr&id=<?php echo $usr_pk; ?>&lettre=<?php echo $lettre; ?>';}" class="dtg-lignes">
                                        Suppr.
                                    </a>
                </td>
                </tr>
            </tbody>
            <?php
}
DbClose($oConnexion);
?>
        </table>
    </form>
    </td>
</tr>

