<?php

$rights_admin = [];
$rights_member = [];
$rights_ext = [];

$ddl_lettre_ovr = 'a';
$ddl_autancien = '0';
$rdbAutAncien = 'tous';
$ddl_titre_ovr = 'FRANCAIS';
$ddl_utilisateur = '0';
$ordre_ovr = 'ASC';
$tri_ovr = 'TX_NOM_FRANCAIS_AAN';

if (isset($_REQUEST['ddl_lettre_ovr'])) {
    $ddl_lettre_ovr = $_REQUEST['ddl_lettre_ovr'];
}elseif (isset($_REQUEST['vparam2']) && !array_key_exists('ddl_lettre_ovr', $_REQUEST)){
    $ddl_lettre_ovr = $_REQUEST['vparam2'];
}else{
    $ddl_lettre_ovr = 'a';
}

if (isset($_REQUEST['ddl_autancien'])) {
    $ddl_autancien = $_REQUEST['ddl_autancien'];
}elseif (isset($_REQUEST['vparam3']) && !array_key_exists('ddl_autancien', $_REQUEST)){
    $ddl_autancien = $_REQUEST['vparam3'];
}else{
    $ddl_autancien = '0';
}

if (isset($_REQUEST['rdbAutAncien'])) {
    $rdbAutAncien = $_REQUEST['rdbAutAncien'];
}elseif (isset($_REQUEST['vparam4']) && !array_key_exists('rdbAutAncien', $_REQUEST)){
    $rdbAutAncien = $_REQUEST['vparam4'];
}else{
    $rdbAutAncien = 'tous';
}

if (isset($_REQUEST['ddl_utilisateur'])) {
    $ddl_utilisateur = $_REQUEST['ddl_utilisateur'];
}elseif (isset($_REQUEST['vparam6']) && !array_key_exists('ddl_utilisateur', $_REQUEST)){
    $ddl_utilisateur = $_REQUEST['vparam6'];
}else{
    $ddl_utilisateur = '0';
}

if (isset($_REQUEST['ordre_ovr'])) {
    if ('' != $_REQUEST['ordre_ovr']) {
        $ordre_ovr = $_REQUEST['ordre_ovr'];
    }
}
if (isset($_REQUEST['tri_ovr'])) {
    if ('' != $_REQUEST['tri_ovr']) {
        $tri_ovr = $_REQUEST['tri_ovr'];
    }
}

if ('ASC' == $ordre_ovr) {
    $nouvordre_ovr = 'DESC';
} else {
    $nouvordre_ovr = 'ASC';
}

$oConnexion = DbConnection();

/* get the rights of the logged user */
$usr_pfl = null;
if (!empty($_SESSION['PK_UTILISATEUR_USR'])) {
    $sQueryUser = <<<SQL
        SELECT
            FK_APT_REF_PFL
        FROM sc_t_utilisateur
        WHERE PK_UTILISATEUR_USR = {$_SESSION['PK_UTILISATEUR_USR']};
    SQL;
    $oRecordset = DbExecRequete($sQueryUser, $oConnexion);
    while ($fetchUser = DbEnregSuivant($oRecordset)) {
        $usr_pfl = $fetchUser->FK_APT_REF_PFL;
    }
}
if($usr_pfl === 'admin'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE LB_DROIT_DRT LIKE '%Oeuvres%'; 
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_admin[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl === 'membre SC' || $usr_pfl === 'membre CS'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Oeuvres%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_member[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}elseif ($usr_pfl === 'extérieur'){
    $reqDrt = <<<SQL
    SELECT 
        PK_DROIT_DRT,
        LB_DROIT_DRT
    FROM sc_t_droit
    WHERE 
        LB_DROIT_DRT LIKE '%Oeuvres%' 
    AND LB_DROIT_DRT NOT LIKE '%Suppression'
    AND LB_DROIT_DRT NOT LIKE '%Edition';
    SQL;
    $oRsDrt = DbExecRequete($reqDrt, $oConnexion);
    while($fetchDrt = DbEnregSuivant($oRsDrt)){
        $rights_ext[$fetchDrt->PK_DROIT_DRT] = $fetchDrt->LB_DROIT_DRT;
    }
}

if (isset($_REQUEST['action']) && !empty($id)) {
    switch ($_REQUEST['action']) {
        case 'suppr':
            if ($rights_admin) {
                begin($oConnexion);

                $sReq1 = <<<SQL
                        SELECT FK_AVO_REF_OVR
                        FROM sc_t_assoc_vol_ovr
                        WHERE FK_AVO_REF_OVR = {$id};
                        SQL;
                $sReq2 = <<<SQL
                        SELECT FK_OUT_REF_OVR
                        FROM sc_t_assoc_ovr_usr_tch
                        WHERE FK_OUT_REF_OVR = {$id};
                        SQL;
                $result1 = DbExecRequete($sReq1, $oConnexion);
                $result2 = DbExecRequete($sReq2, $oConnexion);

                $sQuery1 = <<<SQL
                        DELETE FROM sc_t_oeuvre
                        WHERE PK_OEUVRE_OVR = {$id};
                        SQL;
                $sQuery2 = <<<SQL
                        DELETE FROM sc_t_assoc_au_ovr
                        WHERE FK_OEUVRE_OVR = {$id};
                        SQL;
                $sQuery3 = <<<SQL
                        DELETE FROM sc_t_assoc_vol_ovr
                        WHERE FK_AVO_REF_OVR = {$id};
                        SQL;
                $sQuery4 = <<<SQL
                        DELETE FROM sc_t_assoc_ovr_usr_tch
                        WHERE FK_OUT_REF_OVR = {$id};
                        SQL;

                if (0 != $result1->num_rows) {
                    $oRecordset1 = DbExecRequete($sQuery3, $oConnexion);
                } elseif (0 != $result2->num_rows) {
                    $oRecordset2 = DbExecRequete($sQuery4, $oConnexion);
                }

                // insert trace of the deletion
                $req = <<<SQL
                SELECT TX_TITRE_FRANCAIS_OVR
                FROM sc_t_oeuvre
                WHERE PK_OEUVRE_OVR = {$id};
                SQL;
                $oRecordset = DbExecRequete($req, $oConnexion);
                while ($fetch = DbEnregSuivant($oRecordset)) {
                    $ovr_name = $fetch->TX_TITRE_FRANCAIS_OVR;
                }
                $str_oeuvre = addslashes("l'oeuvre");
                $today = date('Y-m-d H:i:s');
                $sQueryTrace = <<<SQL
                INSERT INTO sc_t_trace(TX_DOMAINE_TRC, NM_ITEMID_TRC, LB_TRACE_TRC, TX_TYPE_TRC,DT_TRACE_TRC, NM_UTILISATEUR_TRC)
                VALUES('OEUVRE',
                       {$id},
                       'Suppression de {$str_oeuvre} {$ovr_name} par {$_SESSION['TX_PRENOM_USR']} {$_SESSION['TX_NOM_USR']}',
                       'SQL-D',
                       "{$today}",
                       {$_SESSION['PK_UTILISATEUR_USR']}
                       );
                SQL;
                $oRecordset = DbExecRequete($sQueryTrace, $oConnexion);

                // delete the work
                $oRecordset3 = DbExecRequete($sQuery1, $oConnexion);
                $oRecordset4 = DbExecRequete($sQuery2, $oConnexion);

                commit($oConnexion);

                addNotice('success', '&#338;uvre correctement supprimée !');
                redirect(sprintf('/?%s', http_build_query([
                    'rubriqueid' => 'intranet',
                    'pageid' => 'gestion',
                    'sectionid' => 'oeuvres',
                    'ddlsectionid' => 'oeuvres',
                    'param2' => 'lettre',
                    'vparam2' => $ddl_lettre_ovr
                ])));
            } else {
                addNotice('danger', 'Vous ne pouvez pas supprimer cette &#339;uvre car une ou plusieurs données lui sont associées !');
            }

            break;
    }
}

?>

<!------------------->
<!--Premier encadré-->
<!------------------->

<?php if (hasNotice()): ?>
    <tr>
        <td align="center"><?php printNotice(); ?></td>
    </tr>
<?php endif; ?>
<tr>
    <td>

        <?php
        $sQuery = <<<SQL
        SELECT *
        FROM sc_t_trace
        WHERE
            TX_DOMAINE_TRC = 'OEUVRE'
        ORDER BY DT_TRACE_TRC DESC LIMIT 3;
    SQL;
        $oRs = DbExecRequete($sQuery, $oConnexion);
        while ($trace = DbEnregSuivant($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                <b>{$trace->DT_TRACE_TRC} : {$trace->LB_TRACE_TRC}</b><br>
            </td>
        </tr>
        HTML;
        }
        if (0 == DbNbreEnreg($oRs)) {
            echo <<<HTML
        <tr style="background-color: #FFFAD9;">
            <td colspan="3" align="center">
                Aucun historique des dernières modifications apportées à une &#338;uvre
            </td>
        </tr>
        HTML;
        }
        ?>

        <form name="frmGestionOeuvres" method="post" enctype="multipart/form-data" data-current-url="<?php echo sprintf('/?%s', http_build_query(array_filter([
            'rubriqueid' => 'intranet',
            'pageid' => 'gestion',
            'sectionid' => 'oeuvres',
            'ddlsectionid' => 'oeuvres',
            'param2' => 'ddl_lettre_ovr',
            'vparam2' => $ddl_lettre_ovr
        ]))) ?>">
            <script language="javascript" src="js/gestion.js"></script>
        <table border="0" width="100%" cellspacing="2" cellpadding="2" class="table table-sm">
            <div class="alert alert-warning">
                <b>Chantiers des &#339;uvres projetées</b>, informations sur un dossier en cours non encore relié à un volume.</b>
            </div>
            <thead style="line-height: 20px;">
                <tr>
                    <th class="title_gestion" colspan="5">
                        <b>LISTE DES &#140;UVRES</b>
                        <?php
                        $url_add = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'oeuvres',
                            'detail' => 'ok',
                            'sourcerub' => 'intranet',
                            'sourcepg' => 'gestion',
                            'param1' => 'sectionid',
                            'vparam1' => 'oeuvres',
                            'param2' => 'ddl_lettre_ovr',
                            'vparam2' => $ddl_lettre_ovr,
                            'param3' => $ddl_autancien,
                            'vparam3' => $ddl_autancien
                        ]));
                        ?>
                        <input type="button" value=" + " class="btn"
                               onclick="javascript:location.href='<?php echo $url_add; ?>'"/>
                        <div style="margin-left: 40px">
                            <b>Renseigner ici les tâches liées aux &#339;uvres, et dans la gestion du volume celles liées aux volumes.</b>
                        </div>
                    </th>
                </tr>
                    <th class="title_gestion" colspan="1">
                        <font class="font-normal"><font class="font-datagrid-entete" size="+1"><b>Auteurs anciens :</b></font></font>
                        <select size="1" name="ddl_autancien"
                                onchange="javascript:document.forms['frmGestionOeuvres'].submit();" class="x-large">


                            <?php if ('tous' == $rdbAutAncien) {
                                /* tous les auteurs */
                                $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        PK_AUTEUR_ANCIEN_AAN,
                                        TX_NOM_FRANCAIS_AAN
                                    FROM sc_t_auteur
                                    ORDER BY TX_NOM_FRANCAIS_AAN;
                                    SQL;

                                $oRecordset_2 = DbExecRequete($sQuery, $oConnexion);

                                /* les auteurs des oeuvres en chantier */
                            } elseif ('chantier' == $rdbAutAncien) {
                                $sQuery = <<<SQL
                                SELECT DISTINCT
                                    A.PK_AUTEUR_ANCIEN_AAN,
                                    A.TX_NOM_FRANCAIS_AAN
                                FROM sc_t_oeuvre AS O
                                LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                    ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                                LEFT JOIN sc_t_assoc_au_ovr AS au
                                    ON au.`FK_OEUVRE_OVR`=O.PK_OEUVRE_OVR
                                LEFT JOIN sc_t_volume AS V
                                    ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                                LEFT JOIN sc_t_auteur AS A
                                    ON A.PK_AUTEUR_ANCIEN_AAN = au.FK_AUTEUR_ANCIEN_AAN
                                INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                    ON ot.`FK_OUT_REF_OVR`=O.PK_OEUVRE_OVR
                                WHERE vo.FK_AVO_REF_VOL IS NULL
                                    AND O.`BL_OEUVRE_INTERNE_OVR`=1
                                    AND A.`TX_NOM_FRANCAIS_AAN` IS NOT NULL
                                ORDER BY A.TX_NOM_FRANCAIS_AAN ;
                                SQL;
                            }
                            $oRecordset_2 = DbExecRequete($sQuery, $oConnexion);
?>
                            <option value="0"<?php if ('0' == $ddl_autancien) {
                                echo ' selected';
                            } ?>>=== Auteurs anciens (<?php echo DbNbreEnreg($oRecordset_2); ?>) === </option>

                            <?php
                            while ($aan = DbEnregSuivant($oRecordset_2)) {
                                echo '<option value="'.$aan->PK_AUTEUR_ANCIEN_AAN.'"';
                                if ($ddl_autancien == $aan->PK_AUTEUR_ANCIEN_AAN) {
                                    echo ' selected';
                                }
                                echo '>'.$aan->TX_NOM_FRANCAIS_AAN.'</option>';
                            }
?>
                        </select>
                    </th>
                    <th class="title_gestion" colspan="1">
                        <font class="font-normal"><font class="font-datagrid-entete" size="+1"><b>Volumes :</b></font></font>
                        <?php

                        if ('chantier' == $rdbAutAncien) {
                            if ('0' != $ddl_autancien) {
                                $sQuery = <<<SQL
                            SELECT DISTINCT
                                O.*,
                                A.PK_AUTEUR_ANCIEN_AAN,
                                A.`TX_NOM_FRANCAIS_AAN`
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                            LEFT JOIN sc_t_assoc_au_ovr AS au
                                ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                            LEFT JOIN sc_t_auteur AS A
                                ON au.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
                            LEFT JOIN sc_t_volume AS V
                                ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                            INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                            WHERE vo.FK_AVO_REF_VOL IS NULL AND
                                O.`BL_OEUVRE_INTERNE_OVR`=1 AND
                                A.`TX_NOM_FRANCAIS_AAN` LIKE '{$ddl_autancien}'
                            ORDER BY {$tri_ovr} {$ordre_ovr};
                            SQL;
                            } elseif ('tous' == $rdbAutAncien) {
                                if ('0' != $ddl_autancien) {
                                    $sQuery = <<<SQL
                            SELECT O.*,
                                A.PK_AUTEUR_ANCIEN_AAN,
                                A.`TX_NOM_FRANCAIS_AAN`
                            FROM sc_t_oeuvre AS O
                            LEFT JOIN sc_t_assoc_au_ovr AS ao
                                ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                            LEFT JOIN sc_t_auteur AS A
                                ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                            WHERE A.`TX_NOM_FRANCAIS_AAN` LIKE '{$ddl_autancien}'
                            ORDER BY {$tri_ovr} {$ordre_ovr};
                            SQL;
                                }
                            }
                        }
?>
                        <input type="radio" id="rdbAutAncien" name="rdbAutAncien"
                               onclick="javascript:document.forms['frmGestionOeuvres'].submit();"
                               value="tous"<?php if ('tous' == $rdbAutAncien) {
                                   echo ' checked';
                               } ?> />
                        &nbsp;<font class="font-normal"><font class="font-datagrid-entete" size="+1">Tous</font></font>&nbsp;

                        <input type="radio" id="rdbAutAncien" name="rdbAutAncien"
                               onclick="javascript:document.forms['frmGestionOeuvres'].submit();"
                               value="chantier"<?php if ('chantier' == $rdbAutAncien) {
                                   echo ' checked';
                               } ?> />
                        &nbsp;<font class="font-normal"><font class="font-datagrid-entete" size="+1">Vol. en chantier</font></font>
                    </th>
                    <th class="title_gestion" colspan="1">
                        <div style="font-size: large"><b>Titre commen&ccedil;ant par :</b></div>
                        <?php

                               if (!empty($tri_ovr)) {
                                   $tri_ovr = $tri_ovr;
                               } else {
                                   echo 'O.TX_TITRE_FRANCAIS_OVR';
                               }

                               /* toutes les oeuvres ou celles en chantier */

                               /* les oeuvres en chantiers */
                               if ('chantier' == $rdbAutAncien) {
                                   /* suivant la lettre sélectionnée */
                                   if ('az' != $ddl_lettre_ovr) {
                                       /* 1. suivant l'auteur sélectionné */
                                       if ('0' != $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_utilisateur AS U
                                        ON ot.`FK_OUT_REF_USR` = U.`PK_UTILISATEUR_USR`
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}' AND
                                        ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }

                                           /* 2. de tous les auteurs */
                                       } elseif ('0' == $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_utilisateur AS U
                                        ON ot.`FK_OUT_REF_USR` = U.`PK_UTILISATEUR_USR`
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                       }

                                       /* toutes les oeuvres en chantiers */
                                   } elseif ('az' == $ddl_lettre_ovr) {
                                       /* 1. suivant l'auteur sélectionné */
                                       if ('0' != $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                        ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                                    LEFT JOIN sc_t_assoc_au_ovr AS au
                                        ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                                    LEFT JOIN sc_t_auteur AS A
                                        ON au.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
                                    LEFT JOIN sc_t_volume AS V
                                        ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                                    INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                                    WHERE vo.FK_AVO_REF_VOL IS NULL AND
                                        O.`BL_OEUVRE_INTERNE_OVR`=1 AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}' AND
                                        ot.`FK_OUT_REF_USR` ={$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                        ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                                    LEFT JOIN sc_t_assoc_au_ovr AS au
                                        ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                                    LEFT JOIN sc_t_auteur AS A
                                        ON au.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
                                    LEFT JOIN sc_t_volume AS V
                                        ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                                    INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                                    WHERE vo.FK_AVO_REF_VOL IS NULL AND
                                        O.`BL_OEUVRE_INTERNE_OVR`=1 AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                       }
                                       /* 2. de tous les auteurs */
                                       elseif ('0' == $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                        ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                                    LEFT JOIN sc_t_assoc_au_ovr AS au
                                        ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                                    LEFT JOIN sc_t_auteur AS A
                                        ON au.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
                                    LEFT JOIN sc_t_volume AS V
                                        ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                                    WHERE vo.FK_AVO_REF_VOL IS NULL AND
                                        O.`BL_OEUVRE_INTERNE_OVR`=1 AND
                                        ot.`FK_OUT_REF_USR` ={$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT DISTINCT
                                        O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_vol_ovr AS vo
                                        ON O.PK_OEUVRE_OVR=vo.FK_AVO_REF_OVR
                                    LEFT JOIN sc_t_assoc_au_ovr AS au
                                        ON au.`FK_OEUVRE_OVR`=O.`PK_OEUVRE_OVR`
                                    LEFT JOIN sc_t_auteur AS A
                                        ON au.`FK_AUTEUR_ANCIEN_AAN` = A.`PK_AUTEUR_ANCIEN_AAN`
                                    LEFT JOIN sc_t_volume AS V
                                        ON V.PK_VOLUMEINFOS_VIF=vo.FK_AVO_REF_VOL
                                    INNER JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.`FK_OUT_REF_OVR`=O.`PK_OEUVRE_OVR`
                                    WHERE vo.FK_AVO_REF_VOL IS NULL AND
                                        O.`BL_OEUVRE_INTERNE_OVR`=1
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                       }
                                   }
                               }

                               /* toutes les oeuvres */
                               elseif ('tous' == $rdbAutAncien) {
                                   /* suivant la lettre sélectionnée */
                                   if ('az' != $ddl_lettre_ovr) {
                                       /* 1. suivant l'auteur sélectionné */
                                       if ('0' != $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_utilisateur AS U
                                        ON ot.`FK_OUT_REF_USR` = U.`PK_UTILISATEUR_USR`
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}' AND
                                        ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }

                                           /* 2. de tous les auteurs */
                                       } elseif ('0' == $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_utilisateur AS U
                                        ON ot.`FK_OUT_REF_USR` = U.`PK_UTILISATEUR_USR`
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%' AND
                                        ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. de tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    WHERE O.TX_TITRE_FRANCAIS_OVR LIKE '{$ddl_lettre_ovr}%'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                       }
                                   }
                                   /* toutes les oeuvres */
                                   elseif ('az' == $ddl_lettre_ovr) {
                                       /* 1. suivant l'auteur sélectionné */
                                       if ('0' != $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR
                                    LEFT JOIN sc_t_utilisateur AS U
                                        ON ot.`FK_OUT_REF_USR` = U.`PK_UTILISATEUR_USR`
                                    WHERE A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}'  AND
                                          ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. suivant tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    WHERE A.PK_AUTEUR_ANCIEN_AAN LIKE '{$ddl_autancien}'
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* 2. suivant tous les auteurs */
                                       } elseif ('0' == $ddl_autancien) {
                                           /* A. suivant l'utilisateur sélectionné */
                                           if ('0' != $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                                        ON ot.FK_OUT_REF_OVR = O.PK_OEUVRE_OVR AND
                                          ot.`FK_OUT_REF_USR` = {$ddl_utilisateur}
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                           /* B. suivant tous les utilisateurs */
                                           elseif ('0' == $ddl_utilisateur) {
                                               $sQuery = <<<SQL
                                    SELECT O.*,
                                        A.PK_AUTEUR_ANCIEN_AAN,
                                        A.`TX_NOM_FRANCAIS_AAN`
                                    FROM sc_t_oeuvre AS O
                                    LEFT JOIN sc_t_assoc_au_ovr AS ao
                                        ON O.PK_OEUVRE_OVR = ao.FK_OEUVRE_OVR
                                    LEFT JOIN sc_t_auteur AS A
                                        ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                                    GROUP BY TX_TITRE_FRANCAIS_OVR
                                    ORDER BY {$tri_ovr} {$ordre_ovr};
                                    SQL;

                                               $oRecordset = DbExecRequete($sQuery, $oConnexion);
                                               $iNbreEnreg = DbNbreEnreg($oRecordset);
                                           }
                                       }
                                   }
                               }

?>
                        <select size="1" name="ddl_lettre_ovr"
                                onchange="javascript:document.forms['frmGestionOeuvres'].submit();" class="x-large">
                            <option value="az"<?php if ('az' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>A-Z</option>

                            <option value="a"<?php if ('a' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>A</option>

                            <option value="b"<?php if ('b' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>B</option>
                            <option value="c"<?php if ('c' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>C</option>
                            <option value="d"<?php if ('d' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>D</option>
                            <option value="e"<?php if ('e' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>E</option>
                            <option value="f"<?php if ('f' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>F</option>
                            <option value="g"<?php if ('g' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>G</option>
                            <option value="h"<?php if ('h' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>H</option>
                            <option value="i"<?php if ('i' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>I</option>
                            <option value="j"<?php if ('j' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>J</option>
                            <option value="k"<?php if ('k' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>K</option>
                            <option value="l"<?php if ('l' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>L</option>
                            <option value="m"<?php if ('m' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>M</option>
                            <option value="n"<?php if ('n' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>N</option>
                            <option value="o"<?php if ('o' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>O</option>
                            <option value="p"<?php if ('p' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>P</option>
                            <option value="q"<?php if ('q' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>Q</option>
                            <option value="r"<?php if ('r' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>R</option>
                            <option value="s"<?php if ('s' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>S</option>
                            <option value="t"<?php if ('t' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>T</option>
                            <option value="u"<?php if ('u' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>U</option>
                            <option value="v"<?php if ('v' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>V</option>
                            <option value="w"<?php if ('w' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>W</option>
                            <option value="x"<?php if ('x' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>X</option>
                            <option value="y"<?php if ('y' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>Y</option>
                            <option value="z"<?php if ('z' == $ddl_lettre_ovr) {
                                echo ' selected';
                            } ?>>Z</option>
                        </select>
                    </th>
                    <th class="title_gestion" colspan="2">
                        <font class="font-normal"><font class="font-datagrid-entete" size="+1"><b>Collaborateur(s) :</b></font></font>
                        <select size="1" name="ddl_utilisateur" class="x-large"
                                onchange="javascript:document.forms['frmGestionOeuvres'].submit();">

                            <option value="0"
                                <?php if ('0' == $ddl_utilisateur) {
                                    echo ' selected';
                                } ?>
                            >=== Tous les collaborateurs ===
                            </option>
                            <?php

                            $sQuery = <<<SQL
                        SELECT DISTINCT
                            U.PK_UTILISATEUR_USR,
                            U.TX_NOM_USR,
                            U.TX_PRENOM_USR,
                            U.BL_DECEDE_USR
                        FROM sc_t_auteur AS A
                        LEFT JOIN sc_t_assoc_au_ovr AS ao
                            ON A.PK_AUTEUR_ANCIEN_AAN = ao.FK_AUTEUR_ANCIEN_AAN
                        LEFT JOIN sc_t_oeuvre AS O
                            ON O.`PK_OEUVRE_OVR`= ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_assoc_ovr_usr_tch AS ot
                            ON ot.FK_OUT_REF_OVR = ao.FK_OEUVRE_OVR
                        LEFT JOIN sc_t_utilisateur AS U
                            ON ot.`FK_OUT_REF_USR` = U.PK_UTILISATEUR_USR
                        WHERE U.TX_NOM_USR IS NOT NULL AND
                            U.TX_PRENOM_USR IS NOT NULL
                        ORDER BY U.TX_NOM_USR;
                        SQL;

$oRecordset_2 = DbExecRequete($sQuery, $oConnexion);

while ($usr = DbEnregSuivant($oRecordset_2)) {
    (empty($usr->TX_NOM_USR)) ? ($vol_utilisateur_nom = '') : ($vol_utilisateur_nom = $usr->TX_NOM_USR);
    (empty($usr->TX_PRENOM_USR)) ? ($vol_utilisateur_prenom = '') : ($vol_utilisateur_prenom = $usr->TX_PRENOM_USR);
    $decede = $usr->BL_DECEDE_USR; ?>

    <option value="<?php echo $usr->PK_UTILISATEUR_USR; ?>"
            <?php if ($ddl_utilisateur == $usr->PK_UTILISATEUR_USR) { ?> selected <?php } ?>>
        <?php if ($decede) { echo '† '; } echo $vol_utilisateur_nom; echo ' '; echo $vol_utilisateur_prenom; ?>
    </option>
<?php } ?>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th class="title_gestion" colspan="5">
                        <div style="margin-left: 30px; font-size: large">
                            <b><u><?php echo $iNbreEnreg; ?> enregistrement(s)</u></b>
                        </div>
                        <div style="text-align: center">
                            <b>Vous pouvez trier les &#339;uvres par ordre alphabétique de leur titre français et latin et de leur(s) auteur(s) anciens en cliquant sur l'en-tête de colonne correspondant</b>
                        </div>
                    </th>
                </tr>
            </thead>
            <thead style="background-color: #C90063;">
                <tr>
                    <th class="text-center" scope="col" class="datagrid-ligne1">
                        <?php
                        $url = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'oeuvres',
                            'ddlsectionid' => 'oeuvres',
                            'ddl_lettre_ovr' => $ddl_lettre_ovr,
                            'ddl_titre_ovr' => $ddl_titre_ovr,
                            'ddl_autancien' => $ddl_autancien,
                            'tri_ovr' => 'TX_NOM_FRANCAIS_AAN',
                            'ordre_ovr' => ('TX_NOM_FRANCAIS_AAN' == $tri_ovr) ? $nouvordre_ovr : $ordre_ovr,
                                                    ]));
                            ?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>Auteur ancien</u>
                            <?php
    if ('TX_NOM_FRANCAIS_AAN' == $tri_ovr && 'ASC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
    } elseif ('TX_NOM_FRANCAIS_AAN' == $tri_ovr && 'DESC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
    }
?>
                        </a>
                    </th>
                    <th class="text-center" scope="col" class="datagrid-ligne1">
                        <?php
                        $url = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'oeuvres',
                            'ddlsectionid' => 'oeuvres',
                            'ddl_lettre_ovr' => $ddl_lettre_ovr,
                            'ddl_titre_ovr' => $ddl_titre_ovr,
                            'tri_ovr' => 'TX_TITRE_FRANCAIS_OVR',
                            'ordre_ovr' => ('TX_TITRE_FRANCAIS_OVR' == $tri_ovr) ? $nouvordre_ovr : $ordre_ovr,
                            ]));
                            ?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>Titre français</u>
                                <?php
        if ('TX_TITRE_FRANCAIS_OVR' == $tri_ovr && 'ASC' == $ordre_ovr) {
            echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
        } elseif ('TX_TITRE_FRANCAIS_OVR' == $tri_ovr && 'DESC' == $ordre_ovr) {
            echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
        }
?>
                        </a>
                    </th>
                    <th class="text-center" scope="col" class="datagrid-ligne1">
                        <?php
                        $url = sprintf('/?%s', http_build_query([
                            'rubriqueid' => 'intranet',
                            'pageid' => 'gestion',
                            'sectionid' => 'oeuvres',
                            'ddlsectionid' => 'oeuvres',
                            'ddl_lettre_ovr' => $ddl_lettre_ovr,
                            'ddl_titre_ovr' => $ddl_titre_ovr,
                            'tri_ovr' => 'TX_TITRE_LATIN_OVR',
                            'ordre_ovr' => ('TX_TITRE_LATIN_OVR' == $tri_ovr) ? $nouvordre_ovr : $ordre_ovr,
                        ]));
?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>Titre latin</u>
                            <?php
    if ('TX_TITRE_LATIN_OVR' == $tri_ovr && 'ASC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
    } elseif ('TX_TITRE_LATIN_OVR' == $tri_ovr && 'DESC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
    }
?>
                        </a>
                    </th>
                    <th class="text-center" scope="col" class="datagrid-ligne1"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                    <?php
                        $url = sprintf('/?%s', http_build_query([
'rubriqueid' => 'intranet',
'pageid' => 'gestion',
'sectionid' => 'oeuvres',
'ddlsectionid' => 'oeuvres',
'ddl_lettre_ovr' => $ddl_lettre_ovr,
'ddl_titre_ovr' => $ddl_titre_ovr,
'tri_ovr' => 'TX_CLAVIS_OVR',
'ordre_ovr' => ('TX_CLAVIS_OVR' == $tri_ovr) ? $nouvordre_ovr : $ordre_ovr,
                        ]));
?>
                        <a href="<?php echo $url; ?>" style="color: white"><u>Clavis</u>
                            <?php
    if ('TX_CLAVIS_OVR' == $tri_ovr && 'ASC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle2.gif" BORDER="0">';
    } elseif ('TX_CLAVIS_OVR' == $tri_ovr && 'DESC' == $ordre_ovr) {
        echo ' <img SRC="img/common/triangle.gif" BORDER="0">';
    }
?>
                        </a>
                    </th>
                    <th class="text-center" scope="col" class="datagrid-ligne1"
                        <?php if (!$rights_admin) { ?> style="display: none"<?php } ?>>Action
                    </th>
                </tr>
            </thead>

            <?php
            $ligneType = 'B';

while ($ovr = DbEnregSuivant($oRecordset)) {
    $ovr_pk = $ovr->PK_OEUVRE_OVR;

    $pk_aan = $ovr->PK_AUTEUR_ANCIEN_AAN;

    $ovr_aan = '<a href="/?rubriqueid=intranet&pageid=gestion&sectionid=autanciens&detail=ok&sourcerub=intranet&sourcepg=gestion&param1=sectionid&vparam1=autanciens&signet='.$pk_aan.'&id_autancien='.$pk_aan.'"
>'.$ovr->TX_NOM_FRANCAIS_AAN.'</a>';

    ('' == $ovr->TX_TITRE_LATIN_OVR) ? ($ovr_latin = '-') : ($ovr_latin = $ovr->TX_TITRE_LATIN_OVR);
    ('' == $ovr->TX_TITRE_FRANCAIS_OVR) ? ($ovr_francais = '-') : ($ovr_francais = $ovr->TX_TITRE_FRANCAIS_OVR);
    ('' == $ovr->TX_CLAVIS_OVR) ? ($ovr_clavis = '-') : ($ovr_clavis = $ovr->TX_CLAVIS_OVR);

    ('A' == $ligneType) ? ($ligneType = 'B') : ($ligneType = 'A');

    ?>
                <tbody>
                <tr style="line-height: 15px;">
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <?php echo $ovr_aan; ?>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <?php
                            $url_ovr_fr = sprintf('/?%s', http_build_query([
                                'rubriqueid' => 'intranet',
                                'pageid' => 'gestion',
                                'sectionid' => 'oeuvres',
                                'detail' => 'ok',
                                'sourcerub' => 'intranet',
                                'sourcepg' => 'getion',
                                'param1' => 'sectionid',
                                'vparam1' => 'oeuvres',
                                'param2' => 'ddl_lettre_ovr',
                                'vparam2' => $ddl_lettre_ovr,
                                'param3' => 'ddl_autancien',
                                'vparam3' => $ddl_autancien,
                                'param4' => 'rdbAutAncien',
                                'vparam4' => $rdbAutAncien,
                                'param6' => 'ddl_utilisateur',
                                'vparam6' => $ddl_utilisateur,
                                'param8' => 'ddl_titre_ovr',
                                'vparam8' => $ddl_titre_ovr,
                                'signet' => $ovr_pk,
                                'id_oeuvre' => $ovr_pk
                            ]));
                        ?>
                        <a href="<?php echo $url_ovr_fr; ?>" class="dtg-lignes">
                            <?php echo $ovr_francais; ?>
                        </a>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>">
                        <a name="<?php echo 'ovr'.$ovr_pk; ?>"><?php echo $ovr_latin; ?></a>
                    </td>

                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                        <?php if (!$rights_admin) { ?> colspan="2" <?php } ?>>
                        <?php echo $ovr_clavis; ?>
                    </td>
                    <td align="center" class="datagrid-lignes<?php echo $ligneType; ?>"
                        <?php if (!$rights_admin) { ?> style="display: none;"<?php } ?>>
                            <a href="javascript: if (confirm('Etes-vous sûr de vouloir supprimer cette &#339;uvre ?')) {
         location.href='/?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&action=suppr&id=<?php echo $ovr_pk; ?>&ddl_lettre_ovr=<?php echo $ddl_lettre_ovr; ?>&ddl_autancien=<?php echo $ddl_autancien; ?>'; }"
                               class="dtg-lignes">Suppr.</a>
                    </td>
                </tr>
            </tbody>
            <?php } ?>
        </table>
        </form>
    </td>
</tr>

<?php
DbClose($oConnexion);
?>










