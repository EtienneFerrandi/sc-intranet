<?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
    <script language="javascript">
        function CheckForm() {
            if (document.forms['connection'].login.value == '' || document.forms['connection'].password.value == '') {
                alert('Vous devez saisir un identifiant et un mot de passe !');
                if (document.forms['connection'].login.value == '') {
                    document.forms['connection'].login.focus();
                } else {
                    document.forms['connection'].password.focus();
                }
                return false;
            }
            return true;
        }
    </script>
    <form name="connection" method="post" action="/?rubriqueid=intranet&pageid=connexion" onSubmit="return CheckForm();">
<?php endif; ?>
<table border="0" cellpadding="4" cellspacing="4">
    <tr>
        <td colspan="3" align="center">
            <a class="font-normal" href="https://drive.google.com/drive/u/4/folders/1AO9YDXGbHKN5wJ7ifkqgfk2MLnFy2qPK" target="_blank">Lien vers le dossier de travail collaboratif du Google Drive des Sources Chrétiennes</a>
        </td>
    </tr>
    <?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
        <tr>
            <td colspan="3" align="center"><font class="font-normal"><i>Veuillez vous identifier s'il vous plaît...</i></font></td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <font class="font-normal font-couleur-erreur"><b>
                    <?php if ($bErreur): ?>
                        Votre couple 'identifiant / mot de passe'<br>
                        est invalide !
                    <?php elseif ($bEndOfSession && 'ok' == $sEndOfSessionAuto): ?>
                        Vous avez été déconnecté automatiquement.
                    <?php elseif ($bEndOfSession && 'no' == $sEndOfSessionAuto): ?>
                        Session terminée suite à votre demande.
                    <?php endif; ?>
                </b></font>
            </td>
        </tr>
        <tr>
            <td align="right">
                <font class="font-normal"><b>identifiant</b> :</font>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <input type="text" name="login" size="20">
            </td>
        </tr>
        <tr>
            <td align="right">
                <font class="font-normal"><b>mot de passe</b> :</font>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <input type="password" name="password" size="20">
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <input type="submit" value="Valider" name="btn_valider" class="btn">
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td colspan="3" align="center">
                <font class="font-normal"><i>Bienvenue <?php echo $_SESSION['TX_PRENOM_USR'].' '.$_SESSION['TX_NOM_USR']; ?></i></font>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center"><font class="font-normal">
                    <a href="/?rubriqueid=intranet&pageid=connexion&endofsession=ok&auto=no" class="std">Me déconnecter</a></font>
            </td>
        </tr>
        <!--Modification du mot de passe-->
        <?php if ($bAncienMotdepasseFaux): ?>
            <tr>
                <td colspan="3" align="center">
                    <font class="font-normal font-couleur-erreur"><b>Le changement de mot de passe a été refusé, l'ancien mot de passe saisi étant incorrect !</b></font>
                </td>
            </tr>
        <?php elseif (!$bAncienMotdepasseFaux && isset($_GET['changer_motdepasse'])): ?>
            <tr>
                <td colspan="3" align="center">
                    <font class="font-normal" color="red"><b>Changement de mot de passe effectué avec succès.</b></font>
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td colspan="3" align="center">
                <form name="frmConnexion" method="post">
                    <script language="javascript" src="js/connexion.js"></script>
                    <table border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td align="center" colspan="4">
                                <font class="font-normal"><b>Changement de mot de passe</b></font>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <font class="font-normal">Ancien</font>
                            </td>
                            <td align="center">
                                <font class="font-normal">Nouveau</font>
                            </td>
                            <td align="center">
                                <font class="font-normal">Confirmation</font>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                <input type="password" name="password_ancien" size="10" maxlength="10">
                            </td>
                            <td align="center">
                                <input type="password" name="password_nouveau" size="10" maxlength="10">
                            </td>
                            <td align="center">
                                <input type="password" name="password_confirmation" size="10" maxlength="10">
                            </td>
                            <td align="center">
                                <input type="button" name="btn_changer_motdepasse" value="Valider" class="btn" onclick="javascript:testFormulaireChangerMotdepasse();">
                            </td>
                        </tr>
                    </table>
                </form>
            </td>
        </tr>
    <?php endif; ?>
</table>
<?php if (!isset($_SESSION['PK_UTILISATEUR_USR'])): ?>
    </form>
    <script language="JavaScript">document.forms['connection'].login.focus();</script>
<?php else: ?>
<br>
<div class="alert alert-info" style="text-align: left" role="alert">
    <table class="table table-hover table-sm">
        <thead>
        <tr>
            <th colspan="3">
                API
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                GET
            </td>
            <td>
                <a href="/api/volume">/api/volume</a>
            </td>
            <th>
                Liste des volumes
            </th>
        </tr>
        <tr>
            <td>
                GET
            </td>
            <td>
                <a href="/api/volume?status=chantier">/api/volume?status=chantier</a>
            </td>
            <th>
                Liste de volumes en chantier
            </th>
        </tr>
        <tr>
            <td>
                GET
            </td>
            <td>
                <a href="/api/volume?status=revision">/api/volume?status=revision</a>
            </td>
            <th>
                Liste de volumes en cours de révision
            </th>
        </tr>
        <tr>
            <td>
                GET
            </td>
            <td>
                <a href="/api/volume/{Num. Coll.}">/api/volume/{Num. Coll.}</a>
            </td>
            <th>
                Détails d'un volume
            </th>
        </tr>
        </tbody>
    </table>
</div>
<?php endif; ?>