<?php

if (isset($PHPSESSID)) {
    session_start($PHPSESSID);
} else {
    session_name('sources');
    session_start();
}

function addNotice(string $level, string $message): void {
    try {
        $notice = json_decode($_SESSION['notice'] ?? '{}', true, 512, JSON_THROW_ON_ERROR);
    } catch (\Throwable $ignore) {
        $notice = [];
    }
    $notice[$level][] = $message;
    $_SESSION['notice'] = json_encode($notice, JSON_THROW_ON_ERROR);
}

function hasNotice(): bool {
    return (bool) ($_SESSION['notice'] ?? false);
}

function printNotice(): void {
    try {
        $notice = json_decode($_SESSION['notice'] ?? '{}', true, 512, JSON_THROW_ON_ERROR);
    } catch (\Throwable $ignore) {
        $notice = [];
    }
    foreach ($notice as $level => $messages) {
        foreach ($messages as $message) {
            echo <<<HTML
            <div class="alert alert-{$level}" role="alert">
                {$message}
            </div>
            HTML;
        }
    }
    unset($_SESSION['notice']);
}
