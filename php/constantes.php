<?php

// Fichier contenant les définitions de constantes
if (!function_exists('define_once')) {
    function define_once(string $name, string $value): void
    {
        if (!defined($name)) {
            define($name, $value);
        }
    }
}

// Chemin physique du site
define_once('SITE_PHYSICALPATH', str_replace('/', '//', realpath('./')));

// Divers
define_once('DIVERS_AUTANCIEN_FICHECERF_URL', 'http://www.editionsducerf.fr/html/fiche/ficheauteur.asp?n_aut=');

// PHP 5 ne supporte pas nativement l'UTF-8, il faut donc utiliser l'extension "mbstring"
// qui permet de gérer l'encodage des caractères de manière très complète.
// ini_set ('mbstring.language', 'utf-8');
// ini_set ('mbstring.internal_encoding', 'utf-8');
// ini_set ('mbstring.http_input', 'utf-8');
// ini_set ('mbstring.http_output', 'utf-8');
// ini_set ('mbstring.detect_order', 'auto');
// ini_set ('mbstring.func_overload', '7');

// Base de données
define_once('DATABASE_SERVEUR', $_ENV['DATABASE_SERVEUR']);
define_once('DATABASE_INSTANCE', $_ENV['DATABASE_INSTANCE']);
define_once('DATABASE_UTILISATEUR', $_ENV['DATABASE_UTILISATEUR']);
define_once('DATABASE_MOTDEPASSE', $_ENV['DATABASE_MOTDEPASSE']);

// Site
define_once('SITE_DOSSIER', $_ENV['SITE_DOSSIER']);
define_once('SITE_GZIP_ACTIF', $_ENV['SITE_GZIP_ACTIF']);

// SMTP
$old_smtp = ini_set('SMTP', $_ENV['SMTP']);
$old_send_from = ini_set('sendmail_from', $_ENV['MAIL_FROM']);
