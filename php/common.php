<?php

function redirect(string $uri): void
{
    // Redirect to the URL via HTTP header
    header("Location: {$uri}");
    // Add fallback in case the header has already been sent
    die(<<<HTML
    <p>Redirection en cours…</p>
    <script type="text/javascript">document.location.href = '{$uri}';</script>
    HTML);
}

function creerMiniature($image)
{
    $size = [];
    $split = [];
    $width_old = 0;
    $height_old = 0;
    $mime = '';
    $width_new = 0;
    $height_new = 0;
    // $orientation = "";
    $image_extension = '';
    $image_old = '';
    $image_new = '';

    $split = explode('.', (string) $image);
    $image_sans_extension = $split[0];

    if (!file_exists(SITE_PHYSICALPATH.'/upload/img/'.$image)) {
        return 'err-fichierimage-introuvable';
    }

    $size = getimagesize(SITE_PHYSICALPATH.'/upload/img/'.$image);	// Array
    $width_old = $size[0];
    $height_old = $size[1];
    $mime = $size['mime'];

    if ($width_old > $height_old) {
        // $orientation = "paysage";
        $width_new = 100;
        $height_new = 75;
    } else {
        // $orientation = "portrait";
        $width_new = 75;
        $height_new = 100;
    }

    switch ($mime) {
        case 'image/gif':
            $image_extension = 'gif';
            echo SITE_PHYSICALPATH.'/upload/img/'.$image;
            $image_old = imagecreatefromgif(SITE_PHYSICALPATH.'/upload/img/'.$image);
            break;
        case 'image/jpeg':
            $image_extension = 'jpg';
            $image_old = imagecreatefromjpeg(SITE_PHYSICALPATH.'/upload/img/'.$image);
            break;
        case 'image/png':
            $image_extension = 'png';
            $image_old = imagecreatefrompng(SITE_PHYSICALPATH.'/upload/img/'.$image);
            break;
        default:
            return 'err-extension-non-geree';
            break;
    }

    $image_new = imagecreatetruecolor($width_new, $height_new);

    imagecopyresized($image_new, $image_old, 0, 0, 0, 0, $width_new, $height_new, $width_old, $height_old);

    /*
    switch ($mime)
    {
        case "image/gif":	// !!!!! GD Lib >= 2.0.28 !!!!!
            if (function_exists("imagegif"))
            {
                imagegif($image_new, SITE_PHYSICALPATH.'\\upload\\img\\'.$image_sans_extension.$suffixe.'.'.$image_extension);
            }
            else
            {
                $image_extension = "jpg";
                imagejpeg($image_new, SITE_PHYSICALPATH.'\\upload\\img\\'.$image_sans_extension.$suffixe.'.'.$image_extension);
                return "err-fonction-imagegif()-nondisponible";
            }
            break;
        case "image/jpeg":
            imagejpeg($image_new, SITE_PHYSICALPATH.'\\upload\\img\\'.$image_sans_extension.$suffixe.'.'.$image_extension);
            break;
        case "image/png":
            imagepng($image_new, SITE_PHYSICALPATH.'\\upload\\img\\'.$image_sans_extension.$suffixe.'.'.$image_extension);
            break;
    }
    */

    imagejpeg($image_new, SITE_PHYSICALPATH.'/upload/img/mini-'.$image_sans_extension.'.jpg');

    imagedestroy($image_old);
    imagedestroy($image_new);

    return 'creerMiniature-ok';
}

function _trim($chaine)
{
    // Cette fonction supprime les espaces situés en début et en fin de chaîne.
    // Par ailleurs, cette fonction supprime les séquences d'espaces consécutifs
    // et les remplace par une seule occurrence.
    // Si le paramètre en entrée n'est pas de type 'string' ou bien si la chaîne est vide,
    // on retourne simplement cette valeur.

    // Si le paramètre en entrée n'est pas de type 'string', on retourne simplement cette valeur.
    if ('string' != gettype($chaine)) {
        return $chaine;
    }
    // Si la chaîne est vide, on retourne simplement cette valeur.
    if (0 == strlen($chaine)) {
        return $chaine;
    }
    $retChaine = $chaine;
    $ch = substr($retChaine, 0, 1);
    // On supprime les espaces au début de la chaîne
    while (' ' == $ch) {
        $retChaine = substr($retChaine, 1, strlen($retChaine));
        $ch = substr($retChaine, 0, 1);
    }
    $ch = substr($retChaine, strlen($retChaine) - 1, strlen($retChaine));
    // On supprime les espaces en fin de chaîne
    while (' ' == $ch) {
        $retChaine = substr($retChaine, 0, strlen($retChaine) - 1);
        $ch = substr($retChaine, strlen($retChaine) - 1, strlen($retChaine));
    }
    // On supprime les séquences d'espaces consécutifs en les remplaçant par une seule occurrence
    while (strpos($retChaine, '  ')) {
        $retChaine = substr($retChaine, 0, strpos($retChaine, '  ')).substr($retChaine, strpos($retChaine, '  ') + 1);
    }

    return stripslashes($retChaine);
}

function _dateUStoDateFR($_date)
{
    // yyyy-mm-dd ==> jj/mm/aaaa
    if ('string' != gettype($_date)) {
        return '';
    }

    if (10 != strlen($_date)) {
        return '';
    }

    return substr($_date, 8, 2).'/'.substr($_date, 5, 2).'/'.substr($_date, 0, 4);
}

function _dateFRtoDateUS($_date)
{
    // jj/mm/aaaa ==> aaaa-mm-jj
    if ('string' != gettype($_date)) {
        return '';
    }

    if (10 != strlen($_date)) {
        return '';
    }

    return substr($_date, 6, 4).'-'.substr($_date, 3, 2).'-'.substr($_date, 0, 2);
}

function _testEmail($chaine)
{
    // Cette fonction s'assure que la chaîne passée en paramètre

    if (!strpos((string) $chaine, '@') || !strpos((string) $chaine, '.') || strlen((string) $chaine) < 5) {
        return false;
    } else {
        return true;
    }
}

// id : identifiant de la partie du code accessible qu'à certains profils
function _VerifDroit($id)
{
    // Cette fonction vérifie que l'utilisateur est autorisé à accéder à la ressource identifiée par le paramètre 'id'
    if (isset($_SESSION['DROITS'])) {
        if (strstr((string) $_SESSION['DROITS'], (string) $id)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function _tronquerChaine($_largeur, $_chaine)
{
    if ('string' != gettype($_chaine)) {
        return $_chaine;
    }
    if (0 == strlen($_chaine) || $_largeur < 10) {
        return $_chaine;
    }
    $chaine = '';
    $i = -1;
    while ($i < strlen($_chaine)) {
        ++$i;
        $chaine .= substr($_chaine, $i, 1);
        if ($i > 0 && 0 == $i % $_largeur) {
            $chaine .= '<br>';
        }
    }

    return $chaine;
}

function _raccourcirChaine($_largeur, $_chaine)
{
    if ('string' != gettype($_chaine)) {
        return $_chaine;
    }
    if (0 == strlen($_chaine) || $_largeur >= strlen($_chaine)) {
        return $_chaine;
    }

    return substr($_chaine, 0, $_largeur).'...';
}

function _prenom($_prenom)
{
    $i = -1;
    $prenom = '';
    while ($i < strlen((string) $_prenom)) {
        ++$i;
        $prenom .= substr((string) $_prenom, $i, 1);
        if (' ' == substr((string) $_prenom, $i, 1) || '-' == substr((string) $_prenom, $i, 1)
        || '.' == substr((string) $_prenom, $i, 1)) {
            $prenom .= strtoupper(substr((string) $_prenom, $i + 1, 1));
            ++$i;
        }
    }

    return $prenom;
}

function mois($num)
{
    switch ($num) {
        case '1': return 'janvier';
        case '2': return 'février';
        case '3': return 'mars';
        case '4': return 'avril';
        case '5': return 'mai';
        case '6': return 'juin';
        case '7': return 'juillet';
        case '8': return 'août';
        case '9': return 'septembre';
        case '10':return 'octobre';
        case '11':return 'novembre';
        case '12':return 'décembre';
    }
}

function iif($expression, $returntrue, $returnfalse = '')
{
    return $expression ? $returntrue : $returnfalse;
}

function arrayStripSlashes($array)
{
    foreach ($array as $value) {
        if (is_array($value)) {
            arrayStripSlashes($value);
        } else {
            $value = stripslashes((string) $value);
        }
    }
}

function url_exists($url)
{
    $fp = @fopen($url, 'r');
    // la fonction renvoi 1 si le fichier (l'URL) existe, 0 sinon.
    return ($fp) ? 1 : 0;
}
