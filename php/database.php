<?php

include 'constantes.php';
/**
 * 20/08/2015 : Rajout de getNomTableEnMinuscule() appelé par DbExecRequete
 * Force les noms de table en minuscule, Auteur D. GOUDARD.
 */

// Connexion à l'instance de base de données - Appel de la fonction : $oConnexion = DbConnection();
function DbConnection()
{
    // Connexion au SGBD (MySQL)
    if (!$oLink = @mysqli_connect(DATABASE_SERVEUR, DATABASE_UTILISATEUR, DATABASE_MOTDEPASSE, DATABASE_INSTANCE)) {
        // La connexion au SGBD (MySQL) a échoué...
        trigger_error('Erreur de connexion au SGBD MySQL', 256);
    }

    // La connexion à l'instance 'babelsc' a échoué...
    if (!@mysqli_select_db($oLink, DATABASE_INSTANCE)) {
        trigger_error("Erreur de connexion à l'instance 'babelsc'", 256);
    }

    mysqli_set_charset($oLink, 'utf8');

    // On renvoie l'objet 'mysql_connect'
    return $oLink;
}

// Fermeture de la connexion
function DbClose($oLink)
{
    mysqli_close($oLink);
}

// Exécution d'une requête avec MySQL - Appel de la fonction : $oRecordset = DbExecRequete("SELECT * FROM ...",$oLink);
function DbExecRequete($sRequete, $oLink, bool $throw = false)
{
    try {
        $result = mysqli_query($oLink, $sRequete);
        $message = false === $result ? null : mysqli_error($oLink);
    } catch (\Exception $e) {
        if ($throw) {
            throw $e;
        }
        $result = false;
        $message = $e->getMessage();
    }
    if (false !== $result) {
        return $result;
    }

    trigger_error("Erreur dans l'exécution de la requête @ {$sRequete} @ - Message de MySQL : {$message}.", 512);
    // header("Location: error.php");
    echo '<p align="center"><font class="font-normal">Une erreur est survenue. L\'administrateur du site a été prévenu.</font></p>';
    echo '<p align="center"><font class="font-normal">Veuillez nous excuser pour le désagrément occasionné.</font></p>';
    echo '<p align="center"><font class="font-normal"><a href="/" class="std">Revenir à la page d\'accueil</a></font></p>';
    if ($_ENV['APP_DEBUG'] ?? false) {
        $debugbar = \debug::getInstance()->getDebugbar();
        $debugbarRenderer = $debugbar->getJavascriptRenderer();
        echo $debugbarRenderer->render();
    }
    exit;
}
// *
function getNomTableEnMinuscule($query, $oLink)
{
    $sql = 'SHOW TABLES;';

    if ($recordSet = mysqli_query($oLink, $sql)) {
        while ($table_name = mysqli_fetch_column($recordSet)) {
            $query = str_ireplace((string) $table_name, (string) $table_name, (string) $query);
        }

        return $query;
    } else {
        trigger_error("Erreur dans l'exécution de la requête @ ".$sql.' @ - Message de MySQL : '.mysqli_error($oLink).'.', 512);
    }
}// */
/*
function getNomTableEnMinuscule($sql, $x) {

    $sRequete = trim($sql);

    $beforeFROM = stristr($sRequete, 'FROM ', true);

    if ($beforeFROM===false) {
        return $sRequete;
    }
    else {
        $startFROM = stristr($sRequete, 'FROM ');
        $afterFROM = substr_replace($startFROM, '', 0, 5);

        $beforeWHERE = stristr($afterFROM, 'WHERE ', true);
        if ($beforeWHERE === false) {
            $listeTableDirty = strtolower($afterFROM);
            $requete = getQuery($beforeFROM, ' FROM '.strtolower($afterFROM), '', $listeTableDirty);
        }
        else {
            $startWHERE = stristr($afterFROM, 'WHERE ');
            $listeTableDirty = strtolower($beforeWHERE);
            $requete = getQuery($beforeFROM, ' FROM '.strtolower($beforeWHERE) , ($startWHERE===false ? '' : $startWHERE), $listeTableDirty);
        }
    }
    return $requete;
}
//*/
function getQuery($s, $f, $w, $listOfTableBrut)
{
    $select = trim((string) $s);
    $from = trim((string) $f);
    $where = trim((string) $w);

    $listOfTable = getListeTableClean($listOfTableBrut);

    foreach ($listOfTable as $t) {
        $table = trim((string) $t);
        $select = str_ireplace(strtoupper($table).'.', $table.'.', $select);
        $where = str_ireplace(strtoupper($table).'.', $table.'.', $where);
    }

    return $select.' '.$from.' '.$where;
}

function getListeTableClean($liste)
{
    $dirtyListe = str_replace('  ', ' ', (string) $liste);
    $dirtyListe = str_replace('inner join', ',', $dirtyListe);
    $dirtyListe = str_replace('left outer join', ',', $dirtyListe);
    $dirtyListe = str_replace('right outer join', ',', $dirtyListe);
    $dirtyListe = str_replace('outer join', ',', $dirtyListe);
    $dirtyListe = str_replace('right join', ',', $dirtyListe);
    $dirtyListe = str_replace('left join', ',', $dirtyListe);
    $dirtyListe = str_replace(' ', '', $dirtyListe);

    return explode(',', $dirtyListe);
}

// On récupère l'ID de la dernière opération INSERT exécutée avec la connexion passée en paramètre
function DbRecupIdentity($oLink)
{
    return mysqli_insert_id($oLink);
}

// On se positionne sur l'enregistrement suivant du recordset (retourne un objet)
function DbEnregSuivant($oRecordset)
{
    return mysqli_fetch_object($oRecordset);
}

// On se positionne sur l'enregistrement suivant du recordset (retourne un tableau associatif)
function DbEnregSuivantTab($oRecordset)
{
    return mysqli_fetch_assoc($oRecordset);
}

function DbEnregTab($oRecordset)
{
    $records = [];
    while ($record = DbEnregSuivantTab($oRecordset)) {
        $records[] = $record;
    }

    return $records;
}

function DbEnregColonne($oRecordset)
{
    $records = [];
    while ($record = DbEnregSuivantTab($oRecordset)) {
        $records[] = current($record);
    }

    return $records;
}

// Retourne le nombre d'enregistrements
function DbNbreEnreg($oRecordset)
{
    // retourne 0 si le recordset est vide
    return mysqli_num_rows($oRecordset);
}

// On se positionne sur le premier enregistrement du recordset
function DbEnregPremier($oRecordset)
{
    mysqli_data_seek($oRecordset, 0);
}

function DbEnregDernier($oRecordset)
{
    mysqli_data_seek($oRecordset, DbNbreEnreg($oRecordset) - 1);
}

// Transactionnel - Début d'une transaction
function begin($oLink)
{
    DbExecRequete('BEGIN', $oLink);
}

// Transactionnel - Fin d'une transaction avec mise à jour des données en base
function commit($oLink)
{
    DbExecRequete('COMMIT', $oLink);
}

// Transactionnel - Fin d'une transaction avec annulation des modifications en cours
function rollback($oLink)
{
    DbExecRequete('ROLLBACK', $oLink);
}
