<?php

$phpver = \PHP_VERSION;
$do_gzip_compress = false;

$useragent = $_SERVER['HTTP_USER_AGENT'] ?? $HTTP_USER_AGENT;

if ($phpver >= '4.0.4pl1' && (strstr((string) $useragent, 'compatible') || strstr((string) $useragent, 'Gecko'))) {
    if (extension_loaded('zlib')) {
        ob_start('ob_gzhandler');
    }
} elseif ($phpver > '4.0') {
    if (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
        if (strstr((string) $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
            if (extension_loaded('zlib')) {
                $do_gzip_compress = true;
                ob_start();
                ob_implicit_flush(0);

                header('Content-Encoding: gzip');
            }
        }
    }
}
