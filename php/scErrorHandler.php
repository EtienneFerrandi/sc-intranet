<?php

// configure le niveau de rapport d'erreur pour ce script
error_reporting(\E_USER_ERROR | \E_USER_WARNING | \E_USER_NOTICE);

/*
ini_set('display_errors','On');
//*/

// gestionnaire d'erreurs
function scErrorHandler($errno, $errstr, $errfile, $errline)
{
    // Date et heure de l'erreur
    $dt = date('Y-m-d H:i:s (T)');

    $erreur = "{$dt}  ";

    switch ($errno) {
        case \E_USER_ERROR:	// $errno = 256
            $erreur .= "ERREUR E_USER_ERRORE [{$errno}] {$errstr}  ";
            $erreur .= "Erreur fatale à la ligne {$errline} du fichier {$errfile}";
            $erreur .= sprintf(', PHP %s (%s)%s%s', \PHP_VERSION, \PHP_OS, chr(13), chr(10));
            break;
        case \E_USER_WARNING:	// $errno = 512
            $erreur .= sprintf('ERREUR [%s] %s%s%s', $errno, $errstr, chr(13), chr(10));
            break;
        case \E_USER_NOTICE:	// $errno = 1024
            $erreur .= sprintf('AVERTISSEMENT [%s] %s%s%s', $errno, $errstr, chr(13), chr(10));
            break;
        default:
            $erreur .= sprintf("Type d'erreur inconnu: [%s] %s. Fichier : %s. Ligne : %s%s%s", $errno, $errstr, $errfile, $errline, chr(13), chr(10));
            break;
    }

    // echo $erreur;

    // !!!!! FROM : me@localhost.com pour l'instant ; paramètre php.ini !!!!!
    // Cf. http://fr.php.net/manual/en/ref.mail.php
    //	if ($errno == E_USER_ERROR)
    //	{
    //		mail("pmellerin@gmail.com","Web SC - Erreur de type $errno",$erreur);
    //		mail("laurence.mellerin@mom.fr","Web SC - Erreur de type $errno",$erreur);
    //	}

    // sauvegarde de l'erreur, et envoi d'un mail si c'est une erreur critique
    // !!!!! chemin du fichier de log : le calculer à partir du 'SERVER_PATH' !!!!!
    error_log($erreur, 3, sprintf('%s/logs/babel_%s.log', dirname(__DIR__), date('Y-m-d')));	// le compte local IUSR doit avoir les droits RXW sur ce fichier

    if ($_ENV['APP_DEBUG'] ?? false) {
        $debugbar = \debug::getInstance()->getDebugbar();
        $debugbar['messages']->error($erreur);
    }

    if (\E_USER_ERROR == $errno) {
        echo '<p align="center"><font class="font-normal">Cette page du site de <font class="font-couleur-sc"><i>Sources Chrétiennes</i></font> est temporairement inaccessible. L\'administrateur du site a été prévenu.</font></p>';
        echo '<p align="center"><font class="font-normal">Veuillez nous excuser pour le désagrément occasionné.</font></p>';
        echo '<p align="center"><font class="font-normal"><a href="/" class="std">Revenir à la page d\'accueil</a></font></p>';
        exit(1);	// Fin d'exécution
    }
}

// fonction pour tester la gestion d'erreur
function scale_by_log($vect, $scale)
{
    $temp = [];
    if (!is_numeric($scale) || $scale <= 0) {
        trigger_error("log(x) for x <= 0 is undefined, you used: scale = $scale",
            \E_USER_ERROR);
    }

    if (!is_array($vect)) {
        trigger_error('Incorrect input vector, array of values expected', \E_USER_WARNING);

        return null;
    }

    for ($i = 0, $iMax = count($vect); $i < $iMax; ++$i) {
        if (!is_numeric($vect[$i])) {
            trigger_error("Value at position $i is not a number, using 0 (zero)",
                \E_USER_NOTICE);
        }
        $temp[$i] = log($scale) * $vect[$i];
    }

    return $temp;
}

// spécifie une fonction utilisateur comme gestionnaire d'erreurs
$old_error_handler = set_error_handler('scErrorHandler');

// génération de quelques erreurs. Commençons par créer un tableau
// echo "vector a\n";
// $a = array(2,3, "foo", 5.5, 43.3, 21.11);
// print_r($a);

// générons maintenant un autre tableau, avec des alertes
// echo "----\nvector b - a warning (b = log(PI) * a)\n";
// $b = scale_by_log($a, M_PI);
// print_r($b);

// ceci est un problème, nous avons utilisé une chaîne au lieu d'un tableau
// echo "----\nvector c - an error\n";
// $c = scale_by_log("not array", 2.3);
// var_dump($c);

// ceci est une erreur critique : le logarithme de zéro ou d'un nombre négatif est indéfini
// echo "----\nvector d - fatal error\n";
// $d = scale_by_log($a, -2.5);
