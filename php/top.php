<?php
$pagetitre = '';
$title = 'Sources Chretiennes - HISOMA UMR 5189'; // .date("d/m/Y H:i:s");
$image_droite_url = '';

switch ($pageid) {
    case 'acces':
        $pagetitre = "&nbsp;Acc&egrave;s &agrave; l'Institut&nbsp;";
        $image_droite_src = 'img/top/entree_ISC.jpg';
        $image_droite_alt = 'entr&eacute;e de l\'immeuble des Sources Chr&eacute;tiennes';
        break;
    case 'auteurs_anciens':
        $pagetitre = '&nbsp;Auteurs anciens&nbsp;';
        $image_droite_src = 'img/publications_projets/cartePerespetitformat.gif';
        $image_droite_alt = 'Carte de localisation des Pères';
        break;
    case 'collaborateurs':
        $pagetitre = '&nbsp;Collaborateurs de la collection&nbsp;';
        $image_droite_src = 'img/top/Doutreleau_pupitre_small.jpg';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
    case 'config.inc':
        $pagetitre = '&nbsp;Bernardus&nbsp;';
        $image_droite_src = 'img/top/logoschre.gif';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
    case 'connexion':
        $pagetitre = '&nbsp;Connexion&nbsp;';
        $image_droite_src = 'img/top/babel.png';
        $image_droite_alt = 'Tour de Babel';
        break;
    case 'enseignement_formation':
        $pagetitre = '&nbsp;S&eacute;minaires&nbsp;';
        $image_droite_src = 'img/top/moines_small.jpg';
        $image_droite_alt = 'Bibliothèque municipale de Lyon, Ms 868, folio 29; Cr&eacute;dit photographique Bibliothèque municipale de Lyon, Didier Nicole';
        break;
    case 'gestion':
        $pagetitre = '&nbsp;Gestion&nbsp;';
        $image_droite_src = 'img/top/babel.png';
        $image_droite_alt = 'Tour de Babel';
        break;
    case 'presentation':
        $pagetitre = "&nbsp;L'Institut des Sources Chr&eacute;tiennes&nbsp;";
        $image_droite_src = 'img/top/collection.jpg';
        $image_droite_alt = 'Collection des Sources Chr&eacute;tiennes';
        break;
    case 'recherche':
        $pagetitre = '&nbsp;Recherche&nbsp;';
        $image_droite_src = 'img/top/logoschre.gif';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
    case 'reimpressions':
        $pagetitre = '&nbsp;R&eacute;impressions&nbsp;';
        $image_droite_src = 'img/top/logoschre.gif';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
    case 'reimpression':
        $pagetitre = '&nbsp;R&eacute;impression&nbsp;';
        $image_droite_src = 'img/top/logoschre.gif';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
    case 'volume_preparation':
        $pagetitre = '&nbsp;Volume en pr&eacute;paration&nbsp;';
        $image_droite_src = 'img/top/livres_petit.jpg';
        $image_droite_alt = 'Collection Sources Chr&eacute;tiennes';
        break;
    case 'volumes_preparation':
        $pagetitre = '&nbsp;Volumes à venir&nbsp;';
        $image_droite_src = 'img/top/livres_petit.jpg';
        $image_droite_alt = 'Collection Sources Chr&eacute;tiennes';
        break;

    default:
        $image_droite_src = 'img/top/logoschre.gif';
        $image_droite_alt = 'Logo de Sources Chr&eacute;tiennes';
        break;
}

$ahref = '';
if ('accueilsc' == $pageid) {
    $image_gauche_src = 'img/top/oiseau.gif';
    $image_gauche_alt = 'Maison de l\'Orient et de la M&eacute;diterran&eacute;e (MOM)';
    $ahref = 'http://www.mom.fr';
} else {
    $image_gauche_src = 'img/top/logoschre.gif';
    $image_gauche_alt = 'Logo de Sources Chr&eacute;tiennes';
}
?>
<table border="0" cellpadding="4" cellspacing="4" width="100%">
  <tr>
  	<td align="left" width="205" valign="middle" nowrap>
        <?php if ('' != $ahref) {
            echo '<a href="http://www.mom.fr">';
        } ?>
        <img border="0" src="<?php echo $image_gauche_src; ?>" alt="<?php echo $image_gauche_alt; ?>"></a>
    </td>
    <td align="center" valign="middle" width="100%">
    	<table border="1" cellpadding="4" cellspacing="0" bordercolor="black">
    		<tr>
    			<td align="center" style="border : solid 1px #95004A;border-right : solid 2px #95004A;border-bottom : solid 2px #95004A">
    				<font class="font-titre1"><?php echo $pagetitre; ?></font>
    			</td>
    		</tr>
    	</table>
    </td>
    <td align="right" width="205" valign="middle" nowrap>
<?php
if (isset($image_droite_url) && !empty($image_droite_url)) {
    echo '<a href="'.$image_droite_url.'">';
}
if (isset($image_droite_src) && !empty($image_droite_src)) {
    ?>

    	<img border="0" src="<?php echo $image_droite_src; ?>" width="100" height="100"
             alt="<?php if (isset($image_droite_alt)) {
                 echo $image_droite_alt;
             } ?>">
<?php
}
if (isset($image_droite_url) && !empty($image_droite_url)) {
    echo '</a>';
}
?>
    </td>
  </tr>
</table>
