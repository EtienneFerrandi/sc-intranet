<?php

$finder =
    (new PhpCsFixer\Finder())
        ->exclude('vendor')
        ->in(__DIR__.'/cartes')
        ->in(__DIR__.'/init')
        ->in(__DIR__.'/js/tiny_mce_v349')
        ->in(__DIR__.'/php')
        ->append([
            __DIR__.'/cassien.php',
            __DIR__.'/index.php',
        ])
    ;

return (new PhpCsFixer\Config())
    ->setRules([
        '@PHP71Migration' => true,
        '@PHPUnit75Migration:risky' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'protected_to_private' => false,
        'native_constant_invocation' => ['strict' => false],
        'nullable_type_declaration_for_default_null_value' => ['use_nullable_type_declaration' => false],
        'no_superfluous_phpdoc_tags' => ['remove_inheritdoc' => true],
        'modernize_strpos' => true,
        'get_class_to_class_keyword' => true,
    ])
    ->setRiskyAllowed(true)
    ->setFinder($finder)
    ->setCacheFile('.php-cs-fixer.cache');