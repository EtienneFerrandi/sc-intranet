//<div id="topdecklink" class="posaide"></div>
//<script language="javascript" src="js/aide.js"></script>
//<a href="void(0)" class="aide"><span onMouseOver="javascript:showAide('<b>Titre</b><br />...<br />...')" onMouseOut="javascript:hideAide()" class="aidemot">...</span></a>

domok = document.getElementById;

if (domok) {
	skn = document.getElementById("topdecklink").style;

	if(navigator.appName.substring(0,3) == "Net")
		document.captureEvents(Event.MOUSEMOVE);

	document.onmousemove = get_mouse;
}

function get_mouse(e) {
	var x = (navigator.appName.substring(0,3) == "Net") ? e.pageX : event.x+document.body.scrollLeft;
	var y = (navigator.appName.substring(0,3) == "Net") ? e.pageY : event.y+document.body.scrollTop;
	skn.top = y+20;
	/* pour éviter que la boite dépasse à droite et à gauche */
	if (x >= 670)
		skn.left = 670-100;
	else {
		if (x <= 250)
			skn.left = 250-100;
		else
			skn.left = x-100;
	}
}

function showAide(msg) {
	var content ="<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 BGCOLOR=#000000 width=200><TR><TD><TABLE WIDTH=100% BORDER=0 CELLPADDING=3 CELLSPACING=1><TR><TD BGCOLOR=#FFFFCC><span class=aide><div align='center' style='color: navy; text-decoration: underline'>Aide contextuelle</div><br />"+msg+"</span></TD></TR></TABLE></TD></TR></TABLE>";

	try
	{
		if (domok) {
		  	document.getElementById("topdecklink").innerHTML = content;
		  	skn.visibility = "visible";
		}
	} catch(ex)
	{
		alert(ex.message);
	}
}

function hideAide() {
	if (domok)
  		skn.visibility = "hidden";
}