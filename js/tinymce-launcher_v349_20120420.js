tinyMCE_GZ.init({
	plugins : 'style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,searchreplace,print,paste,directionality,'+
        'fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager',
	themes : 'advanced',
	languages : 'fr',
	disk_cache : true,
	debug : false
});
tinyMCE.init({
    mode : "specific_textareas",
    theme : "advanced",
    language : 'fr',
    entity_encoding : "raw",
    content_css : "sc.css",
    plugins : "style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",
    theme_advanced_buttons1 : "newdocument,template,|,undo,redo,|,ltr,rtl,formatselect,fontsizeselect,|,bold,italic,underline,strikethrough,smallcaps,|,forecolor,backcolor",
    theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote,|,visualchars,nonbreaking,sub,sup,charmap,emotions,hr,|,link,unlink,anchor,image",
    theme_advanced_buttons3 : "tablecontrols,|,pastetext,pasteword,cleanup,code,|,preview,print,|,fullscreen",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_fonts : "Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;Meltho=Meltho;Palatino=Palatino Linotype;Times=Times",
    theme_advanced_resizing : true,
    editor_selector : "mceEditor",
    
    // Setup a new small-caps format for TinyMCE: <span class="small-caps"></span>
    formats: { 
        smallcaps: {inline : 'span', 'classes': 'small-caps'}
    },
   
    // Give action to the smallcaps button
    setup: function( ed ) {
        ed.addButton('smallcaps', {
            title: 'Small-caps',
            image: 'img/smallcaps.png',
            onclick: function(){
                ed.focus();
                ed.formatter.toggle( 'smallcaps' );
            }
        });
        // Highlight the smallcaps button when the cursor is on small-caps text
        ed.onNodeChange.add(function(ed, cm, e) {
            // Activates the link button when the caret is placed in a anchor element
            cm.setActive('smallcaps', ed.formatter.match('smallcaps'));
        });
    },
    
    //entities : "",  // Si l'on active pas cette option, les accents sont transformés en entités HTML (ex: é => &eacute;)
    //editor_deselector : "mceNoEditor",
    template_templates : [
        {
            title : "Template 1",
            src : "templates/template1.htm",
            description : "Image à gauche + texte"
        },
        {
            title : "Template 2",
            src : "templates/template2.htm",
            description : "Image à droite + texte"
        }
    ]
});