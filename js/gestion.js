
function submitFormTmp(sectionid, champ, ancre)
{

	document.forms['frmGestion'].action = '?rubriqueid=intranet&pageid=gestion&sectionid=' + sectionid + '&detail=ok&champ=' + champ + '#' + ancre;
	document.forms['frmGestion'].submit();
}

function testFormulaireOeuvre()
{
	var oForm = document.forms['frmGestion'];

	oForm.reportValidity();
	if (!oForm.checkValidity()) {
		return;
	}
	
	// On désactive le bouton de validation afin d'éviter que les mêmes données soient envoyées plusieurs fois 
	// vers le serveur...
	oForm.btn_valider.disabled = true;


	if(oForm.ovr_fk_aan[oForm.ovr_fk_aan.selectedIndex].value == '')
	{
		alert('Vous devez renseigner l\'auteur ancien associé à cette \u0153uvre !');
		oForm.btn_valider.disabled = false;
		oForm.ovr_fk_aan.value = _trim(oForm.ovr_fk_aan.value);
		oForm.ovr_fk_aan.focus();
		return false;
	}

	if(_trim(oForm.ovr_francais.value).length == 0 || _trim(oForm.ovr_francais.value).length > 255)
	{
		alert('Vous devez renseigner le titre français de l\'\u0153uvre ! (1 à 255 caractères)');
		oForm.btn_valider.disabled = false;
		oForm.ovr_francais.value = _trim(oForm.ovr_francais.value);
		oForm.ovr_francais.focus();
		return false;
	}

	if (!oForm.ovr_interne[0].checked && !oForm.ovr_interne[1].checked)
	{
		alert('Vous devez préciser s\'il s\'agit d\'une \u0153uvre interne à SC ou non !');
		oForm.btn_valider.disabled = false;
		oForm.ovr_interne.value = _trim(oForm.ovr_interne.value);
		oForm.ovr_interne.focus();
		return false;
	}

	oForm.action = '?rubriqueid=intranet&pageid=gestion&sectionid=oeuvres&detail=ok'
	oForm.submit();

}
function testFormulaireUtilisateur()
{
	var oForm = document.forms['frmGestion'];

	if(_trim(oForm.deces_date.value).length !=0 && !oForm.decede.checked){
		alert('Vous avez indiqué une date de décès sans cocher la case décédé\(e\)');
		oForm.btn_valider.disabled = false;
		return false;
	}

	// Nom
	if(_trim(oForm.nom.value).length == 0 || _trim(oForm.nom.value).length > 50)
	{
		alert('Vous devez renseigner le nom de l\'utilisateur ! (1 à 50 caractères)');
		oForm.btn_valider.disabled = false;
		oForm.nom.value = _trim(oForm.nom.value);
		oForm.nom.focus();
		return false;
	}

	if (!oForm.decede.checked) {
		// Identifiant
		if (_trim(oForm.identifiant.value).length < 4 || _trim(oForm.identifiant.value).length > 50) {
			alert('Vous devez renseigner l\'identifiant de l\'utilisateur ! (4 à 50 caractères)');
			oForm.btn_valider.disabled = false;
			oForm.identifiant.value = _trim(oForm.identifiant.value);
			oForm.identifiant.focus();
			return false;
		}

		// Mot de passe
		if (_trim(oForm.motdepasse.value).length < 5 || _trim(oForm.motdepasse.value).length > 10) {
			alert('Vous devez renseigner le mot de passe de l\'utilisateur ! (5 à 10 caractères)');
			oForm.btn_valider.disabled = false;
			oForm.motdepasse.value = _trim(oForm.motdepasse.value);
			oForm.motdepasse.focus();
			return false;
		}
	}


	if (!oForm.decede.checked)
	{
		// Adresse (perso)
		if(_trim(oForm.perso_adresse.value).length > 255)
		{
			alert('L\'adresse des coordonnées personnelles de l\'utilisateur doit contenir 255 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.perso_adresse.value = _trim(oForm.perso_adresse.value);
			oForm.perso_adresse.focus();
			return false;
		}
		// Adresse - Tél. fixe (perso)
		if(_trim(oForm.perso_tel_fixe.value).length > 25)
		{
			alert('Le téléphone fixe des coordonnées personnelles de l\'utilisateur doit contenir 25 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.perso_tel_fixe.value = _trim(oForm.perso_tel_fixe.value);
			oForm.perso_tel_fixe.focus();
			return false;
		}
		// Adresse - Tél mobile (perso)
		if(_trim(oForm.perso_tel_mobile.value).length > 25)
		{
			alert('Le téléphone mobile des coordonnées personnelles de l\'utilisateur doit contenir 25 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.perso_tel_mobile.value = _trim(oForm.perso_tel_mobile.value);
			oForm.perso_tel_mobile.focus();
			return false;
		}
		// Adresse - E-mail (perso)
		if(_trim(oForm.perso_email.value).length > 75)
		{
			alert('L\'adresse e-mail des coordonnées personnelles de l\'utilisateur doit contenir 75 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.perso_email.value = _trim(oForm.perso_email.value);
			oForm.perso_email.focus();
			return false;
		}
		if(_trim(oForm.perso_email.value).length > 0)
		{
			if (!_isEmail(_trim(oForm.perso_email.value)))
			{
				alert('L\'adresse e-mail des coordonnées personnelles de l\'utilisateur a un format incorrect !');
				oForm.btn_valider.disabled = false;
				oForm.perso_email.value = _trim(oForm.perso_email.value);
				oForm.perso_email.focus();
				return false;
			}
		}
		
		// Adresse (pro)
		if(_trim(oForm.pro_adresse.value).length > 255)
		{
			alert('L\'adresse des coordonnées professionnelles de l\'utilisateur doit contenir 255 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.pro_adresse.value = _trim(oForm.pro_adresse.value);
			oForm.pro_adresse.focus();
			return false;
		}
		// Adresse - Tél. fixe (pro)
		if(_trim(oForm.pro_tel_fixe.value).length > 25)
		{
			alert('Le téléphone fixe des coordonnées professionnelles de l\'utilisateur doit contenir 25 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.pro_tel_fixe.value = _trim(oForm.pro_tel_fixe.value);
			oForm.pro_tel_fixe.focus();
			return false;
		}
		// Adresse - Tél. mobile (pro)
		if(_trim(oForm.pro_tel_mobile.value).length > 25)
		{
			alert('Le téléphone mobile des coordonnées professionnelles de l\'utilisateur doit contenir 25 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.pro_tel_mobile.value = _trim(oForm.pro_tel_mobile.value);
			oForm.pro_tel_mobile.focus();
			return false;
		}
		// Adresse - E-mail (pro)
		if(_trim(oForm.pro_email.value).length > 75)
		{
			alert('L\'adresse e-mail des coordonnées professionnelles de l\'utilisateur doit contenir 75 caractères maximum !');
			oForm.btn_valider.disabled = false;
			oForm.pro_email.value = _trim(oForm.pro_email.value);
			oForm.pro_email.focus();
			return false;
		}
		if(_trim(oForm.pro_email.value).length > 0)
		{
			if (!_isEmail(_trim(oForm.pro_email.value)))
			{
				alert('L\'adresse e-mail des coordonnées professionnelles de l\'utilisateur a un format incorrect !');
				oForm.btn_valider.disabled = false;
				oForm.pro_email.value = _trim(oForm.pro_email.value);
				oForm.pro_email.focus();
				return false;
			}
		}
	}
	
	oForm.action = '?rubriqueid=intranet&pageid=gestion&sectionid=utilisateurs&detail=ok';
	oForm.submit();
}
function testFormulaireAutancien()
{
	var oForm = document.forms['frmGestion'];
	
	// On désactive le bouton de validation afin d'éviter que les mêmes données soient envoyées plusieurs fois 
	// vers le serveur...
	oForm.btn_valider.disabled = true;
	
	// Nom français
	if(_trim(oForm.nom_francais.value).length == 0 || _trim(oForm.nom_francais.value).length > 100)
	{
		alert('Vous devez renseigner le nom français de l\'auteur ancien ! (1 à 100 caractères)');
		oForm.btn_valider.disabled = false;
		oForm.nom_francais.value = _trim(oForm.nom_francais.value);
		oForm.nom_francais.focus();
		return false;
	}

	
	oForm.action = '?rubriqueid=intranet&pageid=gestion&sectionid=autanciens&detail=ok';
	oForm.submit();
}
function testFormulaireVolume()
{
	var oForm = document.forms['frmGestion'];

	oForm.reportValidity();
	if (!oForm.checkValidity()) {
		return;
	}
	
	// On désactive le bouton de validation afin d'éviter que les mêmes données soient envoyées plusieurs fois 
	// vers le serveur...
	oForm.btn_valider.disabled = true;
	
	// Numéro de collection
	if(_trim(oForm.vol_numcollection.value).length == 0 
	|| _trim(oForm.vol_numcollection.value).length > 4 
	|| !_isInteger(_trim(oForm.vol_numcollection.value)))
	{
		alert('Vous devez renseigner le numéro de collection ! (1 à 4 chiffre(s))');
		oForm.btn_valider.disabled = false;
		oForm.vol_numcollection.value = _trim(oForm.vol_numcollection.value);
		oForm.vol_numcollection.focus();
		return false;
	}

	if (_trim(oForm.vol_numcollection_ancien.value).length > 0)
	{
		if(_trim(oForm.vol_numcollection_ancien.value).length > 4
			|| !_isInteger(_trim(oForm.vol_numcollection_ancien.value)))
		{
			alert('L\'ancien numéro de collection est un nombre qui doit contenir 4 chiffre(s) maximum !');
			oForm.btn_valider.disabled = false;
			oForm.vol_numcollection_ancien.value = _trim(oForm.vol_numcollection_ancien.value);
			oForm.vol_numcollection_ancien.focus();
			return false;
		}
	}

	if (_trim(oForm.vol_numcollection_nouveau.value).length > 0)
	{
		if(_trim(oForm.vol_numcollection_nouveau.value).length > 4
			|| !_isInteger(_trim(oForm.vol_numcollection_nouveau.value)))
		{
			alert('Le nouveau numéro de collection est un nombre qui doit contenir 4 chiffre(s) maximum !');
			oForm.btn_valider.disabled = false;
			oForm.vol_numcollection_nouveau.value = _trim(oForm.vol_numcollection_nouveau.value);
			oForm.vol_numcollection_nouveau.focus();
			return false;
		}
	}

	if (_trim(oForm.vol_sscollection_num.value).length > 0)
	{
		if(_trim(oForm.vol_sscollection_num.value).length > 4
			|| !_isInteger(_trim(oForm.vol_sscollection_num.value)))
		{
			alert('Le numéro de sous-collection est un nombre qui doit contenir 4 chiffre(s) maximum !');
			oForm.btn_valider.disabled = false;
			oForm.vol_sscollection_num.value = _trim(oForm.vol_sscollection_num.value);
			oForm.vol_sscollection_num.focus();
			return false;
		}
	}

	if (_trim(oForm.vol_reimp_nb.value).length > 0)
	{
		if(!_isInteger(_trim(oForm.vol_reimp_nb.value)))
		{
			alert('Le nombre de réimpression(s) est un nombre qui doit contenir 2 chiffre(s) maximum !');
			oForm.btn_valider.disabled = false;
			oForm.vol_reimp_nb.value = _trim(oForm.vol_reimp_nb.value);
			oForm.vol_reimp_nb.focus();
			return false;
		}
	}
	
	if (_trim(oForm.vol_devis.value).length > 0)
	{
		if(!_isInteger(_trim(oForm.vol_devis.value)))
		{
			alert('Le montant du devis est un nombre entier !');
			oForm.btn_valider.disabled = false;
			oForm.vol_devis.value = _trim(oForm.vol_devis.value);
			oForm.vol_devis.focus();
			return false;
		}
	}

	if (_trim(oForm.vol_prestations.value).length > 0)
	{
		if(!_isInteger(_trim(oForm.vol_prestations.value)))
		{
			alert('Le montant des prestations est un nombre entier !');
			oForm.btn_valider.disabled = false;
			oForm.vol_prestations.value = _trim(oForm.vol_prestations.value);
			oForm.vol_prestations.focus();
			return false;
		}
	}

	oForm.action = '?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok';
	oForm.submit();
}
