
function testFormulaireChangerMotdepasse() {
	var oForm = document.forms['frmConnexion'];
	oForm.btn_changer_motdepasse.disabled = true;
	
	if (_trim(oForm.password_ancien.value).length < 5 || _trim(oForm.password_ancien.value).length > 10) {
		alert('Vous devez renseigner l\'ancien mot de passe ! (5 à 10 caractères)');
		oForm.btn_changer_motdepasse.disabled = false;
		oForm.password_ancien.value = _trim(oForm.password_ancien.value);
		oForm.password_ancien.focus();
		return false;
	}
	
	if (_trim(oForm.password_nouveau.value).length < 5 || _trim(oForm.password_nouveau.value).length > 10) {
		alert('Le nouveau mot de passe doit contenir entre 5 et 10 caractères !');
		oForm.btn_changer_motdepasse.disabled = false;
		oForm.password_nouveau.value = _trim(oForm.password_nouveau.value);
		oForm.password_nouveau.focus();
		return false;
	}
	
	if (_trim(oForm.password_nouveau.value) != _trim(oForm.password_confirmation.value)) {
		alert('Les champs "nouveau" et "confirmation" doivent être identiques !');
		oForm.btn_changer_motdepasse.disabled = false;
		oForm.password_nouveau.value = _trim(oForm.password_nouveau.value);
		oForm.password_confirmation.value = _trim(oForm.password_confirmation.value);
		oForm.password_confirmation.focus();
		return false;
	}
	
	oForm.action = 'index.php?rubriqueid=intranet&pageid=connexion&changer_motdepasse=ok';
	oForm.submit();
}
//-->