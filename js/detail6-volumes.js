function chargerReimpression()
{
	var oForm = document.forms['frmGestion'];
	
	/*if (oForm.ddl_reimpression[oForm.ddl_reimpression.selectedIndex].value == '')
	{
		alert('Vous devez choisir une réimpression de volume valide !');
		oForm.ddl_reimpression.focus();
		return false;
	}
	else
	{*/
		var sAction = '?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok6&action=chargerreimpression';
		oForm.action = sAction;
		oForm.submit();
	/*}*/
}

function majReimpression()
{
	var oForm = document.forms['frmGestion'];
	
	oForm.btn_valider.disabled = true;
	
	if (oForm.vrp_prix.value.length > 0)
	{
		if (!_isFloat(oForm.vrp_prix.value.replace(',', '.')))
		{
			alert('Le prix que vous avez renseigné est incorrect !');
			oForm.btn_valider.disabled = false;
			oForm.vrp_prix.value = _trim(oForm.vrp_prix.value.replace(',', '.'));
			oForm.vrp_prix.focus();
			return false;
		}
	}

	if (oForm.vrp_nb_pages.value.length > 0)
	{
		if (!_isInteger(oForm.vrp_nb_pages.value))
		{
			alert('Le nombre de pages que vous avez renseigné est incorrect !');
			oForm.btn_valider.disabled = false;
			oForm.vrp_nb_pages.value = _trim(oForm.vrp_nb_pages.value.replace(',', '.'));
			oForm.vrp_nb_pages.focus();
			return false;
		}
	}
	
	var sAction = '?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok6';
	oForm.action = sAction;
	oForm.submit();
}

function nouvelleReimpression()
{
	var oForm = document.forms['frmGestion'];
	
	oForm.btn_valider.disabled = true;
	
	if (oForm.vrp_prix.value.length > 0)
	{
		if (!_isFloat(oForm.vrp_prix.value.replace(',', '.')))
		{
			alert('Le prix que vous avez renseigné est incorrect !');
			oForm.btn_valider.disabled = false;
			oForm.vrp_prix.value = _trim(oForm.vrp_prix.value.replace(',', '.'));
			oForm.vrp_prix.focus();
			return false;
		}
	}

	if (oForm.vrp_nb_pages.value.length > 0)
	{
		if (!_isInteger(oForm.vrp_nb_pages.value))
		{
			alert('Le nombre de pages que vous avez renseigné est incorrect !');
			oForm.btn_valider.disabled = false;
			oForm.vrp_nb_pages.value = _trim(oForm.vrp_nb_pages.value.replace(',', '.'));
			oForm.vrp_nb_pages.focus();
			return false;
		}
	}
	
	var sAction = '?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok6&action=nouvellereimpression&param1=sectionid&vparam1=reimpressions';
	oForm.action = sAction;
	oForm.submit();
}

function supprimerReimpression()
{
	var oForm = document.forms['frmGestion'];
	
	var sAction = '?rubriqueid=intranet&pageid=gestion&sectionid=volumes&detail=ok6&action=supprimerreimpression';
	oForm.action = sAction;
	oForm.submit();
}