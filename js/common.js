// Permet de récupérer l’objet associé à l’élément de page HTML passé en paramètre
function CM_getElement(_sId) {
	if(document.all) { // IE 4
		return document.all[_sId];
	} else if (document.getElementById) {
		// Navigateurs compatibles DOM (IE5+, NS6+/Mozilla, Opera5+)
		// Attention : l'élément appelé doit avoir sa propriété 'id' renseignée !!!
		return document.getElementById(_sId);
	} else { // Navigateurs anciens : Netscape 4 etc...
		for (iLayer = 1; iLayer < document.layers.length; iLayer++) {
			if(document.layers[iLayer].id == _sId) return document.layers[iLayer];
		}
	}
}

// Permet de récupérer les valeurs sélectionnées dans un objet Select à sélection multiple
function getSelectedValues(_oSelect) {
	var r = new Array();
	for (var i = 0; i < _oSelect.options.length; i++)
	{
		if (_oSelect.options[i].selected)
		{
			r[r.length] = _oSelect.options[i].value;	// valeur associée à l'option
			//r[r.length] = select.options[i].text;	// texte de l'option
			//r[r.length] = i;	// indice de l'option
		}
	}
	return r;
}

function changerCouleurFond(typeMenu, typeMouvementSouris, celluleSelectionnee) {
	if (typeMenu == 'menu') {
		switch (typeMouvementSouris) {
			case "onMouseOver":
				celluleSelectionnee.style.background='#019549';
				break;
			case "onMouseOut":
				celluleSelectionnee.style.background='#95004A';
				break;
		}
	}
	else if (typeMenu == 'sousmenu') {
		switch (typeMouvementSouris) {
			case "onMouseOver":
				celluleSelectionnee.style.background='#C90063';
				break;
			case "onMouseOut":
				celluleSelectionnee.style.background='#95004A';
				break;
		}
	}
	else if (typeMenu == 'ssmenu') {
		switch (typeMouvementSouris) {
			case "onMouseOver":
				celluleSelectionnee.style.background='#AC9CFD';
				break;
			case "onMouseOut":
				celluleSelectionnee.style.background='#95004A';
				break;
		}
	}
}

function popup(page,hauteur,largeur)
{
	var popuptop = ((screen.height / 2) - (hauteur / 2)) / 2;
	var popupleft = (screen.width / 2) - (largeur / 2);
	window.open(page,'','height='+hauteur+',width='+largeur+',top='+popuptop+',left='+popupleft+',toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes,status=no');
}

function _trim(chaine) {
	
    // Cette fonction supprime les espaces situés en début et en fin de chaîne.
   // Par ailleurs, cette fonction supprime les séquences d'espaces consécutifs
   // et les remplace par une seule occurrence.
   // Si le paramètre en entrée n'est pas de type 'string' ou bien si la chaîne est vide,
   // on retourne simplement cette valeur.
   // Si le paramètre en entrée n'est pas de type 'string', on retourne simplement cette valeur.
   
   //if (typeOf(chaine) != 'string') { return chaine; }
   // Si la chaîne est vide, on retourne simplement cette valeur.
   if (chaine.length == 0) { return chaine; }
   var retChaine = chaine;
   var ch = retChaine.substring(0, 1);
   // On supprime les espaces au début de la chaîne
   while (ch == " ") {
      retChaine = retChaine.substring(1, retChaine.length);
      ch = retChaine.substring(0, 1);
   }
   ch = retChaine.substring(retChaine.length-1, retChaine.length);
   // On supprime les espaces en fin de chaîne
   while (ch == " ") {
      //retValue = retChaine.substring(0, retChaine.length-1);
	  retChaine = retChaine.substring(0, retChaine.length-1);
      ch = retChaine.substring(retChaine.length-1, retChaine.length);
   }
   // On supprime les séquences d'espaces consécutifs en les remplaçant par une seule occurrence
   while (retChaine.indexOf("  ") != -1) {
      retChaine = retChaine.substring(0, retChaine.indexOf("  ")) + retChaine.substring(retChaine.indexOf("  ")+1, retChaine.length);
   }
   return retChaine;
}

// Cette fonction permet de mettre le focus sur un champ
function _setFocus(elementName)
{
	document.getElementById(elementName).focus();
}

function _isEmail(chaine) {
	
	// Cette fonction s'assure que la chaîne passée en paramètre a les pré requis d'une adresse e-mail
	
	if ((chaine.indexOf('@',0) == -1) || (chaine.indexOf('.',0) ==- 1) || chaine.length < 5)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function _isDate(ladate)	// La date doit être au format 'JJ/MM/AAAA'
{
	// Cette fonction teste si la chaîne passée en paramètre est de type 'date'
	
	if (ladate.length != 10)
		return false;
	
	var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
	var matchArray = ladate.match(datePat); // Le format est-il correct ?
	
	if (matchArray == null) {
		alert("Veuillez saisir une date avec le format jj/mm/aaaa ou jj-mm-aaaa !");
		return false;
	}
	
	day = matchArray[1];
	month = matchArray[3];
	year = matchArray[5];
	
	if (month < 1 || month > 12) {
		// alert("Le mois doit être compris entre 1 et 12 !");
		return false;
	}
	
	if (day < 1 || day > 31) {
		// alert("Le jour doit être compris entre 1 et 31 !");
		return false;
	}
	
	if ((month==4 || month==6 || month==9 || month==11) && day==31) {
		alert("Le mois "+month+" ne comporte pas 31 jours !")
		return false;
	}
	
	if (month == 2) { // Cas particulier des années bissextiles
		var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
		if (day > 29 || (day==29 && !isleap)) {
			alert("Le mois de février " + year + " ne comporte pas " + day + " jours !");
			return false;
		}
	}
	return true; // La date est valide
}

function _isInteger(_param) {
	var regInteger = /^\d+$/;
	return regInteger.test(_param);
	
	//get the modulus: if it's 0, then it's an integer
        //var myMod = myNum % 1;
        //if (myMod == 0) {
	//	return true;
        //} else {
	//	return false;
        //}
}

function _isNumeric(_param)
{
	var ValidChars = "0123456789.,";
	var IsNumber = true;
	var Char;
	for (i = 0; i < _param.length && IsNumber == true; i++)
	{
		Char = _param.charAt(i);
		if (ValidChars.indexOf(Char) == -1)
		{
			IsNumber = false;
			i = _param.length;
		}
	}
	return IsNumber;
}

function _isFloat(_param)
{
	var floatValue = parseFloat(_param);
	if (isNaN(floatValue)) {
		return false;
	} else {
		return true;
	}
}

function _copierDansPressePapier(_param) {
	if (window.clipboardData) {
		if (typeof window.clipboardData.setData != 'undefined') {
			var sText = _param;
			while (sText.search("@") != -1)
			{
				sText = sText.replace("@", String.fromCharCode(9))
			}
			while (sText.search("#") != -1)
			{
				sText = sText.replace("#", String.fromCharCode(13) + String.fromCharCode(10))
			}
			alert(sText);
			window.clipboardData.setData("Text", sText);
			alert("La liste des utilisateurs affichée dans cette page a été copiée dans votre presse-papiers.");
		}
		else { alert("Désolé mais votre navigateur ne gère pas cette fonctionnalité !");
		}
	}
	else { alert("Désolé mais votre navigateur ne gère pas cette fonctionnalité !");
	}
}

//ROLLOVER AVEC LA SOURIS DANS LA PAGE D'UN AUTEUR ANCIEN




/*******************
* infobulles
* Ben, 23/07/2005
*******************/

// espacement entre le curseur et l'infobulle
cursor_padding = 10;

// gestion des navigateurs (IE, MOZ, NS)
nav = navigator.appName;

ie = document.all;
ns = document.layers;
fi = document.getElementById && !document.all;

/*if(!ie && !ns && !fi){
	//alert("navigateur "+nav+" incompatible !");	
}

if(!ie){
	document.captureEvents(Event.MOUSEMOVE); 	
}

document.onmousemove = get_mouse;

// recupere les coordonnees de la souris
// les affecte au style de la div infobulle
function get_mouse(e){
	if(ie){
		x = event.x;
		y = event.y;
		window.status = y;
	}
	else{
		x = e.pageX;
		y = e.pageY;
	}	
	
	//bubble = document.getElementById("infobulle");
	document.getElementById("infobulle").style.left = x + cursor_padding;
	document.getElementById("infobulle").style.top = y + cursor_padding;
	
}*/


// affiche la bubble
function see_bubble(text,idbulle){
	document.getElementById(idbulle).style.visibility = "visible";
    document.getElementById(idbulle).style.display = "block";
	
	document.getElementById(idbulle).innerHTML = text; 
	// déconseillé pas aux normes...tant pis
	
	//longueur_bubble = bubble.firstChild.length;
	//bubble.firstChild.replaceData(0, longueur_bubble, text); 
}

// cache la bubble
function kill_bubble(idbulle){
see_bubble('',idbulle);
	document.getElementById(idbulle).style.visibility = "hidden";
    document.getElementById(idbulle).style.display = "none";
}

// FIN ROLLOVER
//-->

document.addEventListener('DOMContentLoaded', () => {
    const currentUrl = document.querySelector('[data-current-url]')?.dataset?.currentUrl;
	if (!currentUrl) {
		return;
	}
	if (currentUrl && document.location.hash === '#back') {
		const state = JSON.parse(localStorage.getItem('scrollPosition') || '{}');
		const url = currentUrl.split('#').shift();
		const scrollPosition = state[url] || null;
		if (scrollPosition !== null) {
			window.scrollTo({
				behavior: 'smooth',
				top: scrollPosition,
			});
		}
	}
});

window.addEventListener('beforeunload', () => {
	const currentUrl = document.querySelector('[data-current-url]')?.dataset?.currentUrl;
	if (!currentUrl) {
		return;
	}
	const state = JSON.parse(localStorage.getItem('scrollPosition') || '{}');
	const url = currentUrl.split('#').shift();
	state[url] = window.scrollY;
	localStorage.setItem('scrollPosition', JSON.stringify(state));
});