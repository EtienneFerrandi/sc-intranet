function listenToProfileChange(){
	var radios = Array.from(document.forms['frmGestion'].profil);
	radios.forEach(function (radio) {
		var label = Array.from(radio.labels).pop();
		radio.addEventListener('change', function() {
			var foreigner = label.innerText.trim() === 'extérieur';
			console.log(foreigner);
			['identifiant', 'motdepasse'].forEach(function (id) {
				document.getElementById(id).style.display = foreigner ? 'none' : null;
			});
		});
	});
}

function creerIdentifiant()
{
	var oForm = document.forms['frmGestion'];
	var nom = (oForm.nom.value).toLowerCase();
	var prenom = (oForm.prenom.value).toLowerCase();
	var longueurnom = nom.length;
	var longueurprenom = prenom.length;
	var login = "";

	prenom = prenom.replace(/ /g,'');
	prenom = prenom.replace(/'/g,'');
	nom = nom.replace(/ /g,'');
	nom = nom.replace(/'/g,'');
	login = prenom+'.'+nom;
	oForm.identifiant.value = login;
}
function champsDecede()
{
	var oForm = document.forms['frmGestion'];

	if (!oForm.decede.checked)
	{
		document.getElementById("msg_deces").style.visibility = 'hidden';
		oForm.motdepasse.disabled = false;
		oForm.motdepasse.value = "password";
		oForm.perso_adresse.disabled = false;
		oForm.perso_tel_fixe.disabled = false;
		oForm.perso_tel_mobile.disabled = false;
		oForm.perso_email.disabled = false;
		oForm.pro_adresse.disabled = false;
		oForm.pro_tel_fixe.disabled = false;
		oForm.pro_tel_mobile.disabled = false;
		oForm.pro_email.disabled = false;
		oForm.pro_url.disabled = false;
		oForm.deces_date.disabled = true;
		oForm.utilisateur_sc_numint.disabled = false;
	}else{
		document.getElementById("msg_deces").style.visibility = 'visible';
		oForm.motdepasse.disabled = true;
		oForm.motdepasse.value = "password";
		oForm.perso_adresse.disabled = true;
		oForm.perso_tel_fixe.disabled = true;
		oForm.perso_tel_mobile.disabled = true;
		oForm.perso_email.disabled = true;
		oForm.pro_adresse.disabled = true;
		oForm.pro_tel_fixe.disabled = true;
		oForm.pro_tel_mobile.disabled = true;
		oForm.pro_email.disabled = true;
		oForm.pro_url.disabled = true;
		oForm.deces_date.disabled = false;
		oForm.utilisateur_sc_numint.disabled = true;
		oForm.btn_valider.disabled = false;
	}
}




